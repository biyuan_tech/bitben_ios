//
//  FaceWaringTextTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceWaringTextTableViewCell.h"

@interface FaceWaringTextTableViewCell()
@property (nonatomic,strong)UILabel *inputLabel;
@end

@implementation FaceWaringTextTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.inputLabel = [GWViewTool createLabelFont:@"14" textColor:@"656565"];
    self.inputLabel.numberOfLines = 0;
    [self addSubview:self.inputLabel];
}

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info{
    CGFloat cellHeight = 0;
    CGSize infoSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    cellHeight += infoSize.height;
    return cellHeight;
}

@end
