//
//  FaceWaringTextTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FaceWaringTextTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferInfo;

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info;

@end

NS_ASSUME_NONNULL_END
