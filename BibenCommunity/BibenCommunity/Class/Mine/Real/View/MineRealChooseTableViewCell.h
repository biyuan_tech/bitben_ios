//
//  MineRealChooseTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

typedef NS_ENUM(NSInteger,MineRealChooseTableViewCellType) {
    MineRealChooseTableViewCellTypePerson,               /**< 个人*/
    MineRealChooseTableViewCellTypeTeacher,              /**< 讲师*/
    MineRealChooseTableViewCellTypeLtd,                  /**< 机构*/
};

NS_ASSUME_NONNULL_BEGIN

@interface MineRealChooseTableViewCellView : UIView

@property (nonatomic,strong)PDImageView *chooseImgView;

-(void)createSingleViewWithType:(MineRealChooseTableViewCellType)type;
-(void)actionClickWithChooseTypeBlock:(void(^)())block;

-(void)actionSelectBtnStatus:(BOOL)status;

+(CGFloat)calculationHeight;

@end



@interface MineRealChooseTableViewCell : PDBaseTableViewCell


-(void)actionClickWithChooseTypeBackBlock:(void(^)(MineRealChooseTableViewCellType))block;

-(void)walletChoosedType:(MineRealChooseTableViewCellType)type hasSelected:(BOOL)hasSelected;

@end



NS_ASSUME_NONNULL_END
