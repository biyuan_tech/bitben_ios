//
//  MineRealHeaderTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MineRealHeaderTableViewCell.h"

@interface  MineRealHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation MineRealHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    self.imgView.image = [UIImage imageNamed:@"icon_real_idcard"];
    [self addSubview:self.imgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"8F8F8F"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.text = @"为了您在币本获得更多权益，需进行身份验证，\r我们不会再任何情况下，向第三方泄露您的信息。";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    //
    [self frameInfo];
}

#pragma mark - frameInfo
-(void)frameInfo{
    self.imgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(100)) / 2., (LCFloat(91) - 64) , LCFloat(100), LCFloat(73));
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width,2 * [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + LCFloat(27), kScreenBounds.size.width, titleSize.height);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += (LCFloat(91) - 64);
    cellHeight += LCFloat(73);
    cellHeight += LCFloat(27);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]] * 2;
    cellHeight += LCFloat(30);
    return cellHeight;
    
}

@end
