//
//  CenterRewardModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/12.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface CenterRewardModel : FetchModel

@property (nonatomic,assign)NSInteger reward;

@end
