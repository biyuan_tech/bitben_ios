//
//  PowerDetailTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PowerDetailTableViewCell.h"

@interface PowerDetailTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UIView *dotView;
@property (nonatomic,strong)UILabel *detailLabel;
@end

@implementation PowerDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // bgImgView
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = self.bgImgView;
    
    self.dotView = [[UIView alloc]init];
    self.dotView.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.dotView.frame = CGRectMake(LCFloat(35), 5, LCFloat(5), LCFloat(5));
    [self addSubview:self.dotView];
    
    self.detailLabel = [GWViewTool createLabelFont:@"14" textColor:@"393939"];
    self.detailLabel.numberOfLines = 0;
    [self addSubview:self.detailLabel];
}

-(void)setIndexRow:(NSInteger)indexRow{
    _indexRow = indexRow;
}

-(void)setTransferBgImg:(NSString *)transferBgImg{
    _transferBgImg = transferBgImg;
    self.bgImgView.image = [Tool stretchImageWithName:transferBgImg];
}

-(void)setTransferDetailInfo:(NSString *)transferDetailInfo{
    _transferDetailInfo = transferDetailInfo;
    self.detailLabel.text = transferDetailInfo;
    
    CGFloat width = kScreenBounds.size.width - LCFloat(54) - LCFloat(11);
    CGSize detailSize = [self.detailLabel.text sizeWithCalcFont:self.detailLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    
    if (self.indexRow == 0){
        self.detailLabel.frame = CGRectMake(LCFloat(54), LCFloat(17) + LCFloat(16), width, detailSize.height);
        self.dotView.center_y = self.detailLabel.center_y;
    } else {
        self.detailLabel.frame = CGRectMake(LCFloat(54), LCFloat(17), width, detailSize.height);
        
        self.dotView.center_y = self.detailLabel.center_y;
    }
}

+(CGFloat)calculationCellHeightWithTitle:(NSString *)title type:(PowerDetailTableViewCellType)type{
    CGFloat width = kScreenBounds.size.width - LCFloat(54) - LCFloat(11);
    CGSize contentOfSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    CGFloat cellHeight = 0;
    cellHeight = contentOfSize.height + 2 * LCFloat(17);
    if (type == PowerDetailTableViewCellTypeTop || type == PowerDetailTableViewCellTypeBottom){
        cellHeight += LCFloat(16);
    }
    return cellHeight;
}

@end
