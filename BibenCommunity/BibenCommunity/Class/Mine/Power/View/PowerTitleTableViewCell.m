//
//  PowerTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/24.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PowerTitleTableViewCell.h"

@interface PowerTitleTableViewCell()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation PowerTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [self addSubview:self.lineView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"393939"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    self.lineView.frame = CGRectMake(LCFloat(15), ([PowerTitleTableViewCell calculationCellHeight] - LCFloat(15)) / 2., LCFloat(2), LCFloat(15));
    
    self.titleLabel.text = transferTitle;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(13), 0, 100, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.center_y = self.lineView.center_y;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(55);
}



@end
