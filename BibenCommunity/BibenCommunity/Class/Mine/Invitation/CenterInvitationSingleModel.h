//
//  CenterInvitationSingleModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/4.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface CenterInvitationSingleModel : FetchModel

@property (nonatomic,assign)NSInteger count;
@property (nonatomic,assign)NSInteger reward;

@end

@protocol CenterInvitationHistorySingleModel <NSObject>

@end

@interface CenterInvitationHistorySingleModel : FetchModel

@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)CGFloat already_reward;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *invite_account_id;
@property (nonatomic,assign)NSInteger invite_account_reward;
@property (nonatomic,assign)CGFloat once_reward;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,assign)NSTimeInterval register_date;
@property (nonatomic,assign)NSTimeInterval register_time;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,assign)NSInteger total_reward;
@property (nonatomic,assign)CGFloat unclaimed_reward;
@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *head_img;
@property (nonatomic,assign)CGFloat friend_reward;
@property (nonatomic,assign)CGFloat member_reward;
@property (nonatomic,assign)NSInteger rank;
@property (nonatomic,assign)NSInteger reward;
@property (nonatomic,assign)NSInteger level;


@end

@interface CenterInvitationHistoryListModel : FetchModel

@property (nonatomic,strong)NSArray <CenterInvitationHistorySingleModel> *content;

@end

