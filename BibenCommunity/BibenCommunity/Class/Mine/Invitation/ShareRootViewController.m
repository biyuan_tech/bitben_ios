//
//  ShareRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShareRootViewController.h"
#import "UIImage+ImageEffects.h"
#import "NetworkAdapter+Task.h"

#define SheetViewHeight       LCFloat(235 )
@interface ShareRootViewController ()
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@property (nonatomic,strong)UILabel *titleLabel;
@end

static char actionClickWithShareBlockKey;
static char actionClickWithJubaoBlockKey;
@implementation ShareRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];                  // 创建dismissButton
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.view addSubview:self.actionBgimgView];
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
}



-(void)arrayWithInit{
    self.shareButtonArray = @[@[@"微信好友",@"icon_center_share_wechat",@"wechat"],@[@"微信朋友圈",@"icon_center_share_wechat_friend",@"friendsCircle"]];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    if (self.hasJubao){
        viewHeight += LCFloat(51);
    }
    
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.backgroundColor = UURandomColor;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];

    // 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(54));
    self.titleLabel.text = @"分享给好友";
    [_shareView addSubview:self.titleLabel];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, .5f);
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    _shareView.size_height = self.titleLabel.size_height + LCFloat(130) + LCFloat(51);
    if (self.hasJubao){
        _shareView.size_height += LCFloat(51);
    }
    
    // 创建图标
    [self createShareButton];
}

#pragma mark 创建分享图标
-(void)createShareButton{
    CGFloat margin = (kScreenBounds.size.width - 2 * LCFloat(70)) / 3.;
    CGFloat width = LCFloat(70);
    CGFloat height = LCFloat(47) + LCFloat(15) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    for (int i = 0 ; i < self.shareButtonArray.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        NSString *title = [[self.shareButtonArray objectAtIndex:i] objectAtIndex:0];
        NSString *img = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:1];
        NSString *type = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:2];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"7F7F7F"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
        [button setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        button.frame = CGRectMake(margin + (width + margin) * i, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(26), width, height);
        
        [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(15)];
        __weak typeof(self)weakSelf = self;
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *type) = objc_getAssociatedObject(strongSelf, &actionClickWithShareBlockKey);
            if (block){
                block(type);
            }
            [strongSelf dismissFromView:_showViewController];
        }];
        [_shareView addSubview:button];
    }
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    UIView *lineView = [[UIView alloc]init];
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissBtn.frame = CGRectMake(0, _shareView.frame.size.height - LCFloat(51), kScreenBounds.size.width, LCFloat(51));
    [dismissBtn setTitle:@"取消" forState:UIControlStateNormal];
    dismissBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    dismissBtn.backgroundColor = [UIColor whiteColor];
    [dismissBtn setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [dismissBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:dismissBtn];
    
    // 举报按钮
    
    UIButton *jubaoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    jubaoBtn.frame = CGRectMake(0, dismissBtn.orgin_y - LCFloat(51), kScreenBounds.size.width, LCFloat(51));
    [jubaoBtn setTitle:@"举报" forState:UIControlStateNormal];
    jubaoBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    jubaoBtn.backgroundColor = [UIColor whiteColor];
    [jubaoBtn setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
    [jubaoBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithJubaoBlockKey);
        if (block){
            block();
        }
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:jubaoBtn];
    if (self.hasJubao){
        jubaoBtn.hidden = NO;
    } else {
        jubaoBtn.hidden = YES;
    }
    
    
    lineView.frame = CGRectMake(0, dismissBtn.orgin_y - .5f, kScreenBounds.size.width, .5f);
    
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block{
    objc_setAssociatedObject(self, &actionClickWithShareBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithJubaoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithJubaoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}





@end
