//
//  CenterShareImgModel.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/22.
//  Copyright © 2018 币本. All rights reserved.
//

#import "CenterShareImgModel.h"

@implementation CenterShareImgModel

#pragma mark - createImgManager
-(UIImage *)createImgManagerWithShareType:(ShareImgType)type model:(CenterShareSingleModel *)model{
    UIImage *baseImg;
    if (type == ShareImgTypeOne){
        baseImg = [UIImage imageNamed:@"bg_invitation_share"];
    }
    
    UIImage *image = baseImg;
    /*
     size：位图上下文尺寸（新图片的尺寸）
     opaque：不透明YES，透明NO，通常一般是透明上下文
     scale：缩放，通常不需要缩放，取值为0表示不缩放，
     */
    UIGraphicsBeginImageContextWithOptions(image.size, YES, 0);
    
//    //2、绘制原生图片
    [image drawAtPoint:CGPointZero];
    
    //给原生图片添加图片
    UIImage *imageMack = [self createErweimaWithInfo:model.url];
    [imageMack drawInRect:CGRectMake((image.size.width - imageMack.size.width) / 2., image.size.height - imageMack.size.height - 108, imageMack.size.width, imageMack.size.height)];
    

    //4、从上下文中获取生成新图片
    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //5、关闭上下文
    UIGraphicsEndImageContext();
    
    
    
    //    //3、给原生图片添加文字
//    NSString *str = [AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code;
//    NSDictionary *dic = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
//                           NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"36."],};
//    CGFloat fontSizeHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"36."]];
//    CGSize strSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"36."] constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontSizeHeight)];
//
//    [str drawAtPoint:CGPointMake((image.size.width - strSize.width) / 2., image.size.height - 2 * LCFloat(11) - fontSizeHeight) withAttributes:dic];
//
    UIImage *backImg = [self createShareImgWithStr:[AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code img:getImage];
    UIImage *mainImg = [self compressImage:backImg toByte:1024 * 25];

    
    return mainImg;
}


#pragma mark - 根据信息变成二维码
-(UIImage *)createErweimaWithInfo:(NSString *)infoUrl{
    // 1.创建过滤器
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];

    // 2.恢复默认
    [filter setDefaults];
    
    // 3.给过滤器添加数据(正则表达式/账号和密码)
    NSString *dataString = infoUrl;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    // 4.获取输出的二维码
    CIImage *outputImage = [filter outputImage];
    
    //因为生成的二维码模糊，所以通过createNonInterpolatedUIImageFormCIImage:outputImage来获得高清的二维码图片
    
    // 5.显示二维码
    UIImage *img = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:121];

    return img;
}


-(UIImage *)createShareImgWithStr:(NSString *)str img:(UIImage *)img{
    //获取要加工的图片
    UIImage *image = img;
    CGSize size= CGSizeMake (image.size . width , image.size . height ); // 画布大小
    UIGraphicsBeginImageContextWithOptions (size, NO , 0.0 );
    [image drawAtPoint : CGPointMake ( 0 , 0 )];
    // 获得一个位图图形上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawPath(context, kCGPathStroke);
    //画自己想画的内容。。。。。
    CGSize infoSize = [str sizeWithCalcFont:[UIFont systemFontOfCustomeSize:36] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:36] ])];
    
    [str drawAtPoint : CGPointMake ( (kScreenBounds.size.width - infoSize.width) / 2. , img.size.height - LCFloat(332) - infoSize.height + LCFloat(10)) withAttributes : @{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 36], NSForegroundColorAttributeName :[ UIColor hexChangeFloat:@"562F24"]}];
    
    // 返回绘制的新图形
    UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
    
    UIGraphicsEndImageContext ();
    
    return newImage;
}


/**
 *  根据CIImage生成指定大小的UIImage
 *
 *  @param image CIImage
 *  @param size  图片宽度
 */
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}




#pragma mark - createImgManager
+(UIImage *)createImgManagerWithPlacehorderWithRect:(CGSize)size{
    UIImage *baseImg;
    
    baseImg = [Tool createImageWithColor:RGB(234, 234, 234, 1) frame:CGRectMake(0, 0, size.width, size.height)];
    UIImage *image = baseImg;

    /*
     size：位图上下文尺寸（新图片的尺寸）
     opaque：不透明YES，透明NO，通常一般是透明上下文
     scale：缩放，通常不需要缩放，取值为0表示不缩放，
     */
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    //2、绘制原生图片
    [image drawAtPoint:CGPointZero];
    
    //给原生图片添加图片
    UIImage *imageMack = [UIImage imageNamed:@"placehorder"];
    [imageMack drawInRect:CGRectMake((size.width - imageMack.size.width) / 2., (size.height - imageMack.size.height) / 2., imageMack.size.width, imageMack.size.height)];
    
    //    //3、给原生图片添加文字
    //    NSString *str = @"币本";
    //    NSDictionary *dic = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
    //                           NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"20."],};
    //    CGFloat fontSizeHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"20."]];
    //    CGSize strSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"20."] constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontSizeHeight)];
    
    //    [str drawAtPoint:CGPointMake((image.size.width - strSize.width) / 2., image.size.height - 2 * LCFloat(11) - fontSizeHeight) withAttributes:dic];
    
    //4、从上下文中获取生成新图片
    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //5、关闭上下文
    UIGraphicsEndImageContext();
    
    return getImage;
}



#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}
@end
