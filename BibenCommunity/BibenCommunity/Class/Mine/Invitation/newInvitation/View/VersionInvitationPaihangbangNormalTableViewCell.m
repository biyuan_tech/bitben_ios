//
//  VersionInvitationPaihangbangNormalTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationPaihangbangNormalTableViewCell.h"

@interface VersionInvitationPaihangbangNormalTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *indexImgView;
@property (nonatomic,strong)PDImageView *avatarConvertImgView;
@property (nonatomic,strong)PDImageView *levelImgView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UILabel *jiangliLabel;
@end

@implementation VersionInvitationPaihangbangNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_normal"];
    self.backgroundView = self.bgImgView;
    
    self.indexImgView = [[PDImageView alloc]init];
    self.indexImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.indexImgView];
    
    self.avatarConvertImgView = [[PDImageView alloc]init];
    self.avatarConvertImgView.backgroundColor = [UIColor clearColor];
    self.avatarConvertImgView.image = [UIImage imageNamed:@"bg_avatar_convert"];
    [self addSubview:self.avatarConvertImgView];
    
    self.levelImgView = [[PDImageView alloc]init];
    self.levelImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.levelImgView];
    
    self.numberLabel = [GWViewTool createLabelFont:@"11" textColor:@"FFFFFF"];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    [self.indexImgView addSubview:self.numberLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    [self addSubview:self.titleLabel];
    
    self.countLabel = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    [self addSubview:self.countLabel];
    
    self.jiangliLabel = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    [self addSubview:self.jiangliLabel];
}

-(void)setTransferModel:(VersionInvitationRootModel *)transferModel{
    _transferModel = transferModel;
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferType:(VersionInvitationPaihangbangNormalTableViewCellType)transferType{
    _transferType = transferType;
    
    CGFloat margin = (kScreenBounds.size.width - 2 * LCFloat(28)) / 3;
    if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeader){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_top"];
        
        self.indexImgView.hidden = YES;
        self.numberLabel.hidden = YES;
        
        self.titleLabel.hidden = NO;
        self.countLabel.hidden = NO;
        self.jiangliLabel.hidden = NO;
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.countLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.jiangliLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        self.countLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        self.jiangliLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        
        self.titleLabel.text = @"邀请人";
        self.countLabel.text = @"邀请好友数";
        self.jiangliLabel.text = @"累计奖励";
        CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(0, LCFloat(46), titleSize.width, titleSize.height);
        
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        CGSize jiagliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        
        self.jiangliLabel.frame = CGRectMake(0, self.titleLabel.orgin_y,jiagliSize.width, jiagliSize.height);
        
        self.titleLabel.center_x = LCFloat(28) + margin / 2.;
        self.countLabel.center_x = LCFloat(28) + margin + margin / 2.;
        self.jiangliLabel.center_x = LCFloat(28) + 2 * margin + margin / 2.;
        
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeTitle){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_title"];
        
        self.indexImgView.hidden = YES;
        self.numberLabel.hidden = YES;
        
        self.titleLabel.hidden = NO;
        self.countLabel.hidden = NO;
        self.jiangliLabel.hidden = NO;
   
        self.titleLabel.text = @"我";
        if ([_transferModel.count isEqualToString:@"0"]){
            self.countLabel.text = @"暂无邀请";
        } else {
            self.countLabel.text = [NSString stringWithFormat:@"%@人",_transferModel.count];
        }
        
        self.jiangliLabel.text = [self.transferModel.reward isEqualToString:@"0"] ? @"暂无奖励":[NSString stringWithFormat:@"%@BBT",_transferModel.reward];
        CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(0, (LCFloat(36) - titleSize.height) / 2., titleSize.width, titleSize.height);
        
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        CGSize jiagliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        
        self.jiangliLabel.frame = CGRectMake(0, self.titleLabel.orgin_y,jiagliSize.width, jiagliSize.height);
        
        self.titleLabel.center_x = LCFloat(28) + margin / 2.;
        self.countLabel.center_x = LCFloat(28) + margin + margin / 2.;
        self.jiangliLabel.center_x = LCFloat(28) + 2 * margin + margin / 2.;
        
        
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeNormal){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_normal"];
        
        self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)self.transferIndex - 2 + 1];
        self.titleLabel.text = self.transferInvitationModel.phone;
        self.countLabel.text = [NSString stringWithFormat:@"%li人",(long)self.transferInvitationModel.count];
        self.jiangliLabel.text = [NSString stringWithFormat:@"%li",(long)self.transferInvitationModel.total_reward];
        
        // 1.
        self.indexImgView.frame = CGRectMake(0, 0, LCFloat(16), LCFloat(21));
        if (self.transferIndex == 2){           // 第一
            self.indexImgView.image = [UIImage imageNamed:@"icon_invitation_top1"];
        } else if (self.transferIndex == 3){
            self.indexImgView.image = [UIImage imageNamed:@"icon_invitation_top2"];
        } else if (self.transferIndex == 4){
            self.indexImgView.image = [UIImage imageNamed:@"icon_invitation_top3"];
        } else {
            self.indexImgView.image = [UIImage imageNamed:@"icon_invitation_top_other"];
            self.indexImgView.frame = CGRectMake(0, 0, LCFloat(15), LCFloat(15));
            self.numberLabel.frame = self.indexImgView.bounds;
        }
        
        self.indexImgView.center_x = LCFloat(59);
        self.indexImgView.center_y = [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeNormal] / 2.;
        
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, ([VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeNormal] - countSize.height) / 2., countSize.width, countSize.height);
        self.countLabel.center_x = LCFloat(28) + margin + margin / 2.;

        self.jiangliLabel.text = [NSString stringWithFormat:@"%liBBT",(long)self.transferInvitationModel.reward];
        CGSize jiangliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        self.jiangliLabel.frame = CGRectMake(0, self.countLabel.orgin_y, jiangliSize.width, jiangliSize.height);
        self.jiangliLabel.center_x = LCFloat(28) + 2 * margin + margin / 2.;

        self.titleLabel.text = self.transferInvitationModel.phone;
        CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(12), 0, titleSize.width, titleSize.height);
        self.titleLabel.center_y = self.indexImgView.center_y;
        
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeBottom){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_bottom"];
        self.titleLabel.text = @"- 查看更多 -";
        self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(36));
        
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderLeiji || transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderYaoqing || transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderJiasu){         // 累计
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_paihangbang_top"];
        
        self.indexImgView.hidden = YES;
        self.numberLabel.hidden = YES;
        
        self.titleLabel.hidden = NO;
        self.countLabel.hidden = NO;
        self.jiangliLabel.hidden = NO;
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.countLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.jiangliLabel.textColor = [UIColor hexChangeFloat:@"EA6441"];
        self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        self.countLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        self.jiangliLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        
        if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderLeiji){
            self.titleLabel.text = @"被邀请人";
            self.jiangliLabel.text = @"奖励";
            self.countLabel.text = @"邀请日期";
            
        } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderYaoqing){
            self.titleLabel.text = @"被邀请人";
            self.countLabel.text = @"邀请好友日期";
        } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderJiasu){
            self.titleLabel.text = @"被邀请人";
            self.jiangliLabel.text = @"挖矿累计加速";
            self.countLabel.text = @"邀请日期";
        }
        
        CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(0, LCFloat(46), titleSize.width, titleSize.height);
        
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        CGSize jiagliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        self.jiangliLabel.frame = CGRectMake(0, self.titleLabel.orgin_y,jiagliSize.width, jiagliSize.height);
        
        self.titleLabel.center_x = LCFloat(70) + titleSize.width / 2.;
        self.countLabel.center_x = LCFloat(184) + countSize.width / 2.;
        self.jiangliLabel.center_x = LCFloat(293) + jiagliSize.width / 2.;
        
        if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderLeiji){
            self.titleLabel.center_x = LCFloat(70) + titleSize.width / 2.;
            self.countLabel.center_x = LCFloat(184) + countSize.width / 2.;
            self.jiangliLabel.center_x = LCFloat(293) + jiagliSize.width / 2.;
        } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderYaoqing){
            self.titleLabel.center_x = LCFloat(92) + titleSize.width / 2.;
            self.countLabel.center_x = LCFloat(224) + countSize.width / 2.;
        } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeaderJiasu){
            self.titleLabel.center_x = LCFloat(66) + titleSize.width / 2.;
            self.countLabel.center_x = LCFloat(174) + countSize.width / 2.;
            self.jiangliLabel.center_x = LCFloat(251) + jiagliSize.width / 2.;
        }
        
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeNormalLeiji){         // 普通累计
        self.indexImgView.frame = CGRectMake(LCFloat(47), (LCFloat(36) - LCFloat(25)) / 2., LCFloat(25), LCFloat(25));
        [self.indexImgView uploadMainImageWithURL:self.transferInvitationModel.head_img placeholder:nil imgType:PDImgTypeHD callback:NULL];
        
        self.avatarConvertImgView.frame = self.indexImgView.frame;
        
        self.levelImgView.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(2) - LCFloat(13),CGRectGetMaxY(self.indexImgView.frame) + LCFloat(3) - LCFloat(14) , LCFloat(13), LCFloat(14));
        if (self.transferInvitationModel.level == 1){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level1"];
        } else if (self.transferInvitationModel.level == 2){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level2"];
        } else {
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level3"];
        }
        
        self.titleLabel.text = self.transferInvitationModel.phone;
        CGSize phoneSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(8), 0, phoneSize.width, phoneSize.height);
        self.titleLabel.center_y = self.indexImgView.center_y;
        
        self.countLabel.text = [NSDate getTimeWithyaoqingString:self.transferInvitationModel.register_date / 1000.];
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        
        self.jiangliLabel.text = [NSString stringWithFormat:@"%liBBT",(long)self.transferInvitationModel.reward];
        CGSize jiangliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        self.jiangliLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, jiangliSize.width, jiangliSize.height);
        
        CGSize infoSize = [@"恭喜发财" sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
        CGSize infoSize2 = [@"发财" sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
        self.countLabel.center_x = LCFloat(184) + infoSize.width / 2.;
        self.jiangliLabel.center_x = LCFloat(293) + infoSize2.width / 2.;
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeNormalYaoqing){
        self.indexImgView.frame = CGRectMake(LCFloat(69), (LCFloat(36) - LCFloat(25)) / 2., LCFloat(25), LCFloat(25));
        [self.indexImgView uploadMainImageWithURL:self.transferInvitationModel.head_img placeholder:nil imgType:PDImgTypeHD callback:NULL];
        
        self.avatarConvertImgView.frame = self.indexImgView.frame;
        
        self.levelImgView.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(2) - LCFloat(13),CGRectGetMaxY(self.indexImgView.frame) + LCFloat(3) - LCFloat(14) , LCFloat(13), LCFloat(14));
        if (self.transferInvitationModel.level == 1){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level1"];
        } else if (self.transferInvitationModel.level == 2){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level2"];
        } else {
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level3"];
        }
        
        self.titleLabel.text = self.transferInvitationModel.phone;
        CGSize phoneSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(8), 0, phoneSize.width, phoneSize.height);
        self.titleLabel.center_y = self.indexImgView.center_y;
        
        self.countLabel.text = [NSDate getTimeWithyaoqingString:self.transferInvitationModel.register_date / 1000.];
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        CGSize infoSize = [@"邀请好友日期" sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
        self.countLabel.center_x = LCFloat(224) + infoSize.width / 2.;
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeNormalJiasu){
        self.indexImgView.frame = CGRectMake(LCFloat(47), (LCFloat(36) - LCFloat(25)) / 2., LCFloat(25), LCFloat(25));
        [self.indexImgView uploadMainImageWithURL:self.transferInvitationModel.head_img placeholder:nil imgType:PDImgTypeHD callback:NULL];
        
        self.avatarConvertImgView.frame = self.indexImgView.frame;
        
        self.levelImgView.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(2) - LCFloat(13),CGRectGetMaxY(self.indexImgView.frame) + LCFloat(3) - LCFloat(14) , LCFloat(13), LCFloat(14));
        if (self.transferInvitationModel.level == 1){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level1"];
        } else if (self.transferInvitationModel.level == 2){
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level2"];
        } else {
            self.levelImgView.image = [UIImage imageNamed:@"icon_invitation_level3"];
        }
        
        self.titleLabel.text = self.transferInvitationModel.phone;
        CGSize phoneSize = [Tool makeSizeWithLabel:self.titleLabel];
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.indexImgView.frame) + LCFloat(8), 0, phoneSize.width, phoneSize.height);
        self.titleLabel.center_y = self.indexImgView.center_y;
        
        self.countLabel.text = [NSDate getTimeWithyaoqingString:self.transferInvitationModel.create_time / 1000.];
        CGSize countSize = [Tool makeSizeWithLabel:self.countLabel];
        self.countLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, countSize.width, countSize.height);
        
        
        self.jiangliLabel.text = [NSString stringWithFormat:@"%liBP",(long)self.transferInvitationModel.member_reward];
        CGSize jiangliSize = [Tool makeSizeWithLabel:self.jiangliLabel];
        self.jiangliLabel.frame = CGRectMake(0, self.titleLabel.orgin_y, jiangliSize.width, jiangliSize.height);
        
        CGSize infoSize = [@"恭喜发财" sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
        CGSize infoSize2 = [@"挖矿累计加速" sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
        self.countLabel.center_x = LCFloat(174) + infoSize.width / 2.;
        self.jiangliLabel.center_x = LCFloat(251) + infoSize2.width / 2.;
    }
    
    
}

+(CGFloat)calculationCellHeightWithType:(VersionInvitationPaihangbangNormalTableViewCellType)transferType{
    if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeHeader){
        return LCFloat(74);
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeTitle){
        return LCFloat(36);
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeNormal){
        return LCFloat(36);
    } else if (transferType == VersionInvitationPaihangbangNormalTableViewCellTypeBottom){
        return LCFloat(36);
    }
    return LCFloat(36);
}

@end
