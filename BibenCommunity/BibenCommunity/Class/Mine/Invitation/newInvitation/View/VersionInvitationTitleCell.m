//
//  VersionInvitationTitleCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationTitleCell.h"

@interface VersionInvitationTitleCell()
@property (nonatomic,strong)PDImageView *titleImgView;
@end

@implementation VersionInvitationTitleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundColor = RGB(231, 93, 50, 1);
    self.titleImgView = [[PDImageView alloc]init];
    self.titleImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleImgView];
}

-(void)setTransferType:(VersionInvitationTitleCellType)transferType{
    _transferType = transferType;
    self.titleImgView.frame = CGRectMake(0, ([VersionInvitationTitleCell calculationCellHeight] - LCFloat(15)) / 2., kScreenBounds.size.width, LCFloat(15));
    if (transferType == VersionInvitationTitleCellType1){
        self.titleImgView.image = [UIImage imageNamed:@"icon_Invitation_jiangjin"];
    } else if (transferType == VersionInvitationTitleCellType2){
        self.titleImgView.image = [UIImage imageNamed:@"icon_Invitation_jiasu"];
    } else if (transferType == VersionInvitationTitleCellType3){
        self.titleImgView.image = [UIImage imageNamed:@"icon_Invitation_paihangbang"];
    } else if (transferType == VersionInvitationTitleCellType4){
        self.titleImgView.image = [UIImage imageNamed:@"icon_yaoqinghaoyouleiji"];
        self.titleImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(196)) / 2., 0, LCFloat(196), LCFloat(17));
    } else if (transferType == VersionInvitationTitleCellType5){
        self.titleImgView.image = [UIImage imageNamed:@"icon_yiyaoqinghaoyou"];
        self.titleImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(178)) / 2., 0, LCFloat(178), LCFloat(17));
    } else if (transferType == VersionInvitationTitleCellType6){
        self.titleImgView.image = [UIImage imageNamed:@"icon_wakuangjiasu"];
        self.titleImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(124)) / 2., 0, LCFloat(124), LCFloat(17));
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(70);
}

@end
