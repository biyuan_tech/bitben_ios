//
//  VersionInvitationJiasuCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationJiasuCell.h"

@interface VersionInvitationJiasuCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *iconImgView1;
@property (nonatomic,strong)PDImageView *iconImgView2;
@property (nonatomic,strong)PDImageView *iconImgView3;
@property (nonatomic,strong)PDImageView *iconImgView4;
@property (nonatomic,strong)UILabel *label1;
@property (nonatomic,strong)UILabel *label2;
@property (nonatomic,strong)UILabel *label3;
@property (nonatomic,strong)UILabel *label4;
@end

@implementation VersionInvitationJiasuCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_inviation_buzhou"];
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [VersionInvitationJiasuCell calculationCellHeight]);
    [self addSubview:self.bgImgView];
    self.backgroundView = self.bgImgView;
    
    self.iconImgView1 = [[PDImageView alloc]init];
    self.iconImgView1.image = [UIImage imageNamed:@"icon_invitation_jiasu_share"];
    self.iconImgView1.frame = CGRectMake(LCFloat(64), LCFloat(27), LCFloat(35), LCFloat(37));
    [self addSubview:self.iconImgView1];
    
    self.iconImgView2 = [[PDImageView alloc]init];
    self.iconImgView2.image = [UIImage imageNamed:@"icon_invitation_jiasu_firend"];
    self.iconImgView2.frame = CGRectMake(LCFloat(64), CGRectGetMaxY(self.iconImgView1.frame) + LCFloat(13), LCFloat(35), LCFloat(37));
    [self addSubview:self.iconImgView2];
    
    self.iconImgView3 = [[PDImageView alloc]init];
    self.iconImgView3.image = [UIImage imageNamed:@"icon_invitation_jiasu_shop"];
    self.iconImgView3.frame = CGRectMake(LCFloat(64), CGRectGetMaxY(self.iconImgView2.frame) + LCFloat(13), LCFloat(35), LCFloat(37));
    [self addSubview:self.iconImgView3];
    
    self.iconImgView4 = [[PDImageView alloc]init];
    self.iconImgView4.image = [UIImage imageNamed:@"icon_invitation_jiasu_jiasu"];
    self.iconImgView4.frame = CGRectMake(LCFloat(64), CGRectGetMaxY(self.iconImgView3.frame) + LCFloat(13), LCFloat(35), LCFloat(37));
    [self addSubview:self.iconImgView4];
    
    CGFloat width = kScreenBounds.size.width - LCFloat(20) - CGRectGetMaxX(self.iconImgView1.frame) - LCFloat(20) - LCFloat(11);
    self.label1 = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    self.label1.text = @"分享链接给好友或朋友圈";
    self.label1.frame = CGRectMake(CGRectGetMaxX(self.iconImgView1.frame) + LCFloat(20), 0, width, [NSString contentofHeightWithFont:self.label1.font]);
    self.label1.center_y = self.iconImgView1.center_y;
    [self addSubview:self.label1];
    
    self.label2 = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    self.label2.text = @"好友通过分享链接注册";
    self.label2.frame = CGRectMake(CGRectGetMaxX(self.iconImgView1.frame) + LCFloat(20), 0, width, [NSString contentofHeightWithFont:self.label1.font]);
    self.label2.center_y = self.iconImgView2.center_y;
    [self addSubview:self.label2];
    
    self.label3 = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    self.label3.text = @"好友初次购买会员填写你的邀请码";
    self.label3.frame = CGRectMake(CGRectGetMaxX(self.iconImgView1.frame) + LCFloat(20), 0, width, [NSString contentofHeightWithFont:self.label1.font]);
    self.label3.center_y = self.iconImgView3.center_y;
    [self addSubview:self.label3];
    
    self.label4 = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    self.label4.text = @"获得挖矿加速";
    self.label4.frame = CGRectMake(CGRectGetMaxX(self.iconImgView1.frame) + LCFloat(20), 0, width, [NSString contentofHeightWithFont:self.label1.font]);
    self.label4.center_y = self.iconImgView4.center_y;
    [self addSubview:self.label4];
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(27);
    cellHeight += 4 * LCFloat(37) + 3 * LCFloat(13);
    cellHeight += LCFloat(29);
    return cellHeight;
}

@end
