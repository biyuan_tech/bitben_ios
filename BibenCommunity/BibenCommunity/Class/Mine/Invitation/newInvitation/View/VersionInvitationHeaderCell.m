//
//  VersionInvitationHeaderCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationHeaderCell.h"
#import "PDHUD.h"

static char actionClickGuizeBlockKey;
@interface VersionInvitationHeaderCell()
@property (nonatomic,strong)PDImageView *baseImgView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *copButton;
@property (nonatomic,strong)PDImageView *titleImgView;
@property (nonatomic,strong)UIButton *activityButton;
@end

@implementation VersionInvitationHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.baseImgView = [[PDImageView alloc]init];
    self.baseImgView.backgroundColor = [UIColor clearColor];
    self.baseImgView.image = [UIImage imageNamed:@"bg_invitation_header"];
    self.baseImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [VersionInvitationHeaderCell calculationCellHeight]);
    [self addSubview:self.baseImgView];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"FFFFFF"];
    self.fixedLabel.text = @"我的邀请码";
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.frame = CGRectMake(0, LCFloat(134), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    [self addSubview:self.fixedLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"30" textColor:@"FFFFFF"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.text = [AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code;
    self.dymicLabel.font = [self.dymicLabel.font boldFont];
    self.dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
    
    self.copButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.copButton.backgroundColor = [UIColor hexChangeFloat:@"6B9FFF"];
    self.copButton.layer.cornerRadius = LCFloat(4);
    self.copButton.clipsToBounds = YES;
    self.copButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(78)) / 2., CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(21), LCFloat(78), LCFloat(28));
    self.copButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    [self.copButton setTitle:@"一键复制" forState:UIControlStateNormal];
    [self.copButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.copButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [Tool copyWithString:[AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code callback:NULL];
        [PDHUD showText:@"我的邀请码复制成功"];
    }];
    [self addSubview:self.copButton];
    
    self.titleImgView = [[PDImageView alloc]init];
    self.titleImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleImgView];
    
    self.activityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.activityButton.backgroundColor = [UIColor clearColor];
    
    [self.activityButton setImage:[UIImage imageNamed:@"bg_invitation_activityguize"] forState:UIControlStateNormal];
    self.activityButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(73), 17, LCFloat(73), LCFloat(27));
    [self.activityButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickGuizeBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.activityButton];
}

-(void)actionClickGuizeBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickGuizeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferType:(VersionInvitationHeaderCellType)transferType{
    _transferType = transferType;
    
    if (transferType == VersionInvitationHeaderCellTypeNormal){
        self.fixedLabel.hidden = NO;
        self.dymicLabel.hidden = NO;
        self.copButton.hidden = NO;
    } else {
        self.fixedLabel.hidden = YES;
        self.dymicLabel.hidden = YES;
        self.copButton.hidden = YES;
    }
    
    if (transferType == VersionInvitationHeaderCellTypeJiasu){
        
    } else if (transferType == VersionInvitationHeaderCellTypeLeiji){
        
    } else if (transferType == VersionInvitationHeaderCellTypeYaoqing){
        
    }
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(258);
    return cellHeight;
}
@end
