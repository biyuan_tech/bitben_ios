//
//  VersionInvitationItemCell1.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "NetworkAdapter+VersionInvitation.h"

NS_ASSUME_NONNULL_BEGIN

@interface VersionInvitationItemCellView : UIView

@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,strong)UIButton *actionButton;

-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title fixed:(NSString *)fixed info:(NSString *)info action:(void(^)())block;
-(void)reloadFrame;
@end


@interface VersionInvitationItemCell1 : PDBaseTableViewCell
@property (nonatomic,strong)VersionInvitationRootModel *transferInvitationModel;


-(void)actionClickBlock:(void(^)(NSInteger index))block;

@end

NS_ASSUME_NONNULL_END
