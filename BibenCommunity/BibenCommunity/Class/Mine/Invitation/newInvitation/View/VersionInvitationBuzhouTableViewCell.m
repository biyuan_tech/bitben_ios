//
//  VersionInvitationBuzhouTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationBuzhouTableViewCell.h"

@interface VersionInvitationBuzhouTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *iconImgView1;
@property (nonatomic,strong)PDImageView *iconImgView2;
@property (nonatomic,strong)PDImageView *iconImgView3;
@property (nonatomic,strong)PDImageView *arrowImgView1;
@property (nonatomic,strong)PDImageView *arrowImgView2;
@property (nonatomic,strong)UILabel *titleLabel1;
@property (nonatomic,strong)UILabel *titleLabel2;
@property (nonatomic,strong)UILabel *titleLabel3;

@end

@implementation VersionInvitationBuzhouTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_inviation_buzhou"];
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [VersionInvitationBuzhouTableViewCell calculationCellHeight]);
    [self addSubview:self.bgImgView];
    
    CGFloat margin_width = (kScreenBounds.size.width - 2 * LCFloat(20)) / 3;
    
    self.iconImgView1 = [[PDImageView alloc]init];
    self.iconImgView1.image = [UIImage imageNamed:@"icon_inviation_share"];
    self.iconImgView1.frame = CGRectMake(0, LCFloat(28), LCFloat(50), LCFloat(50));
    self.iconImgView1.center_x = LCFloat(15) + margin_width / 2.;
    [self addSubview:self.iconImgView1];
    
    self.iconImgView2 = [[PDImageView alloc]init];
    self.iconImgView2.image = [UIImage imageNamed:@"icon_inviation_firend"];
    self.iconImgView2.frame = CGRectMake(0, self.iconImgView1.orgin_y, self.iconImgView1.size_width, self.iconImgView1.size_height);
    self.iconImgView2.center_x = kScreenBounds.size.width / 2.;
    [self addSubview:self.iconImgView2];
    
    self.iconImgView3 = [[PDImageView alloc]init];
    self.iconImgView3.image = [UIImage imageNamed:@"icon_inviation_gift"];
    self.iconImgView3.frame = CGRectMake(0, self.iconImgView1.orgin_y, self.iconImgView2.size_width, self.iconImgView2.size_height);
    self.iconImgView3.center_x = kScreenBounds.size.width - LCFloat(15) - margin_width / 2.;
    [self addSubview:self.iconImgView3];
    
    self.arrowImgView1 = [[PDImageView alloc]init];
    self.arrowImgView1.image = [UIImage imageNamed:@"icon_inviation_arrow"];
    self.arrowImgView1.frame = CGRectMake(CGRectGetMaxX(self.iconImgView1.frame) + (self.iconImgView2.orgin_x - CGRectGetMaxX(self.iconImgView1.frame) - LCFloat(33)) / 2., 0, LCFloat(33), LCFloat(11));
    self.arrowImgView1.center_y = self.iconImgView1.center_y;
    [self addSubview:self.arrowImgView1];
    
    self.arrowImgView2 = [[PDImageView alloc]init];
    self.arrowImgView2.image = [UIImage imageNamed:@"icon_inviation_arrow"];
    self.arrowImgView2.frame = CGRectMake(CGRectGetMaxX(self.iconImgView2.frame) + (self.iconImgView2.orgin_x - CGRectGetMaxX(self.iconImgView1.frame) - LCFloat(33)) / 2., 0, self.arrowImgView1.size_width, self.arrowImgView1.size_height);
    self.arrowImgView2.center_y = self.arrowImgView1.center_y;
    [self addSubview:self.arrowImgView2];
    
    self.titleLabel1 = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    self.titleLabel1.text = @"分享链接给好友\r微信或朋友圈等";
    self.titleLabel1.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView1.frame) + LCFloat(16), margin_width, 2 * [NSString contentofHeightWithFont:self.titleLabel1.font]);
    self.titleLabel1.center_x = self.iconImgView1.center_x;
    self.titleLabel1.numberOfLines = 2;
    self.titleLabel1.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel1];
    
    self.titleLabel2 = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    self.titleLabel2.text = @"好友通过链接进行注册";
    self.titleLabel2.frame = CGRectMake(0, self.titleLabel1.orgin_y, margin_width, 2 * [NSString contentofHeightWithFont:self.titleLabel1.font]);
    self.titleLabel2.numberOfLines = 2;
    self.titleLabel2.center_x = self.iconImgView2.center_x;
    self.titleLabel2.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel2];
    
    self.titleLabel3 = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    self.titleLabel3.text = @"奖金领取";
    self.titleLabel3.frame = CGRectMake(0, self.titleLabel1.orgin_y, margin_width, [NSString contentofHeightWithFont:self.titleLabel3.font]);
    self.titleLabel3.center_x = self.iconImgView3.center_x;
    self.titleLabel3.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel3];
    
    
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(153);
    return cellHeight;
}

@end
