//
//  VersionInvitationTitleCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,VersionInvitationTitleCellType) {
    VersionInvitationTitleCellType1,
    VersionInvitationTitleCellType2,
    VersionInvitationTitleCellType3,
    VersionInvitationTitleCellType4,
    VersionInvitationTitleCellType5,
    VersionInvitationTitleCellType6,
};

@interface VersionInvitationTitleCell : PDBaseTableViewCell

@property (nonatomic,assign)VersionInvitationTitleCellType transferType;

@end

NS_ASSUME_NONNULL_END
