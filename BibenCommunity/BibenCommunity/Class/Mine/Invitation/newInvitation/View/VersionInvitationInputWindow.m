//
//  VersionInvitationInputWindow.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationInputWindow.h"

static char actionCancelManagerWithBlockKey;
static char actionCheckManagerWithBlockKey;
@interface VersionInvitationInputWindow()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UIButton *cancelButton;
@end

@implementation VersionInvitationInputWindow

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"3C3B3B"];
    self.titleLabel.text = @"邀请码填写";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, LCFloat(25), self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.inputTextField = [GWViewTool inputTextField];
    self.inputTextField.placeholder = @"请输入邀请码";
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"24"];
    self.inputTextField.textAlignment = NSTextAlignmentCenter;
    self.inputTextField.frame = CGRectMake(LCFloat(72), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(36), self.size_width - 2 * LCFloat(72), LCFloat(25));
    [self addSubview:self.inputTextField];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"C5C5C5"];
    self.lineView.frame = CGRectMake(self.inputTextField.orgin_x, CGRectGetMaxY(self.inputTextField.frame) + 1.f, self.inputTextField.size_width, 1.f);
    [self addSubview:self.lineView];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"12" textColor:@"4C4C4C"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.text = @"输入正确邀请码获得更多奖励";
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.lineView.frame) + LCFloat(23), self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    [self addSubview:self.fixedLabel];
    
    __weak typeof(self)weakSelf = self;
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor hexChangeFloat:@"E75E33"];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.actionButton.clipsToBounds = YES;
    self.actionButton.layer.cornerRadius = LCFloat(5);
    [self.actionButton setTitle:@"确定" forState:UIControlStateNormal];
    self.actionButton.frame = CGRectMake((self.size_width - LCFloat(160)) / 2., CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(20), LCFloat(160), LCFloat(40));
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.actionButton];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelButton setTitle:@"算了，不填了" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor hexChangeFloat:@"377AFF"] forState:UIControlStateNormal];
    self.cancelButton.frame = CGRectMake(self.actionButton.orgin_x, CGRectGetMaxY(self.actionButton.frame), self.actionButton.size_width, self.size_height - CGRectGetMaxY(self.actionButton.frame));
    self.cancelButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self addSubview:self.cancelButton];
    
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.inputTextField.text.length != 8){
            [[UIAlertView alertViewWithTitle:@"提示" message:@"请输入正确的邀请码" buttonTitles:@[@"确定"] callBlock:NULL]show];
            return;
        }
        
        
        void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionCheckManagerWithBlockKey);
        if(block){
            block(strongSelf.inputTextField.text);
        }
    }];
    
    [self.cancelButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionCancelManagerWithBlockKey);
        if (block){
            block();
        }
    }];
}


-(void)actionCancelManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionCancelManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionCheckManagerWithBlock:(void(^)(NSString *info))block{
objc_setAssociatedObject(self, &actionCheckManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
