//
//  VersionInvitationLeijiJiangliViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationLeijiJiangliViewController.h"
#import "VersionInvitationHeaderCell.h"
#import "VersionInvitationPaihangbangNormalTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "VersionInvitationTitleCell.h"

@interface VersionInvitationLeijiJiangliViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSInteger page;
}
@property (nonatomic,strong)UITableView *invitationTableView;
@property (nonatomic,strong)NSMutableArray *invitationMutableArr;
@property (nonatomic,strong)NSMutableArray *userMutableArr;
@end

@implementation VersionInvitationLeijiJiangliViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(231, 93, 50, 1);
    
    if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu){
        self.barMainTitle = @"邀请记录";
        [self getListManager];
    } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli){
        self.barMainTitle = @"累计奖励";
        [self getListManager];
    } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeJiasujilu){
        self.barMainTitle = @"挖矿加速";
        [self getMemberListManager];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    page = 0;
    self.invitationMutableArr = [NSMutableArray array];
    [self.invitationMutableArr addObjectsFromArray:@[@[@"头部"],@[@"标题"]]];
    self.userMutableArr = [NSMutableArray array];
    [self.userMutableArr addObject:@"排行榜标题"];
    [self.invitationMutableArr addObject:self.userMutableArr];
    [self.userMutableArr addObject:@"收尾"];

}

#pragma mark - UItableView
-(void)createTableView{
    if (!self.invitationTableView){
        self.invitationTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.invitationTableView.dataSource = self;
        self.invitationTableView.delegate = self;
        [self.view addSubview:self.invitationTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.invitationMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.invitationMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        VersionInvitationHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[VersionInvitationHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu){
            cellWithRowOne.transferType = VersionInvitationHeaderCellTypeYaoqing;
        } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli){
            cellWithRowOne.transferType = VersionInvitationHeaderCellTypeLeiji;
        } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeJiasujilu){
            cellWithRowOne.transferType = VersionInvitationHeaderCellTypeJiasu;
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickGuizeBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strogSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:page1];
            [strogSelf.navigationController pushViewController:webViewController animated:YES];        }];
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        VersionInvitationTitleCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[VersionInvitationTitleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu){
            cellWithRowTwo.transferType = VersionInvitationTitleCellType5;
        } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli){
            cellWithRowTwo.transferType = VersionInvitationTitleCellType4;
        } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeJiasujilu){
            cellWithRowTwo.transferType = VersionInvitationTitleCellType6;
        }
        return cellWithRowTwo;
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"排行榜标题" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang1 = @"cellIdentifyWithRowPaihangbang1";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang1];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang1];
            }
            if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu){
                cellWithRowFiv.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeHeaderYaoqing;
            } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli){
                cellWithRowFiv.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeHeaderLeiji;
            } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeJiasujilu){
                cellWithRowFiv.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeHeaderJiasu;
            }

            return cellWithRowFiv;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的排行" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang2 = @"cellIdentifyWithRowPaihangbang2";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang2];
            if (!cellWithRowSix){
                cellWithRowSix = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang2];
            }
            cellWithRowSix.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeTitle;
            return cellWithRowSix;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"查看更多" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang3 = @"cellIdentifyWithRowPaihangbang3";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang3];
            if (!cellWithRowSev){
                cellWithRowSev = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang3];
            }
            cellWithRowSev.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeBottom;
            return cellWithRowSev;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"收尾" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang5 = @"cellIdentifyWithRowPaihangbang5";
            UITableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang5];
            if (!cellWithRowSev){
                cellWithRowSev = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang5];
                PDImageView *bgImgView = [[PDImageView alloc]init];
                bgImgView.image = [Tool stretchImageWithName:@"bg_invitation_bottom"];
                bgImgView.stringTag = @"bgImgView";
                cellWithRowSev.backgroundView = bgImgView;
            }
            
            return cellWithRowSev;
        } else {
            static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
            if (!cellWithRowFour){
                cellWithRowFour = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            }
            CenterInvitationHistorySingleModel *singleModel = [[self.invitationMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowFour.transferInvitationModel = singleModel;
            
            if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu){
                cellWithRowFour.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeNormalYaoqing;
            } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli){
                cellWithRowFour.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeNormalLeiji;
            } else if (self.transferType == VersionInvitationLeijiJiangliViewControllerTypeJiasujilu){
                cellWithRowFour.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeNormalJiasu;
            }
            return cellWithRowFour;
        }
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [VersionInvitationHeaderCell calculationCellHeight];
    } else if (indexPath.section == 1){
        return [VersionInvitationTitleCell calculationCellHeight];
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"排行榜标题" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeHeader];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的排行" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeight];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"查看更多" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeight];
       } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"收尾" sourceArr:self.invitationMutableArr]){
           return LCFloat(25);
        } else {
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeight];
        }
    }
}



-(void)getListManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetInvitationHistoryPage:page block:^(CenterInvitationHistoryListModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if([strongSelf.userMutableArr containsObject:@"收尾"]){
            [strongSelf.userMutableArr removeObject:@"收尾"];
        }
        [strongSelf.userMutableArr addObjectsFromArray:singleModel.content];
        [strongSelf.userMutableArr addObject:@"收尾"];
        [strongSelf.invitationTableView reloadData];
    }];
}

-(void)getMemberListManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getMemberInvitationHistoryWithPage:page block:^(CenterInvitationHistoryListModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if([strongSelf.userMutableArr containsObject:@"收尾"]){
            [strongSelf.userMutableArr removeObject:@"收尾"];
        }
        [strongSelf.userMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.userMutableArr addObject:@"收尾"];
        [strongSelf.invitationTableView reloadData];
    }];
}
@end
