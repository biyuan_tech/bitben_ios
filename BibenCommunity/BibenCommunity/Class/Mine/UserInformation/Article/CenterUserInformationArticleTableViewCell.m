//
//  CenterUserInformationArticleTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationArticleTableViewCell.h"
#import "CenterUserInformationArticleUserView.h"

@interface CenterUserInformationArticleTableViewCell()
@property (nonatomic,strong)CenterUserInformationArticleUserView *userInfoView;
@property (nonatomic,strong)PDImageView *detailImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *huatiLabel;

@end

@implementation CenterUserInformationArticleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
}
@end
