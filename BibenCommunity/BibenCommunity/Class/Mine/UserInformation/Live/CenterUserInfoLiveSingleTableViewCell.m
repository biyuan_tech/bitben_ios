//
//  CenterUserInfoLiveSingleTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInfoLiveSingleTableViewCell.h"
#import "CenterUserInfoLivePlayerDetailView.h"

@interface CenterUserInfoLiveSingleTableViewCell()
@property (nonatomic,strong)PDImageView *liveImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *colockImgView;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)CenterUserInfoLivePlayerDetailView *userView;
@property (nonatomic,strong)PDImageView *playHistoryImgView;
@property (nonatomic,strong)UILabel *playHistoryLabel;
@property (nonatomic,strong)UIButton *appointButton;            /**< 预约按钮*/
@end

@implementation CenterUserInfoLiveSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.liveImgView = [[PDImageView alloc]init];
    self.liveImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.liveImgView];
    
    //
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.titleLabel];
    
    self.colockImgView = [[PDImageView alloc]init];
    self.colockImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.colockImgView];
    
    // time
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.timeLabel];
    
    self.userView = [[CenterUserInfoLivePlayerDetailView alloc]initWithFrame:CGRectMake(LCFloat(11), 0, LCFloat(100), LCFloat(30))];
    [self addSubview:self.userView];
}

@end
