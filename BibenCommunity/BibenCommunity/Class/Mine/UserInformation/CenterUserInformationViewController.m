//
//  CenterUserInformationViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationViewController.h"
#import "CenterUserInformationEvaluateViewController.h"
#import "CenterUserInformationArticleViewController.h"
#import "CenterUserInformationLiveViewController.h"


@interface CenterUserInformationViewController ()
@property (nonatomic,strong)UIScrollView *mainScrollViw;
@property (nonatomic,strong)UIView *userView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)CenterUserInformationEvaluateViewController *evaluateController;
@property (nonatomic,strong)CenterUserInformationArticleViewController *articleController;
@property (nonatomic,strong)CenterUserInformationLiveViewController *liveController;


@end

@implementation CenterUserInformationViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createUserView];
    
    [self createScrollView];
    
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"更多" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSLog(@"123");
    }];
}

#pragma mark - createUserView
-(void)createUserView{
    self.userView = [[UIView alloc]init];
    self.userView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(180));
    self.userView.backgroundColor = UURandomColor;
    [self.view addSubview:self.userView];
    
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"文章",@"直播",@"评价"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        
    }];
    self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(self.userView.frame), kScreenBounds.size.width, LCFloat(50));
    [self.view addSubview:self.segmentList];
}

#pragma mark - mainScrollView
-(void)createScrollView{
    if (!self.mainScrollViw){
        self.mainScrollViw = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            
        }];
        self.mainScrollViw.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame));
        self.mainScrollViw.size_height -= 64;
        self.mainScrollViw.contentSize = CGSizeMake(kScreenBounds.size.width * 3, self.mainScrollViw.size_height);
        [self.view addSubview:self.mainScrollViw];
    }
    [self createEvaluateView];
    [self createArticleView];
    [self createLiveView];
}

#pragma mark - createView
// 添加评价
-(void)createEvaluateView{
    self.evaluateController = [[CenterUserInformationEvaluateViewController alloc]init];
    [self.mainScrollViw addSubview:self.evaluateController.view];
    self.evaluateController.view.frame = CGRectMake(2 * kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollViw.size_height);
    [self addChildViewController:self.evaluateController];
}

// 添加文章
-(void)createArticleView{
    self.articleController = [[CenterUserInformationArticleViewController alloc]init];
    [self.mainScrollViw addSubview:self.articleController.view];
    self.articleController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollViw.size_height);
    [self addChildViewController:self.articleController];
}

-(void)createLiveView{
    self.liveController = [[CenterUserInformationLiveViewController alloc]init];
    [self.mainScrollViw addSubview:self.liveController.view];
    self.liveController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollViw.size_height);
    [self addChildViewController:self.liveController];
}

@end
