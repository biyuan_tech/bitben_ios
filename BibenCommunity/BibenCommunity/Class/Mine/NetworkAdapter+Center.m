//
//  NetworkAdapter+Center.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/27.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter+Center.h"
#import "CenterRewardModel.h"
#import "MineRealModel.h"

@implementation NetworkAdapter (Center)

-(void)centerGetUserCountInfoManagerBlock:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_root requestParams:nil responseObjectClass:[LoginServerModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        LoginServerModel *loginSerVerModel = (LoginServerModel *)responseObject;
        [AccountModel sharedAccountModel].loginServerModel = loginSerVerModel;
        [AccountModel sharedAccountModel].block_Address = loginSerVerModel.account.block_address;
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 签到领取BBT
-(void)centerSignGetBBTManagerBlock:(void(^)(BOOL isSuccessed,NSInteger reward))block{
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_attendance requestParams:nil responseObjectClass:[CenterRewardModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterRewardModel *singleModel = (CenterRewardModel *)responseObject;
            if (block){
                block (isSucceeded,singleModel.reward);
            }
        } else {
        }
    }];
}

#pragma mark - 获取资金及注册奖励
-(void)centerGetMoneyAndJiangliBlock:(void(^)(CenterBBTHeaderSingleModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_get_finance requestParams:nil responseObjectClass:[CenterBBTHeaderSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterBBTHeaderSingleModel *singleModel = (CenterBBTHeaderSingleModel *)responseObject;
            if(block){
                block(singleModel);
            }
        }
    }];
}

#pragma mark - 获取分页资金历史记录
-(void)centerGetMoneyHistoryListMnagerWithPage:(NSInteger)page type:(NSInteger)type block:(void(^)(CenterBBTHistoryListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(page),@"page_size":@"10",@"finance_type":@(type)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_page_finance_history requestParams:params responseObjectClass:[CenterBBTHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterBBTHistoryListModel *historyListModel = (CenterBBTHistoryListModel *)responseObject;
            if (block){
                block(historyListModel);
            }
        }
    }];
}


#pragma mark - 获取我邀请的用户数量和奖励总额
-(void)centerGetMoneyget_my_invitationBlock:(void(^)(BOOL isSuccessed,CenterInvitationSingleModel *model))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_invitation requestParams:nil responseObjectClass:[CenterInvitationSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterInvitationSingleModel *singleModel = (CenterInvitationSingleModel *)responseObject;
            if (block){
                block(YES,singleModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

#pragma mark - 获取我的邀请记录
-(void)centerGetInvitationHistoryPage:(NSInteger)page block:(void(^)(CenterInvitationHistoryListModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].loginServerModel.account._id.length){
        NSString *info = [AccountModel sharedAccountModel].loginServerModel.account._id;
        [params setObject:info forKey:@"invite_account_id"];
    }
    [params setValue:@(page) forKey:@"page_number"];
    [params setValue:@"10" forKey:@"page_size"];
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_invitation_history requestParams:params responseObjectClass:[CenterInvitationHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterInvitationHistoryListModel *singleModel = (CenterInvitationHistoryListModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

#pragma mark - 获取关注列表
-(void)sendRequestToGetLinkListManagerWithNumber:(NSInteger)number block:(void(^)(BOOL isSuccessed,CenterFollowModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(number),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_page_attention requestParams:params responseObjectClass:[CenterFollowModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterFollowModel *singleModel = (CenterFollowModel *)responseObject;
            if (block){
                block(YES,singleModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

#pragma mark - 获取粉丝列表
-(void)sendRequestToGetMyFansManagerWithNumber:(NSInteger)number block:(void(^)(BOOL isSuccessed,CenterFollowModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(number),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_page_fans requestParams:params responseObjectClass:[CenterFollowModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterFollowModel *singleModel = (CenterFollowModel *)responseObject;
            if (block){
                block(YES,singleModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

#pragma mark - 关注&取消关注
-(void)centerSendRequestToLinkManagerWithUserId:(NSString *)userId hasLink:(BOOL)hasLink block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    
    if (![AccountModel sharedAccountModel].loginServerModel.user._id.length){
        [StatusBarManager statusBarHidenWithText:@"请先登录"];
        return;
    }
    NSDictionary *params;
    NSString *path = @"";
    if (hasLink){           // 关注
        params = @{@"user_id":[AccountModel sharedAccountModel].loginServerModel.user._id,@"attention_user_id":userId};
        path = center_create_attention;
    } else {
        params = @{@"user_id":[AccountModel sharedAccountModel].loginServerModel.user._id,@"attention_user_id":userId};
        path = center_create_cancelAttention;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
    
}

#pragma mark - 分页获取我的消息
-(void)fetchMyMessageWithInfoWithPageNum:(NSInteger)pageNum type:(MessageType)type block:(void(^)(CenterMessageListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(pageNum),@"page_size":@"10",@"type":@(type)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_page_message requestParams:params responseObjectClass:[CenterMessageListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterMessageListModel *listModel = (CenterMessageListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

#pragma mark - 清空我的消息
-(void)centerCleanMyMessagesWithBlock:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_cleanMessage requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(isSucceeded);
        }
    }];
}

#pragma mark - 消息全部设为已读
-(void)centerMessageReadList:(NSArray *)list block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    
    NSString *info = @"";
    for (int i = 0 ; i < list.count;i++){
        NSString *nId = [list objectAtIndex:i];
        if (i == list.count - 1){
            info = [info stringByAppendingString:[NSString stringWithFormat:@"%@",nId]];
        } else {
            info = [info stringByAppendingString:[NSString stringWithFormat:@"%@,",nId]];
        }
    }
    NSDictionary *params = @{@"user_message_id":info};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"read_message" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}

#pragma mark - 获取账户详情信息
-(void)centerGetMainUserInfoManagerBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_getUserInfo requestParams:nil responseObjectClass:[LoginServerModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LoginServerModel *accountModel = (LoginServerModel *)responseObject;
            [AccountModel sharedAccountModel].loginServerModel.account = accountModel.account;
            [AccountModel sharedAccountModel].loginServerModel.user = accountModel.user;
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 修改我的喜欢
-(void)centerChangeMyLike:(NSArray<LoginServerModelAccountTopic> *)transferArr block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSMutableArray *selectedMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < transferArr.count;i++){
        LoginServerModelAccountTopic *topicModel = [transferArr objectAtIndex:i];
        if (topicModel.isLike){
            [selectedMutableArr addObject:topicModel._id];
        }
    }
    NSString *topicStr = @"";
    if (transferArr.count){
        for (int i = 0 ; i < selectedMutableArr.count;i++){
            NSString *topicId = [selectedMutableArr objectAtIndex:i];
            if (i == selectedMutableArr.count - 1){
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@",topicId]];
            } else {
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@,",topicId]];
            }
        }
    }
    
    NSDictionary *params = @{@"like_topic":topicStr};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_updateAccountInfo requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 修改我的签名

#pragma mark - 获取分享
-(void)centerGetShareInfoWithBlock:(void(^)(BOOL isSuccessed,CenterShareSingleModel *shareModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_get_invitation_url requestParams:nil responseObjectClass:[CenterShareSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterShareSingleModel *singleModel = (CenterShareSingleModel *)responseObject;
            if(block){
                block(YES,singleModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}


#pragma mark - 实名认证
-(void)centerShiminrenzhengManagerWithModel:(MineRealModel *)model block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (model.chooseType == MineRealChooseTableViewCellTypeLtd){           //      机构
        [params setValue:@"2" forKey:@"organ_type"];                       // 类型
        [params setValue:model.infoStr forKey:@"real_name"];               // 公司全称
        [params setValue:model.infoSubStr forKey:@"ID_number"];            // 机构代码
        [params setValue:model.imgUrl1 forKey:@"front_img"];               // 机构代码图片
    } else if (model.chooseType == MineRealChooseTableViewCellTypePerson){  //个人
        [params setValue:@"0" forKey:@"organ_type"];
        [params setValue:model.infoStr forKey:@"real_name"];                // 真实姓名
        [params setValue:model.infoSubStr forKey:@"ID_number"];             // 身份证
        [params setValue:model.imgUrl1 forKey:@"front_img"];                // 正面
        [params setValue:model.imgUrl2 forKey:@"opposite_img"];             // 反面
        [params setValue:model.imgUrl3 forKey:@"hold_img"];                 // 手持
    } else if (model.chooseType == MineRealChooseTableViewCellTypeTeacher){
        [params setValue:@"1" forKey:@"organ_type"];
        [params setValue:model.infoStr forKey:@"real_name"];                // 真实姓名
        [params setValue:model.infoSubStr forKey:@"ID_number"];             // 身份证
        [params setValue:model.imgUrl1 forKey:@"front_img"];                // 正面
        [params setValue:model.imgUrl2 forKey:@"opposite_img"];             // 反面
        [params setValue:model.inputStr forKey:@"other_data"];              // 反面
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"user_certification_confirm" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }

        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}


#pragma mark - 获取实名认证状态
-(void)getShimingRenzhengStatusBlock:(void(^)(MineRealModel *model))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_certification_confirm" requestParams:nil responseObjectClass:[MineRealModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            MineRealModel *model = (MineRealModel *)responseObject;
            if (block){
                block(model);
            }
        }
    }];
}


#pragma mark - 获取通知
-(void)centerMessageGetListWithPage:(NSInteger)pageNum type:(MessageType)type block:(void(^)(CenterMessageRootListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(pageNum),@"page_size":@"10",@"type":@(type)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_page_message requestParams:params responseObjectClass:[CenterMessageRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterMessageRootListModel *listModel = (CenterMessageRootListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

@end
