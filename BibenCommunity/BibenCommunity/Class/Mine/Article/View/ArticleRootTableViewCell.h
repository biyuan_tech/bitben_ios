//
//  ArticleRootTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

@interface ArticleRootSingleImgBgView : UIView

@property (nonatomic,strong)NSArray *transferImgArr;
-(void)actionImgClickBlock:(void(^)(NSString *imgUrl,PDImageView *imgView))block;
+(CGFloat)calculationHeight:(NSArray *)imgArr;

@end






@interface ArticleRootTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferSingleModel;
@property (nonatomic,strong)ArticleRootSingleImgBgView *imgBgView;              // temp
@property (nonatomic,strong)PDImageView *selectedImgView;
@property (nonatomic,strong)UIButton *linkButton;

-(void)btnStatusSelected:(BOOL)isSelected;

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block;
-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block;
-(void)actionClickWithLinkButtonBlock:(void(^)())block;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;
-(void)hiddenLinkBtn;



+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferSingleModel;

@end
