//
//  ArticleReleaseInputToolBar.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleReleaseInputToolBar.h"

static char actionClickBiaoqingblockKey;
static char actionClickJinghaoblockKey;
static char actionClickImgSelectedblockKey;
@interface ArticleReleaseInputToolBar()
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UIButton *jinButton;
@property (nonatomic,strong)UIButton *imgSelectedButton;
@property (nonatomic,strong)UIButton *biaoqingButton;
@end

@implementation ArticleReleaseInputToolBar

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createInputTooView];
    }
    return self;
}

#pragma mark - 创建输入框上面的View
-(void)createInputTooView{
    self.backgroundColor = [UIColor whiteColor];
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"E4E4E4"];
    lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, .5f);
    [self addSubview:lineView];
    
    // 1. 创建label
    self.numberLabel = [GWViewTool createLabelFont:@"14" textColor:@"8F8F8F"];
    NSString *infoNumber = @"200/500";
    CGSize numberSize = [infoNumber sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.numberLabel.font])];
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - numberSize.width, 0, numberSize.width, numberSize.height);
    self.numberLabel.center_y = self.size_height / 2.;
    self.numberLabel.text = [NSString stringWithFormat:@"%li/%li",(long)0,(long)500];
    [self addSubview:self.numberLabel];
    
    // 2. 井
    self.jinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.jinButton.frame = CGRectMake(self.numberLabel.orgin_x - LCFloat(20) - LCFloat(43), 0, LCFloat(43), LCFloat(43));
    [self.jinButton setImage:[UIImage imageNamed:@"icon_article_#"] forState:UIControlStateNormal];
    [self addSubview:self.jinButton];
    __weak typeof(self)weakSelf = self;
    [self.jinButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickJinghaoblockKey);
        if (block){
            block();
        }
    }];
    
    // 3. img
    self.imgSelectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.imgSelectedButton.frame = CGRectMake(self.jinButton.orgin_x - LCFloat(20) - LCFloat(43), 0, LCFloat(43), LCFloat(43));
    [self.imgSelectedButton setImage:[UIImage imageNamed:@"icon_article_img_selected"] forState:UIControlStateNormal];
    [self addSubview:self.imgSelectedButton];
    [self.imgSelectedButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickImgSelectedblockKey);
        if (block){
            block();
        }
    }];
    
    
    // 4. 表情
    self.biaoqingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.biaoqingButton.frame = CGRectMake(self.imgSelectedButton.orgin_x - LCFloat(20) - LCFloat(43), 0, LCFloat(43), LCFloat(43));
    [self.biaoqingButton setImage:[UIImage imageNamed:@"icon_article_smail"] forState:UIControlStateNormal];
    [self addSubview:self.biaoqingButton];
    [self.biaoqingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickBiaoqingblockKey);
        if (block){
            block();
        }
    }];
    self.biaoqingButton.hidden = YES;
}

-(void)setCurrentInputCount:(NSInteger)currentInputCount{
    _currentInputCount = currentInputCount;
    self.numberLabel.text = [NSString stringWithFormat:@"%li/%li",currentInputCount,(long)200];
}

-(void)actionClickBiaoqingblock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickBiaoqingblockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionClickJinghaoblock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickJinghaoblockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionClickImgSelectedblock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickImgSelectedblockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
