//
//  ArticleReleaseInputToolBar.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleReleaseInputToolBar : UIView

@property (nonatomic,assign)NSInteger currentInputCount;

-(void)actionClickBiaoqingblock:(void(^)())block;                   /**< 表情*/
-(void)actionClickJinghaoblock:(void(^)())block;                    /**< 井号*/
-(void)actionClickImgSelectedblock:(void(^)())block;                /**< 图片选择*/
@end
