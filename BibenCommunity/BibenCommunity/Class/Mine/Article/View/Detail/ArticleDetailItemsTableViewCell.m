//
//  ArticleDetailItemsTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailItemsTableViewCell.h"
#import "LiveForumRootDingView.h"

@interface ArticleDetailItemsTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIButton *biView;
@property (nonatomic,strong)UIButton *upView;
@property (nonatomic,strong)UIButton *downView;
@property (nonatomic,strong)UIButton *msgView;
@end

@implementation ArticleDetailItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma makr - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    self.bgView.frame = CGRectMake(LCFloat(16), LCFloat(13), kScreenBounds.size.width - 2 * LCFloat(13), LCFloat(87));
    [self addSubview:self.bgView];
    
 
    for (int i = 0 ; i < 4 ;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:[UIColor hexChangeFloat:@"2A2A2A"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        button.titleLabel.adjustsFontSizeToFitWidth = YES;
        if (i == 0){
            self.biView = button;
        } else if (i == 1){
            self.upView = button;
        } else if (i == 2){
            self.downView = button;
        } else if (i == 3){
            self.msgView = button;
        }
        button.frame = CGRectMake(0, LCFloat(13), 50, LCFloat(87) - 2 * LCFloat(13));
        [self.bgView addSubview:button];
    }
    
    CGFloat single_width = self.bgView.size_width / 8;
    self.biView.center_x =  single_width;
    self.upView.center_x =  3 * single_width;
    self.downView.center_x =  5 * single_width;
    self.msgView.center_x =  7 * single_width;
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;

    // 1. 币
    [self.biView setImage:[UIImage imageNamed:@"icon_article_detail_items_bi"] forState:UIControlStateNormal];
    [self.biView setTitle:[NSString stringWithFormat:@"%@",transferArticleModel.reward] forState:UIControlStateNormal];
    [self.biView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(12)];
    
    // 2. 赞
    [self.upView setTitle:[NSString stringWithFormat:@"%li",transferArticleModel.count_support] forState:UIControlStateNormal];
    if (transferArticleModel.isSupport){
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_items_zan_hlt"] forState:UIControlStateNormal];
    } else {
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_items_zan_nor"] forState:UIControlStateNormal];
    }
    [self.upView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(12)];
    
    // 3. 踩
     [self.downView setTitle:[NSString stringWithFormat:@"%li",transferArticleModel.count_tread] forState:UIControlStateNormal];
    if (self.transferArticleModel.isTread){
        [self.downView setImage:[UIImage imageNamed:@"icon_article_detail_items_down_hlt"] forState:UIControlStateNormal];
    } else {
        [self.downView setImage:[UIImage imageNamed:@"icon_article_detail_items_down_nor"] forState:UIControlStateNormal];
    }
    
    [self.downView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(12)];
    
    __weak typeof(self)weakSelf = self;
    [self.upView buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
       __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManagerWithArticleInterface:strongSelf.upView];
    }];
    [self.downView buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManagerWithArticleInterface:strongSelf.downView];
    }];
    
    // 消息
    [self.msgView setTitle:[NSString stringWithFormat:@"%li",transferArticleModel.comment_count] forState:UIControlStateNormal];
    [self.msgView setImage:[UIImage imageNamed:@"icon_article_detail_items_msg"] forState:UIControlStateNormal];
    [self.msgView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(12)];
}

-(void)buttonActionClickManager:(UIButton *)button{
    if (button == self.upView){
        self.transferArticleModel.isSupport = YES;
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_items_zan_hlt"] forState:UIControlStateNormal];
        [self.upView setTitle:[NSString stringWithFormat:@"%li",self.transferArticleModel.count_support + 1] forState:UIControlStateNormal];
    } else if (button == self.downView){
        self.transferArticleModel.isTread = YES;
        [self.downView setImage:[UIImage imageNamed:@"icon_article_detail_items_down_hlt"] forState:UIControlStateNormal];
        [self.downView setTitle:[NSString stringWithFormat:@"%li",self.transferArticleModel.count_tread + 1] forState:UIControlStateNormal];
    }
}


#pragma mark - 文章的顶和踩
-(void)actionClickManagerWithArticleInterface:(UIButton *)button{
    __weak typeof(self)weakSelf = self;
    BOOL hasUp = NO;
    if (button == self.upView){
        hasUp = YES;
    } else if (button == self.downView){
        hasUp = NO;
    }
    [[NetworkAdapter sharedAdapter] articleDingAndCai:self.transferArticleModel._id hasUp:hasUp block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                [strongSelf buttonActionClickManager:button];
            } else {
                if (button == self.upView){
                    [StatusBarManager statusBarHidenWithText:@"你已经顶过该条评论"];
                } else if (button == self.downView) {
                    [StatusBarManager statusBarHidenWithText:@"你已经踩过该条评论"];
                }
            }
        }
    }];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(87);
    cellHeight += LCFloat(13) * 2;
    return cellHeight;
}

@end
