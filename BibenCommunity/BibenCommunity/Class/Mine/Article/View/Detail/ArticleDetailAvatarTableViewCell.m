//
//  ArticleDetailAvatarTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailAvatarTableViewCell.h"

static char actionLinkButtonClickKey;
static char actionClickHeaderImgWithBlockKey;
@interface ArticleDetailAvatarTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *avatarConvertView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *dateLabel;
@property (nonatomic,strong)UIButton *linkButton;
@property (nonatomic,strong)UILabel *huatiLabel;

@end

@implementation ArticleDetailAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakself = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakself){
            return ;
        }
        __strong typeof(weakself)strongSelf = weakself;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    self.avatarConvertView = [[PDImageView alloc]init];
    self.avatarConvertView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarConvertView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"14" textColor:@"343434"];
    [self addSubview:self.nickNameLabel];
    
    self.dateLabel = [GWViewTool createLabelFont:@"11" textColor:@"A2A2A2"];
    [self addSubview:self.dateLabel];
    
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.linkButton];
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakself){
            return ;
        }
        __strong typeof(weakself)strongSelf = weakself;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionLinkButtonClickKey);
        if (block){
            block();
        }
    }];
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;
    
    // avatar
    self.avatarImgView.frame = CGRectMake(LCFloat(17), LCFloat(7), LCFloat(37), LCFloat(37));
    [self.avatarImgView uploadImageWithRoundURL:transferArticleModel.head_img placeholder:nil callback:NULL];
    
    // avatarConvert
    self.avatarConvertView.frame = self.avatarImgView.frame;
    
    self.nickNameLabel.text = transferArticleModel.nickname;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), 0, nickNameSize.width, nickNameSize.height);
    
    self.dateLabel.text = [NSDate getTimeGap:transferArticleModel.create_time / 1000.];
    CGSize dateSize = [Tool makeSizeWithLabel:self.dateLabel];
    self.dateLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, 9, dateSize.width, dateSize.height);
    
    CGFloat margin = (self.avatarImgView.size_height - nickNameSize.height - dateSize.height) / 3.;
    self.nickNameLabel.orgin_y = self.avatarImgView.orgin_y + margin;
    self.dateLabel.orgin_y = CGRectGetMaxY(self.nickNameLabel.frame) + margin;
    
    // linkButton
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(54), 0, LCFloat(54), LCFloat(26));
    self.linkButton.center_y = self.avatarImgView.center_y;
    if (transferArticleModel.isAttention){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
    
    if ([transferArticleModel.author_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
        self.linkButton.hidden = YES;
    } else {
        self.linkButton.hidden = NO;
    }
}


-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferArticleModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
}

-(void)actionLinkButtonClick:(void(^)())block{
    objc_setAssociatedObject(self, &actionLinkButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(37);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
