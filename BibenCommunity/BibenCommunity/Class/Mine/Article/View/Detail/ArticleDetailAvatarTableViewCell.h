//
//  ArticleDetailAvatarTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

@interface ArticleDetailAvatarTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferArticleModel;

-(void)actionLinkButtonClick:(void(^)())block;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;

-(void)btnStatusSelected:(BOOL)isSelected;
@end
