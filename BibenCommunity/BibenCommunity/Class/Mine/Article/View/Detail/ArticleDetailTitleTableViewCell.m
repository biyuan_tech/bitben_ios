//
//  ArticleDetailTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailTitleTableViewCell.h"

@interface ArticleDetailTitleTableViewCell()

@end

@implementation ArticleDetailTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"17" textColor:@"212121"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    CGSize  titleSize = [transferTitle sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), titleSize.height);
}

+(CGFloat)calculationCellHeightTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    CGSize titleSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"17"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += LCFloat(11);
    return cellHeight;
}


//
//
//- (BOOL)canBecomeFirstResponder{
//    return YES;
//}
//
//- (BOOL)canPerformAction:(SEL)selector withSender:(id) sender {
//    if (selector == @selector(copy:)) {
//        return YES;
//    }
//    return NO;
//}
//
//- (void)copy:(id)sender {
//    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//    [pasteboard setString:self.titleLabel.text];
//}
//
//- (void)showMenuAction:(UILongPressGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateBegan) {
//
////        [self.titleLabel becomeFirstResponder];
//
//        CGPoint location = [sender locationInView:[sender view]];
//
//        UIMenuController *menuController = [UIMenuController sharedMenuController];
//        [menuController setTargetRect:CGRectMake(location.x, location.y, 100.0f, 100.0f) inView:self];
//
//        [menuController setMenuVisible:YES animated:YES];
//    }
//}
@end
