//
//  ArticleDetailTagsTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailTagsTableViewCell.h"
#import "ArticleTagsView.h"

static char actionItemClickWithBlockKey;
@interface ArticleDetailTagsTableViewCell()
@property (nonatomic,strong)UILabel *huatiTitleLabel;
@property (nonatomic,strong)ArticleTagsView *huatiDymicView;
@end

@implementation ArticleDetailTagsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.huatiTitleLabel = [GWViewTool createLabelFont:@"12" textColor:@"959595"];
    self.huatiTitleLabel.text = @"话题";
    CGSize huatiTitleSize = [Tool makeSizeWithLabel:self.huatiTitleLabel];
    self.huatiTitleLabel.frame = CGRectMake(LCFloat(15), LCFloat(7), huatiTitleSize.width, huatiTitleSize.height);
    [self addSubview:self.huatiTitleLabel];
    
    self.huatiDymicView = [[ArticleTagsView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.huatiTitleLabel.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.huatiTitleLabel.frame) - 2 * LCFloat(11) - LCFloat(11),[ArticleTagsView calculationCellHeightWithArr:@[@""] hasEdit:NO])];
    [self addSubview:self.huatiDymicView];
    __weak typeof(self)weakSelf = self;
    [self.huatiDymicView actionClickWithTagsBlock:^(NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *itemInfo) = objc_getAssociatedObject(strongSelf, &actionItemClickWithBlockKey);
        if (block){
            block(info);
        }
    }];
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;
    self.huatiDymicView.hasEdit = NO;
    self.huatiTitleLabel.hidden = NO;
    if (![self.huatiDymicView.transferArr containsObject:[transferArticleModel.topic_content firstObject]]){
        self.huatiDymicView.transferArr = transferArticleModel.topic_content;
        self.huatiDymicView.orgin_x = CGRectGetMaxX(self.huatiTitleLabel.frame) + LCFloat(11);
        self.huatiDymicView.orgin_y = self.huatiTitleLabel.orgin_y - LCFloat(4);
    }
}

+(CGFloat)calculationCellHeight:(ArticleRootSingleModel *)transferArticleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    cellHeight += [ArticleTagsView calculationCellHeightWithArr:transferArticleModel.topic_content hasEdit:NO];
    cellHeight += LCFloat(7);
    return cellHeight;
}

-(void)setTransferTagsArr:(NSArray *)transferTagsArr {
    _transferTagsArr = transferTagsArr;
    self.huatiDymicView.hasEdit = YES;
    self.huatiTitleLabel.hidden = YES;
    self.huatiDymicView.transferArr = transferTagsArr;
    self.huatiDymicView.orgin_x = LCFloat(11);
    self.huatiDymicView.orgin_y = self.huatiTitleLabel.orgin_y - LCFloat(4);
    self.huatiDymicView.size_height = [ArticleTagsView calculationCellHeightWithArr:transferTagsArr hasEdit:NO];

}

-(void)actionItemClickWithBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &actionItemClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)tagsArr{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    cellHeight += [ArticleTagsView calculationCellHeightWithArr:tagsArr hasEdit:NO];
    cellHeight += LCFloat(7);
    return cellHeight;
}
@end
