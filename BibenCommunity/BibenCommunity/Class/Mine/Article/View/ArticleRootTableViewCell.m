//
//  ArticleRootTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleRootTableViewCell.h"
#import "ArticleTagsView.h"

static char actionClickWithImgSelectedBlockKey;
static char actionClickWithTagsSelectedBlockKey;
static char actionClickWithLinkButtonBlockKey;
static char actionClickHeaderImgWithBlockKey;
@interface ArticleRootTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *avatarConvertImgView;
@property (nonatomic,strong)UILabel *nicknameLabel;
@property (nonatomic,strong)UILabel *datetimeLabel;
@property (nonatomic,strong)UILabel *desclabel;
@property (nonatomic,strong)UILabel *huatiLabel;
@property (nonatomic,strong)ArticleTagsView *huatiDymicView;
@end

@implementation ArticleRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createTableView];
    }
    return self;
}

#pragma mark - createView
-(void)createTableView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(37), LCFloat(37));
    [self addSubview:self.avatarImgView];
    
    self.avatarConvertImgView = [[PDImageView alloc]init];
    self.avatarConvertImgView.backgroundColor = [UIColor clearColor];
    self.avatarConvertImgView.image = [UIImage imageNamed:@"bg_avatar_convert"];
    self.avatarConvertImgView.frame = self.avatarImgView.frame;
    [self addSubview:self.avatarConvertImgView];
    __weak typeof(self)weakSelf = self;
    [self.avatarConvertImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    
    self.nicknameLabel = [GWViewTool createLabelFont:@"14" textColor:@"343434"];
    self.nicknameLabel.font = [self.nicknameLabel.font boldFont];
    [self addSubview: self.nicknameLabel];
    
    self.datetimeLabel = [GWViewTool createLabelFont:@"11" textColor:@"A2A2A2"];
    [self addSubview:self.datetimeLabel];
    
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.linkButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.linkButton];
    
    self.desclabel = [GWViewTool createLabelFont:@"15" textColor:@"313131"];
    self.desclabel.numberOfLines = 0;
    [self addSubview:self.desclabel];
    
    self.imgBgView = [[ArticleRootSingleImgBgView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 10)];
    self.imgBgView.backgroundColor = [UIColor clearColor];
    [self.imgBgView actionImgClickBlock:^(NSString *imgUrl, PDImageView *imgView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.selectedImgView = imgView;
        void(^block)(NSString *tempImgUrl) = objc_getAssociatedObject(strongSelf, &actionClickWithImgSelectedBlockKey);
        if (block){
            block(imgUrl);
        }
    }];
    
    [self addSubview:self.imgBgView];
    
    self.huatiLabel = [GWViewTool createLabelFont:@"12" textColor:@"浅灰"];
    self.huatiLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.huatiLabel];
}

-(void)setTransferSingleModel:(ArticleRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;

    // avatar
    [self.avatarImgView uploadImageWithURL:transferSingleModel.head_img placeholder:nil callback:NULL];
    
    // title
    self.nicknameLabel.text = transferSingleModel.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nicknameLabel];
    self.nicknameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), 0, nickSize.width, nickSize.height);
    
    // time
    self.datetimeLabel.text = [NSDate getTimeGap:transferSingleModel.create_time / 1000.];
    CGSize dateSize = [Tool makeSizeWithLabel:self.datetimeLabel];
    self.datetimeLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), 0, dateSize.width, dateSize.height);
    
    CGFloat margin = (self.avatarImgView.size_height - [NSString contentofHeightWithFont:self.nicknameLabel.font] - [NSString contentofHeightWithFont:self.datetimeLabel.font]) / 3.;
    self.nicknameLabel.orgin_y = self.avatarImgView.orgin_y + margin;
    self.datetimeLabel.orgin_y = CGRectGetMaxY(self.nicknameLabel.frame) + margin;
    
    // linkButton
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(54), 0, LCFloat(54), LCFloat(26));
    self.linkButton.center_y = self.avatarImgView.center_y;
    if (transferSingleModel.isAttention){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
    
    // title
    self.desclabel.text = transferSingleModel.title.length?transferSingleModel.title:@"";
    CGSize descSize = [self.desclabel.text sizeWithCalcFont:self.desclabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.desclabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(15), MIN(descSize.height, 4 * [NSString contentofHeightWithFont:self.desclabel.font]));

    // imgBgView
    self.imgBgView.orgin_y = CGRectGetMaxY(self.desclabel.frame) + LCFloat(11);
    self.imgBgView.transferImgArr = transferSingleModel.picture;
    self.imgBgView.size_height = [ArticleRootSingleImgBgView calculationHeight:transferSingleModel.picture];
    self.imgBgView.clipsToBounds = YES;
    
    // 话题
    self.huatiLabel.text = @"话题";
    CGSize huatiSize = [Tool makeSizeWithLabel:self.huatiLabel];
    self.huatiLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.imgBgView.frame) + LCFloat(11), huatiSize.width, huatiSize.height);
    
    if (transferSingleModel.topic_content.count){
        if (!self.huatiDymicView){
            self.huatiDymicView = [[ArticleTagsView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.huatiLabel.frame) + LCFloat(11), self.huatiLabel.orgin_y, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.huatiLabel.frame) - LCFloat(11), [ArticleTagsView calculationCellHeightWithArr:transferSingleModel.topic_content hasEdit:NO])];
            __weak typeof(self)weakSelf = self;
            [self.huatiDymicView actionClickWithTagsBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsSelectedBlockKey);
                if (block){
                    block(info);
                }
            }];
            [self addSubview:self.huatiDymicView];
        }
        
        self.huatiDymicView.transferArr = transferSingleModel.topic_content;
        if (self.huatiDymicView){
            self.huatiDymicView.orgin_y = self.huatiLabel.orgin_y - LCFloat(4);
        }
        self.huatiDymicView.hidden = NO;
        self.huatiLabel.hidden = NO;
    } else {
        self.huatiDymicView.hidden = YES;
        self.huatiLabel.hidden = YES;
    }
    
    if ([transferSingleModel.author_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
        self.linkButton.hidden = YES;
    } else {
        self.linkButton.hidden = NO;
    }
    
    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLinkButtonBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block{
    objc_setAssociatedObject(self, &actionClickWithImgSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block{
    objc_setAssociatedObject(self, &actionClickWithTagsSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithLinkButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLinkButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferSingleModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
}


+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferSingleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(37);
    // 内容
    cellHeight += LCFloat(11);
    NSString *info = transferSingleModel.title.length?transferSingleModel.title:@"";
    CGSize descSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    cellHeight += MIN(descSize.height, 4 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]]);
    // 图片
    cellHeight += LCFloat(11);
    CGFloat mainHeight = [ArticleRootSingleImgBgView calculationHeight:transferSingleModel.picture];
    cellHeight += mainHeight;
    
    // 话题
    if (transferSingleModel.topic_content.count){
        cellHeight += LCFloat(11);
        cellHeight += [ArticleTagsView calculationCellHeightWithArr:transferSingleModel.topic_content hasEdit:NO];
        cellHeight += LCFloat(11);
    }
    return cellHeight;
}

- (void)hiddenLinkBtn{
    self.linkButton.layer.opacity = 0.0f;
}

@end


static char actionImgClickBlockKey;
@interface ArticleRootSingleImgBgView()
@end

@implementation ArticleRootSingleImgBgView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
}

-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
    if (self.subviews.count){
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    if (transferImgArr.count >= 3){
        for (int i = 0 ; i < transferImgArr.count;i++){
            NSString *currentImgUrl = [transferImgArr objectAtIndex:i];
            
            CGFloat margin = LCFloat(11);
            CGFloat single_width = (kScreenBounds.size.width - 4 * margin) / 3.;
            // 1. origin_x
            CGFloat origin_x = margin + (i % 3) * single_width + (i % 3) * margin;
            // 2. origin_y
            CGFloat origin_y = LCFloat(11) + ( i / 3 ) * (single_width + margin);
            // 3 .frame
            CGRect tempRect = CGRectMake(origin_x, origin_y, single_width,single_width);
            
            PDImageView *imgView = [[PDImageView alloc]init];
            imgView.frame = tempRect;
            [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];
            [self addSubview:imgView];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.frame = tempRect;
            [self addSubview:actionButton];
            actionButton.userInteractionEnabled = YES;
            __weak typeof(self)weakSelf = self;
            [actionButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
                if (block){
                    block(currentImgUrl,imgView);
                }
            }];
        }
        self.size_height = [ArticleRootSingleImgBgView calculationHeight:transferImgArr];
    } else if (transferImgArr.count == 2){

        CGFloat margin = LCFloat(22);
        CGFloat single_width = (kScreenBounds.size.width - 3 * margin) / 2.;
        for (int i = 0 ; i < transferImgArr.count;i++){
            NSString *currentImgUrl = [transferImgArr objectAtIndex:i];
            
            CGFloat origin_x = margin + i * (single_width + margin);
            
            PDImageView *imgView = [[PDImageView alloc]init];
            imgView.frame = CGRectMake(origin_x, LCFloat(11), single_width, single_width);
            [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];

            [self addSubview:imgView];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.frame = imgView.frame;
            [self addSubview:actionButton];
            actionButton.userInteractionEnabled = YES;
            __weak typeof(self)weakSelf = self;
            [actionButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
                if (block){
                    block(currentImgUrl,imgView);
                }
            }];
        }
        self.size_height = [ArticleRootSingleImgBgView calculationHeight:transferImgArr];
    } else if (transferImgArr.count == 1){
        
        // 1. 获取当前的imgURL
        NSString *currentImgUrl = [transferImgArr lastObject];
        NSArray *imgArr = [currentImgUrl componentsSeparatedByString:@"!!"];
        CGFloat width = 0;
        CGFloat height = 0;
        if (imgArr.count == 3){
            width = [[imgArr objectAtIndex:1] integerValue];
            height = [[imgArr objectAtIndex:2] integerValue];
        }
        
        PDImageView *imgView = [[PDImageView alloc]init];
        if (width != 0 && height != 0){
            CGFloat maxHeight = [ArticleRootSingleImgBgView calculationHeight:transferImgArr] - LCFloat(11);
            if (width < height){            // 图片高度大于宽度
                CGFloat currentWidth = maxHeight * width / height;
                imgView.frame = CGRectMake(LCFloat(11), LCFloat(11), currentWidth, maxHeight);
            } else {
                CGFloat currentHeight = width * maxHeight / width;
                imgView.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11),  currentHeight);
            }
        } else {
            imgView.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11),  [ArticleRootSingleImgBgView calculationHeight:transferImgArr]);
        }

        __weak typeof(self)weakSelf = self;
        [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];
        
        [self addSubview:imgView];
        
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.frame = imgView.frame;
        [self addSubview:actionButton];
        actionButton.userInteractionEnabled = YES;
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
            if (block){
                block(currentImgUrl,imgView);
            }
        }];
    }
}

-(void)actionImgClickBlock:(void(^)(NSString *imgUrl,PDImageView *imgView))block{
    objc_setAssociatedObject(self, &actionImgClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block{
    objc_setAssociatedObject(self, &actionClickWithTagsSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
+(CGFloat)calculationHeight:(NSArray *)imgArr{
    CGFloat height = 0;
    if (imgArr.count >= 3){
        CGFloat margin = LCFloat(11);
        CGFloat width = (kScreenBounds.size.width - 4 * margin) / 3.;
        CGFloat numberOfRow = imgArr.count % 3 == 0? imgArr.count / 3 : imgArr.count / 3 + 1;
        height = margin + numberOfRow * (margin + width);
    } else if (imgArr.count == 2){
        CGFloat margin = LCFloat(22);
        CGFloat single_width = (kScreenBounds.size.width - 3 * margin) / 2.;

        height += 2 * LCFloat(11) + single_width;
    } else if (imgArr.count == 1){
        height += LCFloat(200);
    }
    
    return height;
}

@end

