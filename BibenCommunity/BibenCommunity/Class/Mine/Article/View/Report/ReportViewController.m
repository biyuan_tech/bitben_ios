//
//  ReportViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "ReportViewController.h"
#import "UIImage+ImageEffects.h"
#import "BYReportSheetView.h"
#import "NetworkAdapter+Article.h"
#define SheetViewHeight       LCFloat(235 )
@interface ReportViewController ()<UITableViewDelegate,UITableViewDataSource>{
    BY_THEME_TYPE tempType;
    NSString *tempItemId;
}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)BYReportSheetView *reportView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITableView *jubaoTableView;
@property (nonatomic,strong)NSArray *jubaoArr;

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createArr];
    [self createSheetView];
    
}

#pragma mark - array
-(void)createArr{
    self.jubaoArr = @[@"违法违规",@"色情暴力",@"攻击歧视",@"广告营销",@"与话题无关"];
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    self.view.backgroundColor = [UIColor clearColor];
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.view addSubview:self.actionBgimgView];
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];
    
    // 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(50));
    self.titleLabel.text = @"举报";
    [_shareView addSubview:self.titleLabel];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, .5f);
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    _shareView.size_height = self.titleLabel.size_height + LCFloat(50) * self.jubaoArr.count + LCFloat(50);
    
    [self createView];
}

#pragma mark 创建分享图标
-(void)createView{
    if (!self.jubaoTableView){
        self.jubaoTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, LCFloat(50) * [BYReportSheetView actionItemsArr].count)];
        self.jubaoTableView.dataSource = self;
        self.jubaoTableView.delegate = self;
        [self.shareView addSubview:self.jubaoTableView];
    }
    [self createDismissButton];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.jubaoArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = LCFloat(50);
    cellWithRowOne.transferTitle = [self.jubaoArr objectAtIndex:indexPath.row];
    cellWithRowOne.textAlignmentCenter = YES;
    cellWithRowOne.titleLabel.textColor = [UIColor hexChangeFloat:@"6F6F6F"];
    cellWithRowOne.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *info = [self.jubaoArr objectAtIndex:indexPath.row];
    [self jubaoManager:info];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(50);
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    UIView *lineView = [[UIView alloc]init];
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissBtn.frame = CGRectMake(0, _shareView.frame.size.height - LCFloat(51), kScreenBounds.size.width, LCFloat(51));
    [dismissBtn setTitle:@"取消" forState:UIControlStateNormal];
    dismissBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    dismissBtn.backgroundColor = [UIColor whiteColor];
    [dismissBtn setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [dismissBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:dismissBtn];
    
    lineView.frame = CGRectMake(0, dismissBtn.orgin_y - .5f, kScreenBounds.size.width, .5f);
    
    // Gesture
//    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
//    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ReportViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController type:(BY_THEME_TYPE)type itemId:(NSString *)itemId{
    tempType = type;
    tempItemId = itemId;
    
    __weak ReportViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

#pragma mark - 举报接口
-(void)jubaoManager:(NSString *)info{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] jubaoManagerWithId:tempItemId type:tempType jubaoInfo:info block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf dismissFromView:_showViewController];
        [[UIAlertView alertViewWithTitle:@"举报成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}

@end
