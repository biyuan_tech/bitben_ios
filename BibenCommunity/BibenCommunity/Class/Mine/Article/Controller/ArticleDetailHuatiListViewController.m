//
//  ArticleDetailHuatiListViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailHuatiListViewController.h"
#import "NetworkAdapter+Article.h"

static char actionClickWithSelectedBlockKey;
@interface ArticleDetailHuatiListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *huatiListTableView;
@property (nonatomic,strong)NSMutableArray *huatiMutableArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;

@end

@implementation ArticleDetailHuatiListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetListInfo];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"话题列表";
    
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_nav_success"] barHltImage:[UIImage imageNamed:@"icon_nav_success"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManager];
    }];
}

-(void)actionClickManager{
    NSMutableArray *tempMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < self.selectedMutableArr.count;i++){
        ArticleDetailHuatiListSingleModel *singleModel = [self.selectedMutableArr objectAtIndex:i];
        [tempMutableArr addObject:singleModel.content];
    }
    
    void(^block)(NSArray *infoArr) = objc_getAssociatedObject(self, &actionClickWithSelectedBlockKey);
    if (block){
        block(tempMutableArr);
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.huatiMutableArr = [NSMutableArray array];
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.huatiListTableView){
        self.huatiListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.huatiListTableView.dataSource = self;
        self.huatiListTableView.delegate = self;
        [self.view addSubview:self.huatiListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.huatiMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWSelectedTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWSelectedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    ArticleDetailHuatiListSingleModel *singleModel = [self.huatiMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = singleModel.content;
    if([self.selectedMutableArr containsObject:singleModel]){
        [cellWithRowOne setChecked:YES];
    } else {
        [cellWithRowOne setChecked:NO];
    }
 
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ArticleDetailHuatiListSingleModel *singleModel = [self.huatiMutableArr objectAtIndex:indexPath.row];
    GWSelectedTableViewCell *cellWithRowOne = (GWSelectedTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];

    if ([self.selectedMutableArr containsObject:singleModel]){
        [self.selectedMutableArr removeObject:singleModel];
        [cellWithRowOne setChecked:NO];
    } else {
        if (self.selectedMutableArr.count >= 5){
            [StatusBarManager statusBarHidenWithText:@"话题数量最多5个哦~"];
            return;
        }
        [self.selectedMutableArr addObject:singleModel];
        [cellWithRowOne setChecked:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWSelectedTableViewCell calculationCellHeight];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(1);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.huatiListTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.huatiMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.huatiMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - Info
-(void)sendRequestToGetListInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetHuatiListManagerBlock:^(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiMutableArr removeAllObjects];
            }
            [strongSelf.huatiMutableArr addObjectsFromArray:listModel.content];
            
            for (int i = 0 ; i < strongSelf.transferSelectedHuatiArr.count;i++){
                NSString *selectedStr = [strongSelf.transferSelectedHuatiArr objectAtIndex:i];
                for (int j = 0 ; j < listModel.content.count;j++){
                    ArticleDetailHuatiListSingleModel *singleModel = [listModel.content objectAtIndex:j];
                    if ([singleModel.content isEqualToString:selectedStr]){
                        [strongSelf.selectedMutableArr addObject:singleModel];
                    }
                }
            }
            
            // appStore 审核
            if ([AccountModel sharedAccountModel].isShenhe){
                [strongSelf.huatiMutableArr removeAllObjects];
                for (int i = 0 ; i < 4;i++){
                    ArticleDetailHuatiListSingleModel *singleModel = [[ArticleDetailHuatiListSingleModel alloc]init];
                    if (i == 0){
                        singleModel.content = @"今日心情";
                    } else if(i == 1){
                        singleModel.content = @"浙江高考英语";
                    } else if (i == 2){
                        singleModel.content = @"2018年只剩1个月";
                    } else if (i == 3){
                        singleModel.content = @"蒙面唱将猜猜猜";
                    }
                    [strongSelf.huatiMutableArr addObject:singleModel];
                }
            }
            
            [strongSelf.huatiListTableView reloadData];
            
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiListTableView dismissPrompt];
            } else {
                [strongSelf.huatiListTableView showPrompt:@"当前没有话题" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        } else {
            
        }
    }];
}


-(void)actionClickWithSelectedBlock:(void(^)(NSArray *selectedArr))block{
    objc_setAssociatedObject(self, &actionClickWithSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
