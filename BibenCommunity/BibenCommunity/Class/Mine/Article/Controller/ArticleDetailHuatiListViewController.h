//
//  ArticleDetailHuatiListViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

@interface ArticleDetailHuatiListViewController : AbstractViewController

@property (nonatomic,strong)NSArray *transferSelectedHuatiArr;

-(void)actionClickWithSelectedBlock:(void(^)(NSArray *selectedArr))block;

@end
