//
//  ArticleRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleRootViewController.h"
#import "ArticleRootTableViewCell.h"
#import "ArticleRootActionTableViewCell.h"
#import "ArticleReleaseRootViewController.h"
#import "NetworkAdapter+Article.h"
#import "GWAssetsImgSelectedViewController.h"
#import "ArticleDetailRootViewController.h"
#import "BYPersonHomeController.h"
#import "NetworkAdapter+Center.h"
#import "KxMenu.h"
#import "SegmentListTitleDropView.h"

@interface ArticleRootViewController ()<UITableViewDelegate,UITableViewDataSource,HTHorizontalSelectionListDelegate,HTHorizontalSelectionListDataSource>{
    ArticleListType selectedArticleType;
}
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)KxMenu *sliderMenu;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;
@property (nonatomic,strong)NSMutableArray *sliderMenuRootMutableArr;
@property (nonatomic,strong)NSMutableArray *sliderMenuSelectedMutableArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UITableView *linkArticleTableView;
@property (nonatomic,strong)NSMutableArray *linkArticleMutableArr;

@property (nonatomic,strong)UITableView *articleTableView;
@property (nonatomic,strong)NSMutableArray *articleMutableArr;
@property (nonatomic,strong)NSMutableArray *segmentListMutableArr;
@end


@implementation ArticleRootViewController

+(instancetype)sharedController{
    static ArticleRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[ArticleRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [self createTableView];
    if (self.transferType != ArticleRootViewControllerTypeMine){
        [self createSegment];
    } else {
        self.mainScrollView.scrollEnabled = NO;
    }
    [self linkManager];
}

-(void)linkManager{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] autoLoginWithNoneWindowBlock:^(BOOL isLogin) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetListManager:YES];
    }];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(245, 245, 245, 1);
    if (self.transferType == ArticleRootViewControllerTypeMine){
        self.barMainTitle = @"我的文章";
    } else if (self.transferType == ArticleRootViewControllerTypeNormal){
        self.barMainTitle = @"观点";
    } else if (self.transferType == ArticleRootViewControllerTypeTags){
        if (self.transferTags.length){
            self.barMainTitle = self.transferTags;
        }
    }
}

-(void)autoLoginToRelease{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            ArticleReleaseRootViewController *releaseView = [[ArticleReleaseRootViewController alloc]init];
            [releaseView actionWithReleaseSuccessedBlock:^{
                [strongSelf sendRequestToGetListManager:YES];
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:releaseView];
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    selectedArticleType = ArticleListTypeTuijian;
    self.articleMutableArr = [NSMutableArray array];
    self.sliderMenuRootMutableArr = [NSMutableArray array];
    self.sliderMenuSelectedMutableArr = [NSMutableArray array];
    self.segmentMutableArr = [NSMutableArray array];
    [self.segmentMutableArr addObjectsFromArray:@[@"推荐",@"关注"]];
    self.linkArticleMutableArr = [NSMutableArray array];
    self.segmentListMutableArr = [NSMutableArray array];
    
    for (int i = 0 ; i < 2 ;i++){
        SegmentListTitleDropView *segmentListView = [[SegmentListTitleDropView alloc]initWithFrame:CGRectMake(0, 0, 60, 40)];
        if (i == 0){
            segmentListView.arrowHasHidden = NO;
        } else {
            segmentListView.arrowHasHidden = YES;
        }
        segmentListView.transferTitle = [self.segmentMutableArr objectAtIndex:i];
        [self.segmentListMutableArr addObject:segmentListView];
    }
}

#pragma mark - 创建segment
-(void)createSegment{
    PDSelectionListTitleView *segment = [[PDSelectionListTitleView alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40))];
    segment.delegate = self;
    segment.dataSource = self;
    [segment setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    segment.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    segment.bottomTrimColor = [UIColor clearColor];
    segment.isNotScroll = YES;
    [segment setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    segment.backgroundColor = [UIColor clearColor];
    segment.hasArticle = YES;
    self.segmentList = segment;
    self.navigationItem.titleView = self.segmentList;
    [self createSegmentSlider];
}

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList{
    return self.segmentMutableArr.count;
}

-(UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index{
    SegmentListTitleDropView *segmentListView;
    segmentListView = (SegmentListTitleDropView *) [self.segmentListMutableArr objectAtIndex:index];
    if (index == 0){
        [segmentListView scrollToItemsWithSelected:YES];
    } else {
        [segmentListView scrollToItemsWithSelected:NO];
    }
    return segmentListView;
}

-(void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index{
    SegmentListTitleDropView *segmentListView1 = (SegmentListTitleDropView *) [self.segmentListMutableArr objectAtIndex:0];
    SegmentListTitleDropView *segmentListView2 = (SegmentListTitleDropView *) [self.segmentListMutableArr objectAtIndex:1];
    if (index == 0){
        if (self.mainScrollView.contentOffset.x == 0){
            [segmentListView1 animationArrowTransHasShow:YES];
            [segmentListView2 animationArrowTransHasShow:NO];
        } else {
            [segmentListView1 scrollToItemsWithSelected:YES];
            [segmentListView2 scrollToItemsWithSelected:NO];
        }
    } else {
        
        [segmentListView1 animationArrowTransHasShow:NO];
        [segmentListView1 scrollToItemsWithSelected:NO];
        [segmentListView2 scrollToItemsWithSelected:YES];
        
    }
    [self segmentListSelectedIndex:index];
}

-(void)segmentListSelectedIndex:(NSInteger)index{
    __weak typeof(self)weakSelf = self;
    if (index == 0){
        if (self.mainScrollView.contentOffset.x == 0){
            UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
            CGRect rect = [self.navigationController.navigationBar convertRect:self.segmentList.frame toView:window];
            CGRect nRect = CGRectMake(rect.origin.x - 25, rect.origin.y, rect.size.width,rect.size.height);
            [self showMenuWithRect:nRect];
        } else {
            [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    } else if (index == 1){
        if ([[AccountModel sharedAccountModel] hasLoggedIn]){
            if (!self.linkArticleMutableArr.count){
                [self sendRequestToGetMyLinkArticleWithReload:YES];
            }
            
            [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
        } else {
            [[AccountModel sharedAccountModel] autoLoginWithNoneWindowBlock:^(BOOL isLogin) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (isLogin){               // 已登录
                    strongSelf.mainScrollView.scrollEnabled = YES;
                    [strongSelf.segmentList setSelectedButtonIndex:1 animated:NO];
                    [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
                } else {
                    [strongSelf.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                    [strongSelf.segmentList setSelectedButtonIndex:0 animated:NO];
                    [[AccountModel sharedAccountModel] authorizeWithCompletionHandler:^(BOOL successed) {
                        if (successed){
                            strongSelf.mainScrollView.scrollEnabled = YES;
                            [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
                            [strongSelf sendRequestToGetMyLinkArticleWithReload:YES];
                        }
                    }];
                }
            }];
        }
    }
}

#pragma mark - 创建slider
-(void)createSegmentSlider{
    KxMenuItem *item1 = [KxMenuItem menuItem:@"最新" image:nil target:self action:@selector(menuAction:)];
    item1.title = @"最新";
    
    item1.foreColor = [UIColor hexChangeFloat:@"6A6A6A"];
    
    KxMenuItem *item2 = [KxMenuItem menuItem:@"最热" image:nil target:self action:@selector(menuAction:)];
    item2.title = @"最热";
    item2.foreColor = [UIColor hexChangeFloat:@"6A6A6A"];
    
    KxMenuItem *item3 = [KxMenuItem menuItem:@"推荐" image:nil target:self action:@selector(menuAction:)];
    item3.title = @"推荐";
    item3.foreColor = [UIColor hexChangeFloat:@"6A6A6A"];
    
    [self.sliderMenuRootMutableArr addObjectsFromArray:@[item1,item2,item3]];
    
    [self.sliderMenuSelectedMutableArr addObjectsFromArray:@[item1,item2]];
}

- (void)showMenuWithRect:(CGRect)rect{
    [KxMenu setTitleFont:[UIFont fontWithCustomerSizeName:@"13"]];
    __weak typeof(self)weakSelf = self;
    [[KxMenu sharedMenu] dismissMenuRootBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        SegmentListTitleDropView *segmentListView1 = (SegmentListTitleDropView *) [strongSelf.segmentListMutableArr objectAtIndex:0];
        [segmentListView1 animationArrowTransHasShow:NO];
    }];
    
    [KxMenu showMenuInView:self.view.window fromRect:rect menuItems:self.sliderMenuSelectedMutableArr];
}

-(void)menuAction:(KxMenuItem *)menu{
    SegmentListTitleDropView *segmentListView1 = (SegmentListTitleDropView *) [self.segmentListMutableArr objectAtIndex:0];
    [segmentListView1 scrollToItemsWithSelected:YES];
    [segmentListView1 animationArrowTransHasShow:NO];
    
    KxMenuItem *item1 = [self.sliderMenuRootMutableArr objectAtIndex:0];
    KxMenuItem *item2 = [self.sliderMenuRootMutableArr objectAtIndex:1];
    KxMenuItem *item3 = [self.sliderMenuRootMutableArr objectAtIndex:2];
    [self.sliderMenuSelectedMutableArr removeAllObjects];
    segmentListView1.transferTitle = menu.title;
    if ([menu.title isEqualToString:@"最新"]){
        selectedArticleType = ArticleListTypeNew;
        [self.sliderMenuSelectedMutableArr addObjectsFromArray:@[item2,item3]];
        self.articleTableView.currentPage = 0;
    } else if ([menu.title isEqualToString:@"最热"]){
        selectedArticleType = ArticleListTypeHot;
        [self.sliderMenuSelectedMutableArr addObjectsFromArray:@[item1,item3]];
        self.articleTableView.currentPage = 0;
    } else if ([menu.title isEqualToString:@"推荐"]){
        selectedArticleType = ArticleListTypeTuijian;
        [self.sliderMenuSelectedMutableArr addObjectsFromArray:@[item1,item2]];
        self.articleTableView.currentPage = 0;
    }
    
    // 修改segment

    [self sendRequestToGetListManager:YES];
}

#pragma mark - UIScrollView
-(void)createScrollView{
    __weak typeof(self)weakSelf = self;
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
        }];

        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.contentSize = CGSizeMake(2 * kScreenBounds.size.width, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
        
    
        
        if(self.navigationController.childViewControllers.count == 1){
            
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].tabBarHeight - [BYTabbarViewController sharedController].navBarHeight);
        } else {
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].navBarHeight);
        }
        
        if (self.transferType == ArticleRootViewControllerTypeMine){
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].navBarHeight);
        } else {
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].tabBarHeight - [BYTabbarViewController sharedController].navBarHeight);
            
        }
    }
}


#pragma makr - UITableView
-(void)createTableView{
    if (!self.articleTableView){
        self.articleTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.articleTableView.dataSource = self;
        self.articleTableView.delegate = self;
        [self.mainScrollView addSubview:self.articleTableView];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.articleTableView appendingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetListManager:YES];
    }];
    
    [self.articleTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetListManager:NO];
    }];
    
    // 获取我的关注
    if (!self.linkArticleTableView){
        self.linkArticleTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.linkArticleTableView.orgin_x = kScreenBounds.size.width;
        self.linkArticleTableView.dataSource = self;
        self.linkArticleTableView.delegate = self;
        [self.mainScrollView addSubview:self.linkArticleTableView];
    }
    [self.linkArticleTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMyLinkArticleWithReload:YES];
    }];
    [self.linkArticleTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMyLinkArticleWithReload:NO];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.articleTableView){
        return self.articleMutableArr.count;
    } else if (tableView == self.linkArticleTableView){
        return self.linkArticleMutableArr.count;
    }
    return self.articleMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        ArticleRootSingleModel *singleModel;
        if (tableView == self.articleTableView){
            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.linkArticleTableView){
            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
        }
        
        cellWithRowOne.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf tableView:tableView didSelectRowAtIndexPath:indexPath];
        }];
        
        [cellWithRowOne actionClickWithTagsSelectedBlock:^(NSString *tag) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
            articleRootVC.transferTags = tag;
            [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
        }];
        
        [cellWithRowOne actionClickWithLinkButtonBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
                if (successed){
                    [strongSelf linkWIthCell:cellWithRowOne hasLink:!singleModel.isAttention];
                }
            }];
        }];
        
        [cellWithRowOne actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowOne.transferSingleModel.author_id;
            [weakSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ArticleRootActionTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleRootActionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        ArticleRootSingleModel *singleModel;
        if (tableView == self.articleTableView){
            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.linkArticleTableView){
            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
        }
        cellWithRowTwo.transferSingleModel = singleModel;
        return cellWithRowTwo;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
    viewController.transferPageType = ArticleDetailRootViewControllerTypeArticle;
    ArticleRootSingleModel *singleModel;
    if (tableView == self.articleTableView){
        singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
    } else if (tableView == self.linkArticleTableView){
        singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
    }
    
    __weak typeof(self)weakSelf = self;
    [viewController actionReoloadFollow:^(BOOL link) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 遍历所有的Model 只要是同一个作者的都备注已关注
        for (int i = 0 ; i < strongSelf.articleMutableArr.count;i++){
            ArticleRootSingleModel *tempSingleModel = [strongSelf.articleMutableArr objectAtIndex:i];
            if ([tempSingleModel.author_id isEqualToString:singleModel.author_id]){
                tempSingleModel.isAttention = link;
            }
        }
        [strongSelf.articleTableView reloadData];
    }];
    
    viewController.transferArticleModel = singleModel;
    [self.navigationController pushViewController:viewController animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        ArticleRootSingleModel *singleModel;
        if (tableView == self.articleTableView){
            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.linkArticleTableView){
            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
        }
        return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
    } else {
        return LCFloat(50);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.articleTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.row == 0){
            separatorType  = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 接口
-(void)sendRequestToGetListManager:(BOOL)hasLoad{
    __weak typeof(self)weakSelf = self;
    NSString *authorId = self.transferType == ArticleRootViewControllerTypeMine ? [AccountModel sharedAccountModel].account_id:@"";
    if(!self.articleMutableArr.count){
        [self.articleTableView showListLoadingAnimation];
    }
    [[NetworkAdapter sharedAdapter] articleListWithPage:self.articleTableView.currentPage topic:self.transferTags type:selectedArticleType authorId:authorId block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.articleTableView.isXiaLa || hasLoad){       // 下拉
            [strongSelf.articleTableView dismissListAnimation];
            [strongSelf.articleMutableArr removeAllObjects];
        }
        
        [strongSelf.articleMutableArr addObjectsFromArray:articleList];
        
        [strongSelf.articleTableView stopPullToRefresh];
        [strongSelf.articleTableView stopFinishScrollingRefresh];
        
        if (articleList.count){
            [strongSelf.articleTableView reloadData];
        }

        if (strongSelf.articleMutableArr.count){
            [strongSelf.articleTableView dismissPrompt];
            [strongSelf.articleTableView dismissListAnimation];

        } else {
            [strongSelf.articleTableView showPrompt:@"当前没有文章" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}

#pragma mark - 关注
-(void)linkWIthCell:(ArticleRootTableViewCell *)cell hasLink:(BOOL)link{
    NSString *userId = cell.transferSingleModel.author_id;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (isSuccessed){
            // 遍历所有的Model 只要是同一个作者的都备注已关注
            for (int i = 0 ; i < strongSelf.articleMutableArr.count;i++){
                ArticleRootSingleModel *tempSingleModel = [strongSelf.articleMutableArr objectAtIndex:i];
                if ([tempSingleModel.author_id isEqualToString:cell.transferSingleModel.author_id]){
                    tempSingleModel.isAttention = link;
                }
            }
            [strongSelf.articleTableView reloadData];
        }
    }];
}

#pragma mark - 获取我关注的
-(void)sendRequestToGetMyLinkArticleWithReload:(BOOL)reload{
    __weak typeof(self)weakSelf = self;
    if (!self.linkArticleMutableArr.count){
        [self.linkArticleTableView showListLoadingAnimation];
    }

    [[NetworkAdapter sharedAdapter]articleListWithPage:self.linkArticleTableView.currentPage block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.linkArticleTableView.isXiaLa || reload){
            [strongSelf.linkArticleMutableArr removeAllObjects];
        }

        [strongSelf.linkArticleTableView stopPullToRefresh];
        [strongSelf.linkArticleTableView stopFinishScrollingRefresh];
        
        if (articleList.count){
            [strongSelf.linkArticleMutableArr addObjectsFromArray:articleList];
            [strongSelf.linkArticleTableView reloadData];
        }
        
        if (strongSelf.linkArticleMutableArr.count){
            [strongSelf.linkArticleTableView dismissPrompt];
            [strongSelf.linkArticleTableView dismissListAnimation];

        } else {
            [strongSelf.linkArticleTableView showPrompt:@"当前没有文章信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
    }];
}



@end
