//
//  ArticleDetailRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "ArticleRootSingleModel.h"

typedef NS_ENUM(NSInteger,ArticleDetailRootViewControllerType) {
    ArticleDetailRootViewControllerTypeLive = 0,
    ArticleDetailRootViewControllerTypeArticle = 1,
    ArticleDetailRootViewControllerTypeBanner = 2,
};

@interface ArticleDetailRootViewController : AbstractViewController

@property (nonatomic,strong)ArticleRootSingleModel *transferArticleModel;
@property (nonatomic,copy)NSString *transferArticleId;
@property (nonatomic,assign)ArticleDetailRootViewControllerType transferPageType;

-(void)actionReoloadFollow:(void(^)(BOOL link))block;

@end
