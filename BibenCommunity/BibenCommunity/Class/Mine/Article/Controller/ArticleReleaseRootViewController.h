//
//  ArticleReleaseRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

@interface ArticleReleaseRootViewController : AbstractViewController

-(void)actionWithReleaseSuccessedBlock:(void(^)())block;

@end
