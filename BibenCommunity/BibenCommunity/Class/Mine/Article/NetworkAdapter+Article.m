//
//  NetworkAdapter+Article.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter+Article.h"

@implementation NetworkAdapter (Article)

#pragma mark 1.上传图片接口
-(void)articleAddWithImgList:(NSArray *)imgList topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    if (imgList.count){
        [self uploadImgs:imgList block:^(NSArray *imgUrlArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf subArticleAddWithImgList:imgUrlArr topicArr:topicArr title:title content:content block:block];
        }];
    } else {
           [self subArticleAddWithImgList:nil topicArr:topicArr title:title content:content block:block];
    }
}

// 2. 上传图片
-(void)subArticleAddWithImgList:(NSArray *)imgList topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (title.length){
        [params setObject:title forKey:@"title"];
    }
    [params setObject:@"0" forKey:@"article_type"];
    if (imgList.count){
        NSString *imgString = @"";
        for (int i = 0 ; i < imgList.count;i++){
            NSString *singleImgUrl = [imgList objectAtIndex:i];
            if (i == imgList.count - 1){
                imgString = [imgString stringByAppendingString:[NSString stringWithFormat:@"%@",singleImgUrl]];
            } else {
                imgString = [imgString stringByAppendingString:[NSString stringWithFormat:@"%@,",singleImgUrl]];
            }
        }
        [params setObject:imgString forKey:@"picture"];
    }
    if (topicArr.count){
        NSString *topicStr = @"";
        for (int i = 0 ; i < topicArr.count;i++){
            NSString *topic = [topicArr objectAtIndex:i];
            if (i == topicArr.count - 1){
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@",topic]];
            } else {
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@,",topic]];
            }
        }
        
        [params setObject:topicStr forKey:@"topic_content"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:article_create_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}



#pragma mark - 上传多张图片
-(void)uploadImgs:(NSArray *)imgs block:(void(^)(NSArray *imgUrlArr))block{
    NSMutableArray *fileMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < imgs.count;i++){
        OSSFileModel *fileModle = [[OSSFileModel alloc]init];
        fileModle.objcImage = [imgs objectAtIndex:i];
        [fileMutableArr addObject:fileModle];
    }
    
    __weak typeof(self)weakSelf = self;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[fileMutableArr copy] withUrlBlock:^(NSArray *imgUrlArr) {
        if (!weakSelf){
            return ;
        }
        if (imgUrlArr){
            if (block){
                block(imgUrlArr);
            }
        }
    }];
}

#pragma mark - 获取列表
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    [self articleListWithPage:page topic:topic type:ArticleListTypeNew authorId:authorId block:block];
}

-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic type:(ArticleListType)type authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(page) forKey:@"page_number"];
    [params setValue:@"10" forKey:@"page_size"];
    if (authorId.length){
        [params setValue:authorId forKey:@"author_id"];
    }
    if (topic.length){
        [params setValue:topic forKey:@"topic_content"];
    }
    
    NSString *path = @"";
    if (authorId.length){
        path = @"page_author_article";
    } else {
        if (topic.length){
            path = @"page_topic_article";
        } else {
            path = @"page_article";
        }
    }
    
    if ([AccountModel sharedAccountModel].isShenhe){
        [params setValue:@"2" forKey:@"status"];
    } else {
        [params setValue:@"1" forKey:@"status"];
    }
    if (!authorId.length){
        [params setValue:@(type) forKey:@"type"];
    }
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
}

// 获取我关注的列表
-(void)articleListWithPage:(NSInteger)page block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(page) forKey:@"page_number"];
    [params setValue:@"10" forKey:@"page_size"];
    
    NSString *path = @"page_attention_article";
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
    
}


#pragma mark - 获取指定作者列表
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic author_id:(NSString *)author_id status:(NSString *)status block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(page),@"page_size":@"10",@"author_id":author_id,@"status":status};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_author_article" requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
}



#pragma mark - 获取热门点评
-(void)articleGetHotCommentId:(NSString *)commentId actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"theme_id":commentId,@"theme_type":@"1",@"reply_comment_id":@"0",@"page_number":@(0),@"page_size":@"10"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_hot_comment" requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
    
        if (isSucceeded){
            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
            if (actionBlock){
                actionBlock(YES,listModel);
            }
        } else {
            if (actionBlock){
                actionBlock(NO,nil);
            }
        }
    }];
}

#pragma mark - 添加评论
-(void)articleCreateCommentWiththemeId:(NSString *)themeId ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@"1" forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        if (themeinfo.transferTempCommentId.length){
            [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        } else {
            [params setObject:themeinfo._id forKey:@"reply_comment_id"];
        }
        
        [params setObject:themeinfo.user_id forKey:@"reply_user_id"];
    }
    
    [params setObject:content forKey:@"content"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_ppt_create_comment requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}

#pragma mark  - 获取评论列表
-(void)sendRequestToGetInfoWithCommentId:(NSString *)commentId reReload:(BOOL)rereload page:(NSInteger)page actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock{
    page = rereload ? 0:page;
    NSDictionary *params = @{@"theme_id":commentId,@"theme_type":@"1",@"reply_comment_id":@"0",@"page_number":@(page),@"page_size":@"10"};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_comment" requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
            if (actionBlock){
                actionBlock(YES,listModel);
            }
        }
    }];
}


#pragma mark - 获取话题列表
-(void)sendRequestToGetHuatiListManagerBlock:(void(^)(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_topic_list" requestParams:nil responseObjectClass:[ArticleDetailHuatiListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleDetailHuatiListModel *listModel = (ArticleDetailHuatiListModel *)responseObject;
            if (block){
                block(YES,listModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}


#pragma mark - 顶&踩
-(void)articleDingAndCai:(NSString *)commentId hasUp:(BOOL)up block:(void(^)(BOOL isSuccessed ,BOOL isOperate))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:commentId forKey:@"theme_id"];
    [params setValue:@"1" forKey:@"theme_type"];
    if (up){
        [params setValue:@"0" forKey:@"operate_type"];
    } else {
        [params setValue:@"1" forKey:@"operate_type"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"create_interaction" requestParams:params responseObjectClass:[LiveFourumOperateModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveFourumOperateModel *model = (LiveFourumOperateModel *)responseObject;
            
            if (block){
                block(isSucceeded, model.isOperate);
            }
        }
    }];
}

#pragma mark - 举报
-(void)jubaoManagerWithId:(NSString *)themeId type:(BY_THEME_TYPE)themeType jubaoInfo:(NSString *)jubaoInfo block:(void(^)())block{
    NSDictionary *param = @{@"theme_id":themeId,
                            @"theme_type":@(themeType),
                            @"report_type":jubaoInfo
                            };
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"create_report" requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}


#pragma mark - 取消定时发布
-(void)articleDraftCancelReleaseWithArticleId:(NSString *)articleId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"article_id":articleId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:cancel_timing_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 立即发布
-(void)articleDraftReleaseNowWithArticleId:(NSString *)articleId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"article_id":articleId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:publish_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            block();
        }
    }];
}

@end
