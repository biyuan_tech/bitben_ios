//
//  CenterBBTMyFinaniceHistoryModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTMyFinaniceRewardModel : FetchModel

@property (nonatomic,assign)CGFloat add_reward;
@property (nonatomic,assign)CGFloat sub_reward;
@property (nonatomic,assign)CGFloat time;

@end

@protocol CenterBBTMyFinaniceSubHistoryModel <NSObject>

@end

@interface CenterBBTMyFinaniceSubHistoryModel : FetchModel

@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)CGFloat coin;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)NSInteger finance_type;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,assign)CGFloat transaction_amount;
@property (nonatomic,copy)NSString *datetime;

// 提现使用
@property (nonatomic,copy)NSString *transaction_id;
@property (nonatomic,assign)NSInteger transaction_status;

@end


@interface CenterBBTMyFinaniceHistoryModel : FetchModel

@property (nonatomic,strong)CenterBBTMyFinaniceRewardModel *reward;
@property (nonatomic,strong)NSArray *type_list;
@property (nonatomic,strong)NSArray <CenterBBTMyFinaniceSubHistoryModel> * history_list;
@end




NS_ASSUME_NONNULL_END
