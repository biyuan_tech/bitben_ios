//
//  CenterBBTDetailTableViewHeaderView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTDetailTableViewHeaderView.h"

static char actionClickWithLeftChooseTimeKey;
static char actionClickWithRightChooseTypeKey;
@interface CenterBBTDetailTableViewHeaderView()
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)UIButton *leftButton;

@end

@implementation CenterBBTDetailTableViewHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.timeLabel = [GWViewTool createLabelFont:@"14" textColor:@"393939"];
    [self addSubview:self.timeLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.arrowImgView];
    
    self.infoLabel = [GWViewTool createLabelFont:@"12" textColor:@"999999"];
    [self addSubview:self.infoLabel];
    
    self.transferTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.transferTypeButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - 100, 0, 100, [CenterBBTDetailTableViewHeaderView calculationViewHeight]);
    [self.transferTypeButton setTitle:@"全部交易类型" forState:UIControlStateNormal];
    [self.transferTypeButton setImage:[UIImage imageNamed:@"icon_center_bbt_detail_drop"] forState:UIControlStateNormal];
    [self.transferTypeButton setTitleColor:[UIColor colorWithCustomerName:@"393939"] forState:UIControlStateNormal];
    self.transferTypeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    [self.transferTypeButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleRight imageTitleSpace:LCFloat(3)];
    __weak typeof(self)weakSelf = self;
    [self.transferTypeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithRightChooseTypeKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.transferTypeButton];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSel = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSel, &actionClickWithLeftChooseTimeKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.leftButton];
    
}

-(void)setTransferHistoryModel:(CenterBBTMyFinaniceHistoryModel *)transferHistoryModel{
    _transferHistoryModel = transferHistoryModel;
    
    
    self.timeLabel.text = [NSDate getTimeWithStringWithYearMoon:transferHistoryModel.reward.time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(LCFloat(16), LCFloat(14), timeSize.width, timeSize.height);
    
    self.arrowImgView.image = [UIImage imageNamed:@"icon_center_bbt_detail_downarrow"];
    self.arrowImgView.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + LCFloat(3), 0, LCFloat(12), LCFloat(6));
    self.arrowImgView.center_y = self.timeLabel.center_y;
    
    self.infoLabel.text = [NSString stringWithFormat:@"支出 %.2f 收入 %.2f",transferHistoryModel.reward.sub_reward,transferHistoryModel.reward.add_reward];
    CGSize infoSize = [Tool makeSizeWithLabel:self.infoLabel];
    self.infoLabel.frame = CGRectMake(self.timeLabel.orgin_x, CGRectGetMaxY(self.timeLabel.frame) + LCFloat(8), infoSize.width, infoSize.height);
    
    self.leftButton.frame = CGRectMake(0, 0, CGRectGetMaxX(self.infoLabel.frame), self.size_height);
}

-(void)actionClickWithLeftChooseTime:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLeftChooseTimeKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithRightChooseType:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithRightChooseTypeKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)changeInfoType:(NSString *)infoType{
    [self.transferTypeButton setTitle:infoType forState:UIControlStateNormal];
    [self.transferTypeButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleRight imageTitleSpace:LCFloat(3)];
}

-(void)changeTime:(NSString *)time{
    self.timeLabel.text = time;
}

+(CGFloat)calculationViewHeight{
    return LCFloat(60);
}

@end
