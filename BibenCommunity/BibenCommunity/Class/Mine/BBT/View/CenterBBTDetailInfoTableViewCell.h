//
//  CenterBBTDetailInfoTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterBBTMyFinaniceHistoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTDetailInfoTableViewCell : PDBaseTableViewCell


@property (nonatomic,assign)BOOL hasHiddenStatus;
@property (nonatomic,strong)CenterBBTMyFinaniceSubHistoryModel *transferHistorySingleModel;

@end

NS_ASSUME_NONNULL_END
