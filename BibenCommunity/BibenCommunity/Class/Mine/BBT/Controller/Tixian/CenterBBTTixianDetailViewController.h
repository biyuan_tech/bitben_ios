//
//  CenterBBTTixianDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Assets.h"
NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTTixianDetailViewController : AbstractViewController

@property (nonatomic,strong)CenterBBTMyFinaniceSubHistoryModel *transferModel;

@end

NS_ASSUME_NONNULL_END
