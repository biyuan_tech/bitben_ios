//
//  CenterBBTTixianModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTTixianModel : FetchModel

@property (nonatomic,assign)CGFloat balance;                        /**< 余额*/
@property (nonatomic,copy)NSString *block_address;                  /**< 钱包地址*/
@property (nonatomic,copy)NSString *cash_limit;                     /**< 最低提现数量*/
@property (nonatomic,copy)NSString *cash_service_charge;            /**< 手续费*/
@property (nonatomic,assign)BOOL user_cer_status;                   /**< 是否已实名认证*/
@property (nonatomic,assign)BOOL wallet_status;                     /**< 是否有钱包*/

@property (nonatomic,copy)NSString *hashs;                            /**< 哈希*/
@property (nonatomic,assign)BOOL password_status;                    /**< 密码是否成功*/
@property (nonatomic,assign)NSInteger count;                    /**< 密码次数*/
@end

NS_ASSUME_NONNULL_END
