//
//  CenterBBTHistorySingleModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/8.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@protocol CenterBBTHistorySingleModel <NSObject>

@end

@interface CenterBBTHistorySingleModel : FetchModel

@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)CGFloat coin;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,assign)CGFloat transaction_amount;

@end


@interface CenterBBTHistoryListModel : FetchModel

@property (nonatomic,strong)NSArray<CenterBBTHistorySingleModel> *content;

@end
