
//
//  CenterBBTTransferViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTTransferViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "NetworkAdapter+Assets.h"
#import "CenterBBTDetailedViewController.h"

static char actionTransferManagerWithBlockKey;
@interface CenterBBTTransferViewController()<UITableViewDelegate,UITableViewDataSource>{
    WalletTransferInputTableViewCell *inputCell;
}
@property (nonatomic,strong)UITableView *transferTableView;
@property (nonatomic,strong)NSArray *transferArr;
@property (nonatomic,strong)UILabel *sloganLabel;

@end

@implementation CenterBBTTransferViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createSloganLabel];
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferPageType == CenterBBTTransferViewControllerTypeToBP){
        self.barMainTitle = @"转成BP";
    } else if (self.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
        self.barMainTitle = @"转成BBT";
    }
    
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"记录" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTDetailedViewController *bbtDetailViewController = [[CenterBBTDetailedViewController alloc]init];
        if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBP){
            bbtDetailViewController.transferFinaniceType = BBTMyFinaniceTypeBpYue;
            bbtDetailViewController.transferType = BBTMyFinaniceTransferTypeToBP;
        } else if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
            bbtDetailViewController.transferFinaniceType = BBTMyFinaniceTypeBBTYue;
            bbtDetailViewController.transferType = BBTMyFinaniceTransferTypeToBBT;
        }

        [strongSelf.navigationController pushViewController:bbtDetailViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.transferArr = @[@[@"转账内容"],@[@"立即转换"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.transferTableView){
        self.transferTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.transferTableView.dataSource = self;
        self.transferTableView.delegate = self;
        [self.view addSubview:self.transferTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.transferArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.transferArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"立即转换";
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf transferManager];
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            
        }
        inputCell = cellWithRowOne;
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeTransferBBT;
        if (self.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
            cellWithRowOne.transferYue = self.transferFinaniceModel.fund;
            cellWithRowOne.titleLabel.text = @"转账成BBT数量";
        } else if (self.transferPageType == CenterBBTTransferViewControllerTypeToBP){
            cellWithRowOne.transferYue = self.transferFinaniceModel.coin;
            cellWithRowOne.titleLabel.text = @"转账成BP数量";
        }
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne transferButtonActionClickBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
                cellWithRowOne.inputTextField.text = [NSString stringWithFormat:@"%.2f",strongSelf.transferFinaniceModel.fund];
            } else if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBP){
                cellWithRowOne.inputTextField.text = [NSString stringWithFormat:@"%.2f",strongSelf.transferFinaniceModel.coin];
            }
        }];
        [cellWithRowOne textFieldTextEndBlock:^{
            if (!weakSelf){
                return ;
            }
            BOOL isSuccess = [Tool validateFloatNumber:cellWithRowOne.inputTextField.text];
            if (!isSuccess){
                [[UIAlertView alertViewWithTitle:@"提示" message:@"您输入的提现金额有误，请核对后输入" buttonTitles:@[@"确定"] callBlock:NULL]show];
                return ;
            }
        }];
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [WalletTransferInputTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1){
        return (kScreenBounds.size.height - [WalletTransferInputTableViewCell calculationCellHeight] - [BYTabbarViewController sharedController].navBarHeight - LCFloat(170) - LCFloat(50));
    } else {
        return LCFloat(50);
    }
}

#pragma mark - createWalletTitle
-(void)createSloganLabel{
    self.sloganLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.sloganLabel.textAlignment = NSTextAlignmentCenter;
    self.sloganLabel.font = [self.sloganLabel.font boldFont];
    self.sloganLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(53) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, LCFloat(53));
    if (IS_iPhoneX){
        self.sloganLabel.orgin_y -= 34;
    }
    self.sloganLabel.text = @"Bitben Wallet";
    [self.view addSubview:self.sloganLabel];
}

#pragma mark - 进行转账
-(void)transferManager{
    __weak typeof(self)weakSelf = self;
    NSString *typeStr = @"";
    if (self.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
        typeStr = @"3-1";
    } else if (self.transferPageType == CenterBBTTransferViewControllerTypeToBP){
        typeStr = @"1-3";
    }
    [[NetworkAdapter sharedAdapter] assetsTransferManagerWithConvertType:typeStr coin:inputCell.inputTextField.text block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"转换成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBBT){
                strongSelf.transferFinaniceModel.coin += [strongSelf->inputCell.inputTextField.text floatValue];
                strongSelf.transferFinaniceModel.fund -= [strongSelf->inputCell.inputTextField.text floatValue];
            } else if (strongSelf.transferPageType == CenterBBTTransferViewControllerTypeToBP){
                strongSelf.transferFinaniceModel.coin -= [strongSelf->inputCell.inputTextField.text floatValue];
                strongSelf.transferFinaniceModel.fund += [strongSelf->inputCell.inputTextField.text floatValue];
            }
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionTransferManagerWithBlockKey);
            if (block){
                block();
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }]show];
    }];
}


-(void)actionTransferManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionTransferManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
