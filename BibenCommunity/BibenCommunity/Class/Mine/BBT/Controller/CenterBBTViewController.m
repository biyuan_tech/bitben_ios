//
//  CenterBBTViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTViewController.h"
#import "CenterBBTSingleCell.h"
#import "CenterBBTHeaderView.h"
#import "NetworkAdapter+Center.h"
#import "CenterBBTHistorySingleModel.h"
#import "CenterBBTDetailedViewController.h"
#import "CenterBBTQuestionViewController.h"
#import "NetworkAdapter+Assets.h"
#import "CenterBBTTransferViewController.h"
#import "CenterBBTTixianRootViewController.h"

@interface CenterBBTViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CenterBBTMyFinaniceModel * _Nonnull bbtMyFinaniceModel;
}
@property (nonatomic,strong)CenterBBTHeaderView *headerView;
@property (nonatomic,strong)NSMutableArray *bbtRootMutableArr;
@property (nonatomic,strong)UITableView *bbtTableView;

@property (nonatomic,strong)UIButton *chongzhiButton;
@property (nonatomic,strong)UIButton *transferBBTButton;

@end

@implementation CenterBBTViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(243, 245, 249, 1);
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetFinanceManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.transferControllerType = CenterBBTViewControllerTypeBBT;
    self.headerView = [[CenterBBTHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [CenterBBTHeaderView calculationHeaderViewWithType:CenterBBTViewControllerTypeBBT])];
    self.headerView.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    [self.headerView actionBackButtonClick:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.headerView segmentListActionClickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (index == 0){
            strongSelf.transferControllerType = CenterBBTViewControllerTypeBBT;
            [strongSelf.transferBBTButton setTitle:@"转BP" forState:UIControlStateNormal];
            [strongSelf.chongzhiButton setTitle:@"提现" forState:UIControlStateNormal];
        } else if (index == 1){
            strongSelf.transferControllerType = CenterBBTViewControllerTypeBP;
            [strongSelf.transferBBTButton setTitle:@"转BBT" forState:UIControlStateNormal];
            [strongSelf.chongzhiButton setTitle:@"充值" forState:UIControlStateNormal];
        }
        strongSelf.headerView.transferControllerType = strongSelf.transferControllerType;
        [strongSelf.bbtTableView reloadData];
    }];

    // 点击问题按钮
    [self.headerView actionClickWithQuestionManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:@"提示" message:@"敬请期待" buttonTitles:@[@"确定"] callBlock:NULL]show];
        return;
//        CenterBBTQuestionViewController *questionVC = [[CenterBBTQuestionViewController alloc]init];
//        [strongSelf.navigationController pushViewController:questionVC animated:YES];
    }];
    // 点击跳转详情
    [self.headerView actionClickWithDetailManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTDetailedViewController *bbtDetailVC = [[CenterBBTDetailedViewController alloc]init];
        if (strongSelf.transferControllerType == CenterBBTViewControllerTypeBP){
            bbtDetailVC.transferFinaniceType = BBTMyFinaniceTypeBpYue;
        } else if (strongSelf.transferControllerType == CenterBBTViewControllerTypeBBT){
            bbtDetailVC.transferFinaniceType = BBTMyFinaniceTypeBBTYue;
        }
        [strongSelf.navigationController pushViewController:bbtDetailVC animated:YES];
    }];
    
    [self.headerView actionSwapGestureManagerWithBlock:^(BOOL hasLeft) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasLeft == YES){
            strongSelf.transferControllerType = CenterBBTViewControllerTypeBBT;
            [strongSelf.transferBBTButton setTitle:@"转BP" forState:UIControlStateNormal];
              [strongSelf.chongzhiButton setTitle:@"提现" forState:UIControlStateNormal];
        } else if (hasLeft == NO){
            strongSelf.transferControllerType = CenterBBTViewControllerTypeBP;
            [strongSelf.transferBBTButton setTitle:@"转BBT" forState:UIControlStateNormal];
              [strongSelf.chongzhiButton setTitle:@"充值" forState:UIControlStateNormal];
        }
        strongSelf.headerView.transferControllerType = strongSelf.transferControllerType;
        [strongSelf.bbtTableView reloadData];
    }];
    
    [self.view addSubview:self.headerView];
    
    
    // 创建底部的按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chongzhiButton.backgroundColor = [UIColor whiteColor];
    [self.chongzhiButton setTitle:@"提现" forState:UIControlStateNormal];
    [self.chongzhiButton setTitleColor:[UIColor colorWithCustomerName:@"橙"] forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"18"];
    CGFloat btn_height = 0;
    btn_height = LCFloat(50);
    [self.chongzhiButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if([strongSelf.chongzhiButton.titleLabel.text isEqualToString:@"提现"]){
            CenterBBTTixianRootViewController *tixianViewController = [[CenterBBTTixianRootViewController alloc]init];
            [strongSelf.navigationController pushViewController:tixianViewController animated:YES];
        } else {
            [[UIAlertView alertViewWithTitle:@"提示" message:@"暂未开通，敬请期待" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
    self.chongzhiButton.frame = CGRectMake(0, self.view.size_height - btn_height, kScreenBounds.size.width / 2., btn_height);
    [self.view addSubview:self.chongzhiButton];
    
    self.transferBBTButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.transferBBTButton.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    self.transferBBTButton.frame = CGRectMake(CGRectGetMaxX(self.chongzhiButton.frame), self.chongzhiButton.orgin_y, self.chongzhiButton.size_width, self.chongzhiButton.size_height);
    [self.transferBBTButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.transferBBTButton setTitle:@"转BP" forState:UIControlStateNormal];
    self.transferBBTButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"18"];
    [self.transferBBTButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTTransferViewController *bbtTransferVC = [[CenterBBTTransferViewController alloc]init];
        [bbtTransferVC actionTransferManagerWithBlock:^{
            strongSelf.headerView.transferBBTMyFinaniceModel = strongSelf -> bbtMyFinaniceModel;
            [strongSelf.headerView actionClickManagerWithChange];
            [strongSelf.bbtTableView reloadData];
        }];
        if ([strongSelf.transferBBTButton.titleLabel.text isEqualToString:@"转BP"]){
            bbtTransferVC.transferPageType = CenterBBTTransferViewControllerTypeToBP;
        } else if ([strongSelf.transferBBTButton.titleLabel.text isEqualToString:@"转BBT"]){
            bbtTransferVC.transferPageType = CenterBBTTransferViewControllerTypeToBBT;
        }
        bbtTransferVC.transferFinaniceModel = strongSelf->bbtMyFinaniceModel;
        [strongSelf.navigationController pushViewController:bbtTransferVC animated:YES];
    }];
    [self.view addSubview:self.transferBBTButton];
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.bbtRootMutableArr = [NSMutableArray array];
    [self.bbtRootMutableArr addObject:@[@"锁定",@"已解锁"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bbtTableView){
        self.bbtTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0,CGRectGetMaxY(self.headerView.frame),kScreenBounds.size.width,self.chongzhiButton.orgin_y - CGRectGetMaxY(self.headerView.frame))];
        self.bbtTableView.dataSource = self;
        self.bbtTableView.delegate = self;
        [self.view addSubview:self.bbtTableView];
    }
//    __weak typeof(self)weakSelf = self;
//    [self.bbtTableView appendingPullToRefreshHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
////        [strongSelf sendRequestTogetHistoryWithReload:NO];
//    }];
//
//    [self.bbtTableView appendingFiniteScrollingPullToRefreshHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
////        [strongSelf sendRequestTogetHistoryWithReload:NO];
//    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.bbtRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.bbtRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
   
    static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
    GWNormalTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
    if (!cellWithRowZero){
        cellWithRowZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
    }
    cellWithRowZero.transferCellHeight = cellHeight;
    cellWithRowZero.transferTitle = [[self.bbtRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cellWithRowZero.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    cellWithRowZero.transferHasArrow = YES;
    if (self.transferControllerType == CenterBBTViewControllerTypeBBT){
        if (indexPath.row == 0){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.lock_coin];
        } else {
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.unlock_coin];
        }
    } else if (self.transferControllerType == CenterBBTViewControllerTypeBP){
        if (indexPath.row == 0){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.lock_fund];
        } else {
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.unlock_fund];
        }
    }
    return cellWithRowZero;
   
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
    if (self.transferControllerType == CenterBBTViewControllerTypeBBT){
        if (indexPath.row == 0){
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBBTSuoding;
        } else {
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBBTJiesuo;
        }
    } else if (self.transferControllerType == CenterBBTViewControllerTypeBP){
        if (indexPath.row == 0){
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        } else {
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpJiesuo;
        }
    }
    [self.navigationController pushViewController:bbtVC animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.bbtTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.bbtRootMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.bbtRootMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return LCFloat(58);
    } else {
        return [CenterBBTSingleCell calculationCellHeight];
    }
}

#pragma mark - sendRequest

#pragma mark - 获取奖励金额
-(void)sendRequestToGetFinanceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] assetsGetMyFinanceInfoManagerWithBlcok:^(CenterBBTMyFinaniceModel * _Nonnull bbtMyFinaniceModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->bbtMyFinaniceModel = bbtMyFinaniceModel;
        strongSelf.headerView.transferBBTMyFinaniceModel = bbtMyFinaniceModel;
        strongSelf.headerView.transferControllerType = CenterBBTViewControllerTypeBBT;
        [strongSelf.bbtTableView reloadData];
    }];
}

@end
