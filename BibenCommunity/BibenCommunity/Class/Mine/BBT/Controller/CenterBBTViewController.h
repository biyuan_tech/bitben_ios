//
//  CenterBBTViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,CenterBBTViewControllerType) {
    CenterBBTViewControllerTypeDiamonds = 0,            /**< 钻石*/
    CenterBBTViewControllerTypeBBT = 1,                 /**< BBT*/
    CenterBBTViewControllerTypeBL = 2,                  /**< 币力*/
    CenterBBTViewControllerTypeBP = 3,                  /**< BP*/
};

@interface CenterBBTViewController : AbstractViewController

@property (nonatomic,assign)CenterBBTViewControllerType transferControllerType;

@end
