//
//  CenterBBTShowViewViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger ,CenterBBTShowViewViewControllerType) {
    CenterBBTShowViewViewControllerTypeItems = 0,
    CenterBBTShowViewViewControllerTypeTimes = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTShowViewViewController : AbstractViewController

@property (nonatomic,strong)UIImage *showImgBackgroundImage;                // 背景图片
@property (nonatomic,copy)NSString *transferSelectedInfo;
@property (nonatomic,assign)CenterBBTShowViewViewControllerType tranferType;
@property (nonatomic,strong)NSArray *transferItemsArr;

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
-(void)sheetViewDismiss;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果
-(void)actionClickWithChooseItemsBlock:(void(^)(NSString *chooseItems))block;           // 选择对象
-(void)actionClickWithChooseTimeBlock:(void(^)(NSTimeInterval beginTime,NSTimeInterval endTime))block;
@end

NS_ASSUME_NONNULL_END
