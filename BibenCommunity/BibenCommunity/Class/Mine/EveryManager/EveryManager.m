//
//  EveryManager.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/3.
//  Copyright © 2019 币本. All rights reserved.
//

#import "EveryManager.h"
#import "JCAlertView.h"


@interface EveryManager()
@property (nonatomic,strong)JCAlertView *alert;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *cancelButton;
@end

@implementation EveryManager

-(void)showEveryManagerWithTitle:(NSString *)title{
    UIView *iconImgView = [self createViewWithTitle:title actionBlock:^{
        if (self.alert){
            [self.alert dismissWithCompletion:NULL];
        }
    }];
    if (!self.alert){
        self.alert = [[JCAlertView alloc]initWithCustomView:iconImgView dismissWhenTouchedBackground:YES];
    }
    
    [self.alert show];
}

-(UIView *)createViewWithTitle:(NSString *)title actionBlock:(void(^)())block{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, LCFloat(311), LCFloat(290) + LCFloat(44) + LCFloat(30));
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.image = [UIImage imageNamed:@"icon_every_bg"];
    self.iconImgView.frame = CGRectMake(0, 0, LCFloat(311), LCFloat(290));
    [self.bgView addSubview:self.iconImgView];
    self.iconImgView.userInteractionEnabled = NO;
    
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, LCFloat(185), self.iconImgView.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.text = title;
    [self.iconImgView addSubview:self.titleLabel];
    self.titleLabel.userInteractionEnabled = NO;
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    [self.cancelButton setImage:[UIImage imageNamed:@"icon_every_cancel"] forState:UIControlStateNormal];
    self.cancelButton.frame = CGRectMake((LCFloat(311) - LCFloat(30)) / 2., CGRectGetMaxY(self.iconImgView.frame) + LCFloat(44), LCFloat(30), LCFloat(30));
    [self.bgView addSubview:self.cancelButton];
    [self.cancelButton buttonWithBlock:^(UIButton *button) {
        if (block){
            block();
        }
    }];
    self.cancelButton.userInteractionEnabled = YES;
    
    return self.bgView;
}

@end
