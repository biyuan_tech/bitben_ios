//
//  TaskSingleInfoModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"
#import "TaskSingleTitleTableViewCell.h"
@class  TaskSingleTitleTableViewCell;
NS_ASSUME_NONNULL_BEGIN

@protocol TaskSingleInfoModel <NSObject>

@end

@interface TaskSingleInfoModel : FetchModel

@property (nonatomic,copy)NSString *task_name;
@property (nonatomic,copy)NSString *date;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,assign)NSInteger task_current_time;
@property (nonatomic,assign)NSInteger current_task_status;  /**< */
@property (nonatomic,assign)NSInteger task_max_time;
@property (nonatomic,assign)NSInteger current_fund_status;
@property (nonatomic,assign)NSInteger task_type;
@property (nonatomic,assign)NSInteger task_fund;
@property (nonatomic,copy)NSString *user_task_id;

@end

@protocol TaskSingleInfoListModel <NSObject>

@end

@interface TaskSingleInfoListModel : FetchModel

@property (nonatomic,assign)NSInteger taskType;
@property (nonatomic,strong)NSArray<TaskSingleInfoModel> *taskList;
@property (nonatomic,copy)NSString *taskTypeName;
@property (nonatomic,strong)TaskSingleTitleTableViewCellModel *model;

@end

@interface TaskSingleInfoListRootModel : FetchModel

@property (nonatomic,strong)NSArray<TaskSingleInfoListModel> *total;

@end

NS_ASSUME_NONNULL_END
