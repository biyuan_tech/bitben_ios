//
//  TaskHeaderInfoModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"
#import "BYLiveDefine.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TaskHeaderInfoModelBanner <NSObject>


@end

@interface TaskHeaderInfoModelBanner : FetchModel
/** 图片地址 */
@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString *banner_id;
@property (nonatomic ,copy) NSString *sort_id;
/** 类型 */
@property (nonatomic ,assign) BY_THEME_TYPE theme_type;
/** 直播类型 */
@property (nonatomic ,assign) BY_LIVE_TYPE live_type;
/** 直播、文章id */
@property (nonatomic ,copy) NSString *theme_id;

@end


@interface TaskHeaderInfoModel : FetchModel

@property (nonatomic,strong)NSArray<TaskHeaderInfoModelBanner> *banner;
@property (nonatomic,copy)NSString *fund;
@property (nonatomic,copy)NSString *prestige;
@property (nonatomic,copy)NSString *article_id;

@end

NS_ASSUME_NONNULL_END
