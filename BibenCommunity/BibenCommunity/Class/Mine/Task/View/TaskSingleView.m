//
//  TaskSingleView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "TaskSingleView.h"

@interface TaskSingleView()
@property (nonatomic,strong)PDImageView *ablumView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@end

@implementation TaskSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.ablumView = [[PDImageView alloc]init];
    self.ablumView.backgroundColor = [UIColor clearColor];
    self.ablumView.frame = CGRectMake(0, 0, LCFloat(181), LCFloat(55));
    self.ablumView.image = [UIImage imageNamed:@"bg_task_view"];
    [self addSubview:self.ablumView];
    
    // label
    self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"3B3B3B"];
    [self addSubview:self.titleLabel];
    self.titleLabel.text = @"领取成功";
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.ablumView.frame) + LCFloat(18), self.ablumView.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"ED4A45"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(15), self.titleLabel.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
}

-(void)setTransferBpStr:(NSString *)transferBpStr{
    _transferBpStr = transferBpStr;
    self.dymicLabel.text = transferBpStr;
}
@end
