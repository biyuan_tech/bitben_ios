//
//  TaskSIngleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "TaskSingleInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskSIngleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)TaskSingleInfoModel *transferModel;

-(void)actionClickWithBlockModel:(void(^)(TaskSingleInfoModel *transferModel))block;

@end

NS_ASSUME_NONNULL_END
