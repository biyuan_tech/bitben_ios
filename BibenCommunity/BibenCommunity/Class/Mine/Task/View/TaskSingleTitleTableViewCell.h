//
//  TaskSingleTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskSingleTitleTableViewCellModel : FetchModel
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *subTitle;

@end

@interface TaskSingleTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)TaskSingleTitleTableViewCellModel *transferModel;

+(CGFloat)calculationCellHeightModel:(TaskSingleTitleTableViewCellModel *)model;

@end

NS_ASSUME_NONNULL_END
