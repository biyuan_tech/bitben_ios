//
//  TaskSingleView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaskSingleView : UIView

@property (nonatomic,copy)NSString *transferBpStr;

@end

NS_ASSUME_NONNULL_END
