//
//  TaskSingleTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "TaskSingleTitleTableViewCell.h"

@interface TaskSingleTitleTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *subLabel;
@end

@implementation TaskSingleTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.image = [UIImage imageNamed:@"icon_task_title"];
    self.iconImgView.frame = CGRectMake(LCFloat(15), LCFloat(20), LCFloat(15), LCFloat(15));
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"ED4A45"];
    [self addSubview:self.titleLabel];
    
    self.subLabel = [GWViewTool createLabelFont:@"11" textColor:@"BBBBBB"];
    [self addSubview:self.subLabel];
}

-(void)setTransferModel:(TaskSingleTitleTableViewCellModel *)transferModel{
    _transferModel = transferModel;
    
    // title
    self.titleLabel.text = transferModel.title;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(10), 0, titleSize.width, titleSize.height);
    self.titleLabel.center_y = self.iconImgView.center_y;
    
    self.subLabel.text = transferModel.subTitle;
    self.subLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.iconImgView.frame) + LCFloat(8), kScreenBounds.size.width - 2 * LCFloat(15), [NSString contentofHeightWithFont:self.subLabel.font]);
}

+(CGFloat)calculationCellHeightModel:(TaskSingleTitleTableViewCellModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(19);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(18);
    if (model.subTitle.length){
        cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"11"]];
        cellHeight += LCFloat(3);
    }
    return cellHeight;
}

@end

@implementation TaskSingleTitleTableViewCellModel
@end

