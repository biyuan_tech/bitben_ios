//
//  CenterLinkMeModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterLinkMeModel : FetchModel

@property (nonatomic,copy)NSString *business_email;
@property (nonatomic,copy)NSString *business_wechat;
@property (nonatomic,copy)NSString *community_wechat;
@property (nonatomic,copy)NSString *official_website;
@property (nonatomic,copy)NSString *service_wechat;
@end

NS_ASSUME_NONNULL_END
