//
//  CenterLinkSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterLinkSingleTableViewCell.h"

@interface CenterLinkSingleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *copButton;

@end

@implementation CenterLinkSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - creaeView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"4D4D4D"];
    
    [self addSubview:self.titleLabel];
    
    self.copButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.copButton.backgroundColor = [UIColor clearColor];
    [self.copButton setTitle:@"复制" forState:UIControlStateNormal];
    self.copButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"11"];
    self.copButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(24) - LCFloat(49),([CenterLinkSingleTableViewCell calculationCellHeight] - LCFloat(22)) / 2., LCFloat(49), LCFloat(22));
    [self.copButton setTitleColor:[UIColor colorWithCustomerName:@"ED4A45"] forState:UIControlStateNormal];
    self.copButton.layer.cornerRadius = LCFloat(3);
    self.copButton.layer.borderWidth = 1.f;
    self.copButton.layer.borderColor = [UIColor colorWithCustomerName:@"ED4A45"].CGColor;
    [self addSubview:self.copButton];
    
    self.titleLabel.frame = CGRectMake(LCFloat(26), ([CenterLinkSingleTableViewCell calculationCellHeight] - [NSString contentofHeightWithFont:self.titleLabel.font]) / 2., self.copButton.orgin_x - LCFloat(26) * 2, [NSString contentofHeightWithFont:self.titleLabel.font]);
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    __weak typeof(self)weakSelf = self;
    [self.copButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool copyWithString:strongSelf.transferCopyInfo callback:^{
            [StatusBarManager statusBarHidenWithText:@"复制成功"];
        }];
    }];
}

-(void)setTransferCopyInfo:(NSString *)transferCopyInfo{
    _transferCopyInfo = transferCopyInfo;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}

@end
