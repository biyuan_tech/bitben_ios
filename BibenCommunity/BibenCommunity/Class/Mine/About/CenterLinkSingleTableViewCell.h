//
//  CenterLinkSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterLinkSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,copy)NSString *transferCopyInfo;

@end

NS_ASSUME_NONNULL_END
