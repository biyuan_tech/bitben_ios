//
//  CenterAboutViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/5.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterAboutViewController.h"
#import "AboutTopTableViewCell.h"
#import "DelegateOneCell.h"
#import "EwenCopyLabel.h"
#import "NetworkAdapter.h"
#import "EveryManager.h"
#import "CenterAboutLineTitleTableViewCell.h"

@interface CenterAboutViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *aboutTableView;
@property (nonatomic,strong)NSMutableArray *sectionArr;
@end

@implementation CenterAboutViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self smart];
}




#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"关于币本";
//    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
    //EveryManager *every = [[EveryManager alloc]init];
//    [every showEveryManagerWithTitle:@"+1 BBT"];
//    }];
}

-(void)sendNotice{
    NSDictionary *params = @{@"publish_user_id":@"689403384",@"receive_user_id":@"436889512",@"title":@"站内信测试003",@"content":@"测试003-type401-站内信内容",@"type_code":@"401",@"theme_type":@"1",@"theme_id":@"507191034808107008",@"picture":@"web-20181031135535614-f2627753-578a-42e8-adce-1633b8166b3d.png"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"back_create_message" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        
    }];
}

#pragma mark - createView



#pragma makr - arrayWithInit
-(void)arrayWithInit{
    self.sectionArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.aboutTableView){
        self.aboutTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.aboutTableView.dataSource = self;
        self.aboutTableView.delegate = self;
        [self.view addSubview:self.aboutTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.sectionArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        AboutTopTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[AboutTopTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellStyleDefault;
            cellWithRowZero.backgroundColor = [UIColor clearColor];
        }
        return cellWithRowZero;
    } else {
        NSString *info = [[self.sectionArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        if ([info hasPrefix:@"$"] ||[info hasPrefix:@"#"]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            DelegateOneCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[DelegateOneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellStyleDefault;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferInfo = info;
            return cellWithRowOne;
        } else if ([info hasPrefix:@"!"]){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            CenterAboutLineTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowOne){
                cellWithRowOne = [[CenterAboutLineTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowOne.selectionStyle = UITableViewCellStyleDefault;
            }
            cellWithRowOne.backgroundColor = [UIColor whiteColor];
            cellWithRowOne.transferTitle = info;
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = info;
            cellWithRowTwo.titleLabel.textColor = [UIColor hexChangeFloat:@"464646"];
            if ([info isEqualToString:@"一、币本简介"]){
                cellWithRowTwo.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"16"]boldFont];
                cellWithRowTwo.titleLabel.size_width = kScreenBounds.size.width - 2 * LCFloat(11);
            } else {
                cellWithRowTwo.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
                cellWithRowTwo.titleLabel.size_width = kScreenBounds.size.width - 2 * LCFloat(11);
            }
            return cellWithRowTwo;
        }
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [ AboutTopTableViewCell calculationCellHeight];
    } else {
        NSString *info = [[self.sectionArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([info hasPrefix:@"$"] ||[info hasPrefix:@"#"]){
            return [DelegateOneCell calculationCellHeightWithInfo:info];
        } else if ([info hasPrefix:@"!"]){
            return [CenterAboutLineTitleTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];;
        }
    }
}

-(void)smart{
    [self.sectionArr addObject:@[@"logo"]];
    [self.sectionArr addObject:@[@"一、币本简介"]];
    [self.sectionArr addObject:@[@"$区块链价值分享与协作社区"]];
    [self.sectionArr addObject:@[@"!我们的使命"]];
    [self.sectionArr addObject:@[@"$让价值财富通过TOKEN回归到更多个体"]];
    [self.sectionArr addObject:@[@"!价值观"]];
    [self.sectionArr addObject:@[@"$自由、平等、民主、分享、共赢"]];
    [self.sectionArr addObject:@[@"!理念"]];
    [self.sectionArr addObject:@[@"$价值认同+分布式协作+社区自治"]];
    [self.sectionArr addObject:@[@"!服务内容"]];
    [self.sectionArr addObject:@[@"$为区块链投资者发现优质价值信息，提升对区块链领域投资的知识认知，并获得有价值的投资洞见；为优质内容创作者赋能，意见领袖IP化，并能获得合理的回报；为区块链产业项目方搭建起连接用户的桥梁，建立审核监督共赢机制。"]];
    [self.sectionArr addObject:@[@"!功能板块"]];
    [self.sectionArr addObject:@[@"直播："]];
    [self.sectionArr addObject:@[@"$提供视频直播、语音直播、PPT教学图文直播适用于课程分享、行情分析、峰会沙龙、嘉宾采访等场景。"]];
    [self.sectionArr addObject:@[@"文章："]];
    [self.sectionArr addObject:@[@"$致力于汇聚全网最有价值的区块链资讯和观点，促使“内容贡献”与“收益获得”相匹配，实现激励相容，让价值财富回归创作个体。"]];
    [self.sectionArr addObject:@[@"快讯"]];
    [self.sectionArr addObject:@[@"$7*24小时收集全球区块链的热点信息，通过机器和社区成员的内容核验确保其准确客观中立，实现“全球闪电快讯”。"]];
    [self.sectionArr addObject:@[@"行情"]];
    [self.sectionArr addObject:@[@"$收录全球1933个币种、全球235个交易平台，实时数据获取跟踪。"]];
    
    
    if ([AccountModel sharedAccountModel].isShenhe){
        [self.sectionArr removeAllObjects];
        
        [self.sectionArr addObject:@[@"logo"]];
        [self.sectionArr addObject:@[@"产品详细介绍"]];
        [self.sectionArr addObject:@[@"$终身梦想，执着谋进。"]];
        [self.sectionArr addObject:@[@"我们的使命"]];
        [self.sectionArr addObject:@[@"$社区服务发展模式的实践、探索和研究，在连锁体系投资建设、智慧养老、标准化等方面得到快速发展。"]];
        [self.sectionArr addObject:@[@"价值观"]];
        [self.sectionArr addObject:@[@"$自由、平等、民主、分享、共赢"]];
        [self.sectionArr addObject:@[@"理念"]];
        [self.sectionArr addObject:@[@"$互联网源，共创众赢，诚信为本，细节至上，社会关注，坚守责任，初心不变，亦商亦工"]];
    }
    
    [self.aboutTableView reloadData];
}


@end
