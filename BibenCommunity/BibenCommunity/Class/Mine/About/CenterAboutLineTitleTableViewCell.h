//
//  CenterAboutLineTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/9.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterAboutLineTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

@end

NS_ASSUME_NONNULL_END
