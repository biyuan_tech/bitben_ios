//
//  CenterInformationLikeTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "AccountModel.h"

@interface CenterInformationLikeItemsView : UIView
-(instancetype)initWithFrame:(CGRect)frame model:(LoginServerModelAccountTopic *)model actionBlock:(void(^)())block;
@end


@interface CenterInformationLikeTableViewCell : PDBaseTableViewCell

-(void)actionClickInformationWithSingleItem:(void(^)(LoginServerModelAccountTopic *singleModel))block;


@property (nonatomic,strong)NSArray<LoginServerModelAccountTopic> *transferLikeArr;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)likeArr;

@end


