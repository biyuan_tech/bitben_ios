//
//  InformationChooseShowController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/11.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,InfomationPageType) {
    InfomationPageTypeDate,
    InfomationPageTypeCity,
};

@interface InformationChooseShowController : UIViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,assign)InfomationPageType transferPageType;
- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)actionCheckToChooseCity:(void(^)(NSString *provice ,NSString *city))block;
-(void)actionCheckToChooseDate:(void(^)(NSString *date))block;


@end
