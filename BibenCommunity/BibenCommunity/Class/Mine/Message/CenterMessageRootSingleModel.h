//
//  CenterMessageRootSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"
#import "CenterMessageLiveSingleModel.h"
#import "PushNotifManager.h"
#import "CenterMessageZanModel.h"
#import "CenterMessageCommentFetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CenterMessageRootSingleModel <NSObject>

@end

@interface CenterMessageRootSingleModel : FetchModel


@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)BOOL is_read;
@property (nonatomic,copy)NSString *message_id;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,assign)PushType type_code;
@property (nonatomic,copy)NSString *type_content;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *user_message_id;
@property (nonatomic,strong) CenterMessageLiveSingleModel *live_remind;         /**< 直播Model*/
@property (nonatomic,strong) CenterMessageZanModel *support_remind;         /**< 直播Model*/
@property (nonatomic,strong) CenterMessageCommentFetchModel *comment_remind;         /**< 直播Model*/

@end


@interface CenterMessageRootListModel:FetchModel
@property (nonatomic,strong)NSArray<CenterMessageRootSingleModel > *content;
@end

NS_ASSUME_NONNULL_END
