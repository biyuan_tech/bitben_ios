//
//  CenterNotifyRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterNotifyRootViewController.h"
#import "CenterMessageNotifyTableViewCell.h"
#import "CenterMessageCommentViewController.h"
#import "CenterMessageLiveViewController.h"
#import "CenterMessageFansViewController.h"
#import "CenterMessageZanViewController.h"
@interface CenterNotifyRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *notifyTableView;
@property (nonatomic,strong)NSArray *notifyArr;
@end

@implementation CenterNotifyRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.notifyArr = @[@[@"评论与回复",@"点赞",@"新粉丝",@"直播提醒"]];
}

#pragma mark - TableView
-(void)createTableView{
    self.notifyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
    self.notifyTableView.dataSource = self;
    self.notifyTableView.delegate = self;
    [self.view addSubview:self.notifyTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.notifyArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.notifyArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageNotifyTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageNotifyTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferTitle = [[self.notifyArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterMessageNotifyTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == [self cellIndexPathRowWithcellData:@"评论与回复" sourceArr:self.notifyArr]){
        CenterMessageCommentViewController *centerMessageCommentVC = [[CenterMessageCommentViewController alloc]init];
        [self.navigationController pushViewController:centerMessageCommentVC animated:YES];
    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"直播提醒" sourceArr:self.notifyArr]){
        CenterMessageLiveViewController *messageLiveVC = [[CenterMessageLiveViewController alloc]init];
        [self.navigationController pushViewController:messageLiveVC animated:YES];
    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"新粉丝" sourceArr:self.notifyArr]){
        CenterMessageFansViewController *fansVC = [[CenterMessageFansViewController alloc]init];
        [self.navigationController pushViewController:fansVC animated:YES];
    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"点赞" sourceArr:self.notifyArr]){
        CenterMessageZanViewController *zanVC = [[CenterMessageZanViewController alloc]init];
        [self.navigationController pushViewController:zanVC animated:YES];
    }
}

@end
