//
//  CenterMessageRootViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger ,CenterMessageRootViewControllerType) {
    CenterMessageRootViewControllerTypeMain,                /**< 主*/
    CenterMessageRootViewControllerTypeBlock               /**< 区块链消息*/
};

@interface CenterMessageRootViewController : AbstractViewController

+(instancetype)sharedController;

@property (nonatomic,assign)CenterMessageRootViewControllerType transferType;

-(void)centerGetMessageInfoWithReload:(BOOL)reload;
@end
