//
//  CenterMessageLiveTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterMessageRootSingleModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageLiveTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterMessageRootSingleModel *transferMessageLiveModel;
+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model;
@end

NS_ASSUME_NONNULL_END
