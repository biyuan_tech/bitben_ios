
//
//  CenterMessageNotifyTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageNotifyTableViewCell.h"

@interface CenterMessageNotifyTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end

@implementation CenterMessageNotifyTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(21), ([CenterMessageNotifyTableViewCell calculationCellHeight] - LCFloat(19)) / 2., LCFloat(19), LCFloat(19));
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"3E3E3E"];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(16), 0, LCFloat(130), [CenterMessageNotifyTableViewCell calculationCellHeight]);
    [self addSubview:self.titleLabel];
    
    self.countLabel = [GWViewTool createLabelFont:@"10" textColor:@"FFFFFF"];
    self.countLabel.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    self.countLabel.clipsToBounds = YES;
    [self addSubview:self.countLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_base_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(5), ([CenterMessageNotifyTableViewCell calculationCellHeight] - LCFloat(9)) / 2., LCFloat(5), LCFloat(9));
    [self addSubview:self.arrowImgView];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    if ([transferTitle isEqualToString:@"评论与回复"]){
        self.iconImgView.image = [UIImage imageNamed:@"icon_msg_pinglun"];
    } else if ([transferTitle isEqualToString:@"点赞"]){
        self.iconImgView.image = [UIImage imageNamed:@"icon_msg_zan"];
    } else if ([transferTitle isEqualToString:@"新粉丝"]){
        self.iconImgView.image = [UIImage imageNamed:@"icon_msg_fensi"];
    } else if ([transferTitle isEqualToString:@"直播提醒"]){
        self.iconImgView.image = [UIImage imageNamed:@"icon_msg_zhibo"];
    }
}

-(void)setTransferNumber:(NSString *)transferNumber{
    _transferNumber = transferNumber;
    
    self.countLabel.text = transferNumber;
    CGSize numberSize = [transferNumber sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    CGSize infoSize = CGSizeMake(numberSize.width + 2 * LCFloat(7), numberSize.height + 2 * LCFloat(7));
    self.countLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(12) - infoSize.width, ([CenterMessageNotifyTableViewCell calculationCellHeight] - infoSize.height) / 2., infoSize.width, infoSize.height);
    self.countLabel.layer.cornerRadius = self.countLabel.size_width / 2.;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(56);
}
@end
