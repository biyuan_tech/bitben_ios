//
//  CenterMessageFansTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageFansTableViewCell.h"

@interface CenterMessageFansTableViewCell()
@property (nonatomic,strong)PDImageView *avatarBgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *linkButton;
@property (nonatomic,strong)UILabel *timeLabel;
@end

static char actionClickWithLinkWithBlockKey;
static char actionClickHeaderImgWithBlockKey;
@implementation CenterMessageFansTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.avatarBgImgView = [[PDImageView alloc]init];
    self.avatarBgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarBgImgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(void) = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"4C4C4C"];
    [self addSubview:self.titleLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"12" textColor:@"757474"];
    [self addSubview:self.dymicLabel];
    
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.linkButton];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
}

-(void)setTransferContentModel:(CenterFollowContentModel *)transferContentModel{
    _transferContentModel = transferContentModel;
    self.avatarImgView.frame = CGRectMake(LCFloat(18), LCFloat(17), LCFloat(37), LCFloat(37));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_width / 2.;
    self.avatarImgView.clipsToBounds = YES;
    [self.avatarImgView uploadImageWithURL:transferContentModel.user.head_img placeholder:nil callback:NULL];

    self.titleLabel.text = transferContentModel.user.nickname.length?transferContentModel.user.nickname:transferContentModel.user_id;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), LCFloat(20), titleSize.width, titleSize.height);
    
    // dymic
    self.dymicLabel.text = @"关注了你";
    self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12), LCFloat(100), [NSString contentofHeightWithFont:self.dymicLabel.font]);
    
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(16) - LCFloat(54), 0, LCFloat(54), LCFloat(26));
    self.linkButton.center_y = [CenterMessageFansTableViewCell calculationCellHeight] / 2.;
    
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(13), 100, [NSString contentofHeightWithFont:self.timeLabel.font]);
    self.timeLabel.text = [NSDate getTimeGap:transferContentModel.create_time / 1000.];
    
    
    if (transferContentModel.isAttention){
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_hlt"] forState:UIControlStateNormal];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL hasLink) = objc_getAssociatedObject(strongSelf, &actionClickWithLinkWithBlockKey);
        if (block){
            BOOL hasLink = transferContentModel.isAttention;
            block(!hasLink);
        }
    }];

}


-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block{
    objc_setAssociatedObject(self, &actionClickWithLinkWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(98);
}

-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferContentModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_hlt"] forState:UIControlStateNormal];
    }
}


@end
