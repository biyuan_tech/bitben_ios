//
//  CenterMessageCommentDetailHeaderTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageCommentDetailHeaderTableViewCell.h"

@interface CenterMessageCommentDetailHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *louzhuLabel;
@property (nonatomic,strong)UIButton *zanButton;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIButton *delButton;

@end

@implementation CenterMessageCommentDetailHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.frame = CGRectMake(LCFloat(18), LCFloat(17), LCFloat(37), LCFloat(37));
    [self addSubview:self.avatarImgView];
    // 2. 名称
    self.nickNameLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.nickNameLabel];
    // 3. 创建楼主
    self.louzhuLabel = [GWViewTool createLabelFont:@"11" textColor:@"5C92D5"];
    [self addSubview:self.louzhuLabel];
    // 4. 创建内容
    self.dymicLabel = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    [self addSubview:self.dymicLabel];
    // 5. 赞
    self.zanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.zanButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.zanButton];
    __weak typeof(self)weakSelf = self;
    [self.zanButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        NSLog(@"123");
    }];
    
    // 6.文章背景
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    [self addSubview:self.bgView];
    
    // 7.图片
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 8.文字
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    [self.bgView addSubview:self.dymicLabel];
    
    // 9. 时间
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    self.delButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.delButton];
    [self.delButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return;
        }
        
    }];
    [self.delButton setTitle:@"删除" forState:UIControlStateNormal];
}


@end
