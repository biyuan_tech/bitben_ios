//
//  CenterMessageCommentDetailSubTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageCommentDetailSubTableViewCell.h"

@interface CenterMessageCommentDetailSubTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickLabel;
@property (nonatomic,strong)UILabel *louzhuLabel;
@property (nonatomic,strong)UILabel *detailLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIButton *replyButton;
@property (nonatomic,strong)UIButton *delButton;
@property (nonatomic,strong)UIButton *zanButton;

@end

@implementation CenterMessageCommentDetailSubTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nickLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.nickLabel];
    
    self.louzhuLabel = [GWViewTool createLabelFont:@"11" textColor:@"5C92D5"];
    [self addSubview:self.louzhuLabel];
    
    self.zanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.zanButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.zanButton];
    
    self.detailLabel = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    [self addSubview:self.detailLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    self.replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.replyButton];
    
    self.delButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.delButton];
}


@end
