//
//  CenterMessageCommentTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageCommentTableViewCell.h"

@interface CenterMessageCommentTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)PDImageView *nImgDotView;
@property (nonatomic,strong)UIView *desBgView;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UIView *dymicBgView;
@property (nonatomic,strong)PDImageView *dymicImgView;
@property (nonatomic,strong)UILabel *dymicInfoLabel;

@end

@implementation CenterMessageCommentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 2. 名称
    self.nickNameLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.nickNameLabel];
    
    // 3. 内容
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    [self addSubview:self.titleLabel];
    
    // 4. 时间
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];

    // 5. 消息
    self.nImgDotView = [[PDImageView alloc]init];
    self.nImgDotView.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.nImgDotView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20), LCFloat(24), LCFloat(5), LCFloat(5));
    [self addSubview:self.nImgDotView];
    
    // 回复背景
    self.desBgView = [[UIView alloc]init];
    self.desBgView.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    [self addSubview:self.desBgView];
    
    // 6. 回复
    self.desLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    self.desLabel.backgroundColor = [UIColor colorWithCustomerName:@"F5F5F5"];
    [self.desBgView addSubview:self.desLabel];
    
    // 7.内容背景
    self.dymicBgView = [[UIView alloc]init];
    self.dymicBgView.backgroundColor = [UIColor colorWithCustomerName:@"F5F5F5"];
    [self addSubview:self.dymicBgView];
    
    self.dymicImgView = [[PDImageView alloc]init];
    self.dymicImgView.frame = CGRectMake(LCFloat(12), LCFloat(9), LCFloat(40), LCFloat(40));
    [self.dymicBgView addSubview:self.dymicImgView];
    
    // 内容
    self.dymicInfoLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    self.dymicInfoLabel.numberOfLines = 2;
    [self.dymicBgView addSubview:self.dymicInfoLabel];
}

-(void)setTransferCommentModel:(CenterMessageRootSingleModel *)transferCommentModel{
    _transferCommentModel = transferCommentModel;
    
    // img
    self.avatarImgView.frame = CGRectMake(LCFloat(18), LCFloat(17), LCFloat(37), LCFloat(37));
    [self.avatarImgView uploadImageWithRoundURL:transferCommentModel.comment_remind.operate_pic placeholder:nil callback:NULL];
    
    // name
    self.nickNameLabel.text = transferCommentModel.comment_remind.operate_name;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13), LCFloat(20), nickSize.width, nickSize.height);
    
    //title
    CGFloat desWidth = LCFloat(108);
    CGFloat titleWidth = 0;
    if (transferCommentModel.comment_remind.theme_content.length){
        titleWidth = kScreenBounds.size.width - LCFloat(15) - desWidth - LCFloat(20) - LCFloat(68);
    } else {
        titleWidth = kScreenBounds.size.width - LCFloat(15) - LCFloat(68);
    }
    
    self.titleLabel.text = transferCommentModel.comment_remind.comment_content;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    if (titleSize.height > 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.numberOfLines = 2;
        self.titleLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(12), titleWidth, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
    } else {
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(12), titleWidth, 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
    }
    
    self.desBgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - desWidth, LCFloat(33), desWidth, LCFloat(56));
    
    if (transferCommentModel.comment_remind.theme_content.length){
        self.desBgView.hidden = NO;
    } else {
        self.desBgView.hidden = YES;
    }
    
    self.desLabel.text = transferCommentModel.comment_remind.reply_comment_content;
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(LCFloat(80), CGFLOAT_MAX)];
    if (desSize.height >= 2 * [NSString contentofHeightWithFont:self.desLabel.font]){
        self.desLabel.frame = CGRectMake(LCFloat(14), LCFloat(12), desWidth - 2 * LCFloat(14), 2 * [NSString contentofHeightWithFont:self.desLabel.font]);
        self.desLabel.numberOfLines = 2;
    } else {
        self.desLabel.frame = CGRectMake(LCFloat(14), LCFloat(12), desWidth - 2 * LCFloat(14), 1 * [NSString contentofHeightWithFont:self.desLabel.font]);
        self.desLabel.numberOfLines = 1;
    }
    
    // 是否已读
    self.nImgDotView.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.nImgDotView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20), LCFloat(24), LCFloat(5), LCFloat(5));
    if (transferCommentModel.comment_remind){               // 表示已读
        self.nImgDotView.hidden = YES;
    } else {                                                // 表示未读
        self.nImgDotView.hidden = NO;
    }
    
    // dymicBg
    if (transferCommentModel.comment_remind.theme_content.length){
        self.dymicBgView.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.desLabel.frame) + LCFloat(12), kScreenBounds.size.width - self.nickNameLabel.orgin_x - LCFloat(15), LCFloat(56));
    } else {

    }

    self.dymicInfoLabel.text = transferCommentModel.comment_remind.theme_content;
    self.dymicImgView.frame = CGRectMake(LCFloat(12), LCFloat(9), LCFloat(39), LCFloat(39));
    __weak typeof(self)weakSelf = self;
    [self.dymicImgView uploadImageWithURL:transferCommentModel.comment_remind.theme_pic placeholder:[UIImage imageNamed:@"icon_centerMessage_guandian"] callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (image){
            strongSelf.dymicImgView.image = image;
        } else {
            strongSelf.dymicImgView.image = [UIImage imageNamed:@"icon_centerMessage_guandian"];
        }
    }];
    
    self.dymicInfoLabel.text = transferCommentModel.comment_remind.theme_content;
    CGSize dymicSize = [self.dymicInfoLabel.text sizeWithCalcFont:self.dymicInfoLabel.font constrainedToSize:CGSizeMake(self.dymicBgView.size_width - CGRectGetMaxX(self.dymicImgView.frame) - LCFloat(10) - LCFloat(13), CGFLOAT_MAX)];
    if (dymicSize.height >= 2 * [NSString contentofHeightWithFont:self.dymicInfoLabel.font]){
        self.dymicInfoLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicImgView.frame) + LCFloat(10), LCFloat(12), self.dymicBgView.size_width - CGRectGetMaxX(self.dymicImgView.frame) - LCFloat(10) - LCFloat(13), 2 * [NSString contentofHeightWithFont:self.dymicInfoLabel.font]);
        self.dymicInfoLabel.numberOfLines = 2;
    } else {
        self.dymicInfoLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicImgView.frame) + LCFloat(10), LCFloat(12), self.dymicBgView.size_width - CGRectGetMaxX(self.dymicImgView.frame) - LCFloat(10) - LCFloat(13), 1 * [NSString contentofHeightWithFont:self.dymicInfoLabel.font]);
        self.dymicInfoLabel.numberOfLines = 1;
    }

    // time
    self.timeLabel.text = [NSDate getTimeGap:transferCommentModel.create_time];
    self.timeLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, 0, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    if (self.transferCommentModel.comment_remind.reply_comment_content.length){
        self.dymicBgView.hidden = YES;
        self.timeLabel.orgin_y = CGRectGetMaxY(self.dymicBgView.frame) + LCFloat(14);
    } else {
        self.timeLabel.orgin_y = CGRectGetMaxY(self.desBgView.frame) + LCFloat(3);
        self.dymicBgView.hidden = NO;
    }
    
    if (transferCommentModel.comment_remind.reply_comment_content.length){
        self.dymicBgView.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12), kScreenBounds.size.width - self.nickNameLabel.orgin_x - LCFloat(15), LCFloat(56));
    }
    
    
    // 判断
    if (transferCommentModel.type_code == PushTypeArticleComment) {             // 文章评论
        self.dymicBgView.orgin_y = CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12);
        self.dymicImgView.hidden = NO;
        
        self.timeLabel.orgin_y = CGRectGetMaxY(self.dymicBgView.frame) + LCFloat(14);
        
        self.desBgView.hidden = YES;
    } else if (transferCommentModel.type_code == PushTypeArticleSubComment){
        self.desBgView.hidden = NO;
        
        self.timeLabel.orgin_y = CGRectGetMaxY(self.desBgView.frame) + LCFloat(3);
    }
}

-(void)actionManager{
    
}

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(20);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(12);
    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    
    if (model.type_code == PushTypeArticleComment){ // 文章评论
        cellHeight += LCFloat(12);
        cellHeight += LCFloat(56);
        cellHeight += LCFloat(14);
    } else if (model.type_code == PushTypeArticleSubComment){       // 评论评论
        cellHeight += LCFloat(15);
    }
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(16);
    
    return cellHeight;
}


@end
