//
//  CenterMessageCommentViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageCommentViewController.h"
#import "CenterMessageCommentTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface CenterMessageCommentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *commentTableView;
@property (nonatomic,strong)NSMutableArray *commentMutableArr;
@end

@implementation CenterMessageCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetCommentInfoManer];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"评论与回复";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"标记全部已读" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:@"标记全部已读" message:@"是否确认标记全部已读" buttonTitles:@[@"取消",@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1){
                
            }
        }]show];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.commentMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.commentTableView){
        self.commentTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.commentTableView.dataSource = self;
        self.commentTableView.delegate = self;
        [self.view addSubview:self.commentTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.commentTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentInfoManer];
    }];
    [self.commentTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentInfoManer];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.commentMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageCommentTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageCommentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CenterMessageRootSingleModel *commentModel = [self.commentMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferCommentModel = commentModel;
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CenterMessageRootSingleModel *commentModel = [self.commentMutableArr objectAtIndex:indexPath.row];
    return [CenterMessageCommentTableViewCell calculationCellHeightWithModel:commentModel];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.commentTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.commentMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.commentMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetCommentInfoManer{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageGetListWithPage:self.commentTableView.currentPage type:MessageTypeComment block:^(CenterMessageRootListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.commentMutableArr removeAllObjects];
        [strongSelf.commentMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.commentTableView reloadData];
        if (strongSelf.commentMutableArr.count){
            [strongSelf.commentTableView dismissPrompt];
        } else {
            [strongSelf.commentTableView showPrompt:@"当前没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        [strongSelf.commentTableView stopPullToRefresh];
        [strongSelf.commentTableView stopFinishScrollingRefresh];
    }];
}


@end
