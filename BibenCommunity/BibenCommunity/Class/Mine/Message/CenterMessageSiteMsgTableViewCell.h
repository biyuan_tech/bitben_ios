//
//  CenterMessageSiteMsgTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/12.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterMessageSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageSiteMsgTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong)CenterMessageSingleModel *transferMessageModel;

+(CGFloat)calculationCellHeight:(CenterMessageSingleModel *)transferMessageModel;
@end

NS_ASSUME_NONNULL_END
