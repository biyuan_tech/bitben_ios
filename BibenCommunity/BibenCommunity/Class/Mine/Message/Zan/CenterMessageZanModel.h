//
//  CenterMessageZanModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageZanModel : FetchModel
@property (nonatomic,copy)NSString *operate_name;
@property (nonatomic,copy)NSString *operate_pic;
@property (nonatomic,copy)NSString *operate_user_id;
@property (nonatomic,copy)NSString *theme_content;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,copy)NSString *theme_pic;
@property (nonatomic,copy)NSString *comment_content;
@property (nonatomic,copy)NSString *comment_id;

@end

NS_ASSUME_NONNULL_END
