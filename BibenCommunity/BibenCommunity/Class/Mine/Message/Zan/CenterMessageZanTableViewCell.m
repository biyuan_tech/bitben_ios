//
//  CenterMessageZanTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageZanTableViewCell.h"

@interface CenterMessageZanTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UIView *desInfoBgView;
@property (nonatomic,strong)UILabel *desInfoLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIView *dymicBgView;
@property (nonatomic,strong)PDImageView *dymicImgView;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation CenterMessageZanTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nickLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.nickLabel];
    
    self.desLabel = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    [self addSubview:self.desLabel];
    
    self.desInfoBgView = [[UIView alloc]init];
    self.desInfoBgView.backgroundColor = [UIColor colorWithCustomerName:@"F5F5F5"];
    [self addSubview:self.desInfoBgView];
    
    self.desInfoLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    [self.desInfoBgView addSubview:self.desInfoLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    self.dymicBgView = [[UIView alloc]init];
    self.dymicBgView.backgroundColor = [UIColor colorWithCustomerName:@"F5F5F5"];
    [self addSubview:self.dymicBgView];
    
    self.dymicImgView = [[PDImageView alloc]init];
    self.dymicImgView.backgroundColor = [UIColor clearColor];
    [self.dymicBgView addSubview:self.dymicImgView];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    [self.dymicBgView addSubview:self.dymicLabel];
    
}

-(void)setTransferMessageModel:(CenterMessageRootSingleModel *)transferMessageModel{
    _transferMessageModel = transferMessageModel;
    
    // 1. 头像
    self.avatarImgView.frame = CGRectMake(LCFloat(15), LCFloat(15), LCFloat(42), LCFloat(42));
    [self.avatarImgView uploadImageWithRoundURL:transferMessageModel.support_remind.operate_pic placeholder:nil callback:NULL];
    
    self.nickLabel.text = transferMessageModel.support_remind.operate_name;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickLabel];
    self.nickLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), LCFloat(20), nickSize.width, nickSize.height);
    
    self.desLabel.text = @"赞了你的评论";
    CGSize desSize = [Tool makeSizeWithLabel:self.desLabel];
    self.desLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.nickLabel.frame) + LCFloat(12), desSize.width, desSize.height);
    
    self.timeLabel.text = [NSDate getTimeGap:self.transferMessageModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];

    
    if(transferMessageModel.type_code  == PushTypeArticleSupport){          // 文章赞
        self.desLabel.text = @"赞了你的文章";
        
        self.dymicBgView.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.desLabel.frame) + LCFloat(12), kScreenBounds.size.width - self.nickLabel.orgin_x - LCFloat(15), LCFloat(56));
        
        self.dymicImgView.frame = CGRectMake(LCFloat(12), LCFloat(9), LCFloat(39), LCFloat(39));
        __weak typeof(self)weakSelf = self;
        if (transferMessageModel.support_remind.theme_pic.length){
            [self.dymicImgView uploadImageWithURL:transferMessageModel.support_remind.theme_pic placeholder:[UIImage imageNamed:@"icon_centerMessage_guandian"] callback:^(UIImage *image) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.dymicImgView.image = [UIImage imageNamed:@"icon_centerMessage_guandian"] ;
            }];
        } else {
            self.dymicImgView.image = [UIImage imageNamed:@"icon_centerMessage_guandian"] ;
        }
        
        
        self.dymicLabel.text = transferMessageModel.support_remind.theme_content;
        CGFloat width = self.dymicBgView.size_width - LCFloat(39) - 2 * LCFloat(12) - LCFloat(10);
        CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        if (dymicSize.height >= 2 * [NSString contentofHeightWithFont:self.dymicLabel.font]){
            self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicImgView.frame) + LCFloat(10), LCFloat(12), width, 2 * [NSString contentofHeightWithFont:self.dymicLabel.font]);
        } else {
            self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicImgView.frame) + LCFloat(10), LCFloat(12), width, 1 * [NSString contentofHeightWithFont:self.dymicLabel.font]);
        }
        
        self.desInfoBgView.hidden = YES;
        self.dymicBgView.hidden = NO;
        
        self.timeLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.dymicBgView.frame) + LCFloat(14), timeSize.width, timeSize.height);

    } else if (transferMessageModel.type_code == PushTypeArticleSubSupport){
        self.desLabel.text = @"赞了你的评论";
        
        self.desInfoBgView.hidden = NO;
        self.dymicBgView.hidden = YES;
        
        if (transferMessageModel.support_remind.comment_content.length){
            self.desInfoBgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(172), LCFloat(33), LCFloat(172), LCFloat(56));
            
            self.desInfoLabel.text = transferMessageModel.support_remind.comment_content;
            self.desInfoLabel.frame = CGRectMake(LCFloat(12), LCFloat(12), self.desInfoBgView.size_width - 2 * LCFloat(13), 2 * [NSString contentofHeightWithFont:self.desInfoLabel.font]);
            
            self.timeLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.desInfoBgView.frame) + LCFloat(3), timeSize.width, timeSize.height);
        }
    }
}

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)centerModel{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(20);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(12);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    
    if(centerModel.type_code  == PushTypeArticleSupport){          // 文章赞
        cellHeight += LCFloat(12);
        cellHeight += LCFloat(56);
        cellHeight += LCFloat(14);
        cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    } else if (centerModel.type_code == PushTypeArticleSubSupport){
        cellHeight += LCFloat(35);
        cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    }
    
    cellHeight += LCFloat(16);
    return cellHeight;
}


@end
