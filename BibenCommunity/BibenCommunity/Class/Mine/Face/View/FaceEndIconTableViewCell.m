//
//  FaceEndIconTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/6.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceEndIconTableViewCell.h"

@interface FaceEndIconTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *infoLabel;
@end

@implementation FaceEndIconTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(81)) / 2., 0, LCFloat(81), LCFloat(81));
    [self addSubview:self.iconImgView];
    
    self.infoLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.infoLabel.font = [self.infoLabel.font boldFont];
    self.infoLabel.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + LCFloat(23), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.infoLabel.font]);
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.infoLabel];
}

-(void)setHasSuccessed:(BOOL)hasSuccessed{
    _hasSuccessed = hasSuccessed;
    
    if (hasSuccessed){
        self.iconImgView.image = [UIImage imageNamed:@"icon_face_end_successed"];
        self.infoLabel.text = @"认证成功";
    } else {
        self.iconImgView.image = [UIImage imageNamed:@"icon_face_end_fail"];
        self.infoLabel.text = @"认证失败";
    }
    
    
}

-(void)infoManager{
    
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(81);
    cellHeight += LCFloat(23);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"16"] boldFont]];
    cellHeight += LCFloat(14);
    return cellHeight;
}

@end
