//
//  FaceAlertImgTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/5.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceAlertImgTableViewCell.h"

@interface FaceAlertImgTableViewCell()
@property (nonatomic,strong)PDImageView *mainImgView;
@property (nonatomic,strong)PDImageView *imgView1;
@property (nonatomic,strong)UILabel *label1;
@property (nonatomic,strong)PDImageView *imgView2;
@property (nonatomic,strong)UILabel *label2;
@property (nonatomic,strong)PDImageView *imgView3;
@property (nonatomic,strong)UILabel *label3;

@end

@implementation FaceAlertImgTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // imgView
    self.mainImgView = [[PDImageView alloc]init];
    self.mainImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.mainImgView];
    
    // imgView1;
    self.imgView1 = [[PDImageView alloc]init];
    self.imgView1.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView1];
    
    // label
    self.label1 = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.label1];
    
    self.imgView2 = [[PDImageView alloc]init];
    self.imgView2.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView2];
    
    self.label2 = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.label2];
    
    self.imgView3 = [[PDImageView alloc]init];
    self.imgView3.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView3];
    
    self.label3 = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.label3];
    
    
    [self infoManager];
}

-(void)infoManager{
    self.mainImgView.image = [UIImage imageNamed:@"icon_face_main"];
    self.mainImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(171)) / 2., 0, LCFloat(171), LCFloat(164));
    
    CGFloat margin = (kScreenBounds.size.width - 3 * LCFloat(62)) / 4.;
    self.imgView1.image = [UIImage imageNamed:@"icon_face_alert1"];
    self.imgView1.frame = CGRectMake(margin, CGRectGetMaxY(self.mainImgView.frame) + LCFloat(50), LCFloat(62), LCFloat(62));
    
    self.imgView2.image = [UIImage imageNamed:@"icon_face_alert2"];
    self.imgView2.frame = CGRectMake(CGRectGetMaxX(self.imgView1.frame) + margin, self.imgView1.orgin_y , LCFloat(62), LCFloat(62));
    
    self.imgView3.image = [UIImage imageNamed:@"icon_face_alert3"];
    self.imgView3.frame = CGRectMake(CGRectGetMaxX(self.imgView2.frame) + margin, self.imgView1.orgin_y, LCFloat(62), LCFloat(62));
    
    self.label1.text = @"不能佩戴眼镜";
    CGSize label1Size = [Tool makeSizeWithLabel:self.label1];
    self.label1.frame = CGRectMake(0, CGRectGetMaxY(self.imgView1.frame) + LCFloat(17), label1Size.width, label1Size.height);
    self.label1.center_x = self.imgView1.center_x;
    
    self.label2.text = @"不能遮挡脸部";
    CGSize label2Size = [Tool makeSizeWithLabel:self.label2];
    self.label2.frame = CGRectMake(0, self.label1.orgin_y, label2Size.width, label2Size.height);
    self.label2.center_x = self.imgView2.center_x;
    
    self.label3.text = @"不能仰头俯拍";
    CGSize label3Size = [Tool makeSizeWithLabel:self.label3];
    self.label3.frame = CGRectMake(0, self.label1.orgin_y, label3Size.width, label3Size.height);
    self.label3.center_x = self.imgView3.center_x;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(164);
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(55);
    cellHeight += LCFloat(17);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(17);
    return cellHeight;
}

@end
