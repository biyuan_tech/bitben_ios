//
//  FaceQuickAuthModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/6.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FaceQuickAuthModel : FetchModel

@property (nonatomic,assign)BOOL status;
@property (nonatomic,copy)NSString *fail_message;

@end

NS_ASSUME_NONNULL_END
