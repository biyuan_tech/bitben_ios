//
//  FaceEndViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/5.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "CenterRootViewController.h"
#import "FaceUploadViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class CenterRootViewController;
@class FaceUploadViewController;
@interface FaceEndViewController : AbstractViewController

@property (nonatomic,assign)BOOL hasSuccessed;

-(void)reFaceRenzhengManager:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
