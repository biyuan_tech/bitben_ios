//
//  FaceEndViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/5.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceEndViewController.h"
#import "FaceEndIconTableViewCell.h"
#import "FaceEndNormalTableViewCell.h"

static char reFaceRenzhengManagerKey;
@interface FaceEndViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
}
@property (nonatomic,strong)UITableView *faceEndTableView;
@property (nonatomic,strong)NSArray *faceEndArr;
@end

@implementation FaceEndViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

-(void)pageSetting{
    self.barMainTitle = @"快捷认证";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (self.hasSuccessed){
        self.faceEndArr = @[@[@"图标"],@[@"成功"],@[@"完成按钮"]];
    } else {
        self.faceEndArr = @[@[@"图标"],@[@"可以通过以下方式提高认证通过率",@"1、请确认由本人亲自操作",@"2、保持光线充足并均匀",@"3、摘掉眼镜并漏出耳朵",@"4、将脸置于提示框内，并按提示完成动作"],@[@"重新认证"],@[@"返回"]];
    }
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.faceEndTableView){
        self.faceEndTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.faceEndTableView.dataSource = self;
        self.faceEndTableView.delegate = self;
        [self.view addSubview:self.faceEndTableView];
    }
}

#pragma mark - UItableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.faceEndArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.faceEndArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        FaceEndIconTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[FaceEndIconTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.hasSuccessed = self.hasSuccessed;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"成功" sourceArr:self.faceEndArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        FaceEndNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[FaceEndNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"完成按钮" sourceArr:self.faceEndArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"重新认证" sourceArr:self.faceEndArr]|| indexPath.section == [self cellIndexPathSectionWithcellData:@"返回" sourceArr:self.faceEndArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell  *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = [[self.faceEndArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        __weak typeof(self)weakSelf = self;
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"完成按钮" sourceArr:self.faceEndArr]){
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.navigationController popToRootViewControllerAnimated:YES];
            }];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"重新认证" sourceArr:self.faceEndArr]){
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.navigationController popViewControllerAnimated:YES];
                
                void(^block)() = objc_getAssociatedObject(strongSelf, &reFaceRenzhengManagerKey);
                if (block){
                    block();
                }
            }];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"返回" sourceArr:self.faceEndArr]){
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferType = GWButtonTableViewCellTypeLayer;
            cellWithRowThr.transferTitle = @"返回";
            
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.navigationController popToRootViewControllerAnimated:YES];;
            }];
        }
        
        return cellWithRowThr;
    } else {
        
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        cellWithRowOther.transferCellHeight = cellHeight;
        cellWithRowOther.transferTitle = [[self.faceEndArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.row == 0){
            cellWithRowOther.titleLabel.font = [cellWithRowOther.titleLabel.font boldFont];
        } else {
            cellWithRowOther.titleLabel.font = cellWithRowOther.titleLabel.font;
        }
        return cellWithRowOther;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [FaceEndIconTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"成功" sourceArr:self.faceEndArr]){
        return [FaceEndNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"完成按钮" sourceArr:self.faceEndArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"重新认证" sourceArr:self.faceEndArr]|| indexPath.section == [self cellIndexPathSectionWithcellData:@"返回" sourceArr:self.faceEndArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return [GWNormalTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(42);
    } else {
        return LCFloat(11);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)reFaceRenzhengManager:(void(^)())block{
    objc_setAssociatedObject(self, &reFaceRenzhengManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
