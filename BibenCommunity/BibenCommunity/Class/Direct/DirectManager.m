//
//  DirectManager.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "DirectManager.h"
#import "BYHomeBannerModel.h"
#import "ArticleDetailRootViewController.h"
#import "BYVideoRoomController.h"
#import "BYVODPlayController.h"
#import "BYPPTRoomController.h"
#import "BYLiveConfig.h"
#import "BYILiveRoomController.h"
#import "BYVideoLiveController.h"

@implementation DirectManager

+(void)directToControllerWithBannerModel:(BYHomeBannerModel *)bannerModel childController:(AbstractViewController *)controller{
    AbstractViewController *commonVC;
    commonVC = (AbstractViewController *)controller;
    
    if (bannerModel.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = bannerModel.theme_id;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        
        [controller.navigationController pushViewController:articleController animated:YES];
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_LIVE) { // 跳转直播
        switch (bannerModel.live_type) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                BYLiveConfig *config = [[BYLiveConfig alloc] init];
                config.isHost = NO;
                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                ilivewRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [controller.navigationController pushViewController:ilivewRoomController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [controller.navigationController pushViewController:videoRoomController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
                pptRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                pptRoomController.isHost = NO;
                [controller.navigationController pushViewController:pptRoomController animated:YES];

            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [controller.navigationController pushViewController:vodPlayController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VIDEO:
            {
                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
                videoLiveController.live_record_id = nullToEmpty(bannerModel.theme_id);
                videoLiveController.isHost = NO;
                [controller.navigationController pushViewController:videoLiveController animated:YES];
            }
                break;
            default:
                break;
        }
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
        [webViewController webDirectedWebUrl:bannerModel.theme_id];
        [controller.navigationController pushViewController:webViewController animated:YES];

    }
    
}


@end
