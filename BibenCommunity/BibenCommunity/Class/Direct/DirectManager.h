//
//  DirectManager.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BYHomeBannerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DirectManager : NSObject

+(void)directToControllerWithBannerModel:(BYHomeBannerModel *)bannerModel childController:(BYCommonViewController *)controller;

@end

NS_ASSUME_NONNULL_END
