//
//  BYLiveEditBgImgController.h
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

typedef NS_ENUM(NSInteger ,EDIT_IMG_TYPE){
    EDIT_IMG_TYPE_LIVEROOM = 0, // 直播间背景图
    EDIT_IMG_TYPE_LIVE, // 直播封面图
    EDIT_IMG_TYPE_PUBLICITY// 宣传图
} ;
@class BYCommonLiveModel;
@interface BYLiveEditBgImgController : BYCommonViewController

@property (nonatomic ,assign) EDIT_IMG_TYPE edit_type;

/** 直播记录ID */
@property (nonatomic ,copy) NSString *live_record_id;
/** 直播间数据 */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 刷新直播间数据 */
@property (nonatomic ,copy) void (^reloadLiveRoomHandel)(void);

@end
