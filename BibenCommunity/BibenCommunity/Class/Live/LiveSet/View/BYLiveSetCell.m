//
//  BYLiveSetCell.m
//  BY
//
//  Created by Belief on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveSetCell.h"
#import "BYLiveSetModel.h"

@interface BYLiveSetCell ()

/** tagView */
@property (nonatomic ,strong) UIView *tagView;

@end

static NSInteger baseTag = 0x4842;
@implementation BYLiveSetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveSetCellModel *model = dic.allValues[0][indexPath.row];
    self.tagView.layer.opacity = model.topicArr.count ? 1.0f : 0.0f;
    [self setTitle:model.title];
    [self setRightImage:[UIImage imageNamed:@"common_cell_rightarrow"]];
    [self setRightTitle:model.rightTitle];
    [self showBottomLine:model.showBottomLine];
    if (model.rightlogo) {
        [self setRightLogoImage:model.rightlogo];
    }
    else
    {
        [self serRightLogoImageUrl:model.rightlogoName];
    }
    
    NSMutableArray *tmpArr = [NSMutableArray array];
    if (model.topicArr.count > 2) {
        [tmpArr addObjectsFromArray:model.topicArr];
        [tmpArr removeObjectsInRange:NSMakeRange(2, model.topicArr.count -2)];
        [tmpArr addObject:@"..."];
    }
    else{
        [tmpArr addObjectsFromArray:model.topicArr];
        for (NSInteger i = model.topicArr.count; i < 3; i ++) {
            [tmpArr addObject:@""];
        }
    }
   
//    if (tmpArr.count = 3) {
//
//    }
    for (int i = 0; i < tmpArr.count; i ++ ) {
        UILabel *label = [self.tagView viewWithTag:baseTag + i];
        label.text = tmpArr[tmpArr.count - i - 1];
        label.layer.opacity = label.text.length ? 1.0f : 0.0f;
        if (i == 0) {
            [label mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(stringGetWidth(label.text, label.font.pointSize));
                make.height.mas_equalTo(stringGetHeight(label.text, label.font.pointSize));
                make.centerY.mas_equalTo(0);
                make.right.mas_equalTo(0);
            }];
        }
        else {
            UILabel *lastLabel = [self.tagView viewWithTag:baseTag + i - 1];
            [label mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(label.text.length ? stringGetWidth(label.text, label.font.pointSize) + 10 : 0);
                make.height.mas_equalTo(stringGetHeight(label.text, label.font.pointSize) + 10);
                make.centerY.mas_equalTo(0);
                make.right.mas_equalTo(lastLabel.mas_left).mas_offset(lastLabel.text.length ? -10 : 0);
            }];
        }
    }
}

- (void)setContentView{
    self.tagView = [UIView by_init];
    [self.contentView addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 100, 0, 33));
    }];
    
    for (int i = 0; i < 3; i ++ ) {
        UILabel *label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = kColorRGBValue(0x4a85cf);
        label.font = [UIFont systemFontOfSize:i == 0 ? 12 : 10];
        if (i != 0) {
            label.layer.cornerRadius = 3.0f;
            label.layer.borderColor = kColorRGBValue(0x4a85cf).CGColor;
            label.layer.borderWidth = 1.0f;
        }
        label.tag = baseTag + i;
        [self.tagView addSubview:label];
    }
}
@end
