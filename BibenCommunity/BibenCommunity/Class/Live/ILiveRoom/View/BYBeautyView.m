//
//  BYBeautyView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/2/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYBeautyView.h"

static NSInteger const contentH = 240.f;
@interface BYBeautyView ()
/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;
/** 美颜滑块 */
@property (nonatomic ,strong) UISlider *beautySlider;
/** 美颜度 */
@property (nonatomic ,strong) UILabel *beautyValueLab;
/** 美白滑块 */
@property (nonatomic ,strong) UISlider *whiteSlider;
/** 美白度 */
@property (nonatomic ,strong) UILabel *whiteValueLab;
@end

@implementation BYBeautyView

+ (instancetype)initViewShowInView:(UIView *)view{
    BYBeautyView *beautyView = [[BYBeautyView alloc] init];
    beautyView.fathuerView = view ? view : kCommonWindow;
    beautyView.frame = beautyView.fathuerView.bounds;
    beautyView.hidden = YES;
    [beautyView configView];
    return beautyView;
}

- (void)setBeautyValue:(CGFloat)value{
    [_beautySlider setValue:value animated:YES];
    [self setBeautyValueLabValue:value];
}

- (void)setWhiteValue:(CGFloat)value{
    [_whiteSlider setValue:value animated:YES];
    [self setWhiteValueLabValue:value];
}

- (void)setBeautyValueLabValue:(CGFloat)value{
    _beautyValueLab.text = [NSString stringWithFormat:@"%d",(int)(value*100)];
    CGRect rect = _beautySlider.frame;
    if (rect.origin.x < 60) return;
    CGFloat originX = rect.origin.x + rect.size.width*value - 25/2;
    [_beautyValueLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(originX);
    }];
    if (_didChangeBeautyHandle) {
        _didChangeBeautyHandle(_beautySlider.value,_whiteSlider.value);
    }
}

- (void)setWhiteValueLabValue:(CGFloat)value{
    _whiteValueLab.text = [NSString stringWithFormat:@"%d",(int)(value*100)];
    CGRect rect = _whiteSlider.frame;
    if (rect.origin.x < 60) return;
    CGFloat originX = rect.origin.x + rect.size.width*value - 25/2;
    [_whiteValueLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(originX);
    }];
    if (_didChangeBeautyHandle) {
        _didChangeBeautyHandle(_beautySlider.value,_whiteSlider.value);
    }
}

#pragma mark - action
- (void)beautyChange:(id)sender{
    [self setBeautyValueLabValue:_beautySlider.value];
}

- (void)whiteningChange:(id)sender{
    [self setWhiteValueLabValue:_whiteSlider.value];
}

#pragma mark - animation

- (void)showAnimation{
    self.hidden = NO;
    [self.fathuerView addSubview:self];
    self.maskView.layer.opacity = 1.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            if (self.beautyValueLab.orgin_x == 0) {
                [self setBeautyValueLabValue:self.beautySlider.value];
                [self setWhiteValueLabValue:self.whiteSlider.value];
            }
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    self.maskView.layer.opacity = 0.0f;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(contentH);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}

#pragma mark - config

- (void)layoutSubviews{
    
}

- (void)configView{
    self.maskView = [UIView by_init];
    self.maskView.layer.opacity = 0.0f;
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [self.maskView addGestureRecognizer:tapGestureRecognizer];
    
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.contentView.layer.shadowOpacity = 0.2;
    self.contentView.layer.shadowRadius = 25;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
        make.height.mas_equalTo(contentH);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    UIColor *color = kColorRGBValue(0x353535);
    UIFont *font = [UIFont systemFontOfSize:15];
    NSDictionary *attribut = @{@"title":@"美颜",NSForegroundColorAttributeName:color,NSFontAttributeName:font};
    [button setBy_attributedTitle:attribut forState:UIControlStateNormal];
    [self.contentView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7e7)];
    [button addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-0.5);
    }];
    
    UIImageView *arrowImgView = [[UIImageView alloc] init];
    [arrowImgView by_setImageName:@"common_arrow_down"];
    [button addSubview:arrowImgView];
    [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(arrowImgView.image.size.width);
        make.height.mas_equalTo(arrowImgView.image.size.height);
    }];
    
    UIImageView *beautyImgView = [[UIImageView alloc] init];
    [beautyImgView by_setImageName:@"ilive_beauty_slider"];
    [self.contentView addSubview:beautyImgView];
    [beautyImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.top.mas_equalTo(88);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(21);
    }];
    
    UILabel *beautyTitle = [UILabel initTitle:@"磨皮" font:11 textColor:kTextColor_53];
    beautyTitle.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:beautyTitle];
    [beautyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(beautyImgView.mas_centerX).mas_offset(0);
        make.top.mas_equalTo(beautyImgView.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(stringGetHeight(beautyTitle.text, 11));
        make.width.mas_equalTo(25);
    }];
    
    // 美颜度滑块
    UISlider *beautySlider = [[UISlider alloc] init];
    [beautySlider setMinimumTrackTintColor:kColorRGBValue(0x377aff)];
    [beautySlider setMaximumTrackTintColor:kColorRGBValue(0xe0e0e0)];
    [beautySlider addTarget:self action:@selector(beautyChange:) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:beautySlider];
    self.beautySlider = beautySlider;
    [beautySlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(85);
        make.left.mas_equalTo(beautyImgView.mas_right).mas_offset(18);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(-30);
    }];
    
    // 美颜值
    UILabel *beautyValueLab = [UILabel initTitle:@"" font:13 textColor:kColorRGBValue(0x747373)];
    beautyValueLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:beautyValueLab];
    self.beautyValueLab = beautyValueLab;
    [beautyValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(beautySlider.mas_top).mas_offset(-7);
        make.width.mas_equalTo(25);
        make.height.mas_equalTo(15);
        make.left.mas_equalTo(0);
    }];
    
    // 美白
    
    UIImageView *whiteImgView = [[UIImageView alloc] init];
    [whiteImgView by_setImageName:@"ilive_whitening_slider"];
    [self.contentView addSubview:whiteImgView];
    [whiteImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(beautyImgView.mas_centerX).mas_offset(0);
        make.top.mas_equalTo(162);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(22);
    }];
    
    UILabel *whiteTitle = [UILabel initTitle:@"美白" font:11 textColor:kTextColor_53];
    whiteTitle.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:whiteTitle];
    [whiteTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(whiteImgView.mas_centerX).mas_offset(0);
        make.top.mas_equalTo(whiteImgView.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(stringGetHeight(whiteTitle.text, 11));
        make.width.mas_equalTo(25);
    }];
    
    // 美白度滑块
    UISlider *whiteSlider = [[UISlider alloc] init];
    [whiteSlider setMinimumTrackTintColor:kColorRGBValue(0x377aff)];
    [whiteSlider setMaximumTrackTintColor:kColorRGBValue(0xe0e0e0)];
    [whiteSlider addTarget:self action:@selector(whiteningChange:) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:whiteSlider];
    self.whiteSlider = whiteSlider;
    [whiteSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(160);
        make.left.mas_equalTo(whiteImgView.mas_right).mas_offset(18);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(-30);
    }];
    
    // 美白值
    UILabel *whiteValueLab = [UILabel initTitle:@"" font:13 textColor:kColorRGBValue(0x747373)];
    whiteValueLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:whiteValueLab];
    self.whiteValueLab = whiteValueLab;
    [whiteValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(whiteSlider.mas_top).mas_offset(-7);
        make.width.mas_equalTo(25);
        make.height.mas_equalTo(15);
        make.left.mas_equalTo(0);
    }];
}
@end
