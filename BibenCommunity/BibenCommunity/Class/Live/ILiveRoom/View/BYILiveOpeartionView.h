//
//  BYILiveOpeartionView.h
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYSIMController.h"

@interface BYILiveOpeartionView : UIView

/** 退出直播按钮回调 */
@property (nonatomic ,copy) void(^didExitLiveBtnHandle)(void);
@property (nonatomic ,copy) void(^didShareActionHandle)(void);

/** 主播id */
@property (nonatomic ,copy) NSString *hostId;
@property (nonatomic ,copy) NSString *hostName;
/** stream_id 直播码 */
@property (nonatomic ,copy) NSString *stream_id;
/** 直播id */
@property (nonatomic ,copy) NSString *liveId;
/** 百川Im会话 */
@property (nonatomic ,weak) BYSIMController *aConversationController;
/** 美颜回调 */
@property (nonatomic ,copy) void (^didChangeBeautyHandle)(CGFloat beauty,CGFloat white);

- (void)hiddenReward;

/** 初始化判断是否为主播 */
- (instancetype)initWithIsHost:(BOOL)isHost target:(UIViewController *)target;

@end
