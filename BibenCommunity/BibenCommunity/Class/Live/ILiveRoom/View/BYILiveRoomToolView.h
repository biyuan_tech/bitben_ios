//
//  BYILiveRoomToolView.h
//  BY
//
//  Created by 黄亮 on 2018/8/17.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYILiveRoomToolView : UIView


- (instancetype)initInView:(UIView *)view;

- (void)showAnimation;
@end
