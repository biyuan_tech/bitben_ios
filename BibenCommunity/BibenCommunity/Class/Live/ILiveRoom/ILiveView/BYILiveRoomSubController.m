//
//  BYILiveRoomSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYILiveRoomSubController.h"
#import <ILiveSDK/ILiveCoreHeader.h>
#import <TILLiveSDK/TILLiveSDK.h>
#import "BYIMDefine.h"

// Controller
#import "BYILiveEndController.h"

// View
#import "BYILiveChatView.h"
#import "BYILiveRoomBottomView.h"
#import "BYILiveOpeartionView.h"
#import "BYReportSheetView.h"

// model、request
#import "BYCommonLiveModel.h"
#import "BYLiveHomeRequest.h"

@interface BYILiveRoomSubController ()<QAVRemoteVideoDelegate,ILVLiveAVListener,ILVLiveIMListener,QAVLocalVideoDelegate,ILiveRoomDisconnectListener,ILiveMemStatusListener>
{
    BOOL _firstLoad;
}

@property (nonatomic ,strong) UIImageView *roomBgImgView;
/** 聊天室展示 */
@property (nonatomic ,strong) BYILiveChatView *chatView;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;
/** 推流的id */
@property (nonatomic ,assign) UInt64 channelID;
/** 直播码 */
@property (nonatomic ,copy) NSString *stream_id;
/** 底部footerView */
@property (nonatomic ,strong) BYILiveRoomBottomView *bottomView;

@property (nonatomic ,strong) BYToastView *toastView;
@property (nonatomic ,strong) BYToastView *endToastView;
/** 直播倒计时view */
@property (nonatomic ,strong) UIView *timerView;
@property (nonatomic ,strong) UILabel *timerLab;
@property (nonatomic ,strong) NSTimer *logTimer;
@property (nonatomic ,strong) UITextView *textView;
/** 主播头像 */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 直播时长 */
@property (nonatomic ,strong) UILabel *liveTimerLab;
/** 直播观看人次 */
@property (nonatomic ,strong) UILabel *liveNumLab;
/** bbt数量 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 主播直播信息 */
@property (nonatomic ,strong) UIView *hostInfoView;
@property (nonatomic ,strong) UIView *bbtMaskView;
/** 举报 */
@property (nonatomic ,strong) UIImageView *reportMarkView;
/** maskView */
@property (nonatomic ,strong) UIView *reportMaskView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;

@end

@implementation BYILiveRoomSubController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_bottomView removeFromSuperview];
    [_chatView removeFromSuperview];
    [_opeartionView removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    self.bottomView = nil;
    self.chatView = nil;
    self.opeartionView = nil;
    self.reportSheetView = nil;
    [[BYCommonTool shareManager] stopTimer];
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];}

- (void)setModel:(BYCommonLiveModel *)model{
    if (!_model) {
        _firstLoad = YES;
    }
    _model = model;
    if (_firstLoad) {
        @weakify(self);
        _firstLoad = NO;
        [self verifyILiveLoginStatus:^{
            @strongify(self);
            [self configAll];
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hasCancelSocket = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)verifyILiveLoginStatus:(void(^)(void))cb{
    // 校验当前登录状态
    if (![[ILiveLoginManager getInstance] getLoginId].length) {
        BYToastView *toastView = [BYToastView toastViewPresentLoadingInView:self.view];
        [[BYIMManager sharedInstance] loginILiveSuccessNotif:^(BOOL isSuccess) {
            [toastView dissmissToastView];
            if (isSuccess) {
                if (cb) cb();
            }
        }];
        // 三秒后如还未登录成功则手动调直播登录
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (![ILiveLoginManager getInstance].getLoginId.length) {
                [[BYIMManager sharedInstance] loginILive:^{
                    [toastView dissmissToastView];
                    if (cb) cb();
                } fail:^{
                    [toastView dissmissToastView];
                }];
            }
        });
    }
    else{
        if (cb) cb();
    }
}

- (void)configAll{
    if (_config.isHost) {
        QAVResult result = [[[ILiveSDK getInstance] getAVContext].videoCtrl setRotation:OrientationPortrait];
        NSLog(@"result ------  %li",(long)result);
    }

//    [self configIM];
    [self configUI];
    [self configRoom];
    [self verifyLiveTime];
    [self addNSNotification];
    if (_config.isHost) { // 开播时间校验
        _virtual_pv = 0;
        [self loadRequestGetSurplusBBT];
    }
    else{
        // 虚拟pv设-1,后端区分主播，观众端
        _virtual_pv = -1;
    }
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveStopPushStream_notic) name:NSNotification_stopPushStream object:nil];
}

- (void)configRoom{
    TILLiveManager *manager = [TILLiveManager getInstance];
    [manager setAVListener:self];
//    [manager setIMListener:self];
    //如果要使用美颜，必须设置本地视频代理
    [[ILiveRoomManager getInstance] setLocalVideoDelegate:self];
    [[ILiveRoomManager getInstance] setRemoteVideoDelegate:self];
    if (!_config.isHost) {
        [self joinRoom];
    }
    else{
    }
    
}

// 当前观众观看的主播状态为直播时刷新
- (void)reloadGuestLivingStatus:(BOOL)isLiving{
    if (!isLiving) {
        [_hostInfoView removeFromSuperview];
        if (_roomTimer) {
            [_roomTimer invalidate];
            _roomTimer = nil;
        }
        [[BYCommonTool shareManager] stopTimer];
        return;
    }
    if (self.model.status == BY_LIVE_STATUS_LIVING) {
        [self addHostLiveInfoView];
        // 开始直播计时
        [self startLiveTimer];
        // 开始发送心跳包
        [self startHeartBeatTimer];
    }
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        [self creatRoom];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.segmentView setLiveSubViewBgColor:[UIColor blackColor]];
    }
    else // 未到开播时间，倒计时自动开播
    {
        self.timerView.layer.opacity = 1.0f;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                [self creatRoom];
                self.timerView.layer.opacity = 0.0f;
                [self.segmentView setLiveSubViewBgColor:[UIColor blackColor]];
            }
            else{
                self.timerLab.text = [NSString stringWithFormat:@"%02d : %02d : %02d : %02d", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
            }
        }];
    }
}

// 直播时长
- (void)startLiveTimer{
    @weakify(self);
    [[BYCommonTool shareManager] timerBeginTime:self.model.real_begin_time cb:^(NSDictionary * _Nonnull param) {
        @strongify(self);
        int hour = [param[@"hours"] intValue];
        int min = [param[@"minute"] intValue];
        int second = [param[@"second"] intValue];
        self.liveTimerLab.text = [NSString stringWithFormat:@"%02d:%02d",min + hour*60,second];
    }];
}

#pragma mark - 心跳(房间保活)

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

#pragma mark - customMethod

// 获取直播测试信息
- (void)setLogInfo{
    NSString *string = [self.config onLogTimer];
    NSLog(@"%@",string);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.text = string;
    });
}

// 进入直播房间
- (void)joinRoom{
    TILLiveRoomOption *roomOption = [TILLiveRoomOption defaultGuestLiveOption];
    roomOption.controlRole = LIVE_GUEST;
    roomOption.roomDisconnectListener = self;
    roomOption.memberStatusListener = self;
    TILLiveManager *manager = [TILLiveManager getInstance];
    [manager setAVRootView:self.view];
    ILiveRenderView *renderView = [manager addAVRenderView:self.view.bounds forIdentifier:_model.room_id srcType:QAVVIDEO_SRC_TYPE_CAMERA];
//    renderView.autoRotate = NO;
    [BYIMManager sharedInstance].renderView = renderView;
    @weakify(self);
    [manager joinRoom:[_model.room_id intValue] option:roomOption succ:^{
        @strongify(self);
        self.roomBgImgView.hidden = YES;
        [self reloadGuestLivingStatus:YES];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
//        @strongify(self);
        if ([module isEqualToString:@"IMSDK"]) return ; // 屏蔽IM错误信息
        NSString *errinfo = [NSString stringWithFormat:@"join room fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        PDLog(@"%@",errinfo);
//        [self showAlertView:@"进入房间失败" message:errinfo];
    }];
}

// 创建互动直播间
- (void)creatRoom{
    if (!_config.isHost) return;
    _toastView = [BYToastView toastViewPresentLoading];
    TILLiveRoomOption *option = [TILLiveRoomOption defaultHostLiveOption];
    option.avOption.cameraPos = CameraPosBack;
    option.controlRole = _config.liveQuality;
    TILLiveManager *manager = [TILLiveManager getInstance];
    //设置渲染承载的视图
    [manager setAVRootView:self.view];
    //添加渲染视图，userid：画面所属者id type:相机/屏幕共享
    ILiveRenderView *renderView = [manager addAVRenderView:self.view.bounds forIdentifier:[AccountModel sharedAccountModel].account_id srcType:QAVVIDEO_SRC_TYPE_CAMERA];
    [BYIMManager sharedInstance].renderView = renderView;
    @weakify(self);
    [manager createRoom:[_model.room_id intValue] option:option succ:^{
        @strongify(self);
        [self.toastView dissmissToastView];
        CGFloat beantyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
        CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
        [BYLiveConfig setBeauty:beantyValue];
        [BYLiveConfig setWhite:whiteValue];
        @weakify(self);
        [[BYIMManager sharedInstance] openCameraAndMic:^{
            @strongify(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self startPushStream];
            });
        } fail:nil];
        showDebugToastView(@"创建互动房间成功", self.view);
    } failed:^(NSString *moudle, int errId, NSString *errMsg) {
        @strongify(self);
        [self.toastView dissmissToastView];
        showDebugToastView(@"创建互动房间失败", self.view);
    }];
}

// 退出直播间
- (void)quitRoom{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_bottomView removeFromSuperview];
    [_chatView removeFromSuperview];
    [_opeartionView removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    self.bottomView = nil;
    self.chatView = nil;
    self.opeartionView = nil;
    self.reportSheetView = nil;
    [[BYCommonTool shareManager] stopTimer];
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [self backBtnAction];
}

- (void)sendOpenLiveNotic{
    // 向当前聊天室发送开播通知
    [[BYIMManager sharedInstance] sendOpenLiveMessage:nil fail:nil];
}

- (void)reportMaskViewAction{
    [self showReportMarkViewHidden:YES];
}

- (void)showReportMarkViewHidden:(BOOL)hidden{
    [self.superController.view bringSubviewToFront:self.reportMaskView];
    [self.superController.view bringSubviewToFront:self.reportMarkView];
    [UIView animateWithDuration:0.1 animations:^{
        self.reportMarkView.layer.opacity = !hidden;
        self.reportMaskView.layer.opacity = !hidden;
    }];
}

- (void)receiveStopPushStream_notic{
    // 移除所有渲染界面
    [[TILLiveManager getInstance] removeAllAVRenderViews];
    [self showAlertView:@"直播已经结束" message:nil];
    [self reloadGuestLivingStatus:NO];
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.attention;
    UIColor *color = !_model.attention ? kColorRGBValue(0xea6441) : kColorRGBValue(0xffffff);
    [_attentionBtn setBackgroundColor:color];
//    _attentionBtn.layer.borderColor = !_model.attention ? kColorRGBValue(0xea6441).CGColor : kColorRGBValue(0x9b9b9b).CGColor;
}

#pragma mark - buttonAction Method
// 退出
- (void)backBtnAction{
    // 角色为非主播退出时
    if (!_config.isHost) {
        TILLiveManager *manager = [TILLiveManager getInstance];
        @weakify(self);
        [manager quitRoom:^{
            @strongify(self);
            showDebugToastView(@"退出房间成功", kCommonWindow);
            [self.superController.navigationController popViewControllerAnimated:YES];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            @strongify(self);
            showDebugToastView(@"退出房间失败", self.view);
        }];
        return;
    }
    
    // 角色为主播未开播退出时
    if ([[ILiveRoomManager getInstance] getRoomId] <= 0) {
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }
    _endToastView = [BYToastView toastViewPresentLoading];

    // 向当前还存在的连麦观众发送下麦消息
    [[BYIMManager sharedInstance] sendStopPushStreamMessage:^{
        // 清空上麦成员
        [[BYIMManager sharedInstance] removeAllUpToVideoMember];
    } fail:nil];
    
    // 停止推流
    @weakify(self);
    [self stopPushStream:^{
        @strongify(self);
        @weakify(self);
        [[TILLiveManager getInstance] quitRoom:^{
            @strongify(self);
            [self.endToastView dissmissToastView];
            // 计时器置空
            [self.roomTimer invalidate];
            self.roomTimer = nil;
            
            [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END];
            BYILiveEndController *liveEndController = [[BYILiveEndController alloc] init];
            liveEndController.model = self.model;
            [self.superController.navigationController pushViewController:liveEndController animated:YES];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            @strongify(self);
            [self.endToastView dissmissToastView];
            showDebugToastView(@"退出房间失败", self.view);
        }];
    }];
}


// 切换摄像头
- (void)swichCameraBtnAction{
    // 切换摄像头
    [BYLiveConfig changeCameraPos];
}

// 查看直播信息log
- (void)infoBtnAction:(UIButton *)sender{
    if (!sender.selected) {
        self.textView = [[UITextView alloc] init];
        self.textView.editable = NO;
        [self.textView setFont:[UIFont systemFontOfSize:17]];
        [self.textView setTextColor:kColorRGBValue(0xed4a45)];
        [self.textView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.textView];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kCommonScreenWidth);
            make.centerX.bottom.mas_equalTo(0);
            make.top.mas_equalTo(50);
        }];
        
        self.logTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(setLogInfo) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.logTimer forMode:NSDefaultRunLoopMode];
    }
    else
    {
        [self.textView removeFromSuperview];
        self.textView = nil;
        if (self.logTimer) {
            [self.logTimer invalidate];
            self.logTimer = nil;
        }
    }
    sender.selected = !sender.selected;
}

// 举报
- (void)reportBtnAction:(UIButton *)sender{
    [self showReportMarkViewHidden:YES];
    [self.reportSheetView showAnimation];
}

// 申请连麦
- (void)upVideoBtnAction:(UIButton *)sender{
    // 发送上麦请求
    [self showReportMarkViewHidden:YES];
    [[BYIMManager sharedInstance] sendUpVideoMessage:nil fail:nil];
}

// 开始直播
- (void)openLiveAction{
    // 主播创建房间
    [self creatRoom];
}

- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
    successBlock:^(id object) {
        @strongify(self);
        self.model.attention = !self.attentionBtn.selected;
        [self reloadAttentionStatus];
     } faileBlock:nil];
}

// 加载当前观看人次（virtual_pv 虚拟人次）
- (void)loadCurrentWatchNum{
    if (self.virtual_pv >= 500) {
        // 获取当前直播真实人数
        @weakify(self);
        [[TIMGroupManager sharedInstance] GetGroupMembers:_model.live_record_id succ:^(NSArray *members) {
            @strongify(self);
            self.model.watch_times = self.virtual_pv + members.count;
            self.liveNumLab.text = [NSString stringWithFormat:@"%d人次",(int)(500 + members.count)];
        } fail:^(int code, NSString *msg) {
            NSLog(@"");
        }];
    }
    else
    {
        self.model.watch_times = self.virtual_pv;
        self.liveNumLab.text = [NSString stringWithFormat:@"%d人次",(int)self.virtual_pv];
    }
}

- (void)updateRenderViewAnimation{
    
}

#pragma mark - requestMethod
// 心跳包请求
- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:_model.live_record_id
                                             virtual_pv:_config.isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   [self loadCurrentWatchNum];
                                               }
                                             } faileBlock:nil];
}

// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status{
//    NSString *stream_id = status == BY_LIVE_STATUS_END ? self.stream_id : nil;
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
//        [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:self.model.live_record_id param:@{@"real_begin_time":self.model.real_begin_time} successBlock:nil faileBlock:nil];
    }
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:self.model.live_record_id stream_id:nullToEmpty(self.stream_id) real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}

// 获取bbt余额
- (void)loadRequestGetSurplusBBT{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestSurplusBBTSuccessBlock:^(id object) {
        @strongify(self);
        self.bbtNumLab.text = [NSString stringWithFormat:@"%d",(int)[object[@"balance"] floatValue]];
    } faileBlock:nil];
}


#pragma mark - 推流
// 开始推流
- (void)startPushStream{
    @weakify(self);
    [[ILiveRoomManager getInstance] startPushStream:[BYLiveConfig getHostPushOption] succ:^(id selfPtr) {
        @strongify(self);
        [self.toastView dissmissToastView];
        AVStreamerResp *resp = (AVStreamerResp *)selfPtr;
        AVLiveUrl *url = nil;
        if (resp && resp.urls && resp.urls.count > 0)
        {
            url = resp.urls[0];
        }
        if (url) {
            self.channelID = resp.channelID;
            self.stream_id = [url.playUrl getStreamId];
            self.opeartionView.stream_id = self.stream_id;
            if (self.model.status != 2) {
                // 重置播放起始时间
                [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING];
               
            }
            // 开始直播计时
            [self startLiveTimer];
            // 开始发送心跳包
            [self startHeartBeatTimer];
            // 发送开播通知
            [self sendOpenLiveNotic];
            self.roomBgImgView.hidden = YES;
            PDLog(@"推流地址：%@",url.playUrl);
        }
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        @strongify(self);
        NSString *errinfo = [NSString stringWithFormat:@"push stream fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [self showAlertView:@"推流失败" message:errinfo];
    }];
}

// 停止推流
- (void)stopPushStream:(void(^)(void))block{
    @weakify(self);
    [[ILiveRoomManager getInstance] stopPushStreams:@[@(self.channelID)] succ:^{
        block ? block() : nil ;
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        @strongify(self);
        NSString *errinfo = [NSString stringWithFormat:@"stop push stream fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [self.endToastView dissmissToastView];
        [self showAlertView:@"停止推流失败" message:errinfo];
    }];
}

#pragma mark - ILVLiveIMListener

- (void)onTextMessage:(ILVLiveTextMessage *)msg{
    NSLog(@"%s",__func__);
}
- (void)onCustomMessage:(ILVLiveCustomMessage *)msg{
    NSLog(@"%s",__func__);
}
- (void)onOtherMessage:(TIMMessage *)msg{
    NSLog(@"%s",__func__);
}

#pragma mark - QAVLocalVideoDelegate

- (void)OnLocalVideoPreview:(QAVVideoFrame *)frameData{
    //仅仅是为了打log
    NSString *key = frameData.identifier;
    if (key.length == 0)
    {
        key = [[ILiveLoginManager getInstance] getLoginId];
    }
    if (!key.length) return;
    QAVFrameDesc *desc = [[QAVFrameDesc alloc] init];
    desc.width = frameData.frameDesc.width;
    desc.height = frameData.frameDesc.height;
    [_config.resolutionDic setObject:desc forKey:key];
}
- (void)OnLocalVideoPreProcess:(QAVVideoFrame *)frameData{
    
}
- (void)OnLocalVideoRawSampleBuf:(CMSampleBufferRef)buf result:(CMSampleBufferRef *)ret{
    
}

#pragma mark - QAVRemoteVideoDelegate
- (void)OnVideoPreview:(QAVVideoFrame *)frameData{
    NSString *key = frameData.identifier;
    QAVFrameDesc *desc = [[QAVFrameDesc alloc] init];
    desc.width = frameData.frameDesc.width;
    desc.height = frameData.frameDesc.height;
    [_config.resolutionDic setObject:desc forKey:key];
    
}

#pragma mark - initMethod/configUI

// 初始化ui
- (void)configUI{
    
//    UIImageView *roomBgImgView = [[UIImageView alloc] init];
//    [roomBgImgView by_setImageName:@"room_bg"];
//    [self.view addSubview:roomBgImgView];
//    _roomBgImgView = roomBgImgView;
//    [roomBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
    
    if (_config.isHost) {
//        [self addBBTMaskView];
        [self addHostLiveInfoView];
    }
    else{
        if (_model.status == BY_LIVE_STATUS_LIVING) {
            [self addHostLiveInfoView];
        }
    }

    
    if (!_config.isHost) {
        [self.view addSubview:self.bottomView];
        @weakify(self);
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.height.mas_equalTo(54);
            make.left.right.mas_equalTo(0);
            if (self.model.status == BY_LIVE_STATUS_END ||
                self.model.status == BY_LIVE_STATUS_OVERTIME) {
                make.bottom.mas_equalTo(-kSafe_Mas_Bottom(45));
            }else{
                make.bottom.mas_equalTo(-kSafe_Mas_Bottom(5));
            }
        }];
        [self addRepeortMarkView];
    }
    
    [self.view addSubview:self.chatView];
    @weakify(self);
    [_chatView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(0);
        if (self.config.isHost) {
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(30));
        }
        else {
            make.bottom.equalTo(self.bottomView.mas_top).with.offset(-20);
        }
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(250);
    }];
    
    // 添加倒计时view
    [self addTimerView];
    
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_config.isHost target:self];
    _opeartionView.liveId = _model.live_record_id;
    _opeartionView.hostId = _model.user_id;
    _opeartionView.hostName = _model.nickname;
    _opeartionView.didExitLiveBtnHandle = ^{
        @strongify(self);
        [self quitRoom];
//        [self backBtnAction];
    };
    _opeartionView.didShareActionHandle = ^{
        @strongify(self);
        if (self.didShareActionHandle) {
            self.didShareActionHandle();
        }
    };
    _opeartionView.didChangeBeautyHandle = ^(CGFloat beauty, CGFloat white) {
        [BYLiveConfig setBeauty:beauty*9];
        [BYLiveConfig setWhite:white*9];
        [[NSUserDefaults standardUserDefaults] setObject:@(beauty*9) forKey:kBeautyValue];
        [[NSUserDefaults standardUserDefaults] setObject:@(white*9) forKey:kWhiteValue];
    };
    [self.view addSubview:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (self.config.isHost) {
            make.width.mas_equalTo(33);
            make.height.mas_equalTo(268);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(kSafe_Mas_Top(0));
        }
        else
        {
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(53);
            make.height.mas_equalTo(130);
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(130));
        }
    }];
    
}

// 添加倒计时view
- (void)addTimerView{
    if (_timerLab) return;
    self.timerView = [UIView by_init];
    self.timerView.backgroundColor = [UIColor clearColor];
    self.timerView.layer.opacity = 0.0f;
    [self.view addSubview:self.timerView];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setTextColor:kColorRGBValue(0x292929)];
    [titleLab setBy_font:15];
    titleLab.text = @"直播倒计时";
    [self.timerView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 15));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, 15));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    _timerLab = [UILabel by_init];
    _timerLab.font = [UIFont systemFontOfSize:30];
    _timerLab.textColor = kColorRGBValue(0x292929);
    _timerLab.text = @"00 : 00 : 00 : 00";
    _timerLab.textAlignment = NSTextAlignmentCenter;
    //    self.timerLab.hidden = YES;
    [self.timerView addSubview:_timerLab];
    @weakify(self);
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(self.timerLab.text, 30));
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(25);
    }];
    
    UILabel *unitLab = [UILabel by_init];
    unitLab.textColor = kColorRGBValue(0x292929);
    [unitLab setBy_font:11];
    unitLab.text = @"天               时               分               秒";
    [self.timerView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(unitLab.text, 11));
        make.height.mas_equalTo(stringGetHeight(unitLab.text, 11));
        make.top.mas_equalTo(self.timerLab.mas_bottom).mas_offset(0);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.timerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-50);
        make.top.mas_equalTo(titleLab.mas_top).mas_offset(0);
        make.bottom.mas_equalTo(unitLab.mas_bottom).mas_offset(0);
    }];
}

- (void)addHostLiveInfoView{
    if (_hostInfoView) return;
    UIView *infoMaskView = [UIView by_init];
    infoMaskView.layer.cornerRadius = 16;
    infoMaskView.backgroundColor = kColorRGB(75, 75, 75, 0.4);
    [self.view addSubview:infoMaskView];
    _hostInfoView = infoMaskView;
    CGFloat width = _config.isHost ? 96 : 140;
    [infoMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(75));
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(32);
    }];
    // 主播头像
    PDImageView *logoImgView = [PDImageView by_init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    logoImgView.clipsToBounds = YES;
    [infoMaskView addSubview:logoImgView];
    _logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(28);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(3);
    }];
    [logoImgView addCornerRadius:14 size:CGSizeMake(28, 28)];
    [logoImgView uploadHDImageWithURL:_model.head_img callback:nil];
    
    // 主播时长
    UILabel *liveTimerLab = [UILabel by_init];
    [liveTimerLab setBy_font:10];
    liveTimerLab.textColor = [UIColor whiteColor];
    liveTimerLab.text = @"00:00";
    [infoMaskView addSubview:liveTimerLab];
    _liveTimerLab = liveTimerLab;
    [liveTimerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(4);
        make.left.mas_equalTo(35);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(10);
    }];
    
    // 观看人次
    UILabel *liveNumLab = [UILabel by_init];
    [liveNumLab setBy_font:10];
    liveNumLab.textColor = [UIColor whiteColor];
    liveNumLab.text = @"0人次";
    [infoMaskView addSubview:liveNumLab];
    _liveNumLab = liveNumLab;
    [liveNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(liveTimerLab.mas_bottom).with.offset(2);
        make.left.mas_equalTo(35);
        make.height.mas_equalTo(10);
        make.right.mas_equalTo(-5);
    }];
    
    if (!_config.isHost) {
        // 关注按钮
        UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
        NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"+ 关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xee4944),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        [attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
        [attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
        [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [infoMaskView addSubview:attentionBtn];
        self.attentionBtn = attentionBtn;
        attentionBtn.layer.cornerRadius = 12;
        attentionBtn.selected = _model.attention;
        [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-5);
            make.height.mas_equalTo(24);
            make.width.mas_equalTo(48);
            make.centerY.mas_equalTo(0);
        }];
    }
    [self reloadAttentionStatus];
}

- (void)addBBTMaskView{
    if (_bbtMaskView) return;
    UIView *bbtMaskView = [UIView by_init];
    bbtMaskView.layer.cornerRadius = 11;
    bbtMaskView.backgroundColor = kColorRGB(75, 75, 75, 0.4);
    [self.view addSubview:bbtMaskView];
    _bbtMaskView = bbtMaskView;
    [bbtMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(120));
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(22);
    }];
    
    // app图标
    UIImageView *iconImgView = [UIImageView by_init];
    [iconImgView setImage:[[UIImage imageNamed:@"common_icon_17"] addCornerRadius:8 size:CGSizeMake(16, 16)]];
    [bbtMaskView addSubview:iconImgView];
    [iconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(16);
        make.left.mas_equalTo(3);
        make.centerY.mas_equalTo(0);
    }];
    
    // bbt数量
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:12];
    bbtNumLab.textColor = [UIColor whiteColor];
    bbtNumLab.text = @"0";
    [bbtMaskView addSubview:bbtNumLab];
    _bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(26);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(14);
    }];
}

// 举报
- (void)addRepeortMarkView{
    if (_reportMarkView) return;
    
    // 举报
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = _model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    
    self.reportMaskView = [UIView by_init];
    self.reportMaskView.layer.opacity = 0.0f;
    [self.superController.view addSubview:self.reportMaskView];
    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
    
    self.reportMarkView = [[UIImageView alloc] init];
    [self.reportMarkView by_setImageName:@"common_report_double"];
    [self.superController.view addSubview:self.reportMarkView];
    self.reportMarkView.userInteractionEnabled = YES;
    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.reportMarkView.layer.shadowOpacity = 0.2;
    self.reportMarkView.layer.shadowRadius = 25;
    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
    self.reportMarkView.layer.opacity = 0.0f;
    @weakify(self);
    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-45);
        make.width.mas_equalTo(self.reportMarkView.image.size.width);
        make.height.mas_equalTo(self.reportMarkView.image.size.height);
    }];
    
    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    UIButton *upVideoBtn = [UIButton by_buttonWithCustomType];
    [upVideoBtn setBy_attributedTitle:@{@"title":@"申请连麦",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [upVideoBtn addTarget:self action:@selector(upVideoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:upVideoBtn];
    [upVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kColorRGBValue(0xededed);
    [self.reportMarkView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(0.5);
    }];
}

- (BYILiveRoomBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[BYILiveRoomBottomView alloc] initWithFathureView:self.view isHost:_config.isHost];
        @weakify(self);
        _bottomView.openLiveBtnHandle = ^{
            @strongify(self);
            [self openLiveAction];
        };
        _bottomView.moreBtnHandle = ^{
            @strongify(self);
            [self showReportMarkViewHidden:NO];
        };
    }
    return _bottomView;
}

- (BYILiveChatView *)chatView{
    if (!_chatView) {
        _chatView = [[BYILiveChatView alloc] initWithModel:self.model];
        _chatView.isHost = _config.isHost;
        @weakify(self);
        _chatView.didSurplusBBTHandle = ^(void) {
            @strongify(self);
            [self loadRequestGetSurplusBBT];
        };
        _chatView.didReceiveOpenLiveHandle = ^{
            @strongify(self);
            if (!self.config.isHost) {
                self.model.status = BY_LIVE_STATUS_LIVING;
                [self joinRoom];
            }
        };
    }
    return _chatView;
}

@end
