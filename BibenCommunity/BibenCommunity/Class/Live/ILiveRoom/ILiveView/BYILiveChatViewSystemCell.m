//
//  BYILiveChatViewSystemCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYILiveChatViewSystemCell.h"
#import "BYILiveChatViewCellModel.h"

@interface BYILiveChatViewSystemCell ()
@property (nonatomic ,strong) BYILiveChatViewCellModel *model;

@property (nonatomic ,strong) UIView *maskBgView;

/** 发表的信息 */
@property (nonatomic ,strong) UILabel *message;

@end

@implementation BYILiveChatViewSystemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYILiveChatViewCellModel *model = object[indexPath.row];
    _model = model;
    _message.text = model.message;
    [_message sizeToFit];
}

- (void)setContentView{
    UIView *maskBgView = [UIView by_init];
    [maskBgView setBackgroundColor:kColorRGB(183, 183, 183, 0.6)];
    maskBgView.layer.cornerRadius = 12;
    [self.contentView addSubview:maskBgView];
    _maskBgView = maskBgView;
    [maskBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.right.mas_lessThanOrEqualTo(-10);
    }];
    
    // 消息
    self.message = [UILabel by_init];
    _message.font = [UIFont systemFontOfSize:12];
    _message.textColor = kColorRGBValue(0xffffff);
    _message.numberOfLines = 0;
    [maskBgView addSubview:_message];
    [_message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(4);
        make.bottom.mas_equalTo(-4);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(12);
    }];
}

@end
