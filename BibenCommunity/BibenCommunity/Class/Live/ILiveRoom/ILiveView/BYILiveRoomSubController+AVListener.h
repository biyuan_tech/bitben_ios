//
//  BYILiveRoomSubController+AVListener.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYILiveRoomSubController.h"
#import <TILLiveSDK/TILLiveSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveRoomSubController (AVListener)<ILVLiveAVListener,ILiveRoomDisconnectListener,ILiveMemStatusListener>

@end

NS_ASSUME_NONNULL_END
