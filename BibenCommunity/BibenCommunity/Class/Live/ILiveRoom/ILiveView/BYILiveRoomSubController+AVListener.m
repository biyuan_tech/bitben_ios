//
//  BYILiveRoomSubController+AVListener.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYILiveRoomSubController+AVListener.h"
#import "BYIMManager.h"
#import "BYUpVideoCellModel.h"

@implementation BYILiveRoomSubController (AVListener)

- (void)onUserUpdateInfo:(ILVLiveAVEvent)event users:(NSArray *)users{
    switch (event)
    {
        case ILVLIVE_AVEVENT_CAMERA_ON:
        {
            [self onVideoType:QAVVIDEO_SRC_TYPE_CAMERA users:users];
        }
            break;
        case ILVLIVE_AVEVENT_CAMERA_OFF:
        {
            [self offVideoType:QAVVIDEO_SRC_TYPE_CAMERA users:users];
        }
            break;
        case ILVLIVE_AVEVENT_SCREEN_ON:
            [self onVideoType:QAVVIDEO_SRC_TYPE_SCREEN users:users];
            break;
        case ILVLIVE_AVEVENT_SCREEN_OFF:
            [self offVideoType:QAVVIDEO_SRC_TYPE_SCREEN users:users];
            break;
        case ILVLIVE_AVEVENT_MEDIA_ON:
            [self onVideoType:QAVVIDEO_SRC_TYPE_MEDIA users:users];
            break;
        case ILVLIVE_AVEVENT_MEDIA_OFF:
            [self offVideoType:QAVVIDEO_SRC_TYPE_MEDIA users:users];
            break;
        default:
            break;
    }
}

- (void)onFirstFrameRecved:(int)width height:(int)height identifier:(NSString *)identifier srcType:(avVideoSrcType)srcType{
    
}

- (BOOL)onRoomDisconnect:(int)reason{
    BYAlertView *alertView = [BYAlertView initWithTitle:@"房间失去连接" message:@"" inView:self.view alertViewStyle:BYAlertViewStyleAlert];
    [alertView showAnimation];
    return YES;
}

- (BOOL)onEndpointsUpdateInfo:(QAVUpdateEvent)event updateList:(NSArray *)endpoints{
    return YES;
}


- (void)onVideoType:(avVideoSrcType)type users:(NSArray *)users
{
    TILLiveManager *manager = [TILLiveManager getInstance];
    for (NSString *user in users) {
        if(![user isEqualToString:self.model.user_id]){
            [manager addAVRenderView:[RENDERVIEW_FRAMES[0] CGRectValue] forIdentifier:user srcType:type];
        }
        else{
//            ILiveRenderView *renderView = [BYIMManager sharedInstance].renderView;
//            renderView.autoRotate = NO;
//            renderView.rotateAngle = ILIVEROTATION_90;
        }
    }
    NSArray *allRenderView = [[TILLiveManager getInstance] getAllAVRenderViews];
    NSMutableArray *tmpRenderView = [NSMutableArray arrayWithArray:allRenderView];
    ILiveRenderView *renderView = [BYIMManager sharedInstance].renderView;
    if (renderView) {
        [tmpRenderView removeObject:renderView];
    }
    for (int i = 0; i < tmpRenderView.count; i ++) {
        ILiveRenderView *renderView = tmpRenderView[i];
        [UIView animateWithDuration:0.2 animations:^{
            if (!renderView) {
                [manager addAVRenderView:[RENDERVIEW_FRAMES[i] CGRectValue] forIdentifier:renderView.identifier srcType:renderView.srcType];
            }
            else{
                CGRect rect = [RENDERVIEW_FRAMES[i] CGRectValue];
                [renderView setFrame:rect];
            }
            if (!self.config.isHost) {
                [self.opeartionView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(tmpRenderView.count >= 2 ? -364 : -215);
                }];
                [self.view layoutIfNeeded];
            }
        }];
    }
}

- (void)offVideoType:(avVideoSrcType)type users:(NSArray *)users
{
    TILLiveManager *manager = [TILLiveManager getInstance];
    for (NSString *user in users) {
        if(![user isEqualToString:self.model.user_id]){ // 观众退出连麦
            [manager removeAVRenderView:user srcType:type];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotification_downVideo object:user];
        }
        else{  // 主播离开
       
            return;
        }
    }
    
    NSArray *allRenderView = [[TILLiveManager getInstance] getAllAVRenderViews];
    if (allRenderView.count == 1) {
        if (!self.config.isHost) {
            [self.opeartionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-130);
            }];
            [self.view layoutIfNeeded];
        }
        return;
    }
    NSMutableArray *tmpRenderView = [NSMutableArray arrayWithArray:allRenderView];
    ILiveRenderView *renderView = [BYIMManager sharedInstance].renderView;
    if (renderView) {
        [tmpRenderView removeObject:renderView];
    }
    for (int i = 0; i < tmpRenderView.count; i ++) {
        ILiveRenderView *renderView = tmpRenderView[i];
        [UIView animateWithDuration:0.2 animations:^{
            if (!renderView) {
                [manager addAVRenderView:[RENDERVIEW_FRAMES[i] CGRectValue] forIdentifier:renderView.identifier srcType:renderView.srcType];
            }
            else{
                CGRect rect = [RENDERVIEW_FRAMES[i] CGRectValue];
                [renderView setFrame:rect];
            }
            if (!self.config.isHost) {
                [self.opeartionView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(tmpRenderView.count >= 2 ? -364 : -215);
                }];
                [self.view layoutIfNeeded];
            }
        }];
    }
}

@end

