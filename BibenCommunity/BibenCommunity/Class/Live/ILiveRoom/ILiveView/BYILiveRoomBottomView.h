//
//  BYILiveRoomBottomView.h
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYILiveRoomBottomView : UIView



/**
 开始直播按钮回调
 */
@property (nonatomic ,copy) void (^openLiveBtnHandle)(void);

@property (nonatomic ,copy) void (^moreBtnHandle)(void);
/**
 初始化创建

 @param isHost 是否为主播
 @return return BYLiveRoomBottomView
 */
- (instancetype)initWithFathureView:(UIView *)fathureView isHost:(BOOL)isHost;
//+ (BYILiveRoomBottomView *)initWithIsHost:(BOOL)isHost inView:(UIView *)view;

@end
