//
//  BYILiveRoomSubController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYLiveConfig.h"
#import "BYILiveOpeartionView.h"
#import "BYILiveRoomSegmentView.h"

@class BYCommonLiveModel;
@interface BYILiveRoomSubController : BYCommonViewController

/** 配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 主播、用户操作台 */
@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;

/** 父Controller */
@property (nonatomic ,weak) UIViewController *superController;
@property (nonatomic ,copy) void(^didShareActionHandle)(void);
/** segment */
@property (nonatomic ,weak) BYILiveRoomSegmentView *segmentView;


- (void)quitRoom;
- (void)backBtnAction;
- (void)reloadGuestLivingStatus:(BOOL)isLiving;
@end
