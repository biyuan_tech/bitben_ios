//
//  BYVODPlayController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYVODPlayController.h"
#import "BYVODPlayViewModel.h"

@interface BYVODPlayController ()


@end

@implementation BYVODPlayController

- (void)dealloc
{
    
}
- (Class)getViewModelClass{
    return [BYVODPlayViewModel class];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationTitle = self.naTitle;
}

@end
