//
//  BYLiveForumView.m
//  BibenCommunity
//
//  Created by Belief on 2018/10/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveForumView.h"
#import "NetworkAdapter.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumRootSingleSubTableViewCell.h"
#import "LiveForumRootInputView.h"
#import "NetworkAdapter+PPTLive.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumDetailViewController.h"
#import "BYCommonPlayerView.h"
#import "BYPersonHomeController.h"
#import "BYLiveHomeRequest.h"

@interface BYLiveForumView ()<UITableViewDelegate,UITableViewDataSource>
{
    CGRect _newKeyboardRect;
    LiveFourumRootListSingleModel *_tempSingleModle;
}
@property (nonatomic,strong)BYCommonTableView *dianpingTableView;
@property (nonatomic,strong)NSMutableArray *dianpingMutableArr;
@property (nonatomic,strong)UIViewController *fathureController;
@property (nonatomic,strong)UIView *maskView;

@end
@implementation BYLiveForumView

- (void)dealloc
{
    [self.dianpingTableView dismissPrompt];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (instancetype)initWithFatureController:(UIViewController *)viewController isReport:(BOOL)isReport{
    self = [super init];
    if (self) {
        _isReport = isReport;
        _fathureController = viewController;
        [self setContentView];
    }
    return self;
}

- (void)setTransferRoomId:(NSString *)transferRoomId{
    _transferRoomId = transferRoomId;
    [self sendRequestToGetInfoWithReReload:NO];
}

- (void)setContentView{
    [self arrayWithInit];
    [self createInputView];
    [self createTableView];
    [self creatMaskView];
    [self createNotifi];                // 添加键盘代理
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dianpingMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.dianpingTableView){
        self.dianpingTableView = [[BYCommonTableView alloc] init];
        self.dianpingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.dianpingTableView.dataSource = self;
        self.dianpingTableView.delegate = self;
        [self addSubview:self.dianpingTableView];
        [self sendSubviewToBack:self.dianpingTableView];
        [_dianpingTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, [LiveForumRootInputView  calculationHeight:@""]-kSafeAreaInsetsBottom, 0));
        }];
    }
    __weak typeof(self)weakSelf = self;
    [self.dianpingTableView addHeaderRefreshHandle:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:YES];
    }];

    [self.dianpingTableView addFooterRefreshHandle:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:NO];
    }];

//    [self addSubview:self.inputView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dianpingMutableArr.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:section];
    return singleModel.child.count + 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LiveForumRootSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LiveForumRootSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferInfoManager = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowOne.transferInfoManager.user_id;
            if (strongSelf.viewController) {
                [strongSelf.viewController.navigationController pushViewController:personHomeController animated:YES];
            }
        }];
        return cellWithRowOne;
    } else if (indexPath.row == rowCount - 1){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        LiveForumItemsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowOne){
            cellWithRowOne = [[LiveForumItemsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowOne.transferItemId = self.transferRoomId;
        cellWithRowOne.transferType = LiveForumDetailViewControllerTypeNormal;
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferInfoManager = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne itemsReplyManagerWithModel:^(LiveFourumRootListSingleModel *singleModle) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->_tempSingleModle = singleModel;
            strongSelf.inputView.transferListModel = singleModel;
            
            if (![strongSelf.inputView.inputView isFirstResponder]){
                [strongSelf.inputView.inputView becomeFirstResponder];
            }
            strongSelf.inputView.inputView.text = @"";
            strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModel.comment_name];
        }];
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LiveForumRootSingleSubTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LiveForumRootSingleSubTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowTwo.transferCount = singleModel.childCount <= 3 ? -1:singleModel.childCount;
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeTop;
            } else if (indexPath.row == singleModel.child.count){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeBottom;
            } else {
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeNormal;
            }
        } else {
            cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeSingle;
        }
        cellWithRowTwo.transferInfoManager = [singleModel.child objectAtIndex:index];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_inputView.inputView isFirstResponder]) {
        [_inputView releaseKeyboard];
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    if (indexPath.row == 0){
        LiveForumDetailViewController *controller = [[LiveForumDetailViewController alloc]init];
        controller.transferFourumRootModel = singleModel;
        controller.transferRoomId = self.transferRoomId;
        [_viewController.navigationController pushViewController:controller animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row == 0){
        return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:singleModel];
    } else if (indexPath.row == rowCount - 1){
        return [LiveForumItemsTableViewCell calculationCellHeight];
    } else {
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeTop];
            } else if (indexPath.row == singleModel.child.count){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeBottom];
            } else {
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeNormal];
            }
        } else {
            return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeSingle];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"E4E4E4"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return .5f;
    }
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        
        self.inputView = [[LiveForumRootInputView alloc] initWithFrame:CGRectZero isReport:_isReport];
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        self.inputView.inputView.returnKeyType = UIReturnKeySend;
        if (_fathureController) {
            [_fathureController.view addSubview:self.inputView];
            [_fathureController.view bringSubviewToFront:self.inputView];
        }
        else{
            [self addSubview:self.inputView];
            [self bringSubviewToFront:self.inputView];
        }
        [_inputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(kSafe_Mas_Bottom(0));
            make.height.mas_equalTo([LiveForumRootInputView  calculationHeight:@""]);
        }];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.inputView.inputView resignFirstResponder];
        [strongSelf sendRequestToSendInfo];
    }];
    [self.inputView.inputView textViewSendActionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.inputView.inputView resignFirstResponder];
        [strongSelf sendRequestToSendInfo];
    }];
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [_inputView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text]);
        }];
        
    }];
}

- (void)creatMaskView{
    if (!_maskView) {
        self.maskView = [UIView by_init];
        self.maskView.hidden = YES;
        self.maskView.backgroundColor = [UIColor clearColor];
        if (_fathureController) {
            [_fathureController.view addSubview:_maskView];
            [_fathureController.view insertSubview:_maskView belowSubview:_inputView];
        }
        else{
            [self addSubview:_maskView];
            [self insertSubview:_maskView belowSubview:_inputView];
        }
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(maskViewAction)];
        [self.maskView addGestureRecognizer:tapGestureRecognizer];
    }
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    [self.inputView releaseKeyboard];
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    [self.inputView releaseKeyboard];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_scrollViewDidScrollView) {
        _scrollViewDidScrollView(scrollView);
    }
}

- (void)maskViewAction{
    [_inputView releaseKeyboard];
}

#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    if (!_inputView.inputView.isFirstResponder) return;
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self convertRect:keyboardRect fromView:nil];
    CGFloat detal = self.needAdaptiveBottom ? kSafeAreaInsetsBottom : 0;
    CGFloat detal1 = IS_PhoneXAll ? LCFloat(20) : 0;
    [UIView animateWithDuration:0.1 animations:^{
        [_inputView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-keyboardRect.size.height + detal + detal1);
        }];
        [self layoutIfNeeded];
    }];
 
    _newKeyboardRect = keyboardRect;
    _maskView.hidden = NO;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    [_inputView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(kSafe_Mas_Bottom(0));
    }];
    [UIView commitAnimations];
    [self layoutIfNeeded];
    _maskView.hidden = YES;
}

- (void)keyBoardDidShow:(NSNotification *)notification{
    if (!_inputView.inputView.isFirstResponder) return;
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self convertRect:keyboardRect fromView:nil];
    CGFloat detal = self.needAdaptiveBottom ? kSafeAreaInsetsBottom : 0;
    CGFloat detal1 = IS_PhoneXAll ? LCFloat(20) : 0;
    [UIView animateWithDuration:0.1 animations:^{
        [_inputView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-keyboardRect.size.height + detal + detal1);
        }];
        [self layoutIfNeeded];
    }];
    _newKeyboardRect = keyboardRect;
    _maskView.hidden = NO;
}

- (void)removeNotic{
    [_inputView removeFromSuperview];
    _inputView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

#pragma mark  - Interface
-(void)sendRequestToGetInfoWithReReload:(BOOL)rereload{
    NSInteger page = rereload ?0:self.dianpingTableView.currentPage;
    NSDictionary *params = @{@"theme_id":self.transferRoomId,@"theme_type":@"0",@"reply_comment_id":@"0",@"page_number":@(page),@"page_size":@"10"};
    __weak typeof(self)weakSelf = self;
    
    [[BYLiveHomeRequest alloc] loadRequestPageComment:params successBlock:^(id object) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
            self.dianpingTableView.currentPage ++;
            if (strongSelf.dianpingTableView.isXiaLa || rereload){
                [strongSelf.dianpingMutableArr removeAllObjects];
            }
        LiveFourumRootListModel *listModel = [[LiveFourumRootListModel alloc] init];
        if ([object[@"content"] isKindOfClass:[NSArray class]]) {
            if ([object[@"content"] count]) {
                listModel.content = [[LiveFourumRootListSingleModel mj_objectArrayWithKeyValuesArray:object[@"content"]] copy];
                for (LiveFourumRootListSingleModel *model in listModel.content) {
                    if (model.child.count) {
                        model.child = [[LiveFourumRootListSingleModel mj_objectArrayWithKeyValuesArray:model.child] copy];
                    }
                }
            }
        }
        [strongSelf.dianpingMutableArr addObjectsFromArray:listModel.content];
        
        for (int i = 0; i <strongSelf.dianpingMutableArr.count;i++){
            LiveFourumRootListSingleModel *singleModel = [strongSelf.dianpingMutableArr objectAtIndex:i];
            if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
                LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
                childModel.reply_comment_name = @"共同NNNNN条回复";
                childModel.childCount = singleModel.childCount;
                NSMutableArray *childRootArr = [NSMutableArray array];
                [childRootArr addObjectsFromArray:singleModel.child];
                [childRootArr addObject:childModel];
                singleModel.child = [childRootArr copy];
            }
        }
        
        [strongSelf.dianpingTableView reloadData];
        
        if (strongSelf.dianpingMutableArr.count){
            [strongSelf.dianpingTableView dismissPrompt];
        } else {
            [strongSelf.dianpingTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
            //            [strongSelf.dianpingTableView stopPullToRefresh];
            //            [strongSelf.dianpingTableView stopFinishScrollingRefresh];
        [strongSelf.dianpingTableView endRefreshing];
    } faileBlock:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.dianpingTableView endRefreshing];
    }];
//    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_comment" requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            self.dianpingTableView.currentPage ++;
//            if (strongSelf.dianpingTableView.isXiaLa || rereload){
//                [strongSelf.dianpingMutableArr removeAllObjects];
//            }
//            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
//            [strongSelf.dianpingMutableArr addObjectsFromArray:listModel.content];
//
//            for (int i = 0; i <strongSelf.dianpingMutableArr.count;i++){
//                LiveFourumRootListSingleModel *singleModel = [strongSelf.dianpingMutableArr objectAtIndex:i];
//                if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
//                    LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
//                    childModel.reply_comment_name = @"共同NNNNN条回复";
//                    childModel.childCount = singleModel.childCount;
//                    NSMutableArray *childRootArr = [NSMutableArray array];
//                    [childRootArr addObjectsFromArray:singleModel.child];
//                    [childRootArr addObject:childModel];
//                    singleModel.child = [childRootArr copy];
//                }
//            }
//
//            [strongSelf.dianpingTableView reloadData];
//
//            if (strongSelf.dianpingMutableArr.count){
//                [strongSelf.dianpingTableView dismissPrompt];
//            } else {
//                [strongSelf.dianpingTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
//            }
////            [strongSelf.dianpingTableView stopPullToRefresh];
////            [strongSelf.dianpingTableView stopFinishScrollingRefresh];
//        }
//        [strongSelf.dianpingTableView endRefreshing];
//    }];
}

#pragma mark - 发送信息
-(void)sendRequestToSendInfo{
    NSString *string = [self.inputView.inputView.text removeBothEndsEmptyString:BYRemoveTypeAll];
    __weak typeof(self)weakSelf = self;
    if (!string.length) return;
    [[BYLiveHomeRequest alloc] loadRequestCreateCommentWithThemeId:self.transferRoomId type:LiveForumDetailViewControllerTypeNormal themeInfo:self.inputView.transferListModel content:string successBlock:^(id object) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.transferListModel = nil;
        [strongSelf sendRequestToGetInfoWithReReload:YES];
        [strongSelf.inputView cleanKeyboard];
    } faileBlock:^(NSError *error) {
        
    }];
//    [[NetworkAdapter sharedAdapter] liveCreateCommentWiththemeId:self.transferRoomId type:LiveForumDetailViewControllerTypeNormal ThemeInfo:self.inputView.transferListModel content:string block:^(BOOL isSuccessed) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            strongSelf.inputView.transferListModel = nil;
//            [strongSelf sendRequestToGetInfoWithReReload:YES];
//            [strongSelf.inputView cleanKeyboard];
//        }
//    }];
}


@end
