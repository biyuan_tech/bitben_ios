//
//  BYVODPlayViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYVODPlayViewModel.h"
#import "BYVODPlaySubController.h"
#import "BYVODPlayController.h"
#import "ShareRootViewController.h"
#import "BYPersonHomeController.h"

#import "BYLiveRoomSegmentView.h"
#import "BYILiveIntroView.h"
#import "BYCommonPlayerView.h"

#import "BYCommonLiveModel.h"

#import "BYLiveHomeRequest.h"

@interface BYVODPlayViewModel ()<BYLiveRoomSegmentViewDelegate,UIGestureRecognizerDelegate>
{
    CGFloat _lastOffsetY;
    BOOL _isRefreshMas;
}
@property (nonatomic ,strong) BYLiveRoomSegmentView *segmentView;
/** 简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;
/** 点播播放 */
@property (nonatomic ,strong) BYVODPlaySubController *vodPlaySubController;

@property (nonatomic ,strong) BYCommonLiveModel *model;

@end

#define K_VC ((BYVODPlayController *)self.S_VC)
@implementation BYVODPlayViewModel
- (void)dealloc
{
    [_vodPlaySubController reset];
    [_vodPlaySubController removeFromParentViewController];
    [_vodPlaySubController.view removeFromSuperview];
    [_introView removeFromSuperview];
    [_segmentView removeFromSuperview];
    _vodPlaySubController = nil;
    _introView = nil;
    _segmentView = nil;
}

- (void)viewWillAppear{
    [_vodPlaySubController.playerView resume];
}

- (void)viewWillDisappear{
    [_vodPlaySubController.playerView pause];
}

- (void)viewControllerPopAction{
//    if (![SuperPlayerWindow sharedInstance].isShowing) {
//        [self.vodPlaySubController.playerView resetPlayer];
//    }
//    [self.vodPlaySubController removeFromParentViewController];
//    self.vodPlaySubController = nil;
}

- (void)setContentView{
    @weakify(self);
    [K_VC rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_live_share"] barHltImage:nil action:^{
        @strongify(self);
        [self shareAction];
    }];
    [self addSegmentView];
    [self loadRequestGetDetail];
}

// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    [shareViewController showInView:S_VC];
}

#pragma mark - BYLiveRoomSegmentViewDelegate

- (void)by_segmentViewWillScrollToIndex:(NSInteger)index{
    [self.vodPlaySubController.view endEditing:YES];
    [self showHeaderViewAnimation:YES];
}

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.introView;
    }
    return self.vodPlaySubController.view;
}

- (void)subScrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
  
    CGFloat temOffsetY = _lastOffsetY;
    _lastOffsetY = offset.y;
   
    // 在刚刷新布局时不做滑动判断
    if (_isRefreshMas) {
        _isRefreshMas = NO;
        return;
    }

    if (temOffsetY >= 120) {
        [self showHeaderViewAnimation:NO];
    }
    else{
        [self showHeaderViewAnimation:YES];
    }
}

- (void)showHeaderViewAnimation:(BOOL)isShow{
    _isRefreshMas = YES;
    [S_V_NC setNavigationBarHidden:!isShow animated:YES];
    
    [S_V_VIEW updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.segmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(isShow ? 0 : -64 + kSafeAreaInsetsTop);
        }];
        [S_V_VIEW layoutIfNeeded];
    }];
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        self.vodPlaySubController.model = self.model;
        K_VC.navigationTitle = self.model.live_title;
        [self.introView reloadData:object];
        
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addSegmentView{
    self.segmentView = [[BYLiveRoomSegmentView alloc] initWithTitles:@"简介",@"点播", nil];
    _segmentView.delegate = self;
    _segmentView.defultSelIndex = 1;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYVODPlaySubController *)vodPlaySubController{
    if (!_vodPlaySubController) {
        _vodPlaySubController = [[BYVODPlaySubController alloc] init];
        _vodPlaySubController.model = _model;
        _vodPlaySubController.viewController = S_VC;
        @weakify(self);
        _vodPlaySubController.scrollViewDidScrollView = ^(UIScrollView * _Nonnull scrollView) {
            @strongify(self);
            [self subScrollViewDidScroll:scrollView];
        };
    }
    return _vodPlaySubController;
}

- (BYILiveIntroView *)introView{
    if (!_introView) {
        _introView = [[BYILiveIntroView alloc] init];
        @weakify(self);
        _introView.tapHeaderImgHandle = ^{
            @strongify(self);
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = self.model.user_id;
            [S_V_NC pushViewController:personHomeController animated:YES];
        };
    }
    return _introView;
}
@end
