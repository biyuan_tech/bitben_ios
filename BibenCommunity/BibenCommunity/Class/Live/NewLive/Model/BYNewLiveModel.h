//
//  BYNewLiveModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"
#import "BYLiveDefine.h"

@interface BYNewLiveModel : BYCommonModel

/** 直播主题 */
@property (nonatomic ,strong) NSString *liveTheme;

/**
 开始之间
 */
@property (nonatomic ,strong) NSString *beginTime;

/**
 直播形式
 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE liveType;


@property (nonatomic ,strong) NSArray *tableData;

@end
