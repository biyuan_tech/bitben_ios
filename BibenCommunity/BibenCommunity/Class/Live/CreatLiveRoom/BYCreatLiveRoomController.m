//
//  BYCreatLiveRoomController.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomController.h"
#import "BYCreatLiveRoomViewModel.h"

@interface BYCreatLiveRoomController ()

@end

@implementation BYCreatLiveRoomController

- (Class)getViewModelClass{
    return [BYCreatLiveRoomViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"创建直播间";
    self.titleColor = kNaTitleColor_53;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
