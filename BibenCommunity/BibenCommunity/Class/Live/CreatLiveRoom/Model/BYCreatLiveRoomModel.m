//
//  BYCreatLiveRoomModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomModel.h"
#import "BYCreatLiveRoomCellModel.h"

@implementation BYCreatLiveRoomModel

- (NSArray *)tableData{
    NSMutableArray *tmpArr = [NSMutableArray array];
    for (int i = 0; i < 7; i ++) {
        BYCreatLiveRoomCellModel *model = [[BYCreatLiveRoomCellModel alloc] init];
        if (i == 0 || i == 2|| i == 4) { // 创建标题类cell
            model.cellString = @"BYCreatLiveRoomTitleCell";
            model.cellHeight = 40;
            switch (i) {
                case 0:
                    model.title = @"直播间主题";
                    break;
                case 2:
                    model.title = @"可联系的微信";
                    break;
                case 4:
                    model.title = @"选择您的身份";
                    break;
                default:
                    break;
            }
        }
        
        if (i == 1 || i == 3) { // 创建输入内容cell
            model.cellString = @"BYCreatLiveRoomTextFieldCell";
            model.cellHeight = 48;
            switch (i) {
                case 1:
                    model.placeholder = @"请输入直播间名称";
                    break;
                case 3:
                    model.placeholder = @"请输入微信号...";
                    break;
                default:
                    break;
            }
        }
        
        if (i == 5) { // 创建选择身份cell
            model.cellString = @"BYCreatLiveRoomIdentityCell";
            model.cellHeight = 118;
        }
        if (i == 6) { // footer部分
            model.cellString = @"BYCreatLiveRoomFooterCell";
            model.cellHeight = 260;
        }
        [tmpArr addObject:model];
    }
    return tmpArr;
}

@end
