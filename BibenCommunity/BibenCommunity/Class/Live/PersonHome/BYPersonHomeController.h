//
//  BYPersonHomeController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPersonHomeController : BYCommonViewController

/** user_id */
@property (nonatomic ,copy) NSString *user_id;


@end

NS_ASSUME_NONNULL_END
