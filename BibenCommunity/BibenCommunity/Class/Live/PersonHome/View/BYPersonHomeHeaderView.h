//
//  BYPersonHomeHeaderView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYPersonHomeHeaderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPersonHomeHeaderView : UIView

/** user_id */
@property (nonatomic ,copy) NSString *user_id;

- (void)reloadData:(BYPersonHomeHeaderModel *)model;

@end

NS_ASSUME_NONNULL_END
