//
//  BYPersonHomeHeaderView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonHomeHeaderView.h"
#import "BYLiveHomeRequest.h"

@interface BYPersonHomeHeaderView ()

/** 头像 */
@property (nonatomic ,strong) PDImageView *headerImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 签名 */
@property (nonatomic ,strong) UILabel *signLab;
/** 关注数 */
@property (nonatomic ,strong) UILabel *attentionNumLab;
/** 粉丝数 */
@property (nonatomic ,strong) UILabel *fansNumLab;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionStatus;



@end

@implementation BYPersonHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BYPersonHomeHeaderModel *)model{
    [self.headerImgView uploadMainImageWithURL:model.head_img
                                   placeholder:nil
                                       imgType:PDImgTypeOriginal
                                      callback:nil];
    self.userNameLab.text = nullToEmpty(model.nickname);
    self.signLab.text = nullToEmpty(model.self_introduction);
    self.attentionNumLab.text = nullToEmpty(model.attention);
    self.fansNumLab.text = nullToEmpty(model.fans);
    self.attentionStatus.selected = model.isAttention;
    [self reloadAttentionStatus:model.isAttention];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionStatus.selected = isAttention;
    self.attentionStatus.backgroundColor = isAttention ? kColorRGBValue(0xDD694F) : [UIColor whiteColor];
}

#pragma mark - buttonAction
- (void)attentionStatusAction:(UIButton *)sender{
    [self loadRequestAttention:!sender.selected];
}

#pragma makr - request
- (void)loadRequestAttention:(BOOL)isAttention{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:self.user_id isAttention:isAttention successBlock:^(id object) {
        @strongify(self);
        [self reloadAttentionStatus:isAttention];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)setContentView{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, kCommonScreenWidth, 114 + kNavigationHeight);
    gradient.colors = @[(id)kColorRGBValue(0xed6145).CGColor,(id)kColorRGBValue(0xed4a45).CGColor];
    gradient.startPoint = CGPointMake(0.5, 0);
    gradient.endPoint = CGPointMake(0.5, 1);
    [self.layer addSublayer:gradient];
    
    // 关注按钮
    self.attentionStatus = [UIButton by_buttonWithCustomType];
    [self.attentionStatus setBy_attributedTitle:@{@"title":@"+ 关注",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0xee4944),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                  } forState:UIControlStateNormal];
    [self.attentionStatus setBy_attributedTitle:@{@"title":@"已关注",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                  } forState:UIControlStateSelected];
    [self.attentionStatus addTarget:self
                             action:@selector(attentionStatusAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    self.attentionStatus.layer.cornerRadius = 5.0f;
    self.attentionStatus.layer.borderColor = [UIColor whiteColor].CGColor;
    self.attentionStatus.layer.borderWidth = 1.0f;
    [self addSubview:self.attentionStatus];
    [self.attentionStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(26);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-68);
    }];
    [self reloadAttentionStatus:NO];
    
    UIView *headerImgBgView = [UIView by_init];
    [headerImgBgView setBackgroundColor:[UIColor whiteColor]];
    headerImgBgView.layer.cornerRadius = 33.0f;
    [self addSubview:headerImgBgView];
    [headerImgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(66.0f);
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-35);
    }];
    
    // 头像
    self.headerImgView = [[PDImageView alloc] init];
    self.headerImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.headerImgView.backgroundColor = [UIColor clearColor];
    self.headerImgView.layer.cornerRadius = 29;
    self.headerImgView.clipsToBounds = YES;
    [self addSubview:self.headerImgView];
    [self.headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(19);
        make.width.height.mas_equalTo(58);
        make.bottom.mas_equalTo(-39);
    }];
    
    // 昵称
    self.userNameLab = [UILabel by_init];
    self.userNameLab.font = [UIFont systemFontOfSize:15];
    self.userNameLab.textColor = [UIColor whiteColor];
    [self addSubview:self.userNameLab];
    @weakify(self);
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
        make.right.mas_equalTo(self.attentionStatus.mas_left).mas_offset(-35);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.headerImgView.mas_top).mas_offset(-5);
    }];
    
    // 签名
    self.signLab = [UILabel by_init];
    self.signLab.textColor = [UIColor whiteColor];
    self.signLab.numberOfLines = 2;
    [self.signLab setBy_font:12];
    [self addSubview:self.signLab];
    [self.signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
        make.top.mas_equalTo(self.userNameLab.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(self.attentionStatus.mas_left).mas_offset(-25);
        make.height.mas_greaterThanOrEqualTo(12);
    }];
    
    UILabel *attentionLab = [UILabel by_init];
    attentionLab.text = @"关注";
    attentionLab.textColor = [UIColor whiteColor];
    [attentionLab setBy_font:12];
    [self addSubview:attentionLab];
    [attentionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
        make.top.mas_equalTo(self.signLab.mas_bottom).mas_offset(15);
        make.height.mas_equalTo(stringGetHeight(attentionLab.text, 12));
        make.width.mas_equalTo(stringGetWidth(attentionLab.text, 12));
    }];
    
    // 关注数
    self.attentionNumLab = [UILabel by_init];
    self.attentionNumLab.textColor = [UIColor whiteColor];
    [self.attentionNumLab setBy_font:13];
    [self addSubview:self.attentionNumLab];
    [self.attentionNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(attentionLab.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(attentionLab.mas_height).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = [UIColor whiteColor];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(10);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(self.attentionNumLab.mas_right).mas_offset(15);
    }];
    
    UILabel *fansLab = [UILabel by_init];
    fansLab.text = @"粉丝";
    fansLab.textColor = [UIColor whiteColor];
    [fansLab setBy_font:12];
    [self addSubview:fansLab];
    [fansLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(fansLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(fansLab.text, 12));
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(lineView.mas_right).mas_offset(15);
    }];
    
    // 粉丝数
    self.fansNumLab = [UILabel by_init];
    self.fansNumLab.textColor = [UIColor whiteColor];
    [self.fansNumLab setBy_font:13];
    [self addSubview:self.fansNumLab];
    [self.fansNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(fansLab.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(fansLab.mas_height).mas_offset(0);
    }];
}


@end
