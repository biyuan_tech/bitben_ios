//
//  BYPersonArticleSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonArticleSubController.h"
#import "NetworkAdapter+Article.h"
#import "ArticleDetailRootViewController.h"
#import "ArticleRootSingleModel.h"
#import "ArticleRootTableViewCell.h"
#import "ArticleRootActionTableViewCell.h"

@interface BYPersonArticleSubController ()<UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;


@end

@implementation BYPersonArticleSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setContentView];
    [self loadRequestArticle];
    // Do any additional setup after loading the view.
}

#pragma mark - UITableViewDelegate/DataScoure
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableView.tableData.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        ArticleRootSingleModel *singleModel = [self.tableView.tableData objectAtIndex:indexPath.section];
        cellWithRowOne.transferSingleModel = singleModel;
        [cellWithRowOne hiddenLinkBtn];
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf tableView:tableView didSelectRowAtIndexPath:indexPath];
        }];
        
        [cellWithRowOne actionClickWithTagsSelectedBlock:^(NSString *tag) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
            articleRootVC.transferTags = tag;
            [strongSelf.superController.navigationController pushViewController:articleRootVC animated:YES];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ArticleRootActionTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleRootActionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        ArticleRootSingleModel *singleModel = [self.tableView.tableData objectAtIndex:indexPath.section];
        cellWithRowTwo.transferSingleModel = singleModel;
        return cellWithRowTwo;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
    ArticleRootSingleModel *singleModel = [self.tableView.tableData objectAtIndex:indexPath.section];
    
    __weak typeof(self)weakSelf = self;
    [viewController actionReoloadFollow:^(BOOL link) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 遍历所有的Model 只要是同一个作者的都备注已关注
        for (int i = 0 ; i < strongSelf.tableView.tableData.count;i++){
            ArticleRootSingleModel *tempSingleModel = [strongSelf.tableView.tableData objectAtIndex:i];
            if ([tempSingleModel.author_id isEqualToString:singleModel.author_id]){
                tempSingleModel.isAttention = link;
            }
        }
        [strongSelf.tableView reloadData];
    }];
    
    viewController.transferArticleModel = singleModel;
    [self.superController.navigationController pushViewController:viewController animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        ArticleRootSingleModel *singleModel = [self.tableView.tableData objectAtIndex:indexPath.section];
        return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
    } else {
        return LCFloat(50);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.tableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.row == 0){
            separatorType  = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - request
- (void)loadRequestArticle{
    @weakify(self);
    [[NetworkAdapter sharedAdapter] articleListWithPage:self.pageNum topic:nil  authorId:self.user_id block:^(NSArray<ArticleRootSingleModel> *articleList) {
        @strongify(self);
        if (self.pageNum == 0) {
            self.tableView.tableData = articleList;
            if (!articleList.count) {
                [self.tableView addEmptyView:@"暂无文章"];
            }else{
                [self.tableView removeEmptyView];
            }
        }
        else{
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [array addObjectsFromArray:articleList];
            self.tableView.tableData = [array copy];
        }
        [self.tableView endRefreshing];
        self.pageNum ++;
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.pageNum = 0;
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    @weakify(self);
    [self.tableView addHeaderRefreshHandle:^{
        @strongify(self);
        self.pageNum = 0;
        [self loadRequestArticle];
        
    }];
    [self.tableView addFooterRefreshHandle:^{
        @strongify(self);
        [self loadRequestArticle];
    }];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
