//
//  BYPersonLiveSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonLiveSubController.h"
#import "BYLiveHomeRequest.h"
#import "BYLiveRPCDefine.h"
#import "BYLiveHomeVideoModel.h"

#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"
#import "BYVideoLiveController.h"

@interface BYPersonLiveSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pagenum */
@property (nonatomic ,assign) NSInteger pageNum;


@end

@implementation BYPersonLiveSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setContentView];
    [self loadRequestLive];
    // Do any additional setup after loading the view.
}

#pragma mark - BYCommonTableViewDelegate
#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BYLiveHomeVideoModel *model = tableView.tableData[indexPath.row];
    switch (model.live_type) {
        case BY_NEWLIVE_TYPE_ILIVE:
        {
            BYLiveConfig *config = [[BYLiveConfig alloc] init];
            config.isHost = NO;
            BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
            ilivewRoomController.live_record_id = model.live_record_id;
            [self.superController.navigationController pushViewController:ilivewRoomController animated:YES];
//            [S_VC authorizePush:ilivewRoomController animation:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
        {
            BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
            videoRoomController.introModel = model;
            [self.superController.navigationController pushViewController:videoRoomController animated:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        {
            BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
            pptRoomController.live_record_id = model.live_record_id;
            pptRoomController.isHost = NO;
            [self.superController.navigationController pushViewController:pptRoomController animated:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_VOD_AUDIO:
        case BY_NEWLIVE_TYPE_VOD_VIDEO:
        {
            BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
            vodPlayController.live_record_id = model.live_record_id;
            vodPlayController.naTitle = model.live_title;
            [self.superController.navigationController pushViewController:vodPlayController animated:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
        {
            BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
            videoLiveController.live_record_id = model.live_record_id;
            videoLiveController.isHost = NO;
            [self.superController.navigationController pushViewController:videoLiveController animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYLiveHomeVideoModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"appointAction"]) { // 关注
        if (model.attention) {
            [self loadRequestCancelAttentionLive:model tableView:tableView indexPath:indexPath];
            return;
        }
        [self loadRequestAttentionLive:model tableView:tableView indexPath:indexPath];
    }
}

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *view = [UIView by_init];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    CGFloat maximumOffset = size.height;
    
    // 滑动区域小于展示y区域或滑动区域大于展示区域不超过30
    if (maximumOffset < (bounds.size.height + inset.bottom) || maximumOffset - (bounds.size.height + inset.bottom) < 30) {
        return;
    }
    
//    if (scrollView.contentOffset.y >= 120) {
//        [self showHeaderViewAnimation:NO];
//    }
//    else{
//        [self showHeaderViewAnimation:YES];
//    }
    
}

//- (void)showHeaderViewAnimation:(BOOL)isShow{
//    [ updateConstraintsIfNeeded];
//    [UIView animateWithDuration:0.2 animations:^{
//        [self.headerView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(isShow ? 0 : -195);
//        }];
//        [S_V_VIEW layoutIfNeeded];
//    }];
//}

#pragma mark - request
- (void)loadRequestLive{
    @weakify(self);
    NSDictionary *param = @{@"owner_id":_user_id,@"page_number":@(self.pageNum),@"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        @strongify(self);
        NSMutableArray *tableData = [NSMutableArray array];
        if ([result[@"content"] count]) {
            NSArray *tmpArr = (NSArray *)result[@"content"];
            [tmpArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:dic];
                [tableData addObject:model];
            }];
        }
        if (self.pageNum == 0) {
            self.tableView.tableData = tableData;
            if (!tableData.count) {
                [self.tableView addEmptyView:@"暂无直播"];
            }else{
                [self.tableView removeEmptyView];
            }
        }else{
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpArr addObjectsFromArray:tableData];
            self.tableView.tableData = [tmpArr copy];
        }
        [self.tableView endRefreshing];
        self.pageNum ++;

    } failureBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

- (void)loadRequestAttentionLive:(BYLiveHomeVideoModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestAttentionLive:model.live_record_id successBlock:^(id object) {
        BYLiveHomeVideoModel *model = tableView.tableData[indexPath.row];
        model.attention = [object boolValue];
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestCancelAttentionLive:(BYLiveHomeVideoModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestCancelAttentionLive:model.live_record_id successBlock:^(id object) {
        BYLiveHomeVideoModel *model = tableView.tableData[indexPath.row];
        model.attention = [object boolValue];
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    @weakify(self);
    [self.tableView addHeaderRefreshHandle:^{
        @strongify(self);
        self.pageNum = 0;
        [self loadRequestLive];
    }];
    [self.tableView addFooterRefreshHandle:^{
        @strongify(self);
        [self loadRequestLive];
    }];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
