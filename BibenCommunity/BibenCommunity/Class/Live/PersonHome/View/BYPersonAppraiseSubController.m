//
//  BYPersonAppraiseSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonAppraiseSubController.h"
#import "ArticleDetailRootViewController.h"
#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"

#import "BYLiveHomeRequest.h"
#import "BYAppraiseModel.h"

@interface BYPersonAppraiseSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYPersonAppraiseSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setContentView];
    [self loadRequestAppraise];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYAppraiseModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"themeAction"]) {
        if (model.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
            ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
            viewController.transferArticleModel = model.articleModel;
            [self.superController.navigationController pushViewController:viewController animated:YES];
        }
        else if (model.theme_type == BY_THEME_TYPE_LIVE){ // 跳直播
            switch (model.liveModel.live_type) {
                case BY_NEWLIVE_TYPE_ILIVE:
                {
                    BYLiveConfig *config = [[BYLiveConfig alloc] init];
                    config.isHost = NO;
//                    switch (model.liveModel.status) {
//                        case BY_LIVE_STATUS_END:
//                        case BY_LIVE_STATUS_OVERTIME:
//                        {
//                            config.isVOD = YES;
////                            config.video_url = model.liveModel.courseware[0];
//                        }
//                            break;
//                        case BY_LIVE_STATUS_WAITING:
//                            return;
//                            break;
//                        default:
//                            break;
//                    }
                    BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                    ilivewRoomController.live_record_id = model.liveModel.live_record_id;
                    [self.superController.navigationController pushViewController:ilivewRoomController animated:YES];
                    //            [S_VC authorizePush:ilivewRoomController animation:YES];
                }
                    break;
                case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
                {
                    BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                    videoRoomController.introModel = model.liveModel;
                    [self.superController.navigationController pushViewController:videoRoomController animated:YES];
                }
                    break;
                case BY_NEWLIVE_TYPE_AUDIO_PPT:
                case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
                {
                    //                LivePPTRootViewController *pptRootViewController = [[LivePPTRootViewController alloc] init];
                    //                pptRootViewController.transferRoomId = model.live_record_id;
                    //                pptRootViewController.isHost = NO;
                    //                [S_VC authorizePush:pptRootViewController animation:YES];
                    BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
                    pptRoomController.live_record_id = model.liveModel.live_record_id;
                    pptRoomController.isHost = NO;
                    [self.superController.navigationController pushViewController:pptRoomController animated:YES];
                }
                    break;
                case BY_NEWLIVE_TYPE_VOD_AUDIO:
                case BY_NEWLIVE_TYPE_VOD_VIDEO:
                {
                    BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                    vodPlayController.live_record_id = model.liveModel.live_record_id;
                    vodPlayController.naTitle = model.liveModel.live_title;
                    [self.superController.navigationController pushViewController:vodPlayController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
    }
}

#pragma mark - request
- (void)loadRequestAppraise{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetMyComment:self.user_id pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无评论"];
            }else{
                [self.tableView removeEmptyView];
            }
        }else{
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpArr addObjectsFromArray:object];
            self.tableView.tableData = [tmpArr copy];
        }
        self.pageNum ++;
        [self.tableView endRefreshing];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    @weakify(self);
    [self.tableView addHeaderRefreshHandle:^{
        @strongify(self);
        self.pageNum = 0;
        [self loadRequestAppraise];
    }];
    [self.tableView addFooterRefreshHandle:^{
        @strongify(self);
        [self loadRequestAppraise];
    }];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}


@end
