//
//  BYPersonHomeHeaderModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPersonHomeHeaderModel : NSObject

/** 头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 会员 */
@property (nonatomic ,assign) BOOL vip;
/** 简介 */
@property (nonatomic ,copy) NSString *self_introduction;
/** 会员级别 */
@property (nonatomic ,copy) NSString *level;
/** 关注数 */
@property (nonatomic ,copy) NSString *attention;
/** 粉丝数 */
@property (nonatomic ,copy) NSString *fans;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;

- (BYPersonHomeHeaderModel *)getHeaderData:(NSDictionary *)respondDic;

@end

NS_ASSUME_NONNULL_END
