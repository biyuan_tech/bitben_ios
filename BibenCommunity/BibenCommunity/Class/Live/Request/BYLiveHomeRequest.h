//
//  BYLiveHomeRequest.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYRequestManager.h"
#import "BYLiveConfig.h"
#import "BYLiveDefine.h"

@interface BYLiveHomeRequest : BYRequestManager

/**
 获取直播首页数据

 @param listType 分类列表
 @param pageNum 页码
 @param sortType 排序
 */
- (void)loadRequestLiveHome:(BY_LIVE_HOME_LIST_TYPE)listType
                    pageNum:(NSInteger)pageNum
                   sortType:(BY_SORT_TYPE)sortType
               successBlock:(void(^)(id object))successBlock
                 faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取首页推荐直播+感兴趣用户 */
- (void)loadRequestLiveHomeRecommendLive:(void(^)(id object))successBlock
                              faileBlock:(void(^)(NSError *error))faileBlock;

/**
 获取视频上传凭证

 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadGetUploadVodSignatureSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock;


/**
 上传视频成功后同步到服务端


 @param userId 用户id
 @param title 视频标标题
 @param videoUrl 视频连接
 @param videoId 视频id（腾讯云返回的filedId）
 @param coverUrl 封面图url
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)uploadVodInfoWithUserId:(NSInteger)userId
                          title:(NSString *)title
                       videoUrl:(NSString *)videoUrl
                        videoId:(NSString *)videoId
                       coverUrl:(NSString *)coverUrl
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock;


/**
 直播创建

 @param userId 用户id
 @param coverUrl 封面图
 @param title 标题
 @param message 内容
 @param beginTime 开始时间
 @param organType 组织形式
 @param liveType 直播形式
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestCreatLiveWithUserId:(NSInteger)userId
                              coverUrl:(NSString *)coverUrl
                                 title:(NSString *)title
                               message:(NSString *)message
                             beginTime:(NSString *)beginTime
                             organType:(BY_LIVE_ORGAN_TYPE)organType
                              liveType:(BY_LIVE_TYPE)liveType
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock;


/**
 更新直播状态

 @param status 直播状态
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock;

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                          stream_id:(NSString *)stream_id
                    real_begin_time:(NSString *)real_begin_time
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock;


/** 获取直播登录的sig凭证 */
- (void)loadRequestGetILiveLoginSigSuccessBlock:(void(^)(id object))successBlock
                                     faileBlock:(void(^)(NSError *error))faileBlock;
;
#pragma mark - new

/**
 创建直播间
 
 @param roomTitle 直播间主题
 @param weChat 联系微信
 @param organType 身份
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestCreatLiveRoom:(NSString *)roomTitle
                          weChat:(NSString *)weChat
                       organType:(BY_LIVE_ORGAN_TYPE)organType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/**
 新建直播

 @param title 直播主题
 @param beginTime 开始时间
 @param liveType 直播形式
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestNewLive:(NSString *)title
                 beginTime:(NSString *)beginTime
                  liveType:(BY_NEWLIVE_TYPE)liveType
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock;

/** 更新直播间信息 */
- (void)loadReuqestUpdateLiveRoom:(NSDictionary *)param
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock;

/** 更新直播信息 */
- (void)loadRequestUpdateLiveSet:(NSString*)liveId
                           param:(NSDictionary *)param
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取直播间信息 */
- (void)loadRequestGetLiveRoomInfoSuccessBlock:(void(^)(id object))successBlock
                                    faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取个人直播记录 */
- (void)loadRequestGetLivelist:(NSString *)userId
                       pageNum:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取首页banner数据 */
- (void)loadRequestGetHomeBannerSuccessBlock:(void(^)(id object))successBlock
                                  faileBlock:(void(^)(NSError *error))faileBlock;

/** 判断是否有直播间 */
- (void)loadReuqestIsHaveLiveRoomSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock;

/** 更新ppt直播间课件 */
- (void)loadRequestUpdateCourseware:(NSArray *)pptImgs
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock;

/**
 直播互动的顶踩

 @param type 操作类型
 @param theme_id 直播id
 @param receiver_user_id 被顶踩人id
 @param successBlock successBlock description
 @param faileBlock faileBlock description
 */
- (void)loadRequestLiveGuestOpearType:(BY_GUEST_OPERA_TYPE)type
                             theme_id:(NSString *)theme_id
                     receiver_user_id:(NSString *)receiver_user_id
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock;

/** 直播打赏 */
- (void)loadRequestReward:(NSString *)receicer_user_id
            reward_amount:(CGFloat)reward_amount
             successBlock:(void(^)(id object))successBlock
               faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取剩余bbt金额 */
- (void)loadRequestSurplusBBTSuccessBlock:(void(^)(id object))successBlock
                               faileBlock:(void(^)(NSError *error))faileBlock;

/** 心跳 */
- (void)loadRequestPostHeartBeat:(NSString *)live_record_id
                      virtual_pv:(NSInteger )virtual_pv
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取直播详情 */
- (void)loadRequestGetLiveDetail:(NSString *)live_record_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 直播关注 */
- (void)loadRequestAttentionLive:(NSString *)live_record_id
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock;

/** 取消关注 */
- (void)loadRequestCancelAttentionLive:(NSString *)live_record_id
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock;

/** 插入聊天记录 */
- (void)loadRequestInsertChatHistory:(NSString *)live_record_id
                             groupId:(NSString *)groupId
                           beginTime:(NSString *)begieTime
                        successBlock:(void(^)(id object))successBlock
                          faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取聊天记录 */
- (void)loadRequestGetChatHistory:(NSString *)live_record_id
                          groupId:(NSString *)groupId
                          pageNum:(NSInteger)pageNum
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock;

/** 举报 */
- (void)loadRequestReportThemeId:(NSString *)themeId
                       themeType:(BY_THEME_TYPE)themeType
                      reportType:(NSString *)reportType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 关注/取消关注他人 */
- (void)loadRequestCreatAttention:(NSString *)attention_id
                      isAttention:(BOOL)isAttention
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取他人主页 */
- (void)loadRequestGetPersonData:(NSString *)user_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock;

/** 查询个人点评 */
- (void)loadRequestGetMyComment:(NSString *)user_id
                        pageNum:(NSInteger)pageNum
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock;


/**
 直播混流
 @param streamer_id 主播直播码
 @param viewer_id 连麦观众直播码
 */
- (void)loadRequestMixStream:(NSString *)streamer_id
                   viewer_id:(NSString *)viewer_id
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock;

/** 推流地址获取 */
- (void)loadRequestGetPushURL:(NSString *)live_record_id
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock;

/** 获取聊天记录 */
- (void)loadRequestGetPageChatHistory:(NSString *)groupId
                              pageNum:(NSInteger)pageNum
                             sendTime:(NSString *)sendTime
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock;

/** banner点击统计 */
- (void)loadRequestClickBanner:(NSString *)bannerId;

/** 获取点评评论 */
- (void)loadRequestPageComment:(NSDictionary *)param
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock;

/** 发起点评评论 */
- (void)loadRequestCreateCommentWithThemeId:(NSString *)themeId
                                       type:(NSInteger)type
                                  themeInfo:(LiveFourumRootListSingleModel *)themeinfo
                                    content:(NSString *)content
                               successBlock:(void(^)(id object))successBlock
                                 faileBlock:(void(^)(NSError *error))faileBlock;

@end
