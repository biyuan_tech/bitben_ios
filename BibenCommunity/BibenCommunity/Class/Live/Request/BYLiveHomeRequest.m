//
//  BYLiveHomeRequest.m
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeRequest.h"
#import "BYLiveRPCDefine.h"

#import "BYLiveHomeVideoModel.h"
#import "BYCommonLiveModel.h"
#import "BYHomeBannerModel.h"
#import "BYPersonHomeHeaderModel.h"
#import "BYAppraiseModel.h"
#import "BYRecommendUserModel.h"
@implementation BYLiveHomeRequest

- (void)loadRequestLiveHome:(BY_LIVE_HOME_LIST_TYPE)listType
                    pageNum:(NSInteger)pageNum
                   sortType:(BY_SORT_TYPE)sortType
               successBlock:(void(^)(id object))successBlock
                 faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *sortValue = sortType == BY_SORT_TYPE_HOT ? @"watch_times" : (sortType == BY_SORT_TYPE_TIME ? @"begin_time" : @"");
    NSDictionary *param = @{@"list_type":@(listType),@"page_number":[NSNumber numberWithInteger:pageNum],@"page_size":@(10),@"sort_param":sortValue};
    [BYRequestManager ansyRequestWithURLString:RPC_get_homepage_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [[BYLiveHomeVideoModel alloc] getRespondData:result type:listType pageNum:pageNum];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestLiveHomeRecommendLive:(void(^)(id object))successBlock
                              faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_recommend_live parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSMutableArray *array = [NSMutableArray array];
        BYCommonLiveModel *recommendLive = [BYCommonLiveModel mj_objectWithKeyValues:result[@"live_result"]];
        recommendLive.cellString = @"BYRecommendLiveNoticeCell";
        recommendLive.cellHeight = 174;
        NSArray *recommendUser = [BYRecommendUserModel getRecommendUser:result[@"attention_users"]];
        [array addObject:@{@"data":@[recommendLive]}];
        [array addObject:@{@"data":recommendUser}];
        if (successBlock) successBlock(array);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadGetUploadVodSignatureSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_Get_upload_signature parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSString *sign = result[@"data"][@"sign"];
        if (successBlock) successBlock(sign);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)uploadVodInfoWithUserId:(NSInteger)userId
                          title:(NSString *)title
                       videoUrl:(NSString *)videoUrl
                        videoId:(NSString *)videoId
                       coverUrl:(NSString *)coverUrl
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
                            @"video_url":nullToEmpty(videoUrl),
                            @"file_id":nullToEmpty(videoId),
                            @"cover_Url":nullToEmpty(coverUrl),
                            @"title":nullToEmpty(title)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_insert_vod_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCreatLiveWithUserId:(NSInteger)userId
                              coverUrl:(NSString *)coverUrl
                                 title:(NSString *)title
                               message:(NSString *)message
                             beginTime:(NSString *)beginTime
                             organType:(BY_LIVE_ORGAN_TYPE)organType
                              liveType:(BY_LIVE_TYPE)liveType
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
//    NSString *type = liveType == BY_LIVE_TYPE_AUDIO ? @"audio" : (liveType == BY_LIVE_TYPE_PPT ? @"ppt" : @"video");
//    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
//                            @"coverUrl":nullToEmpty(coverUrl),
//                            @"title":nullToEmpty(title),
//                            @"subtitle":nullToEmpty(message),
//                            @"beginTime":nullToEmpty(beginTime),
//                            @"organType":[NSNumber numberWithInteger:organType],
//                            @"typeType":type,
//                            @"status":@"WAITING"
//                            };
//    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
//        if (successBlock) successBlock(result[@"data"][@"roomId"]);
//    } failureBlock:^(NSError *error) {
//        if (faileBlock) faileBlock(error);
//    }];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
//    NSDictionary *param = @{@"status":@(status),
//                            @"live_record_id":nullToEmpty(live_record_id)
//                            };
//    [BYRequestManager ansyRequestWithURLString:RPC_switch_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
//        if (successBlock) successBlock(result);
//    } failureBlock:^(NSError *error) {
//        if (faileBlock) faileBlock(error);
//    }];
    [self loadRequestUpdateLiveStatus:status live_record_id:live_record_id stream_id:nil real_begin_time:nil successBlock:successBlock faileBlock:faileBlock];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                          stream_id:(NSString *)stream_id
                    real_begin_time:(NSString *)real_begin_time
                       successBlock:(void (^)(id))successBlock
                         faileBlock:(void (^)(NSError *))faileBlock{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:@(status) forKey:@"status"];
    [param setObject:nullToEmpty(live_record_id) forKey:@"live_record_id"];
    if (stream_id.length) {
        [param setObject:nullToEmpty(stream_id) forKey:@"streamer_id"];
    }
    if (real_begin_time.length) {
        [param setObject:nullToEmpty(real_begin_time) forKey:@"real_begin_time"];
    }
    [BYRequestManager ansyRequestWithURLString:RPC_switch_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetILiveLoginSigSuccessBlock:(void(^)(id object))successBlock
                                     faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"userSig"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

// --------------------------

- (void)loadRequestCreatLiveRoom:(NSString *)roomTitle
                          weChat:(NSString *)weChat
                       organType:(BY_LIVE_ORGAN_TYPE)organType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_liveroom_bg_%02d.jpg",arc4random()%6];
    NSDictionary *param = @{@"room_title":roomTitle,
                            @"wechat":weChat,
                            @"organ_type":@(organType),
                            @"cover_url":defultRoomBg};
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"roomId"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestNewLive:(NSString *)title
                 beginTime:(NSString *)beginTime
                  liveType:(BY_NEWLIVE_TYPE)liveType
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_live_bg_%02d.jpg",arc4random()%6];
    NSDictionary *param = @{@"live_title":nullToEmpty(title),
                            @"begin_time":nullToEmpty(beginTime),
                            @"live_type":@(liveType),
                            @"live_cover_url":defultRoomBg
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadReuqestUpdateLiveRoom:(NSDictionary *)param
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"room_title",@"room_intro",@"cover_url",@"organ_type",@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

// 更新直播信息
- (void)loadRequestUpdateLiveSet:(NSString*)liveId
                           param:(NSDictionary *)param
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url",@"topic_content",@"real_begin_time"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:nullToEmpty(liveId) forKey:@"live_record_id"];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [param.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([allKeys indexOfObject:key] == NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_record parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


- (void)loadRequestGetLiveRoomInfoSuccessBlock:(void(^)(id object))successBlock
                                    faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        BYCommonLiveModel *liveModel = [BYCommonLiveModel shareManager];
        id keyValues = [result mj_JSONObject];
        liveModel = [liveModel mj_setKeyValues:keyValues];
        liveModel.room_id = liveModel.user_id;
        if (successBlock) successBlock(liveModel);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetLivelist:(NSString *)userId
                       pageNum:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"owner_id":userId,@"page_number":@(pageNum),@"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSMutableArray *tableData = [NSMutableArray array];
        if ([result[@"content"] count]) {
            NSArray *tmpArr = (NSArray *)result[@"content"];
            [tmpArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                BYCommonLiveModel *liveModel = [BYCommonLiveModel mj_objectWithKeyValues:dic];
                liveModel.cellString = @"BYLiveRoomHomeCell";
                NSDate *date = [NSDate by_dateFromString:liveModel.begin_time dateformatter:K_D_F];
                NSString *time = [NSDate by_stringFromDate:date dateformatter:@"MM-dd HH:mm"];
                liveModel.begin_time = time;
                liveModel.cellHeight = 118;
                [tableData addObject:liveModel];
            }];
        }
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetHomeBannerSuccessBlock:(void(^)(id object))successBlock
                                  faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_banner_list parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeBannerModel getBannerData:result];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadReuqestIsHaveLiveRoomSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_whether_has_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestUpdateCourseware:(NSArray *)pptImgs
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"courseware":pptImgs,@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_update_courseware parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestLiveGuestOpearType:(BY_GUEST_OPERA_TYPE)type
                             theme_id:(NSString *)theme_id
                     receiver_user_id:(NSString *)receiver_user_id
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"theme_id":nullToEmpty(theme_id),
                            @"operate_type":@(type),
                            @"theme_type":@(0),
                            @"receiver_user_id":nullToEmpty(receiver_user_id)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_interaction parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestReward:(NSString *)receicer_user_id
            reward_amount:(CGFloat)reward_amount
             successBlock:(void(^)(id object))successBlock
               faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"receiver_user_id":nullToEmpty(receicer_user_id),
                            @"reward_amount":[NSString stringWithFormat:@"%.2f",reward_amount],
                            @"operate_type":@(0)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_reward parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestSurplusBBTSuccessBlock:(void(^)(id object))successBlock
                               faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_reward_window parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestPostHeartBeat:(NSString *)live_record_id
                      virtual_pv:(NSInteger )virtual_pv
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id,
                            @"virtual_PV":@(virtual_pv)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_get_heartbeat parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetLiveDetail:(NSString *)live_record_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_details parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        
        BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:result];
        model.shareMap = [BYCommonShareModel mj_objectWithKeyValues:result[@"shareMap"]];
        model.intro_begin_time = [NSDate transFromIntroTime:model.begin_time];
        if (successBlock) successBlock(model);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestAttentionLive:(NSString *)live_record_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_do_attention_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCancelAttentionLive:(NSString *)live_record_id
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_delete_attention_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 插入聊天记录 */
- (void)loadRequestInsertChatHistory:(NSString *)live_record_id
                             groupId:(NSString *)groupId
                           beginTime:(NSString *)begieTime
                        successBlock:(void(^)(id object))successBlock
                          faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id,
                            @"group_id":groupId,
                            @"real_begin_time":nullToEmpty(begieTime)};
    [BYRequestManager ansyRequestWithURLString:RPC_insert_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)extracted:(void (^)(NSError *))faileBlock param:(NSDictionary *)param successBlock:(void (^)(id))successBlock {
    [BYRequestManager ansyRequestWithURLString:RPC_get_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取聊天记录 */
- (void)loadRequestGetChatHistory:(NSString *)live_record_id
                          groupId:(NSString *)groupId
                          pageNum:(NSInteger)pageNum
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id,
                            @"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [self extracted:faileBlock param:param successBlock:successBlock];
}

/** 举报 */
- (void)loadRequestReportThemeId:(NSString *)themeId
                       themeType:(BY_THEME_TYPE)themeType
                      reportType:(NSString *)reportType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"theme_id":themeId,
                            @"theme_type":@(themeType),
                            @"report_type":reportType
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_report parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 关注/取消关注他人 */
- (void)loadRequestCreatAttention:(NSString *)attention_id
                      isAttention:(BOOL)isAttention
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"attention_user_id":attention_id};
    NSString *url = isAttention ? center_create_attention : center_create_cancelAttention;
    [BYRequestManager ansyRequestWithURLString:url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取他人主页 */
- (void)loadRequestGetPersonData:(NSString *)user_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"other_id":user_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_his_data parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        BYPersonHomeHeaderModel *model = [[BYPersonHomeHeaderModel alloc] init];
        if (successBlock) successBlock([model getHeaderData:result]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 查询个人点评 */
- (void)loadRequestGetMyComment:(NSString *)user_id
                        pageNum:(NSInteger)pageNum
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"comment_user_id":user_id,
                            @"page_number":@(pageNum),
                            @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_my_comment parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *tableData = [BYAppraiseModel getTableData:result];
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestMixStream:(NSString *)streamer_id
                   viewer_id:(NSString *)viewer_id
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"streamer_id":streamer_id,
                            @"viewer_id":viewer_id,
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_mix_stream parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetPushURL:(NSString *)live_record_id
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_push_url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetPageChatHistory:(NSString *)groupId
                              pageNum:(NSInteger)pageNum
                             sendTime:(NSString *)sendTime
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    sendTime = sendTime.length ? sendTime : [NSDate getCurrentTimeStr];
    NSDictionary *param = @{@"room_id":nullToEmpty(groupId),
                            @"page_number":@(pageNum),
                            @"page_size":@(10),
                            @"send_time":nullToEmpty(sendTime)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestClickBanner:(NSString *)bannerId{
    if (!bannerId.length) return;
    [BYRequestManager ansyRequestWithURLString:RPC_click_banner parameters:@{@"banner_id":nullToEmpty(bannerId)} operationType:BYOperationTypePost successBlock:^(id result) {
    } failureBlock:^(NSError *error) {
    }];
}

- (void)loadRequestPageComment:(NSDictionary *)param
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_page_comment parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCreateCommentWithThemeId:(NSString *)themeId
                                       type:(NSInteger)type
                                  themeInfo:(LiveFourumRootListSingleModel *)themeinfo
                                    content:(NSString *)content
                               successBlock:(void(^)(id object))successBlock
                                 faileBlock:(void(^)(NSError *error))faileBlock{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@(type) forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        if (themeinfo.transferTempCommentId.length){
            [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        } else {
            [params setObject:themeinfo._id forKey:@"reply_comment_id"];
        }
        
        [params setObject:themeinfo.user_id forKey:@"reply_user_id"];
    }
    [params setObject:content forKey:@"content"];
    [BYRequestManager ansyRequestWithURLString:live_ppt_create_comment parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}
@end
