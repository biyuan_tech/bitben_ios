//
//  BYLiveRoomSetModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomSetModel.h"
#import "BYCommonLiveModel.h"

@implementation BYLiveRoomSetModel

- (NSArray *)getTableData:(BYCommonLiveModel *)liveModel{
    NSMutableArray *tmpArr = [NSMutableArray array];
    for (int i = 0; i < 3; i ++) {
        BYLiveRoomSetCellModel *model = [[BYLiveRoomSetCellModel alloc] init];
        model.cellString = @"BYLiveRoomSetCell";
        model.cellHeight = 48;
        switch (i) {
            case 0:
                model.title = @"直播间名称";
                model.rightTitle = liveModel.room_title;
                model.showBottomLine = YES;
                break;
            case 1:
                model.title = @"直播间简介";
                model.rightTitle = @"直播间介绍";
                model.showBottomLine = YES;
                break;
            case 2:
                model.title = @"直播间背景图";
                model.showBottomLine = NO;
                break;
            default:
                break;
        }
        [tmpArr addObject:model];
    }
    
    BYLiveRoomSetCellModel *model = [[BYLiveRoomSetCellModel alloc] init];
    model.title = @"直播间认证";
    model.cellString = @"BYLiveRoomSetCell";
    model.cellHeight = 48;
    model.showBottomLine = NO;
    return @[@{@"cell":tmpArr}];
}

@end

@implementation BYLiveRoomSetCellModel

@end
