//
//  BYLiveHostTimerHeaderView.h
//  BibenCommunity
//
//  Created by 黄亮 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYLiveHostTimerHeaderView : UIView

/** 倒计时到达开播时间 */
@property (nonatomic ,copy) void (^didTimerBeginLive)(void);

@end
