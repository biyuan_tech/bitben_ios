//
//  BYILiveEndController.m
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveEndController.h"
#import "BYILiveEndViewModel.h"

@interface BYILiveEndController ()

@end

@implementation BYILiveEndController

- (Class)getViewModelClass{
    return [BYILiveEndViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
