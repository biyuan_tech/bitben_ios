//
//  BYPPTUploadController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPPTUploadController : BYCommonViewController

@property (nonatomic ,copy) void (^didUploadPPTHandle)(NSArray *pptImgs);
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
@property (nonatomic ,strong) NSArray *pptImgs;
@property (nonatomic ,copy) NSString *record_id;

@end

NS_ASSUME_NONNULL_END
