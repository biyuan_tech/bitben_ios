//
//  BYPPTImgEditView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPPTImgEditView.h"

@interface BYPPTImgEditView ()
/** 课件 */
@property (nonatomic ,strong) PDImageView *imageView;
/** 删除按钮 */
@property (nonatomic ,strong) UIButton *delBtn;
/** 上传状态 */
@property (nonatomic ,strong) UILabel *uploadStatusLab;
/** 是否处于编辑状态 */
//@property (nonatomic ,assign) BOOL isEdit;
@property (nonatomic ,assign) CGFloat editViewW;
@property (nonatomic ,assign) CGFloat editViewH;

@end

@implementation BYPPTImgEditView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.editViewW = (kCommonScreenWidth - 35)/2;
        self.editViewH = 100*self.editViewW/172;
//        self.isEdit = NO;
        [self setContentView];
        [self addGestureRecognizer];
    }
    return self;
}

#pragma mark - custom Method

- (void)addGestureRecognizer{
    UILongPressGestureRecognizer *longGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGestureRecognizerAction:)];
    //长按响应时间
    longGestureRecognizer.minimumPressDuration = 0.2;
    [self addGestureRecognizer:longGestureRecognizer];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)reloadData:(BYPPTImgModel *)model{
    self.model = model;
    if (model.url.length) {
        @weakify(self);
        [self.imageView uploadMainImageWithURL:model.url placeholder:nil imgType:PDImgTypeOriginal callback:^(UIImage *image) {
            @strongify(self);
            self.model.image = image;
            self.imageView.image = image;
//            self.imageView.image = [image addCornerRadius:8 size:CGSizeMake(self.editViewW, self.editViewH)];
        }];
    }
    else
    {
        self.imageView.image = model.image;
    }
    self.indexLab.text = [NSString stringWithFormat:@"%02d",[model.index intValue]];
}

- (void)setEditStatus:(BOOL)isEdit{
//    self.isEdit = isEdit;
    _delBtn.hidden = !isEdit;
    _uploadStatusLab.hidden = !_model.isSend;
}

#pragma mark - action

- (void)delBtnAction:(UIButton *)sender{
    if (self.didDelBtnActionHandle) {
        self.didDelBtnActionHandle(self);
    }
}

//长按响应事件
- (void)longGestureRecognizerAction:(UIGestureRecognizer *)gestureRecognizer{
//    if (!_isEdit) return;
    if (self.longGestureRecognizerBlock) {
        self.longGestureRecognizerBlock(gestureRecognizer);
    }
}

- (void)tapGestureRecognizerAction:(UIGestureRecognizer *)gestureRecognizer{
    if (_clickActionBlock) {
        _clickActionBlock(self);
    }
}
#pragma mark - configUI

- (void)setContentView{
    self.imageView = [[PDImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.layer.cornerRadius = 8;
    self.imageView.clipsToBounds = YES;
    [self addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 0, 0));
    }];
    
    self.indexLab = [UILabel by_init];
    self.indexLab.textColor = kTextColor_53;
    self.indexLab.backgroundColor = kColorRGB(255, 255, 255, 0.65);
    [self.indexLab setBy_font:11];
    self.indexLab.textAlignment = NSTextAlignmentCenter;
    [self.indexLab layerCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft size:CGSizeMake(19, 17)];
    [self addSubview:self.indexLab];
    [self.indexLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(19);
        make.height.mas_equalTo(17);
        make.left.mas_equalTo(5);
        make.bottom.mas_equalTo(0);
    }];
    
    self.uploadStatusLab = [UILabel by_init];
    self.uploadStatusLab.textColor = kColorRGBValue(0xef504c);
    self.uploadStatusLab.text = @"已发";
    self.uploadStatusLab.backgroundColor = kColorRGB(255, 255, 255, 0.65);
    [self.uploadStatusLab setBy_font:10];
    self.uploadStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.uploadStatusLab layerCornerRadius:5 byRoundingCorners:UIRectCornerTopRight size:CGSizeMake(27, 17)];
    self.uploadStatusLab.hidden = YES;
    [self addSubview:self.uploadStatusLab];
    [self.uploadStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(27);
        make.height.mas_equalTo(17);
        make.top.mas_equalTo(5);
        make.right.mas_equalTo(0);
    }];
    
    self.delBtn = [UIButton by_buttonWithCustomType];
    [self.delBtn setBy_imageName:@"live_pptupload_del" forState:UIControlStateNormal];
    [self.delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    self.delBtn.hidden = YES;
    [self addSubview:self.delBtn];
    [self.delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.height.mas_equalTo(19);
    }];
}

@end
