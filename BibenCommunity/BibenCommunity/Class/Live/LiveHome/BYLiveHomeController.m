//
//  BYLiveHomeController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeController.h"
#import "BYLiveHomeViewModel.h"
#import "BYCreatLiveRoomController.h"
#import "BYLiveDefine.h"
#import "BYLiveHomeRequest.h"
#import "BYIMManager.h"

#import "BYLiveHomeControllerV1.h"

@interface BYLiveHomeController ()

@end

@implementation BYLiveHomeController

- (Class)getViewModelClass{
    return [BYLiveHomeViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self actionAutoLoginBlock:nil];
    @weakify(self);
    [self loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        if (!isSuccessed) return ;
        [(BYLiveHomeViewModel *)self.viewModel loginReloadData];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) return;
        // 校验直播间
        [self verifyLiveRoom];
    }];
    
    
    [self rightBarButtonWithTitle:@"新版首页" barNorImage:nil barHltImage:nil action:^{
        @strongify(self);
        BYLiveHomeControllerV1 *liveHomeController = [[BYLiveHomeControllerV1 alloc] init];
        [self.navigationController pushViewController:liveHomeController animated:YES];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [(BYLiveHomeViewModel *)self.viewModel reloadlistData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

// 设置直播间状态
- (void)verifyLiveRoom{
    [[BYLiveHomeRequest alloc] loadReuqestIsHaveLiveRoomSuccessBlock:^(id object) {
        [[NSUserDefaults standardUserDefaults] setObject:@([object boolValue]) forKey:kiSHaveLiveRoomKey];
    } faileBlock:^(NSError *error) {

    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
