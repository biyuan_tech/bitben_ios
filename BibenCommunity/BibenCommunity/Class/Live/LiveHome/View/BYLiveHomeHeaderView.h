//
//  BYLiveHomeHeaderView.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYLiveHomeHeaderView : UIView


/**
 广告数据
 */
@property (nonatomic ,strong) NSArray *advertData;

/**
 广告点击index
 */
@property (nonatomic ,copy) void (^didSelectAdvertIndex)(NSInteger index);

/**
 点击分类标题
 */
@property (nonatomic ,copy) void (^didSelectHeaderTitle)(NSInteger index);


- (void)reloadData;

/** 启动定时 */
- (void)startTimer;
/** 销毁定时 */
- (void)destoryTimer;
@end
