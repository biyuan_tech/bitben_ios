//
//  BYLiveHomeTitleCell.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeTitleCell.h"
#import "BYLiveHomeVideoModel.h"

@interface BYLiveHomeTitleCell()

// ** 标题
@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) BYLiveHomeVideoModel *model;

@end

@implementation BYLiveHomeTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    _titleLab.text = model.title;
}

#pragma makr - initMethod

- (void)setContentView{
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont fontWithName:@"MicrosoftYaHei-Bold" size:17];
    titleLab.textColor = kTextColor_48;
    titleLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(19);
    }];
}


@end
