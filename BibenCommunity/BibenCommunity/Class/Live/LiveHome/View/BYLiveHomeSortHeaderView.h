//
//  BYLiveHomeSortHeaderView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYLiveDefine.h"

@interface BYLiveHomeSortHeaderView : UIView

@property (nonatomic ,assign ,readonly) BY_SORT_TYPE sort_type;

/** 排序按钮回调 */
@property (nonatomic, copy) void (^didSelectSortBtnHandle)(void);

- (void)reloadSortStatus;

@end
