//
//  BYLiveRoomHomeTitleCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeTitleCell.h"
#import "BYCommonModel.h"

@interface BYLiveRoomHomeTitleCell()

@property (nonatomic ,strong) UILabel *titleLab;

@end

@implementation BYLiveRoomHomeTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 标题
        UILabel *titleLab = [[UILabel alloc] init];
        titleLab.font = [UIFont fontWithName:@"MicrosoftYaHei-Bold" size:18];
        titleLab.textColor = kTextColor_48;
        titleLab.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:titleLab];
        _titleLab = titleLab;
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kCellLeftSpace);
            make.top.mas_equalTo(20);
            make.width.mas_equalTo(120);
            make.height.mas_equalTo(19);
        }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCommonModel *model = object[indexPath.row];
    _titleLab.text = model.title;
}

@end
