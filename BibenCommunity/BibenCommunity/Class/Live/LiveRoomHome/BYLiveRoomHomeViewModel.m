//
//  BYLiveRoomHomeViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeViewModel.h"
// Controller
#import "BYNewLiveController.h"
#import "BYLiveRoomSetController.h"
#import "BYLiveHostController.h"
#import "BYLiveController.h"
// View
#import "BYLiveRoomHomeHeaderView.h"

#import "BYCommonModel.h"
#import "BYCommonLiveModel.h"
// Request
#import "BYLiveHomeRequest.h"
#import "BYIMCommon.h"

#import "BYSIMController.h"


@interface BYLiveRoomHomeViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;
@property (nonatomic ,strong) BYLiveRoomHomeHeaderView *headerView;
@property (nonatomic ,assign) NSInteger pageNum;
@property (nonatomic ,strong) BYCommonLiveModel *model;

@end

@implementation BYLiveRoomHomeViewModel

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setContentView{
    _pageNum = 0;
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0));
    }];
    [self addNSNotification];
    [self reloadData];
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveStopPushStream_notic) name:NSNotification_stopPushStream object:nil];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequestGetLiveRoomInfo];
    [self loadRequestGetLiveList];
}

- (void)loadMoreData{
    [self loadRequestGetLiveList];
}

- (void)viewWillAppear{
    [self.tableView reloadData];
}

#pragma mark - notic
// 接收关闭直播消息刷新界面
- (void)receiveStopPushStream_notic{
    [self reloadData];
}

#pragma mark - buttonAction
// 新建直播
- (void)didNewLiveBtnAction{
    BYNewLiveController *newLiveController = [[BYNewLiveController alloc] init];
    [S_V_NC pushViewController:newLiveController animated:YES];
}
// 直播间设置
- (void)didRoomSetBtnAction{
    BYLiveRoomSetController *liveRoomSetController = [[BYLiveRoomSetController alloc] init];
    liveRoomSetController.model = self.model;
    @weakify(self);
    liveRoomSetController.reloadLiveRoomHandel = ^{
        @strongify(self);
        [self.headerView reloadData:self.model];
    };
    [S_V_NC pushViewController:liveRoomSetController animated:YES];
}

#pragma mark - request
- (void)loadRequestGetLiveRoomInfo{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveRoomInfoSuccessBlock:^(id object) {
        @strongify(self);
        self.model = object;
        [self.headerView reloadData:self.model];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestGetLiveList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLivelist:[AccountModel sharedAccountModel].account_id pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        [self.tableView endRefreshing];
        NSMutableArray *tmpArr = [NSMutableArray array];
        if (self->_pageNum == 0) {
            self.pageNum ++;
            BYCommonModel *model = [[BYCommonModel alloc] init];
            model.cellString = @"BYLiveRoomHomeTitleCell";
            model.cellHeight = 58;
            model.title = @"直播列表";
            [tmpArr addObject:model];
            [tmpArr addObjectsFromArray:object];
            self.tableView.tableData = tmpArr;
            return ;
        }
        self.pageNum ++;
        [tmpArr addObjectsFromArray:self.tableView.tableData];
        [tmpArr addObjectsFromArray:object];
        self.tableView.tableData = tmpArr;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}
#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    BYLiveController *livecontroller = [[BYLiveController alloc] init];
    livecontroller.model = model;
    [S_V_NC pushViewController:livecontroller animated:YES];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableHeaderView = self.headerView;
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYLiveRoomHomeHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveRoomHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 395)];
        @weakify(self);
        _headerView.didNewLiveBtnHandle = ^{
            @strongify(self);
            [self didNewLiveBtnAction];
        };
        _headerView.didRoomSetBtnHandle = ^{
            @strongify(self);
            [self didRoomSetBtnAction];
        };
    }
    return _headerView;
}



@end
