//
//  LiveForumRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "LiveForumRootInputView.h"

#define PPTLiveSegmentSizeHeight LCFloat(47)
@interface LiveForumRootViewController : AbstractViewController

@property (nonatomic,assign)CGFloat viewSizeHeight;
@property (nonatomic,strong)LiveForumRootInputView *inputView;
/** superController */
@property (nonatomic ,weak) UIViewController *superController;

@property (nonatomic,copy)NSString *transferRoomId;

@end
