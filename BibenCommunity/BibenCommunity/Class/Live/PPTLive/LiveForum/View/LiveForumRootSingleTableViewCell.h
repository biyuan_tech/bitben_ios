//
//  LiveForumRootSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootDingView.h"


@interface LiveForumRootSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;

-(void)createView;

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model;

-(void)actionClickMoreBlock:(void(^)())block;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;

@end
