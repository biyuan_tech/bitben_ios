//
//  LiveForumItemsTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumItemsTableViewCell.h"
#import "LiveForumRootDingView.h"

static char itemsReplyManagerWithModelKey;
@interface LiveForumItemsTableViewCell()
@property (nonatomic,strong)LiveForumRootDingView *replyView;
@property (nonatomic,strong)LiveForumRootDingView *dingView;
@property (nonatomic,strong)LiveForumRootDingView *downView;
@property (nonatomic,strong)LiveForumRootDingView *moneyView;
@end

@implementation LiveForumItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - - createView
-(void)createView{
    self.dingView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.dingView.transferType = LiveForumRootDingViewTypeUp;
    [self addSubview:self.dingView];
    
    self.downView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(16) - [LiveForumRootDingView calculationSize].width, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.downView.transferType = LiveForumRootDingViewTypeDown;
    [self addSubview:self.downView];
    
    // 回复
    self.replyView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.replyView.transferType = LiveForumRootDingViewTypeReply;
    [self addSubview:self.replyView];
    
    __weak typeof(self)weakSelf = self;
    [self.replyView actionButtonWithReplyBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(LiveFourumRootListSingleModel *singleModle) = objc_getAssociatedObject(strongSelf, &itemsReplyManagerWithModelKey);
        if (block){
            block(strongSelf.transferInfoManager);
        }
    }];
    
    self.moneyView = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
    self.moneyView.transferType = LiveForumRootDingViewTypeArticleMoney;
    [self addSubview:self.moneyView];
}

-(void)itemsReplyManagerWithModel:(void(^)(LiveFourumRootListSingleModel *singleModle))block{
    objc_setAssociatedObject(self, &itemsReplyManagerWithModelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;

    self.downView.transferItemId = self.transferItemId;
    self.downView.transferTempType = self.transferType;
    self.downView.transferInfoManager = transferInfoManager;

    self.dingView.transferItemId = self.transferItemId;
    self.dingView.transferTempType = self.transferType;
    self.dingView.transferInfoManager = transferInfoManager;
    
    self.replyView.transferItemId = self.transferItemId;
    self.replyView.transferTempType = self.transferType;
    self.replyView.transferInfoManager = transferInfoManager;
    
    self.moneyView.transferItemId = self.transferItemId;
    self.moneyView.transferInfoManager = transferInfoManager;
    
    self.downView.frame = CGRectMake(kScreenBounds.size.width - self.downView.size_width, 0 , [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height);
    self.dingView.frame = CGRectMake(self.downView.orgin_x - self.dingView.size_width, self.downView.orgin_y, self.downView.size_width, self.downView.size_height);
    self.replyView.frame = CGRectMake(self.dingView.orgin_x - self.replyView.size_width, self.downView.orgin_y, self.replyView.size_width, self.replyView.size_height);
    
    self.moneyView.frame = CGRectMake(LCFloat(60), 0, self.moneyView.size_width, self.moneyView.size_height);
}

-(void)setTransferItemId:(NSString *)transferItemId{
    _transferItemId = transferItemId;
}

-(void)setTransferType:(LiveForumDetailViewControllerType)transferType{
    _transferType = transferType;
}

+(CGFloat)calculationCellHeight{
    return [LiveForumRootDingView calculationSize].height;
}

@end
