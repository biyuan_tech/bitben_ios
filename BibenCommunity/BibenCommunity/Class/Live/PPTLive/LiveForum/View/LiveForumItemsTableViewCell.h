//
//  LiveForumItemsTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"
#import "NetworkAdapter+PPTLive.h"

@interface LiveForumItemsTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;

@property (nonatomic,assign) LiveForumDetailViewControllerType transferType;
@property (nonatomic,copy)NSString *transferItemId;

// 回复
-(void)itemsReplyManagerWithModel:(void(^)(LiveFourumRootListSingleModel *singleModle))block;
-(void)createView;

@end
