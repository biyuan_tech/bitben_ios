//
//  PPTLiveMainInfoModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
#import "LivePPTAdTableViewCell.h"
#import "BYCommonShareModel.h"
@class LivePPTAdTableViewCell;

@interface PPTLiveMainInfoModel : FetchModel

@property (nonatomic,copy)NSString *_id;                /**< id*/
@property (nonatomic,copy)NSString *account_id;         /**< 账户id*/
@property (nonatomic,copy)NSString *ad_img;             /**< 广告图片*/
@property (nonatomic,assign)NSInteger article;          /**< 文章*/
@property (nonatomic,assign)NSInteger attention;        /**< 注意*/
@property (nonatomic,copy)NSString *attention_user_id;  /**< 用户id*/
@property (nonatomic,copy)NSString *begin_time;         /**< 开始时间*/
@property (nonatomic,copy)NSString *live_cover_url;
@property (nonatomic,copy)NSString *cover_url;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,assign)NSInteger fans;             /**< 粉丝数量*/
@property (nonatomic,copy)NSString *head_img;           /**< 头像*/
@property (nonatomic,copy)NSString *invite_account_id;
@property (nonatomic,assign)NSInteger live;
@property (nonatomic,assign)NSInteger live_income;
@property (nonatomic,copy)NSString *live_intro;             /**< 直播介绍*/
@property (nonatomic,copy)NSString *live_title;
@property (nonatomic,copy)NSString *live_type;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *room_intro;             /**< 房间简介*/
@property (nonatomic,assign)NSInteger organ_type;
@property (nonatomic,copy)NSString *room_title;             /**< 房间标题*/
@property (nonatomic,copy)NSString *speaker;                /**< 演讲者*/
@property (nonatomic,copy)NSString *speaker_head_img;       /**< 演讲者头像*/
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *wechat;
@property (nonatomic,copy)NSString *share_detail;
@property (nonatomic,copy)NSString *share_img;
@property (nonatomic,copy)NSString *share_title;
@property (nonatomic,copy)NSString *share_url;
@property (nonatomic,copy)NSString *room_id;
@property (nonatomic,assign)NSInteger watch_times;
@property (nonatomic,copy)NSString *group_id;
@property (nonatomic,strong) BYCommonShareModel *shareMap;

// temp
@property (nonatomic,strong)LivePPTAdTableViewCellModel *adImgModel;

@end
