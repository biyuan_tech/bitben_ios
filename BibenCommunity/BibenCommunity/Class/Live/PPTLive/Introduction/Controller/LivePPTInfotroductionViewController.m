//
//  LivePPTInfotroductionViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/14.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTInfotroductionViewController.h"
#import "LivePPTBannerTableViewCell.h"
#import "LivePPTPlayerTableViewCell.h"
#import "LivePPTDescTableViewCell.h"
#import "LivePPTAdTableViewCell.h"
#import "NetworkAdapter+PPTLive.h"

@interface LivePPTInfotroductionViewController ()<UITableViewDelegate,UITableViewDataSource>{
    PPTLiveMainInfoModel *mainModel;
    LivePPTAdTableViewCell *adCell;
}
@property (nonatomic,strong)UITableView *descTableView;
@property (nonatomic,strong)NSMutableArray *descRootMutableArr;
@property (nonatomic,strong)NSMutableArray *descDetailMutableArr;
@end

@implementation LivePPTInfotroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"简介";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.descRootMutableArr = [NSMutableArray array];
    self.descDetailMutableArr = [NSMutableArray array];
    [self.descRootMutableArr addObject:@[@"封面",@"标题"]];
    
}

-(void)createTableView{
    if (!self.descTableView){
        self.descTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.descTableView.dataSource = self;
        self.descTableView.delegate = self;
        [self.view addSubview:self.descTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.descRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.descRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"封面" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"封面" sourceArr:self.descRootMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LivePPTBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LivePPTBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferBanner = mainModel.live_cover_url;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.descRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LivePPTPlayerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[LivePPTPlayerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowOne.transferMainModel = mainModel;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"直播介绍" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"直播介绍" sourceArr:self.descRootMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"直播介绍";
        cellWithRowThr.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"15"]boldFont];
        return cellWithRowThr;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr])){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr] || indexPath.row == [self cellIndexPathRowWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr]){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"14"]boldFont];
            if (indexPath.section == [self cellIndexPathSectionWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr]){
                cellWithRowThr.transferTitle = @"【主持人简介】";
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr]){
                cellWithRowThr.transferTitle = @"【直播简介】";
            }
            
            return cellWithRowThr;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"广告图片" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"广告图片" sourceArr:self.descRootMutableArr]){
                static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
                adCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
                if (!adCell){
                adCell = [[LivePPTAdTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            }
            adCell.transferCellHeight = cellHeight;
            __weak typeof(self)weakSelf = self;
            [adCell actionReloadImgHeightBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.descTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }];
            adCell.transferAdModel = mainModel.adImgModel;

            return adCell;
        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            LivePPTDescTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowThr){
                cellWithRowThr = [[LivePPTDescTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferTitle = [[self.descRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            return cellWithRowThr;
        }
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        UITableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        cellWithRowOther.backgroundColor = UURandomColor;
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"封面" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"封面" sourceArr:self.descRootMutableArr]){
        return [LivePPTBannerTableViewCell calculationCellHeight];
      } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.descRootMutableArr]){
          return [LivePPTPlayerTableViewCell calculationCellHeight:mainModel];
      } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"直播介绍" sourceArr:self.descRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"直播介绍" sourceArr:self.descRootMutableArr]){
          return [GWNormalTableViewCell calculationCellHeight];
      } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr]) || indexPath.section == [self cellIndexPathSectionWithcellData:@"广告图片" sourceArr:self.descRootMutableArr]){
          
          if (indexPath.row == [self cellIndexPathRowWithcellData:@"【主持人简介】" sourceArr:self.descRootMutableArr] || indexPath.row == [self cellIndexPathRowWithcellData:@"【直播简介】" sourceArr:self.descRootMutableArr]){
              return 0;
          } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"广告图片" sourceArr:self.descRootMutableArr]){
              return adCell.transferAdModel.imgHeight;
          } else {
              NSString *title = [[self.descRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
              return [LivePPTDescTableViewCell calculationCellHeightWIthTitle:title];
          }
      } else {
          return 44;
      }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(9);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"F8F8F8"];
    return headerView;
}

#pragma mark - Interface
-(void)setTransferInfoModel:(PPTLiveMainInfoModel *)transferInfoModel{
    _transferInfoModel = transferInfoModel;
    
    mainModel = transferInfoModel;
    
    if (transferInfoModel.live_intro.length){           // 直播人介绍
        [self.descDetailMutableArr addObjectsFromArray:@[@"【主持人简介】",transferInfoModel.live_intro]];
        
    }
    if (transferInfoModel.room_intro.length){
        
        [self.descDetailMutableArr addObjectsFromArray:@[@"【直播简介】",transferInfoModel.room_intro]];
    }
    if (transferInfoModel.ad_img.length){
        [self.descDetailMutableArr addObjectsFromArray:@[@"广告图片"]];
    }
    
    if (self.descDetailMutableArr.count){
        [self.descRootMutableArr addObject:self.descDetailMutableArr];
    }
    
    if (transferInfoModel.ad_img.length){
        LivePPTAdTableViewCellModel *adModel = [[LivePPTAdTableViewCellModel alloc]init];
        adModel.imgUrl = transferInfoModel.ad_img;
        transferInfoModel.adImgModel = adModel;
    }
    
    [self.descTableView reloadData];
}


@end
