//
//  LivePPTRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/14.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "LivePPTInfotroductionViewController.h"
#import "LiveForumRootViewController.h"

@interface LivePPTRootViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferRoomId;         /**< 传递直播间ID*/
@property (nonatomic ,assign) BOOL isHost;

@end
