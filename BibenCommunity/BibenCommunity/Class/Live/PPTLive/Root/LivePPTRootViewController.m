//
//  LivePPTRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/14.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTRootViewController.h"
#import "LivePPTInfotroductionViewController.h"
#import "LiveForumRootViewController.h"
#import "NetworkAdapter+PPTLive.h"
#import "SmartViewController.h"
#import "ShareRootViewController.h"


@interface LivePPTRootViewController (){
    PPTLiveMainInfoModel *liveMainModel;
}

@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)LivePPTInfotroductionViewController *descController;
@property (nonatomic,strong)GWCurrentIMViewController *IMController;
@property (nonatomic,strong)LiveForumRootViewController *dianpingViewController;
/** 分享的数据 */
@property (nonatomic,strong)BYCommonShareModel *shareModel;
@end

@implementation LivePPTRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createSegmentList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    self.barMainTitle = @"币本直播";
    self.barSubTitle = @"正在直播";
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_live_share"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        [weakSelf shareAction];
    }];
}

#pragma mark - SegmentList
-(void)createSegmentList{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"简介",@"直播",@"点评"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
        [strongSelf releaseKeyboard];
    }];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, PPTLiveSegmentSizeHeight);
    [self.segmentList setTitleColor:[UIColor hexChangeFloat:@"EE4944"] forState:UIControlStateSelected];
    [self.segmentList setTitleColor:[UIColor hexChangeFloat:@"676767"] forState:UIControlStateNormal];
    self.segmentList.selectionIndicatorColor = [UIColor hexChangeFloat:@"EE4944"];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame) - .5f,  kScreenBounds.size.width, .5f);
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    [self.segmentList addSubview:lineView];

    [self.view addSubview:self.segmentList];
    [self createScrollView];
}

#pragma mark - scrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        __weak typeof(self)weakSelf = self;
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger index = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:index animated:YES];
            [strongSelf releaseKeyboard];
        }];
        self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame) - [BYTabbarViewController sharedController].navBarHeight);
        self.mainScrollView.contentSize = CGSizeMake(3 * kScreenBounds.size.width, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
    }
    
    [self createDescController];        // 创建详情
    [self createDianpingView];          // 创建点评
    
    [self sendRequestToGetInfo];
}

#pragma mark - 创建详情
-(void)createDescController{
    self.descController = [[LivePPTInfotroductionViewController alloc]init];
    [self.mainScrollView addSubview:self.descController.view];
    
    self.descController.view.frame = CGRectMake(0 * kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self addChildViewController:self.descController];
}

#pragma mark - 创建IM
-(void)createIMWithNUmber:(NSString *)number{
    if (!self.IMController){
        __weak typeof(self)weakSelf = self;
        [[AliChatManager sharedInstance] joinTribeWithNumber:number fromNav:self.navigationController controller:[GWCurrentIMViewController class] callBcak:^(YWConversationViewController *conversationController) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            

            strongSelf.IMController = (GWCurrentIMViewController *)conversationController;
            strongSelf.IMController.transferRoomId = strongSelf.transferRoomId;
            strongSelf.IMController.isHost = strongSelf.isHost;
            strongSelf.IMController.viewController = strongSelf;
//            strongSelf.IMController.transferInfoModel = strongSelf->liveMainModel;
            [strongSelf.mainScrollView addSubview:strongSelf.IMController.view];
            strongSelf.IMController.view.clipsToBounds = YES;
            strongSelf.IMController.view.frame = CGRectMake(1 * kScreenBounds.size.width, 0, kScreenBounds.size.width, strongSelf.mainScrollView.size_height);
            [strongSelf addChildViewController:strongSelf.IMController];
        }];
    }
}


#pragma mark - 创建点评View
-(void)createDianpingView{
    self.dianpingViewController = [[LiveForumRootViewController alloc]init];
    self.dianpingViewController.superController = self;
    self.dianpingViewController.transferRoomId = self.transferRoomId;
    self.dianpingViewController.viewSizeHeight = self.mainScrollView.size_height;
    self.dianpingViewController.view.frame = CGRectMake(2 * kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.dianpingViewController.view];
    [self addChildViewController:self.dianpingViewController];
}

#pragma mark - 接口
-(void)sendRequestToGetInfo{
    NSDictionary *params = @{@"live_record_id":self.transferRoomId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_live_details" requestParams:params responseObjectClass:[PPTLiveMainInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PPTLiveMainInfoModel *liveMainModel = (PPTLiveMainInfoModel *)responseObject;
            strongSelf->liveMainModel = liveMainModel;
            weakSelf.shareModel = liveMainModel.shareMap;
            strongSelf.descController.transferInfoModel = strongSelf->liveMainModel;
            strongSelf.barMainTitle = strongSelf->liveMainModel.live_title;
            strongSelf.barSubTitle = [NSString stringWithFormat:@"正在直播·%li万人次",strongSelf->liveMainModel.watch_times];
            [strongSelf createIMWithNUmber:strongSelf->liveMainModel.group_id];

//            [strongSelf sendRequestToGetLiveRoom:liveMainModel];
        }
    }];
}

-(void)sendRequestToGetLiveRoom:(PPTLiveMainInfoModel *)mainModel{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] liveGetMainInfoWithBlock:^(PPTLiveMainInfoModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (model){
            mainModel._id = model._id;
            mainModel.head_img = model.head_img;
            mainModel.nickname = model.nickname;
            mainModel.room_title = model.room_title;
            mainModel.cover_url = model.cover_url;
//            mainModel.live_income = model.live_income;
            if (model.group_id.length){
                mainModel.group_id = model.group_id;
            }
        }
        strongSelf->liveMainModel = mainModel;
        strongSelf.descController.transferInfoModel = strongSelf->liveMainModel;
        strongSelf.barMainTitle = strongSelf->liveMainModel.live_title;
        strongSelf.barSubTitle = [NSString stringWithFormat:@"正在直播·%li万人次",strongSelf->liveMainModel.watch_times];
        
        [strongSelf createIMWithNUmber:strongSelf->liveMainModel.group_id];
    }];
}


#pragma mark - releaseKeyboard
-(void)releaseKeyboard{
    if (self.dianpingViewController){
        if ([self.dianpingViewController.inputView.inputView isFirstResponder]){
            [self.dianpingViewController.inputView.inputView resignFirstResponder];
        }
    }
}

#pragma mark - 创建直播
//-(void)createQun{
//    return;
//    __weak typeof(self)weakSelf = self;
//    [[AliChatManager sharedInstance] joinTribeWithNumber:@"2726301606" fromNav:self.navigationController controller:[GWCurrentIMViewController class] callBcak:^(YWConversationViewController *conversationController) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//
//        strongSelf.IMController = (GWCurrentIMViewController *)conversationController;
//
//        [strongSelf.mainScrollView addSubview:strongSelf.IMController.view];
//
//        strongSelf.IMController.view.frame = CGRectMake(1 * kScreenBounds.size.width, 0, kScreenBounds.size.width, strongSelf.mainScrollView.size_height);
//        [strongSelf addChildViewController:strongSelf.IMController];
//    }];
//}

// 分享
- (void)shareAction{
    if (!_shareModel) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.shareModel.share_img];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.shareModel.share_title desc:weakSelf.shareModel.share_intro img:mainImgUrl url:weakSelf.shareModel.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    [shareViewController showInView:self.parentViewController];
}

//-(void)opensas{
////    -(void)openConversationWithPerson:(YWPerson *)person controllerClass:(Class)controllerClass callBack:(void(^)(YWConversationViewController *conversationController))block{
//    YWPerson *person = [[YWPerson alloc]initWithPersonId:@"632941147"];
////    936163704
////
//    @weakify(self);
//    [[AliChatManager sharedInstance] openConversationWithPerson:person controllerClass:[SmartViewController class] callBack:^(YWConversationViewController *conversationController) {
//        @strongify(self);
//        [self.navigationController pushViewController:conversationController animated:YES];
//    }];
//}

//-(void)sma{
//    [[AliChatManager sharedInstance] joinTribeWithNumber:@"2726301606" fromNav:self.navigationController controller:[sssasViewController class] callBcak:^(YWConversationViewController *conversationController) {
//
//        sssasViewController *vc = [[sssasViewController alloc]init];
//
//        vc = (sssasViewController *)conversationController;
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
//}
@end
