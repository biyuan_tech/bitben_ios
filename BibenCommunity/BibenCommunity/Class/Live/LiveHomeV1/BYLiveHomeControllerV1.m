//
//  BYLiveHomeControllerV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeControllerV1.h"
#import "BYLiveHomeViewModelV1.h"

@interface BYLiveHomeControllerV1 ()

@end

@implementation BYLiveHomeControllerV1

- (Class)getViewModelClass{
    return [BYLiveHomeViewModelV1 class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


@end
