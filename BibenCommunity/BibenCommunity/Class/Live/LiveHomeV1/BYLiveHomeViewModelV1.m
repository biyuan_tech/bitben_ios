//
//  BYLiveHomeViewModelV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeViewModelV1.h"

#import "BYHomeRecommendController.h"

#import "BYLiveHomeSegmentViewV1.h"


@interface BYLiveHomeViewModelV1 ()<BYLiveHomeSegmentViewDelegateV1>

/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentViewV1 *segmentView;
/** 推荐分类 */
@property (nonatomic ,strong) BYHomeRecommendController *recommendController;

@end

@implementation BYLiveHomeViewModelV1

- (void)setContentView{
    [self addSearchView];
    [self addSegmentView];
}

#pragma mark - BYLiveHomeSegmentViewDelegateV1
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index==1) {
        return self.recommendController.view;
    }
    return [UIView new];
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{

}

#pragma mark - configUI
- (void)addSearchView{
    UIView *searchView = [[UIView alloc] init];
    searchView.backgroundColor = [UIColor whiteColor];
    [S_V_VIEW addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(60));
    }];
    
    UIImageView *searchBgView = [UIImageView by_init];
    [searchBgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    searchBgView.layer.cornerRadius = 4.0f;
    searchBgView.userInteractionEnabled = YES;
    [searchView addSubview:searchBgView];
    [searchBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(-kCellRightSpace);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    UIImageView *searchImgView = [UIImageView by_init];
    [searchImgView setImage:[UIImage imageNamed:@"livehome_search"]];
    [searchBgView addSubview:searchImgView];
    [searchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(searchImgView.image.size.width);
        make.height.mas_equalTo(searchImgView.image.size.height);
    }];
    
    UILabel *searchTitleLab = [UILabel by_init];
    [searchTitleLab setBy_font:14];
    [searchTitleLab setTextColor:kColorRGBValue(0xb6b6b7)];
    searchTitleLab.textAlignment = NSTextAlignmentLeft;
    searchTitleLab.text = @"搜索视频 / 用户 / 观点";
    [searchBgView addSubview:searchTitleLab];
    [searchTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchImgView.center_y).mas_offset(0);
        make.left.mas_equalTo(38);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(17);
    }];
}

- (void)addSegmentView{
    
    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"关注",@"推荐",@"热点要闻",@"小白入门",@"通证经济",@"资产安全",@"行情解读", nil];
    self.segmentView.delegate = self;
    self.segmentView.defultSelIndex = 1;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSafe_Mas_Top(60) + 7);
        make.left.right.bottom.mas_equalTo(0);
    }];
}

- (BYHomeRecommendController *)recommendController{
    if (!_recommendController) {
        _recommendController = [[BYHomeRecommendController alloc] init];
    }
    return _recommendController;
}

@end
