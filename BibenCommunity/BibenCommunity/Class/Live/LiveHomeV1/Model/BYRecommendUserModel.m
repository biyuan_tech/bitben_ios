//
//  BYRecommendUserModel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRecommendUserModel.h"

@implementation BYRecommendUserModel

+ (NSArray *)getRecommendUser:(NSArray *)respond{
    BYRecommendUserModel *model = [[BYRecommendUserModel alloc] init];
    model.cellString = @"BYRecommendUserCell";
    model.cellHeight = 212;
    model.cellData = [BYRecommendUserCellModel getRecommendUser:respond];
    return @[model];
}
@end

@implementation BYRecommendUserCellModel

+ (NSArray *)getRecommendUser:(NSArray *)respond{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYRecommendUserCellModel *model = [BYRecommendUserCellModel setValuesForKeysWithDictionary:dic];
        model.cellString = @"BYRecommendUserCell";
        model.cellHeight = 160;
//        model.user_id = dic[@"user_id"];
//        model.head_img = nullToEmpty(dic[@"user"][@"head_img"]);
//        model.nickname = nullToEmpty(dic[@"user"][@"nickname"]);
//        model.isAttention = [dic[@"isAttention"] boolValue];
        [array addObject:model];
    }
    return array;
}

@end

