//
//  BYRecommendUserModel.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BYRecommendUserCellModel;
@interface BYRecommendUserModel : BYCommonModel

/** 数据源 */
@property (nonatomic ,strong) NSArray <BYRecommendUserCellModel  *>*cellData;


+ (NSArray *)getRecommendUser:(NSArray *)respond;

@end

@interface BYRecommendUserCellModel : BYCommonModel

/** 用户头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;

+ (NSArray *)getRecommendUser:(NSArray *)respond;

@end

NS_ASSUME_NONNULL_END
