//
//  BYHomeRecommendHeaderVIew.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYHomeRecommendHeaderView.h"
#import "iCarousel.h"
#import "BYHomeBannerModel.h"

@interface BYHomeRecommendHeaderView ()<iCarouselDelegate,iCarouselDataSource>
// **广告轮播
@property (nonatomic ,strong) iCarousel *adScrollerView;

@property (nonatomic ,strong) NSTimer *timer;
@property (nonatomic ,strong) UIPageControl *pageControl;
@end

static NSInteger kBaseTag = 0x442;
@implementation BYHomeRecommendHeaderView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    [self addSubview:self.adScrollerView];
    [self addSubview:self.pageControl];
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(80);
        make.height.mas_equalTo(40);
    }];
    
    [self addSortView];
    [self addMessageView];
}

- (void)reloadData{
    [_adScrollerView reloadData];
    self.pageControl.numberOfPages = _advertData.count;
    self.pageControl.currentPage = _adScrollerView.currentItemIndex;
    [self startTimer];
}

- (void)startTimer{
    if (!_advertData.count) return;
    [self destoryTimer];
    if (!_timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollAdBanner) userInfo:nil repeats:YES];
    }
}

- (void)destoryTimer{
    if (_timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)scrollAdBanner{
    NSInteger index;
    if (_adScrollerView.currentItemIndex < _advertData.count - 1)
        index = _adScrollerView.currentItemIndex + 1;
    else
        index = 0;
    [_adScrollerView scrollToItemAtIndex:index duration:0.5];
}

#pragma mark - action
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseTag;
    if (self.didSelectModuleIndex) {
        self.didSelectModuleIndex(index);
    }
}

#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return _advertData.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, CGRectGetHeight(carousel.frame))];
        PDImageView *imageView = [[PDImageView alloc] initWithFrame:CGRectMake(15, 0, kCommonScreenWidth - 30, CGRectGetHeight(carousel.frame))];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.layer.cornerRadius = 5.0f;
        [view addSubview:imageView];
        imageView.tag = 315;
    }
    PDImageView *imageView = [view viewWithTag:315];
    BYHomeBannerModel *model = _advertData[index];
    [imageView uploadMainImageWithURL:model.content placeholder:nil imgType:PDImgTypeOriginal callback:nil];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
            // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (self.didSelectAdvertIndex) {
        self.didSelectAdvertIndex(index);
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
    [self destoryTimer];
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
    [self startTimer];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    _pageControl.currentPage = carousel.currentItemIndex;
}

#pragma makr - initMethod

- (void)addSortView{
    NSArray *imgNames = @[@"livehome_ course",@"livehome_youyue",@"livehome_task",@"livehome_kxian",@"livehome_zhuanlan"];
    NSArray *titles = @[@"课程",@"币本有约",@"社区任务",@"K线直击",@"专栏"];
    CGFloat spaceW = (kCommonScreenWidth - 44*imgNames.count - 30)/(imgNames.count - 1);
    for (int i = 0; i < 5; i ++) {
        UIButton *sortBtn = [UIButton by_buttonWithCustomType];
        [sortBtn setBy_imageName:imgNames[i] forState:UIControlStateNormal];
        [sortBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        sortBtn.tag = kBaseTag + i;
        [self addSubview:sortBtn];
        [sortBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15 + (sortBtn.imageView.image.size.width + spaceW)*i);
            make.top.mas_equalTo(self.adScrollerView.mas_bottom).mas_offset(17);
            make.width.mas_equalTo(sortBtn.imageView.image.size.width);
            make.height.mas_equalTo(sortBtn.imageView.image.size.height);
        }];
        
        UILabel *titleLab = [UILabel by_init];
        titleLab.text = titles[i];
        titleLab.font = [UIFont systemFontOfSize:14];
        titleLab.textColor = kColorRGBValue(0x323232);
        [self addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(stringGetWidth(titles[i], 14));
            make.height.mas_equalTo(stringGetHeight(titles[i], 14));
            make.top.mas_equalTo(sortBtn.mas_bottom).mas_offset(12);
            make.centerX.mas_equalTo(sortBtn.mas_centerX).mas_offset(0);
        }];
    }
}

- (void)addMessageView{
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-42);
    }];
    
    UIImageView *msgImgView = [[UIImageView alloc] init];
    [msgImgView setImage:[UIImage imageNamed:@"livehome_msg"]];
    [self addSubview:msgImgView];
    [msgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-15);
        make.width.mas_equalTo(msgImgView.image.size.width);
        make.height.mas_equalTo(msgImgView.image.size.height);
    }];
    
}

- (iCarousel *)adScrollerView{
    if (!_adScrollerView) {
        _adScrollerView = [[iCarousel alloc] initWithFrame:CGRectMake(0, 7, kCommonScreenWidth, 120)];
        _adScrollerView.delegate = self;
        _adScrollerView.dataSource = self;
        _adScrollerView.pagingEnabled = YES;
    }
    return _adScrollerView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor = kBgColor_237;
        _pageControl.currentPageIndicatorTintColor = kTextColor_238;
    }
    return _pageControl;
}


@end
