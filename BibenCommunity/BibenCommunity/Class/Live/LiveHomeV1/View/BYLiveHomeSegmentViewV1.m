//
//  BYLiveHomeSegmentViewV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeSegmentViewV1.h"

@interface BYLiveHomeSegmentViewV1()<UIScrollViewDelegate>
{
    /** subView width */
    CGFloat _sub_width;
    NSInteger _index;
    NSInteger _lastIndex;
    CGFloat _beginDragOffsetX;
}

/** 标题存储，也用于初始化子View */
@property (nonatomic ,strong) NSArray *titles;

/** 存储headerView按钮 */
@property (nonatomic ,strong) NSArray *buttons;

/** headerView */
@property (nonatomic ,strong) UIScrollView *headerView;

/** 选择项跟踪横线 */
@property (nonatomic ,strong) UIView *selLineView;

/** contentView,subView容器 */
@property (nonatomic ,strong) UIScrollView *scrollView;

@end

static NSInteger baseTag = 0x943;
static CGFloat headerHeight = 33.0f;
@implementation BYLiveHomeSegmentViewV1

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION{
    NSMutableArray *arrays = [NSMutableArray array];
    va_list argList;
    if (titles.length){
        [arrays addObject:titles];
        va_start(argList, titles);
        id temp;
        while ((temp = va_arg(argList, id))){
            [arrays addObject:temp];
        }
    }
    BYLiveHomeSegmentViewV1 *segmentView = [[BYLiveHomeSegmentViewV1 alloc] init];
    segmentView.titles = arrays;
    [segmentView setContentView];
    segmentView.defultSelIndex = 0;
    segmentView->_lastIndex = 0;
    return segmentView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    UIButton *button = [self viewWithTag:baseTag + _lastIndex];
    self.selLineView.center = CGPointMake(button.center.x, self.selLineView.center.y);
}

- (void)setDelegate:(id<BYLiveHomeSegmentViewDelegateV1>)delegate{
    _delegate = delegate;
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:_defultSelIndex];
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + _defultSelIndex];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:_defultSelIndex];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
}

#pragma mark - customMehtod
- (void)didSelectSubViewIndex:(NSInteger)index{
    // 设置高亮
    UIButton *button = [self viewWithTag:baseTag + index];
    for (UIButton *btn in self.buttons) {
        btn.selected = button == btn ? YES : NO;
        if (btn == button) {
            [UIView animateWithDuration:0.1 animations:^{
                self.selLineView.center = CGPointMake(btn.center.x, self.selLineView.center.y);
            }];
            
            if (btn.center.x > self.headerView.center.x) {
                if (self.headerView.contentSize.width - btn.center.x < self.headerView.center.x) {
                    [self.headerView setContentOffset:CGPointMake(self.headerView.contentSize.width - CGRectGetWidth(self.headerView.frame), 0) animated:YES];
                    return;
                }
                CGFloat offsetX = btn.center.x - self.headerView.center.x;
                [self.headerView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
                
            }
            else if (btn.center.x < self.headerView.center.x && self.headerView.contentOffset.x != 0) {
                [self.headerView setContentOffset:CGPointMake(0, 0) animated:YES];
            }
        }
    }
    _lastIndex = index;
}


#pragma mark - buttonAction
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    // 如与上次选择一样则返回
    if (index == _lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}

- (void)setDefultSelIndex:(NSInteger)defultSelIndex{
    _defultSelIndex = defultSelIndex;
//    [self didSelectSubViewIndex:defultSelIndex];
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*defultSelIndex, 0) animated:YES];
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat indexFloat = (CGFloat)scrollView.contentOffset.x/kCommonScreenWidth;
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (indexFloat < index) {
        index = round(scrollView.contentOffset.x/kCommonScreenWidth);
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + index];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:index];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            [self scrollViewDidEndScroll:scrollView];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndScroll:(UIScrollView *)scrollView{
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (_lastIndex == index) return;
    [self didSelectSubViewIndex:index];
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:index];
    }
}

#pragma mark - configSubView Method

- (void)setContentView{
    UIScrollView *headerView = [self getHeaderView];
    [self addSubview:headerView];
    _headerView = headerView;
    UIButton *button = [headerView viewWithTag:baseTag + _titles.count - 1];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.mas_equalTo(0);
        make.height.mas_equalTo(headerHeight);
        make.right.equalTo(button.mas_right).offset(15).priorityLow();
    }];
    
    
    UIScrollView *scrollView = [self getScrollView];
    scrollView.delegate = self;
    [self addSubview:scrollView];
    _scrollView = scrollView;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(headerHeight, 0, 0, 0));
    }];
    
}

// headerView
- (UIScrollView *)getHeaderView{
    UIScrollView *headerView = [UIScrollView by_init];
    headerView.showsHorizontalScrollIndicator = NO;
    NSMutableArray *arrays = [NSMutableArray array];
    _sub_width = 93;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)};
        NSDictionary *highlight = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x323232)};
//        button.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, -15 , 0);
        [button setBy_attributedTitle:normalDic forState:UIControlStateNormal];
        [button setBy_attributedTitle:highlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [arrays addObject:button];
        UIButton *lastBtn = [headerView viewWithTag:baseTag + i - 1];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(stringGetWidth(title, 14));
            make.top.bottom.mas_equalTo(0);
            if (i == 0) {
                make.left.mas_equalTo(15);
            }else{
                make.left.mas_equalTo(lastBtn.mas_right).mas_offset(20);
            }
        }];
    }
    self.buttons = arrays;
    
    UIButton *button = [headerView viewWithTag:baseTag];
    UIView *selLineView = [UIView by_init];
    selLineView.backgroundColor = kColorRGBValue(0xea6441);
    selLineView.layer.cornerRadius = 1.0f;
    [headerView addSubview:selLineView];
    _selLineView = selLineView;
    selLineView.frame = CGRectMake(0, headerHeight - 2, 16, 2);
    selLineView.center = CGPointMake(button.center.x, selLineView.center.y);
    
    return headerView;
}

// contentView
- (UIScrollView *)getScrollView{
    UIScrollView *scrollView = [UIScrollView by_init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    for (int i = 0; i < _titles.count; i ++) {
        UIView *view = [UIView by_init];
        [scrollView addSubview:view];
        view.tag = 2*baseTag + i;
        @weakify(self);
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.mas_equalTo(kCommonScreenWidth*i);
            make.top.mas_equalTo(0);
            make.height.equalTo(scrollView.mas_height).with.offset(0);
            make.width.equalTo(scrollView.mas_width).with.offset(0);
            if (i == self.titles.count - 1) {
                make.right.mas_equalTo(0);
            }
        }];
    }
    return scrollView;
}

@end
