//
//  BYHomeRecommendHeaderView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeRecommendHeaderView : UIView

/**
 广告数据
 */
@property (nonatomic ,strong) NSArray *advertData;

/**
 广告点击index
 */
@property (nonatomic ,copy) void (^didSelectAdvertIndex)(NSInteger index);

@property (nonatomic ,copy) void (^didSelectModuleIndex)(NSInteger index);


- (void)reloadData;

/** 启动定时 */
- (void)startTimer;
/** 销毁定时 */
- (void)destoryTimer;

@end

NS_ASSUME_NONNULL_END
