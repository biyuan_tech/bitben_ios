//
//  BYLiveHomeSegmentViewV1.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BYLiveHomeSegmentViewDelegateV1 <NSObject>

/** 根据index自定义添加View */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

/** 当前滚动到的Index */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;

@end

@interface BYLiveHomeSegmentViewV1 : UIView



/** 初始化选择的index (默认0) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
@property (nonatomic ,weak) id<BYLiveHomeSegmentViewDelegateV1>delegate;

/** 点击分类回调 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION;

@end

NS_ASSUME_NONNULL_END
