//
//  BYHomeRecommendController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYHomeRecommendController.h"
#import "BYVideoRoomController.h"
#import "BYILiveRoomController.h"
#import "LivePPTRootViewController.h"
#import "BYPPTUploadController.h"
#import "BYVODPlayController.h"
#import "BYLiveHomeController.h"
#import "BYPPTRoomController.h"
#import "ArticleDetailRootViewController.h"
#import "BYPersonHomeController.h"
#import "BYVideoLiveController.h"

#import "BYHomeBannerModel.h"

#import "BYHomeRecommendHeaderView.h"

@interface BYHomeRecommendController ()

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** headerView */
@property (nonatomic ,strong) BYHomeRecommendHeaderView *headerView;

@end

@implementation BYHomeRecommendController


- (void)viewDidLoad{
    [super viewDidLoad];
    [self addTableView];
    
    [self setEmptyTableData];
    [self loadRequestGetBanner];
    [self loadRequestGetRecommendLive];
}

#pragma mark - custom Method

- (void)setEmptyTableData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 4; i ++) {
        BYCommonLiveModel *model = [[BYCommonLiveModel alloc] init];
        model.cellString = @"BYCommonTableViewCell";
        model.cellHeight = 0;
        [array addObject:@{@"data":@[model]}];
    }
    self.tableView.tableData = [array copy];
}

- (void)headerViewDidSelectAdvertIndex:(NSInteger)index{
    BYHomeBannerModel *bannerModel = _headerView.advertData[index];
    // banner点击统计
    [[BYLiveHomeRequest alloc] loadRequestClickBanner:bannerModel.banner_id];
    
    if (bannerModel.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = bannerModel.theme_id;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [self authorizePush:articleController animation:YES];
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_LIVE) { // 跳转直播
        switch (bannerModel.live_type) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                BYLiveConfig *config = [[BYLiveConfig alloc] init];
                config.isHost = NO;
                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                ilivewRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self authorizePush:ilivewRoomController animation:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self authorizePush:videoRoomController animation:YES];
                
            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
                pptRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                pptRoomController.isHost = NO;
                [self authorizePush:pptRoomController animation:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self authorizePush:vodPlayController animation:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VIDEO:
            {
                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
                videoLiveController.live_record_id = nullToEmpty(bannerModel.theme_id);
                videoLiveController.isHost = NO;
                [self authorizePush:videoLiveController animation:YES];
            }
                break;
            default:
                break;
        }
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
        [webViewController webDirectedWebUrl:bannerModel.theme_id];
        [self authorizePush:webViewController animation:YES];
    }
}

#pragma mark - request

- (void)loadRequestGetBanner{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetHomeBannerSuccessBlock:^(id object) {
        @strongify(self);
        self.headerView.advertData = object;
        [self.headerView reloadData];
    } faileBlock:^(NSError *error) {
        PDLog(@"%@",error);
    }];
}

- (void)loadRequestGetRecommendLive{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveHomeRecommendLive:^(id object) {
        @strongify(self);
        NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [tableData replaceObjectAtIndex:0 withObject:object[0]];
        [tableData replaceObjectAtIndex:1 withObject:object[1]];
        self.tableView.tableData = tableData;
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.tableHeaderView = self.headerView;;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYHomeRecommendHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYHomeRecommendHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 275);
        @weakify(self);
        _headerView.didSelectAdvertIndex = ^(NSInteger index) {
            @strongify(self);
            [self headerViewDidSelectAdvertIndex:index];
        };
    }
    return _headerView;
}

@end
