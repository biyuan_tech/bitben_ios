//
//  BYVideoLiveController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoLiveController : BYCommonViewController

/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 是否为主播 */
@property (nonatomic ,assign) BOOL isHost;

@end

NS_ASSUME_NONNULL_END
