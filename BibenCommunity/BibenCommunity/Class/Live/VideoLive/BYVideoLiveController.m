//
//  BYVideoLiveController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveController.h"
#import "BYVideoLiveViewModel.h"

@interface BYVideoLiveController ()<NetworkAdapterDelegate>

@end

@implementation BYVideoLiveController

- (Class)getViewModelClass{
    return [BYVideoLiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.popGestureRecognizerEnale = NO;
    [NetworkAdapter sharedAdapter].delegate = self;
    [[BYSIMManager shareManager] connectionSocket];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[BYSIMManager shareManager] closeSocket];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popGestureRecognizerEnale = NO;
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}


@end
