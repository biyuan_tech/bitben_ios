//
//  BYVideoLiveIMController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveIMController.h"
#import "BYPPTUploadController.h"
#import "BYLiveRoomHomeController.h"
#import "BYIMChatViewController.h"
#import "NetworkAdapter.h"
#import "PDScrollView.h"
#import "BYHostOperationView.h"
#import "LiveDetailModel.h"
#import "SPMessageInputView.h"
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import "BYReportSheetView.h"
@interface BYVideoLiveIMController ()<UIGestureRecognizerDelegate,YWMessageInputViewDelegate,AliChatManagerDelegate>

@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)BYHostOperationView *hostOpeartionView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYVideoLiveIMController


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
//        SPMessageInputView *messageInputView = [[SPMessageInputView alloc] init];
//        self.messageInputView = messageInputView;
    [AliChatManager sharedInstance].ywIMKit.IMCore.getContactService.enableContactOnlineStatus = YES;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    if (_isHost) {
        [self addHostOperationPlugin];
    }
    else{
        [self addGuestOperationPlugin];
    }
    [[AliChatManager sharedInstance] removeExcessPlugin:self];
    if (!_isHost) {
        [[AliChatManager sharedInstance] disableVoiceWithConversationController:self];
    }
    [self bottomSetting];
    //    [self addOpeartionView];
    //    [self sendRequestToGetLiveClass];
    // 键盘代理
    self.messageInputView.messageInputViewDelegate = self;
    self.customizeDelegate = self;
    // 隐藏输入框
    //    [self setMessageInputViewHidden:YES animated:NO];
    
    
    [AliChatManager sharedInstance].messageDelegate = self;
    [self.conversation setOnNewMessageBlockV2:^(NSArray *aMessages, BOOL aIsOffline) {
        NSLog(@"%@",aMessages);
    }];
    
    // 禁言
    if (_transferInfoModel.status == BY_LIVE_STATUS_END ||
        _transferInfoModel.status == BY_LIVE_STATUS_OVERTIME) {
        [[AliChatManager sharedInstance] disableSendMsgWithConversationController:self];
    }
    //    if (!_isHost) {
    //        [[AliChatManager sharedInstance] disableVoiceWithConversationController:self];
    //    }
}

#pragma mark - plugin添加插件
// 添加主播插件
- (void)addHostOperationPlugin{
    self.hostOpeartionView = [[BYHostOperationView alloc] initWithFathureView:kCommonWindow];
    @weakify(self);
//    _hostOpeartionView.didEndLiveHandle = ^{
//        @strongify(self);
//        [self showExitSheetView];
//    };
    
    // 主播操作
    [[AliChatManager sharedInstance] exampleAddHostInputViewPluginToConversationController:self pluginHandle:^{
        @strongify(self); 
        [self.hostOpeartionView showAnimation];
    }];
    
    // 关闭直播
    [[AliChatManager sharedInstance] exampleAddHostExitLiveInputViewPluginToConversationController:self pluginHandle:^{
        @strongify(self);
        [self showExitSheetView];
    }];
}

// 添加观众插件
- (void)addGuestOperationPlugin{
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = self.transferInfoModel.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.transferInfoModel.user_id;
    
    // 举报操作
    @weakify(self);
    [[AliChatManager sharedInstance] exampleAddGuestReportInputViewPluginToConversationConstroller:self pluginHandle:^{
        @strongify(self);
        [self.reportSheetView showAnimation];
    }];
}

// 结束直播弹框
#pragma mark - 结束直播
- (void)showExitSheetView{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"确定要结束直播吗？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        if (self.didFinishLiveHandle) self.didFinishLiveHandle();
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //        [[AliChatManager sharedInstance] exampleSendFinishLiveMessageWithConversationController:self];
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [self presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - AliChatManagerDelegate
-(void)openProfileWithPersonId:(NSString *)personId{
    
}

-(void)listenNewMessageManagerWithMessage:(id<IYWMessage>)message{
    if ([message.messageBody isKindOfClass:[YWMessageBodyCustomize class]]) {
        YWMessageBodyCustomize *customizeMessageBody = (YWMessageBodyCustomize *)[message messageBody];
        NSData *contentData = [customizeMessageBody.content dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *contentDictionary = [NSJSONSerialization JSONObjectWithData:contentData
                                                                          options:0
                                                                            error:NULL];
        NSString *messageType = contentDictionary[kBYCustomizeMessageType];
        if ([messageType isEqualToString:BYCustomizeMessageType_EndLive]) {
            self.transferInfoModel.status = BY_LIVE_STATUS_END;
            [[AliChatManager sharedInstance] disableSendMsgWithConversationController:self];
            if (self.didReceiveLiveEndMsgHandle) {
                self.didReceiveLiveEndMsgHandle();
            }
            //            [[self.kitRef.IMCore getConversationService] setDisableInputStatus:YES];
        }
        else if ([messageType isEqualToString:BYCustomizeMessageType_BeginLive]){
            self.transferInfoModel.status = BY_LIVE_STATUS_LIVING;
            if (self.didBeginLiveHandle) {
                self.didBeginLiveHandle();
            }
        }
    }
}

#pragma mark - bottom

-(void)bottomSetting{
    if (self.tableView.superview) {
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, self.messageInputView.size_height, 0));
        }];
        
    }
}

- (BOOL)messageInputViewShouldEndEditing:(UIView<IYWMessageInputView> *)inputView{
    
    return YES;
}

//beginListeningForKeyboard
- (BOOL)messageInputViewShouldBeginEditing:(UIView<IYWMessageInputView> *)inputView{
    if (_transferInfoModel.status == BY_LIVE_STATUS_END ||
        _transferInfoModel.status == BY_LIVE_STATUS_OVERTIME) {
        showToastView(@"直播已结束，请至点评区发言", self.viewController.view);
        return NO;
    }
    return YES;
}

- (void)messageInputView:(UIView<IYWMessageInputView> *)inputView heightDidChange:(CGFloat)height{
    PDLog(@"%s-%f",__func__,height);
    if (height > 200) {
        CGRect frame = inputView.frame;
        frame.origin.y = inputView.superview.frame.size.height - height + kSafeAreaInsetsBottom;
        inputView.frame = frame;
    }
    else{
        CGRect frame = inputView.frame;
        frame.origin.y = inputView.superview.frame.size.height - height;
        inputView.frame = frame;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    if (self.scrollViewDidScroll) self.scrollViewDidScroll(scrollView);
}



@end
