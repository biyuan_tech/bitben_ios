//
//  BYVideoLiveRewradTableCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveRewradTableCell.h"

@interface BYVideoLiveRewradTableCell ()

/** title */
@property (nonatomic ,strong) UILabel *rewardLab;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;


@end

@implementation BYVideoLiveRewradTableCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *rewardLab = [[UILabel alloc] init];
        rewardLab.font = [UIFont systemFontOfSize:12];
        rewardLab.textColor = [UIColor whiteColor];
        rewardLab.textAlignment = NSTextAlignmentCenter;
        rewardLab.numberOfLines = 0;
        [self.contentView addSubview:rewardLab];
        self.rewardLab = rewardLab;
        [rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.width.mas_greaterThanOrEqualTo(0);
            make.height.mas_greaterThanOrEqualTo(0);
        }];
        
        UIView *maskView = [UIView by_init];
        maskView.backgroundColor = kColorRGBValue(0x424242);
        [self addSubview:maskView];
        [self sendSubviewToBack:maskView];
        self.maskView = maskView;
        [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.width.mas_greaterThanOrEqualTo(0);
            make.height.mas_greaterThanOrEqualTo(0);
        }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configCellByData:(CDChatMessage)data table:(CDChatListView *)table{
    self.rewardLab.text = data.customData[@"text"];
    CGSize size = [self.rewardLab.text getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:kCommonScreenWidth-60];
    [self.rewardLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
    }];
    
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(size.width) + 30);
        make.height.mas_equalTo(ceil(size.height) + 12);
    }];
    self.maskView.layer.cornerRadius = (ceil(size.height) + 12)/2;
}

@end
