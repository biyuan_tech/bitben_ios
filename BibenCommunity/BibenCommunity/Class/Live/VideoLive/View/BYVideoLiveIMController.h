//
//  BYVideoLiveIMController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import <WXOUIModule/YWConversationViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoLiveIMController : YWConversationViewController

@property (nonatomic,weak)UIViewController *viewController;
@property (nonatomic,copy)NSString *transferRoomId;
@property (nonatomic,assign)BOOL isHost;
@property (nonatomic,strong)BYCommonLiveModel *transferInfoModel;
/** 当前用于push的viewController */
@property (nonatomic,weak)UIViewController *pushViewController;

/** 观众端接收到下播消息 */
@property (nonatomic,copy) void (^didFinishLiveHandle)(void);
/** 观众端接收到开播消息 */
@property (nonatomic ,copy) void (^didBeginLiveHandle)(void);

@property (nonatomic,copy) void (^scrollViewDidScroll)(UIScrollView *scrollView);
/** 直播结束回调 */
@property (nonatomic ,copy) void (^didReceiveLiveEndMsgHandle)(void);


@end

NS_ASSUME_NONNULL_END
