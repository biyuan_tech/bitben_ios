//
//  BYLiveRPCDefine.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#ifndef BYLiveRPCDefine_h
#define BYLiveRPCDefine_h

/** 首页获取推荐直播+感兴趣用户 */
static NSString * RPC_recommend_live = @"recommend_live";

/** 获取直播首页列表 */
//static NSString * RPC_get_all_live_list = @"get_all_live_list";
/** 获取直播首页列表 */
static NSString * RPC_get_homepage_list = @"get_homepage_list";

/** 首页banner点击统计 */
static NSString * RPC_click_banner = @"click_banner";

/** 获取上传凭证 */
static NSString * RPC_Get_upload_signature = @"get_upload_signature";

/** 上传视频信息成功后同步到服务端 */
static NSString * RPC_insert_vod_record = @"insert_vod_record";

/** 创建在直播间 */
static NSString * RPC_create_live_room = @"create_live_room";

/** 更新直播状态(上报直播状态，结束直播) */
static NSString * RPC_switch_live = @"switch_live";

/** 获取直播登录的sig */
static NSString * RPC_get_user_sig = @"get_user_sig";

/** 新建直播记录 */
static NSString * RPC_create_live_record = @"create_live_record";

/** 更新直播间信息 */
static NSString * RPC_update_live_room = @"update_live_room";

/** 更新直播信息 */
static NSString * RPC_update_live_record = @"update_live_record";

/** 获取直播间信息 */
static NSString * RPC_get_live_room = @"get_live_room";

/** 获取直播列表 */
static NSString * RPC_get_live_list = @"get_live_list";

/** 获取首页banner列表 */
static NSString * RPC_get_banner_list = @"get_banner_list";

/** 判断是否有直播间 */
static NSString * RPC_whether_has_room = @"whether_has_room";

/** 更新ppt直播课件 */
static NSString * RPC_update_courseware = @"update_courseware";

/** 直播顶踩 */
static NSString * RPC_create_interaction = @"create_interaction";

/** 直播打赏、付费提问 */
static NSString * RPC_create_reward = @"create_reward";

/** 获取当前打赏的剩余金额 */
static NSString * RPC_reward_window = @"reward_window";

/** 心跳包 */
static NSString * RPC_get_heartbeat = @"get_heartbeat";;

/** 获取直播详情数据 */
static NSString * RPC_get_live_details = @"get_live_details";

/** 关注预约直播 */
static NSString * RPC_do_attention_live = @"do_attention_live";

/** 取消预约直播 */
static NSString * RPC_delete_attention_live = @"delete_attention_live";

/** 插入聊天记录 */
static NSString * RPC_insert_chat_history = @"insert_chat_history";

/** 获取聊天记录 */
static NSString * RPC_get_chat_history = @"get_chat_history";

/** 举报 */
static NSString * RPC_create_report = @"create_report";

/** 获取他人主页 */
static NSString * RPC_get_his_data = @"get_his_data";

/** 获取指定作者文章 */
static NSString *RPC_page_author_article = @"page_author_article";

/** 查询我的点评 */
static NSString *RPC_page_my_comment = @"page_my_comment";

/** 连麦混流 */
static NSString *RPC_mix_stream = @"mix_stream";

/** 获取推流地址 */
static NSString *RPC_get_push_url = @"get_push_url";

/** 分页获取消息记录 */
static NSString *RPC_page_chat_history = @"page_chat_history";

/** 评论 */
static NSString *RPC_page_comment = @"page_comment";

#endif /* BYLiveRPCDefine_h */
