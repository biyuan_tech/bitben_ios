//
//  BYLiveController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveController.h"
#import "BYLiveViewModel.h"

@interface BYLiveController ()

@end

@implementation BYLiveController

- (Class)getViewModelClass{
    return [BYLiveViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationTitle = @"直播";
  
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 移除新建直播
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        // 移除直播创建界面
        if ([NSStringFromClass([viewController class]) isEqualToString:@"BYNewLiveController"]) {
            NSMutableArray *tmpViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [tmpViewControllers removeObject:viewController];
            self.navigationController.viewControllers = tmpViewControllers;
        }
        else if ([NSStringFromClass([viewController class]) isEqualToString:@"BYLiveController"]){  // 移除前直播界面
            
            if (viewController != self) {
                NSMutableArray *tmpViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                [tmpViewControllers removeObject:viewController];
                self.navigationController.viewControllers = tmpViewControllers;
                
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
