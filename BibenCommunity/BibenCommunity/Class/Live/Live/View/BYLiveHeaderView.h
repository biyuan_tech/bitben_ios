//
//  BYLiveHeaderView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonLiveModel;
@interface BYLiveHeaderView : UIView

@property (nonatomic ,strong ,readonly) BYCommonLiveModel *model;

- (void)reloadData:(BYCommonLiveModel *)model;
@end
