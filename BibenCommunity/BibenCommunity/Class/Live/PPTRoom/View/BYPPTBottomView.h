//
//  BYPPTBottomView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/17.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPPTBottomView : UIView


/** more handle */
@property (nonatomic ,copy) void (^moreBtnHandle)(void);


@end

NS_ASSUME_NONNULL_END
