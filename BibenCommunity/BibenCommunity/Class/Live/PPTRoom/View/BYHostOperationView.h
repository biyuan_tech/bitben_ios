//
//  BYHostOperationView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/11.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHostOperationView : UIView

/** 结束直播回调 */
@property (nonatomic ,copy) void (^didEndLiveHandle)(void);

- (instancetype)initWithFathureView:(UIView *)fathureView;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
