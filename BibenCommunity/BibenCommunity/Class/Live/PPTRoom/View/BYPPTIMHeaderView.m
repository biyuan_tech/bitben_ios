//
//  BYPPTIMHeaderView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPPTIMHeaderView.h"
#import "BYPPTImgModel.h"
#import "iCarousel.h"

@interface BYPPTIMHeaderView ()<iCarouselDelegate,iCarouselDataSource>
/** 广告轮播 */
@property (nonatomic ,strong) iCarousel *adScrollerView;
@property (nonatomic ,strong) UIPageControl *pageControl;
/** 页码 */
@property (nonatomic ,strong) UILabel *pageLab;
/** 倒计时 */
@property (nonatomic ,strong) UILabel *timerLab;
@property (nonatomic ,strong) UIView *bottomView;
@end

@implementation BYPPTIMHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kColorRGBValue(0xefefef);
        [self setContentView];
    }
    return self;
}

- (void)setBegin_time:(NSString *)begin_time{
    _begin_time = begin_time;
    [self verifyLiveTime];
}

- (void)destoryTimer{
    [[BYCommonTool shareManager] stopTimer];
}

- (void)setContentView{
    [self addEmptyView];
    [self addSubview:self.adScrollerView];
    [_adScrollerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    _adScrollerView.hidden = YES;
    [self addSubview:self.pageControl];
    _pageControl.hidden = YES;
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    [self addBottomView];
    
    [self addSubview:self.pageLab];
    @weakify(self);
    [_pageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
        make.centerY.equalTo(self.bottomView.mas_centerY).with.offset(0);
    }];
}

- (void)addBottomView{
    self.bottomView = [UIView by_init];
    self.bottomView.hidden = YES;
    self.bottomView.backgroundColor = kColorRGB(50, 50, 50, 0.47);
    [self addSubview:self.bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    
    self.timerLab = [UILabel by_init];
    [self.timerLab setBy_font:13];
    self.timerLab.textColor = kColorRGBValue(0xfefefe);
    [_bottomView addSubview:self.timerLab];
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.top.bottom.mas_equalTo(0);
    }];
}

- (void)addEmptyView{
    UIView *emptyView = [UIView by_init];
    [self addSubview:emptyView];
    [emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.centerY.mas_equalTo(-40/2);
    }];
    
    UILabel *dfLab = [UILabel by_init];
    [dfLab setBy_font:14];
    dfLab.textColor = kColorRGBValue(0x808080);
    dfLab.text = @"1.点击底部课件，可提前上传课件；";
    dfLab.numberOfLines = 0;
    [dfLab sizeToFit];
    [emptyView addSubview:dfLab];
    [dfLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(14);
    }];
    
    UILabel *subDfLab = [UILabel by_init];
    [subDfLab setBy_font:14];
    subDfLab.textColor = kColorRGBValue(0x808080);
    subDfLab.text = @"2.如需上传PPT，请将PPT批量另存为图片后，再批量上传，最佳比例为";
    subDfLab.numberOfLines = 0;
    [subDfLab sizeToFit];
    [emptyView addSubview:subDfLab];
    [subDfLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.equalTo(dfLab.mas_bottom).with.offset(14);
        make.height.mas_greaterThanOrEqualTo(14);
        make.bottom.mas_equalTo(0);
    }];
    
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        if (_beginLiveHandle) _beginLiveHandle();
    }
    else // 未到开播时间，倒计时自动开播
    {
        // 直播倒计时
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                if (self.beginLiveHandle) self.beginLiveHandle();
                self.bottomView.hidden = YES;
            }
            else{
                self.bottomView.hidden = NO;
                self.timerLab.text = [NSString stringWithFormat:@"直播倒计时 : %02d天 %02d时 %02d分 %02d秒", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
                
            }
        }];
    }
}

- (void)reloadData{
    _adScrollerView.scrollEnabled = _advertData.count > 1 ? YES : NO;
    _pageControl.hidden = _advertData.count > 1 ? NO : YES;
    [_adScrollerView reloadData];
    self.pageControl.numberOfPages = _advertData.count;
    self.pageControl.currentPage = _adScrollerView.currentItemIndex;
    self.adScrollerView.hidden = !_advertData.count ? YES : NO;
    self.pageControl.hidden = !_advertData.count ? YES : NO;
    
    self.pageLab.text = [NSString stringWithFormat:@"%i / %i",(int)(_adScrollerView.currentItemIndex) + 1,(int)(_advertData.count)];
    
}

- (void)scrollAdBanner{
    NSInteger index;
    if (_adScrollerView.currentItemIndex < _advertData.count - 1)
        index = _adScrollerView.currentItemIndex + 1;
    else
        index = 0;
    [_adScrollerView scrollToItemAtIndex:index duration:0.5];
}

#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return _advertData.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (view == nil) {
        view = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, LCFloat(195))];
        ((PDImageView *)view).contentMode = UIViewContentModeScaleAspectFill;
        ((PDImageView *)view).clipsToBounds = YES;
    }
    BYPPTImgModel *model = _advertData[index];
    if (model.image) {
        [(PDImageView *)view setImage:model.image];
    }
    else{
        [(PDImageView *)view uploadMainImageWithURL:model.url placeholder:nil imgType:PDImgTypeOriginal callback:nil];
    }
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
            // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    _pageControl.currentPage = carousel.currentItemIndex;
    self.pageLab.text = [NSString stringWithFormat:@"%i / %i",(int)(carousel.currentItemIndex) + 1,(int)(_advertData.count)];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_viewController.view endEditing:YES];
}

#pragma makr - initMethod

- (iCarousel *)adScrollerView{
    if (!_adScrollerView) {
        _adScrollerView = [[iCarousel alloc] init];
        _adScrollerView.delegate = self;
        _adScrollerView.dataSource = self;
        _adScrollerView.pagingEnabled = YES;
        _adScrollerView.clipsToBounds = YES;
    }
    return _adScrollerView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor = kBgColor_237;
        _pageControl.currentPageIndicatorTintColor = kTextColor_238;
    }
    return _pageControl;
}

- (UILabel *)pageLab{
    if (!_pageLab) {
        _pageLab = [[UILabel alloc] init];
        [_pageLab setBy_font:12];
        _pageLab.textColor = [UIColor whiteColor];
    }
    return _pageLab;
}

@end
