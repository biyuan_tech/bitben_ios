//
//  BYPPTBottomView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/17.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPPTBottomView.h"

@implementation BYPPTBottomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    UIImageView *voiceImgView = [[UIImageView alloc] init];
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"CustomizedUIResources" ofType:@"bundle"];
    [voiceImgView setImage:[UIImage imageNamed:@"input_ico_voice_nor" inBundle:[NSBundle bundleWithPath:bundlePath] compatibleWithTraitCollection:nil]];
    [self addSubview:voiceImgView];
    [voiceImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-10);
        make.width.mas_equalTo(voiceImgView.image.size.width);
        make.height.mas_equalTo(voiceImgView.image.size.height);
    }];
    
    UIView *emptyView = [[UIView alloc] init];
    emptyView.backgroundColor = kColorRGBValue(0xeeeeee);
    emptyView.layer.cornerRadius = 5.0f;
    [self addSubview:emptyView];
    [emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(voiceImgView.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(-10);
        make.right.mas_equalTo(-55);
        make.height.mas_equalTo(27);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    [label setBy_font:12];
    [label setTextColor:kColorRGBValue(0x4e4e4e)];
    label.text = @"直播已经结束，禁止发言";
    [emptyView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(label.text, 12));
        make.height.mas_equalTo(stringGetHeight(label.text, 12));
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
    }];
    
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"talk_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-10);
        make.width.mas_equalTo(moreBtn.imageView.image.size.width);
        make.height.mas_equalTo(moreBtn.imageView.image.size.height);
    }];
}

- (void)moreBtnAction:(UIButton *)sender{
    if (self.moreBtnHandle) self.moreBtnHandle();
}
@end
