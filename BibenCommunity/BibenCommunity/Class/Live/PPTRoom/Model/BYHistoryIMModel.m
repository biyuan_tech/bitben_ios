//
//  BYHistoryIMModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYHistoryIMModel.h"

@implementation BYHistoryIMModel

@synthesize audioSufix;

@synthesize audioText;

@synthesize audioTime;

@synthesize bubbleWidth;

@synthesize cellHeight;

@synthesize chatConfig;

@synthesize createTime;

@synthesize ctDataconfig;

@synthesize isLeft;

@synthesize messageId;

@synthesize modalInfo;

@synthesize msg;

@synthesize msgState;

@synthesize msgType;

@synthesize reuseIdentifierForCustomeCell;

@synthesize textlayout;

@synthesize userName;

@synthesize userThumImage;

@synthesize userThumImageURL;

@synthesize willDisplayTime;

@synthesize identity;

@synthesize isRead;

@synthesize hasReward;

@synthesize hasBanned;

@synthesize bubbleSize;

@synthesize userId;

@synthesize customData;

@synthesize audioPath;

+ (NSArray *)getTableData:(NSArray *)responData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *data = [NSMutableArray array];
    NSMutableArray *tmpMuArr = [NSMutableArray arrayWithArray:responData];
//    NSArray *tmpArr = [[tmpMuArr reverseObjectEnumerator] allObjects];
    for (NSDictionary *tmpdic in tmpMuArr) {
        NSDictionary *dic = tmpdic[@"message"];
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = YES;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"time"] integerValue]);
        model.isLeft = YES;
        model.userId = nullToEmpty(dic[@"from_id"][@"uid"]);
        if ([liveData.user_id isEqualToString:model.userId]) {
            model.identity = @"主持人";
        }
        model.userName = dic[@"nickname"];
        model.messageId = [NSString stringWithFormat:@"%i",[dic[@"uuid"] intValue]];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 36;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = [UIColor whiteColor];
//        chatConfig.headSideLength = 18.0f;
//        chatConfig.messageMargin = 15.0f;
        model.chatConfig = chatConfig;
        NSArray *contentArr = dic[@"content"][@"message_item"];
        if ([contentArr count] == 1) {
            NSDictionary *tmpDic = contentArr[0];
            if ([contentArr[0][@"type"] isEqualToString:@"T"]) { // 语音
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeAudio;
                model.msgState = CDMessageStateSending;
                model.audioSufix = @"amr";
                model.audioTime = [[model.msg componentsSeparatedByString:@"duration="].lastObject intValue];
                model.isRead = [[BYIMMessageDB shareManager] selectIsReadWhereMsgId:model.messageId];
                // 语音消息已读状态存储
                [[BYIMMessageDB shareManager] addIMMessages:model];
            }
            else{
                continue;
            }
        }
        else{
            NSDictionary *tmpDic = contentArr[0];
            if ([tmpDic[@"type"] isEqualToString:@"T"]) { // 纯文案
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeText;
            }
            else if ([tmpDic[@"type"] isEqualToString:@"P"]) { // 图片
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeImage;
            }
        }
        [data addObject:model];
    }
    return data;
}

+ (NSArray *)getSIMTableData:(NSArray *)respondData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = NO;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"send_time"] integerValue]);
        model.isLeft = YES;
        model.userId = dic[@"user_id"];
        model.userName = dic[@"nickname"];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
        if ([liveData.user_id isEqualToString:model.userId]) {
            model.identity = @"主持人";
        }
        model.messageId = dic[@"chat_id"];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 36;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = [UIColor whiteColor];
        chatConfig.sysInfoPadding = 6.0f;
        chatConfig.sysInfoMessageFont = [UIFont systemFontOfSize:11.0f];
        model.chatConfig = chatConfig;
        
        switch (type) {
            case BY_SIM_MSG_TYPE_TEXT:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeText;
            }
                break;
            case BY_SIM_MSG_TYPE_AUDIO:
            {
                model.msg = nullToEmpty(dic[@"content"]);
                model.audioSufix = @"wav";
                model.msgType = CDMessageTypeAudio;
                model.isRead = [[BYIMMessageDB shareManager] selectIsReadWhereMsgId:model.messageId];
                // 语音消息已读状态存储
                [[BYIMMessageDB shareManager] addIMMessages:model];
            }
                break;
            case BY_SIM_MSG_TYPE_CUSTOME:
            {
                model.msgType = CDMessageTypeCustome;
                model.reuseIdentifierForCustomeCell = @"RewardCell";
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                model.customData = customData;
            }
                break;
            case BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeSystemInfo;
            }
                break;
            default:
                break;
        }
        [array addObject:model];
    }

    return array;
}

@end
