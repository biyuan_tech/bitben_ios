//
//  BYHistoryIMModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CDChatList/CDChatList.h>
NS_ASSUME_NONNULL_BEGIN

@interface BYHistoryIMModel : NSObject<MessageModalProtocal>

+ (NSArray *)getTableData:(NSArray *)responData liveData:(BYCommonLiveModel *)liveData;

+ (NSArray *)getSIMTableData:(NSArray *)respondData liveData:(BYCommonLiveModel *)liveData;

@end

NS_ASSUME_NONNULL_END
