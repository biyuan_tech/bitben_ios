//
//  BYHistotyIMViewController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYCommonLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHistotyIMViewController : BYCommonViewController

/** data */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** superController */
@property (nonatomic ,weak) UIViewController *superController;

@property (nonatomic, copy) void (^scrollViewDidScrollView)(UIScrollView *scrollView);

@end

NS_ASSUME_NONNULL_END
