//
//  GWCurrentIMViewController.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <WXOUIModule/YWConversationViewController.h>
#import "BYCommonLiveModel.h"

@interface GWCurrentIMViewController : YWConversationViewController

@property (nonatomic,weak)UIViewController *viewController;
@property (nonatomic,copy)NSString *transferRoomId;
@property (nonatomic,assign)BOOL isHost;
@property (nonatomic,strong)BYCommonLiveModel *transferInfoModel;
/** 当前用于push的viewController */
@property (nonatomic,weak)UIViewController *pushViewController;

@property (nonatomic,copy) void (^didFinishLiveHandle)(void);
@property (nonatomic,copy) void (^scrollViewDidScroll)(UIScrollView *scrollView);
@property (nonatomic, copy) void (^didUpdatePPTImgs)(NSArray *imgs);

@end


