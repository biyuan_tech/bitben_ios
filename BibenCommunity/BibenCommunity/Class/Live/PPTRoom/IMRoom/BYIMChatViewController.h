//
//  BYIMChatViewController.h
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYPPTIMHeaderView.h"

@class BYCommonLiveModel;
@interface BYIMChatViewController : BYCommonViewController

@property (nonatomic,strong,readonly)BYPPTIMHeaderView *headerView;

@property (nonatomic, strong) BYCommonLiveModel *model;
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, assign) BOOL isHost;
@property (nonatomic, copy) void (^scrollViewDidScrollView)(UIScrollView *scrollView);

@end
