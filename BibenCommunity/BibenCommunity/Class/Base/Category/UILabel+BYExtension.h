//
//  UILabel+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (BYExtension)






/**
 设置高亮文字属性

 @param attributedString attributedString description
 */
- (void)setTagAttributedString:(NSAttributedString *)attributedString;

/**
 设置高亮文字属性

 @param attributedString 高亮属性
 @param string label.text
 */
- (void)setTagAttributedString:(NSAttributedString *)attributedString string:(NSString *)string;

+ (instancetype)initTitle:(NSString *)title font:(CGFloat)font textColor:(UIColor *)textColor;

@end
