//
//  NSString+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,BYRemoveType) {
    BYRemoveTypeBothEndsSpace = 0,             // 只移除两端空格
    BYRemoveTypeBothEndsWrap,              // 只移除两端换行
    BYRemoveTypeBothEndsSpaceWithWrap,    // 只移除两端的空格和换行
    BYRemoveTypeAll                       // 移除文案中的所有空格与换行
};

@interface NSString (BYExtension)

/**
 移除字符村两端的空格
 
 @return string
 */
- (NSString *)removeBothEndsEmptyString:(BYRemoveType )removeType;

/**
 计算string展示的Size
 
 @param font 字体大小
 @param maxWidth 展示的最大宽度
 @return 计算的Size
 */
- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/** 计算string展示的Size */
- (CGSize )getStringSizeWithAttributes:(NSDictionary<NSAttributedStringKey, id> *)attrs maxSize:(CGSize)maxSize;


/**
 格式化integer返回相对于的x万、x千、x百

 @param num 输入
 @return 格式化格式
 */
- (NSString *)formatIntegerNum:(NSInteger)num;

/** 格式化file:// 开头的文件路径(移除file:) */
- (NSString *)formatFilePath;

/** 截取互动直播channel id */
- (NSString *)getChannelIdForm:(NSString *)url;
- (NSString *)getStreamId;
@end
