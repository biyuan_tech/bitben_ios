//
//  UIImageView+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UIImageView+BYExtension.h"
#import "UIImage+BYExtension.h"

static char const tapGesture;
@implementation UIImageView (BYExtension)


- (void)addCornerRadius:(CGFloat)radius size:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius,radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    [self layoutIfNeeded];
}

- (void)setImageColor:(UIColor *)color{
    self.image = [UIImage imageWithColor:color];
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}

- (void)addTapGestureRecognizer:(void(^)(void))handle{
    objc_setAssociatedObject(self, &tapGesture, handle, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)tapGestureRecognizerAction{
    void(^handle)(void) = objc_getAssociatedObject(self, &tapGesture);
    if (handle) {
        handle();
    }
}
@end
