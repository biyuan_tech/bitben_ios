//
//  AppDelegate+Config.m
//  BY
//
//  Created by 黄亮 on 2018/8/23.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "AppDelegate+Config.h"
#import <ILiveSDK/ILiveCoreHeader.h>
#import "BYIMManager.h"
#import "BYALIMManager.h"

#import "BYLiveConfig.h"
#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"
#import "BYVideoLiveController.h"

@interface AppDelegate ()<QAVLogger>

@end

@implementation AppDelegate (Config)


- (void)configTIM{
    [[BYIMManager sharedInstance] initSdk];
}

- (void)configILiveSDK{
    // 设置非云上环境(必须在初始化sdk之前设置)
    [[ILiveSDK getInstance] setChannelMode:E_ChannelIMSDK withHost:@""];
    [[ILiveSDK getInstance] initSdk:kTXLiveAppid accountType:kTXLiveAccountType];
    [[ILiveSDK getInstance] setConsoleLogPrint:kIsOpenLog];
//    TIMManager *immanager = [[ILiveSDK getInstance] getTIMManager];
//    [immanager disableIM];
    [QAVAppChannelMgr setExternalLogger:self];
    PDLog(@"互动直播SDK :%@",[[ILiveSDK getInstance] getVersion]);
    PDLog(@"腾讯云IMSDK :%@",[[TIMManager sharedInstance] GetVersion]);

}

- (void)configALIMSDK{
    [[BYALIMManager shareInstance] initSDK];
}

- (BOOL)openURLManagerWithURL:(NSURL *)url{
    if (!url) return NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 登录状态校验
        @weakify(self);
        [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            [self openSchemeWithURL:url];
        }];
    });
    return NO;
}

- (BOOL)openSchemeWithURL:(NSURL *)url{
    NSString *sourceId = [url query];
    NSDictionary *param = [self getSchemeParam:sourceId];
    if (![AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.ip.length &&
        ![AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.port.length) {
        showToastView(@"未分发", kCommonWindow);
        NSLog(@"未分发");
        return NO;
    }
    UITabBarController *tabBarController = [BYTabbarViewController sharedController];
    UINavigationController *navigationController = [tabBarController selectedViewController];
    UIViewController *viewController = [navigationController.viewControllers lastObject];
    UIViewController *currentController;
    if ([viewController isKindOfClass:[RTContainerController class]]) {
        currentController = ((RTContainerController *)viewController).contentViewController;
    }
    else{
        currentController = viewController;
    }
    if ([url.description hasPrefix:schemeAction_Live]) {
        if ([param.allKeys indexOfObject:@"livetype"] == NSNotFound) {
            showToastView(@"链接存在问题", kCommonWindow);
            return NO;
        }
        switch ([param[@"livetype"] integerValue]) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                BYLiveConfig *config = [[BYLiveConfig alloc] init];
                config.isHost = NO;
//                switch ([param[@"status"] integerValue]) {
//                    case BY_LIVE_STATUS_END:
//                    {
//                        config.isVOD = YES;
//                        //                    config.video_url = model.video_url;
//                    }
//                        break;
//                    case BY_LIVE_STATUS_WAITING:
//                        break;
//                    default:
//                        break;
//                }
                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                ilivewRoomController.live_record_id = nullToEmpty(param[@"recordId"]);;
                [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
                    [currentController.navigationController pushViewController:ilivewRoomController animated:YES];
                }];            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                //            videoRoomController.introModel = model;
                videoRoomController.live_record_id = nullToEmpty(param[@"recordId"]);
                [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
                    [currentController.navigationController pushViewController:videoRoomController animated:YES];
                }];

            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
                pptRoomController.live_record_id = nullToEmpty(param[@"recordId"]);
                pptRoomController.isHost = NO;
                [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
                    [currentController.navigationController pushViewController:pptRoomController animated:YES];
                }];
            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(param[@"recordId"]);
                [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
                    [currentController.navigationController pushViewController:vodPlayController animated:YES];
                }];
            }
                break;
            case BY_NEWLIVE_TYPE_VIDEO:
            {
                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
                videoLiveController.live_record_id = nullToEmpty(param[@"recordId"]);
                videoLiveController.isHost = NO;
                [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
                    [currentController.navigationController pushViewController:videoLiveController animated:YES];
                }];
                
            }
                break;
            default:
                break;
        }
    }
    else if ([url.description hasPrefix:schemeAction_Article]){
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = nullToEmpty(param[@"articleId"]);
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
            [currentController.navigationController pushViewController:articleController animated:YES];
        }];
    }
    return YES;
}

- (NSDictionary *)getSchemeParam:(NSString *)param{
    NSArray *arr = [param componentsSeparatedByString:@"&"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (int i = 0; i < arr.count; i ++) {
        NSString *paramStr = arr[i];
        NSArray *paramArr = [paramStr componentsSeparatedByString:@"="];
        if (paramArr.count == 2) {
            [dic setObject:paramArr[1] forKey:paramArr[0]];
        }
    }
    return dic;
}

#pragma mark - avsdk日志代理
- (BOOL)isLogPrint
{
    return NO;
}

- (NSString *)getLogPath {
    return [[TIMManager sharedInstance] getLogPath];
}

@end
