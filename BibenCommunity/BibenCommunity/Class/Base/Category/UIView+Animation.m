//
//  UIView+Animation.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "UIView+Animation.h"

static char listLoadingAnimationKey;
@implementation UIView (Animation)

- (void)showListLoadingAnimation:(CGFloat)originY{
    UIView *animationView  = objc_getAssociatedObject(self, &listLoadingAnimationKey);
    UIView *animationView1 = [self viewWithStringTag:@"animationView"];
    
    if (animationView) {
        animationView.layer.opacity = 0.0;
        [animationView removeFromSuperview];
        animationView = nil;
    }
    if (animationView1){
        animationView1.layer.opacity = 0.0;
        [animationView1 removeFromSuperview];
        animationView1 = nil;
    }
    
    animationView = [[UIView alloc] init];
    animationView.stringTag = @"animationView";
    [animationView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:animationView];
    
    @weakify(self);
    [animationView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(self.mas_width).mas_offset(0);
        make.height.mas_equalTo(self.mas_height).mas_offset(0);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
    }];
    objc_setAssociatedObject(self, &listLoadingAnimationKey, animationView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    UIView *contentView = [[UIView alloc] init];
    [animationView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(120);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(originY);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i <= 8; i ++) {
        NSString *imageName = [NSString stringWithFormat:@"animation_loading%i.jpg",i];
        UIImage *image = [UIImage imageNamed:imageName];
        [array addObject:image];
    }
    imageView.image = [UIImage animatedImageWithImages:array duration:1.0];
    [contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(100);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.text = @"加载中...";
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kTextColor_144;
    [contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.bottom.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(17);
    }];
}

- (void)showListLoadingAnimation{
    [self showListLoadingAnimation:0];
}

- (void)dismissListAnimation{
    UIImageView *animationView = objc_getAssociatedObject(self, &listLoadingAnimationKey);
    if (animationView) {
        [UIView animateWithDuration:0.2 animations:^{
            animationView.layer.opacity = 0.0;
        } completion:^(BOOL finished) {
            [animationView removeFromSuperview];
        }];
    }
}
@end
