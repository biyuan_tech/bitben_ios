//
//  UIView+Animation.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Animation)



- (void)showListLoadingAnimation:(CGFloat)originY;
- (void)showListLoadingAnimation;
- (void)dismissListAnimation;


@end

NS_ASSUME_NONNULL_END
