//
//  PDImageView+DefultBg.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDImageView.h"

@interface PDImageView (DefultBg)

- (BOOL)verifyIsLocalDefultBg:(NSString *)urlString;

@end
