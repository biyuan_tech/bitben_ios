//
//  NSString+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "NSString+BYExtension.h"

@implementation NSString (BYExtension)

- (NSString *)removeBothEndsEmptyString:(BYRemoveType )removeType{
    NSString *string = self;
    if (removeType == BYRemoveTypeBothEndsSpace) { // 移除两端空格
        while ([string hasPrefix:@" "]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@" "]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeBothEndsWrap){ // 移除两端换行
        while ([string hasPrefix:@"\n"] ||
               [string hasPrefix:@"\r"]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@"\n"] ||
               [string hasSuffix:@"\r"]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeBothEndsSpaceWithWrap) { // 移除两端空格换行
        while ([string hasPrefix:@" "] ||
               [string hasPrefix:@"\n"] ||
               [string hasPrefix:@"\r"]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@" "] ||
               [string hasSuffix:@"\n"] ||
               [string hasSuffix:@"\r"]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeAll) { // 移除所有空格换行
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    }
    return string;
}

// 移除头部指定string
- (NSString *)removeHeadString{
    NSString *string = self;
    NSInteger length = 0;
    if ([string hasPrefix:@" "]) {
        length = [NSString stringWithFormat:@" "].length;
    }
    else if ([string hasPrefix:@"\n"]){
        length = [NSString stringWithFormat:@"\n"].length;
    }
    else if ([string hasPrefix:@"\r"]){
        length = [NSString stringWithFormat:@"\r"].length;
    }
//    NSInteger length = [string hasPrefix:@" "] ? 1 : 2;
    if (string.length > length) {
        string = [string substringFromIndex:length];
    }
    else
    {
        return @"";
    }
    return string;
}

// 移除尾部指定string
- (NSString *)removeBottomString{
    NSString *string = self;
    NSInteger length = 0;
    if ([string hasSuffix:@" "]) {
        length = [NSString stringWithFormat:@" "].length;
    }
    else if ([string hasSuffix:@"\n"]){
        length = [NSString stringWithFormat:@"\n"].length;
    }
    else if ([string hasSuffix:@"\r"]){
        length = [NSString stringWithFormat:@"\r"].length;
    }
//    NSInteger length = [string hasSuffix:@" "] ? 1 : 2;
    if (string.length > length) {
        string = [string substringWithRange:NSMakeRange(0, string.length - length)];
    }
    else
    {
        return @"";
    }
    return string;
}

- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth{
    NSDictionary *attributes = @{NSFontAttributeName:font};
    return [self getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxWidth, MAXFLOAT)];
}

- (CGSize )getStringSizeWithAttributes:(NSDictionary<NSAttributedStringKey, id> *)attrs maxSize:(CGSize)maxSize{
    CGSize size;
    if (kCommonGetSystemVersion() >= 7) {
        size = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    }
    else{
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [self sizeWithFont:attrs[NSFontAttributeName] constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

- (NSString *)formatIntegerNum:(NSInteger)num{
    if (num > 9999) {
        CGFloat numF = num/1000.0;
        return [NSString stringWithFormat:@"%.f万",numF];
    }
    return [NSString stringWithFormat:@"%i",(int)num];
}

- (NSString *)formatFilePath{
    if (!self.length) return @"";
    if ([self hasPrefix:@"file://"]) {
        NSString *string = [self stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        return string;
    }
    return self;
}

- (NSString *)getChannelIdForm:(NSString *)url{
    NSRange range = [url rangeOfString:@"_"];
    NSRange range1 = [url rangeOfString:@".m3u8"];
    NSInteger begin = range.location + range.length;
    NSString *channelId = [url substringWithRange:NSMakeRange(begin, range1.location - begin)];
    return channelId;
}

- (NSString *)getStreamId{
    NSRange range = [self rangeOfString:@"_"];
    NSRange range1 = [self rangeOfString:@".m3u8"];
    NSInteger begin = range.location + range.length;
    NSString *channelId = [self substringWithRange:NSMakeRange(begin, range1.location - begin)];
    return channelId;
}
@end
