//
//  BYIMMsgModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger , BY_IM_MSG_TYPE) {  // 消息类型
    BY_IM_MSG_TYPE_TEXT,    // 文字类型
    BY_IM_MSG_TYPE_IMAGE,   // 图片类型
    BY_IM_MSG_TYPE_AUDIO,   // 音频类型
    BY_IM_MSG_TYPE_SYSTEM,  // 系统信息类型
    BY_IM_MSG_TYPE_CUSTOME, // 自定义类型
};

typedef NS_ENUM(NSInteger , BY_IM_MSG_STATE) { // 消息状态
    BY_IM_MSG_STATE_NORMAL,         //
    BY_IM_MSG_STATE_SENDING,        // 消息发送中
    BY_IM_MSG_STATE_SEND_FAILD,     // 消息发送失败
    BY_IM_MSG_STATE_DOWN_LOADING,   // 消息下载中
    BY_IM_MSG_STATE_DOWN_FAILD      // 消息下载失败
};

typedef NS_ENUM(NSInteger , BYMsgAlignment) { // 消息显示方位
    BYMsgAlignmentLeft,
    BYMsgAlignmentCenter,
    BYMsgAlignmentRight,
};

@interface BYIMMsgModel : NSObject


/** 消息内容 */
@property (nonatomic ,copy) NSString *msg;
/** 消息时间 */
@property (nonatomic ,copy) NSString *creatTime;
/** 是否显示时间 */
@property (nonatomic ,assign) BOOL isShowTime;
/** 消息类型 */
@property (nonatomic ,assign) BY_IM_MSG_TYPE msgType;
/** 消息状态 */
@property (nonatomic ,assign) BY_IM_MSG_STATE msgState;
/** 消息id */
@property (nonatomic ,copy) NSString *msgId;
/** 消息左中右显示 */
@property (nonatomic ,assign) BYMsgAlignment msgAlignment;
/** 用户名 */
@property (nonatomic ,copy) NSString *userName;
/** 用户头像 */
@property (nonatomic ,copy) NSString *userLogoImageURL;
/** UI配置 */
//@property (nonatomic ,strong) BYIMUIConfig *uiConfig;

#pragma mark - 缓存内容
/** 气泡size */
@property (nonatomic ,assign) CGSize bubbleSize;
/** cell高度 */
@property (nonatomic ,assign) CGFloat cellHeight;
/** 音频消息内容时长 */
@property (nonatomic ,assign) int audioTime;
/** 音频 */
//@property (nonatomic ,copy) <#type#> *<#typeName#>;

/** 消息图片 */
@property (nonatomic ,strong) UIImage *msgImage;
/** 消息图片地址 */
@property (nonatomic ,copy) NSString *msgImageURL;
/** 语音播放地址 */
@property (nonatomic ,copy) NSString *msgAudioPath;


@end

NS_ASSUME_NONNULL_END

