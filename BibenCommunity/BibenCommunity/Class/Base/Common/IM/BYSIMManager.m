//
//  BYSIMManager.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/10.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYSIMManager.h"
#import "BYHistoryIMModel.h"

static NSString * const msg_broadcast_in_room = @"broadcast_in_room"; // 聊天消息
static NSString * const msg_create_room = @"create_room";             // 房间创建
static NSString * const msg_close_room = @"close_room";               // 房间关闭
static NSString * const msg_join_room = @"join_room";                 // 进入房间
static NSString * const msg_leave_room = @"leave_room";               // 退出房间


static char const joinRoomSucKey;
static char const joinRoomFailKey;

static char const createRoomSucKey;
static char const createRoomFailKey;

static char const sendMsgSucKey;
static char const sendMsgFailKey;

static char const customMsgSucKey;
static char const customMsgFailKey;

typedef NS_ENUM(NSInteger ,BY_HANDLE_TYPE) { // 回调类型
    BY_HANDLE_TYPE_JOINROOM,    // 加入房间消息回调
    BY_HANDLE_TYPE_LEAVEROOM,   // 离开房间消息回调
    BY_HANDLE_TYPE_CREATEROOM,  // 创建房间消息回调
    BY_HANDLE_TYPE_SENDMSG,     // 发送消息回调
    BY_HANDLE_TYPE_CUSTOM,      // 自定义消息回调
};

@interface BYSIMManager ()<NetworkAdapterDelegate>

@property (nonatomic ,copy) void (^messageHandle)(NSArray *msgs);

/** 成功返回 */
@property (nonatomic ,copy) succHandle tmpSucHandle;
@property (nonatomic ,copy) succRoomIdHandle tmpSucRoomIdHandle;

/** 错误返回 */
@property (nonatomic ,copy) failHandle tmpFailHandle;
/** 房间id */
@property (nonatomic ,copy ) NSString *roomId;
@property (nonatomic ,copy ) NSString *joinTime;


@end

@implementation BYSIMManager

+ (instancetype)shareManager{
    static BYSIMManager *staticManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticManager = [[BYSIMManager alloc] init];
    });
    return staticManager;
}

- (void)sendMessage:(BYSIMMessage *)message suc:(succHandle)suc fail:(failHandle)fail{
    if (!self.roomId) {
        showDebugToastView(@"房间号为空", kCommonWindow);
        return;
    }
    
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType != SocketConnectTypeOpen) {
        if (fail) fail();
        return;
    }
    
    objc_setAssociatedObject(self, &sendMsgSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &sendMsgFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(message.msg_type) forKey:@"content_type"];
//    [params setObject:nullToEmpty(message.msg_id) forKey:@"chat_id"];
    [params setObject:message.time forKey:@"send_time"];
    [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
    [params setObject:msg_broadcast_in_room forKey:@"type"];
    [params setObject:self.roomId forKey:@"room_id"];
    [params setObject:message.msg_senderName forKey:@"nickname"];
    [params setObject:message.msg_senderlogoUrl forKey:@"head_img"];
    if (message.msg_type == BY_SIM_MSG_TYPE_TEXT) {
        BYSIMTextElem *elem = (BYSIMTextElem *)message.elem;
        [params setObject:elem.text forKey:@"content"];
    }
    else if (message.msg_type == BY_SIM_MSG_TYPE_AUDIO) {
        BYSIMAudioElem *elem = (BYSIMAudioElem *)message.elem;
        [params setObject:elem.path forKey:@"content"];
    }

    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

- (void)sendCustomMessage:(BY_GUEST_OPERA_TYPE)msgType data:(NSDictionary * __nullable )data suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType != SocketConnectTypeOpen) {
        if (fail) fail();
        return;
    }
    
    objc_setAssociatedObject(self, &customMsgSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &customMsgFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSData *jsonData;
    NSString *jsonString;
    switch (msgType) {
        case BY_GUEST_OPERA_TYPE_RECEIVE_STREAM: // 开始直播
        {
            NSError *error = nil;
            jsonData = [NSJSONSerialization dataWithJSONObject:@{kBYCustomMessageType:@(BY_GUEST_OPERA_TYPE_RECEIVE_STREAM)} options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        }
            break;
        case BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM: // 关闭直播
        {
            NSError *error = nil;
            jsonData = [NSJSONSerialization dataWithJSONObject:@{kBYCustomMessageType:@(BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM)} options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        }
            break;
        case BY_GUEST_OPERA_TYPE_REWARD: // 打赏
        case BY_GUEST_OPERA_TYPE_UP_VIDEO:  // 上麦
        case BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO: // 同意上麦
        case BY_GUEST_OPERA_TYPE_DOWN_VIDEO: // 下麦
        {
            NSError *error = nil;
            jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
            break;
        default:
            break;
    }
    [params setObject:@(BY_SIM_MSG_TYPE_SYSTEM) forKey:@"content_type"];
    if (msgType == BY_GUEST_OPERA_TYPE_REWARD) {
        [params setObject:@(BY_SIM_MSG_TYPE_CUSTOME) forKey:@"content_type"];
    }
    [params setObject:jsonString forKey:@"content"];
    [params setObject:[NSDate getCurrentTimeStr] forKey:@"send_time"];
    [params setObject:[AccountModel sharedAccountModel].account_id forKey:@"user_id"];
    [params setObject:[AccountModel sharedAccountModel].loginServerModel.user.nickname forKey:@"nickname"];
    [params setObject:[AccountModel sharedAccountModel].loginServerModel.account.head_img forKey:@"head_img"];
    [params setObject:nullToEmpty(self.roomId) forKey:@"room_id"];
    [params setObject:msg_broadcast_in_room forKey:@"type"];

    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

- (void)setMessageListener:(void(^)(NSArray *msgs))msgs{
    self.messageHandle = msgs;
}

- (void)createRoom:(succRoomIdHandle)suc fail:(failHandle)fail{
    
    objc_setAssociatedObject(self, &createRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &createRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_create_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)joinRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
//    self.tmpSucHandle = suc;
    objc_setAssociatedObject(self, &joinRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &joinRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.roomId = roomId;
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        message.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        message.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        NSString *string = [NSString stringWithFormat:@"%@进入直播间",nullToEmpty(message.msg_senderName)];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:string forKey:@"content"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:nullToEmpty(message.msg_senderName) forKey:@"nickname"];
        [params setObject:nullToEmpty(message.msg_senderlogoUrl) forKey:@"head_img"];
        [params setObject:msg_join_room forKey:@"type"];

        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)leaveRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    self.tmpSucHandle = suc;
    self.tmpFailHandle = fail;
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_leave_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)connectionSocket{
    [self connectionSocket:nil];
}

- (void)connectionSocket:(__nullable succHandle)suc{
    _tmpSucHandle = suc;
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType == SocketConnectTypeOpen) {
        if (suc) suc();
        return;
    }
    [NetworkAdapter sharedAdapter].mainRootSocketConnection = [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].mainRootSocketConnection host:kSocketHost port:kSocketPort];
//    [NetworkAdapter sharedAdapter].mainRootSocketConnection = [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].mainRootSocketConnection host:@"47.244.112.55" port:9710];

}

- (void)closeSocket{
//    self.roomId = @"";
//    self.joinTime = @"";
    [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

- (void)removeHandle:(BY_HANDLE_TYPE)type{
    switch (type) {
        case BY_HANDLE_TYPE_JOINROOM:
            objc_setAssociatedObject(self, &joinRoomSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &joinRoomFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_LEAVEROOM:
            self.tmpSucHandle = NULL;
            self.tmpFailHandle = NULL;
            break;
        case BY_HANDLE_TYPE_CREATEROOM:
            objc_setAssociatedObject(self, &createRoomSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &createRoomFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_SENDMSG:
            objc_setAssociatedObject(self, &sendMsgSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &sendMsgFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_CUSTOM:
            objc_setAssociatedObject(self, &customMsgSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &customMsgFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        default:
            break;
    }
}

#pragma mark - socker消息回调
-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    if (type == WebSocketTypeIM) {
        succHandle joinSucHandle = objc_getAssociatedObject(self, &joinRoomSucKey);
        failHandle joinFailHandle = objc_getAssociatedObject(self, &joinRoomFailKey);
        
        succRoomIdHandle createSucHandle = objc_getAssociatedObject(self, &createRoomSucKey);
        failHandle createFailHandle = objc_getAssociatedObject(self, &createRoomFailKey);
        
        succHandle sendSucHandle = objc_getAssociatedObject(self, &sendMsgSucKey);
        failHandle sendFailHandle = objc_getAssociatedObject(self, &sendMsgFailKey);
        
        succHandle customSucHandle = objc_getAssociatedObject(self, &customMsgSucKey);
        failHandle customFailHandle = objc_getAssociatedObject(self, &customMsgFailKey);

        if ([data.receiveDataDic[@"result"] integerValue] == 0) { // 操作成功
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_join_room]) { // 加入房间
                self.joinTime = data.receiveDataDic[@"data"][@"send_time"];
                if (joinSucHandle) {
                    joinSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_JOINROOM];
                }
            }
            
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_create_room]) { // 创建房间
                if (createSucHandle) {
                    createSucHandle(data.receiveDataDic[@"data"][@"room_id"]);
                    [self removeHandle:BY_HANDLE_TYPE_CREATEROOM];
                }
                [[BYSIMManager shareManager] closeSocket];
            }
            
            // 接收聊天消息
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_broadcast_in_room]) {
                if (sendSucHandle) {
                    sendSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_SENDMSG];
                }
                NSDictionary *msgs = data.receiveDataDic[@"data"];
                if (self.messageHandle) {
                    self.messageHandle(@[msgs]);
                }
            }
            
            // 接收appp显示的系统消息
//            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW) {
//                NSDictionary *msgs = data.receiveDataDic[@"data"];
//                if (self.messageHandle) {
//                    self.messageHandle(@[msgs]);
//                }
//            }
            // 接收自定义消息
            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME ||
                [data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                if (customSucHandle) {
                    customSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CUSTOM];
                }
            }
        }
        else{
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_join_room]) { // 加入房间
                if (joinFailHandle) {
                    joinFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_JOINROOM];
                    self.roomId = @"";
                }
            }
            
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_create_room]) { // 创建房间
                if (createFailHandle) {
                    createFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CREATEROOM];
                }
                [[BYSIMManager shareManager] closeSocket];
            }
            
            // 接收聊天消息
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_broadcast_in_room]) {
                if (sendFailHandle) {
                    sendFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_SENDMSG];
                }
            }
            
            // 接收自定义消息
            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME ||
                [data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                if (customFailHandle) {
                    customFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CUSTOM];
                }
            }
            
            showDebugToastView(data.receiveDataDic[@"resultMsg"], kCommonWindow);
        
        }
    }
    PDLog(@"socket消息回调--------------%@",data);
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocket{
    if (_tmpSucHandle) {
        _tmpSucHandle();
        _tmpSucHandle = NULL;
    }
}

@end
