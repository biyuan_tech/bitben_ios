//
//  BYSIMManager.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/10.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BYSIMMessage.h"
#import "NetworkAdapter.h"
#import "NetworkAdapter+MemberVIP.h"


NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,BY_MSG_CUSTOM_TYPE) { // 自定义消息类型
    BY_MSG_CUSTOM_TYPE_OPENLIVE, // 开播
    BY_MSG_CUSTOM_TYPE_CLOSELIVE, // 关播
    BY_MSG_CUSTOM_TYPE_REWARD, // 赞赏
};

#define kBYCustomMessageType @"opera_type"

// 结束直播
static NSString * const BYCustomMessageType_EndLive = @"ENDLIVE";
// 开始直播
static NSString * const BYCustomMessageType_BeginLive = @"BEGINLIVE";
// 打赏
static NSString * const BYCustomMessageType_Reward = @"REWARD";

typedef void (^succHandle)(void);
typedef void (^failHandle)(void);

typedef void (^succRoomIdHandle)(NSString *roomId);

@interface BYSIMManager : NSObject

/** 房间号，记录当前聊天 */
@property (nonatomic ,copy ,readonly) NSString *roomId;
/** 加入聊天室时间 */
@property (nonatomic ,copy ,readonly) NSString *joinTime;


+ (instancetype)shareManager;

/** 消息发送 */
- (void)sendMessage:(BYSIMMessage *)message suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;

/** 自定义消息 */
- (void)sendCustomMessage:(BY_GUEST_OPERA_TYPE)msgType data:(NSDictionary *__nullable)data suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;

/** 消息回调 */
- (void)setMessageListener:(void(^)(NSArray *msgs))msgs;
/** 创建房间 */
- (void)createRoom:(__nullable succRoomIdHandle)suc fail:(__nullable failHandle)fail;
/** 进入房间 */
- (void)joinRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;
/** 离开房间 */
- (void)leaveRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;


- (void)connectionSocket;
- (void)connectionSocket:(__nullable succHandle)suc;
- (void)closeSocket;


- (void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data;
- (void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocket;

@end

NS_ASSUME_NONNULL_END
