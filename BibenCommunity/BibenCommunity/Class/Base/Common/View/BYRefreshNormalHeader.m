//
//  BYRefreshNormalHeader.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRefreshNormalHeader.h"

@implementation BYRefreshNormalHeader

- (void)prepare{
    [super prepare];
    self.stateLabel.font = [UIFont systemFontOfSize:13];
    self.stateLabel.textColor = kColorRGBValue(0xa5a5a5);
    self.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:13];
    self.lastUpdatedTimeLabel.textColor = kColorRGBValue(0xa5a5a5);
    [self.arrowView setImage:[UIImage imageNamed:@"common_arrow"]];
}

@end
