//
//  BYRewardView.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//  打赏

#import <UIKit/UIKit.h>

@interface BYRewardView : UIView

/** 打赏金额 */
@property (nonatomic ,readonly ,assign) CGFloat reward_amount;

/** 被打赏人昵称 */
@property (nonatomic ,copy) NSString *receicer_name;

/** 被打赏人ID */
@property (nonatomic ,copy) NSString *receicer_id;

/** 打赏回调 */
@property (nonatomic ,copy) void (^confirmBtnHandle)(CGFloat amount);

/** 初始化 */
- (instancetype)initWithFathureView:(UIView *)fathureView;

/** 设置默认可选择的bbt金额 */
- (void)setChooseBBTAmount:(NSArray *)amounts;

- (void)reloadSurplusBBT:(CGFloat)bbt;

/** 开始动画显示 */
- (void)showAnimation;
- (void)hiddenAnimation;
@end
