//
//  BYRefreshNormalFooter.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRefreshNormalFooter.h"

@implementation BYRefreshNormalFooter

- (void)prepare{
    [super prepare];
    self.arrowView.layer.opacity = 0.0f;
    [self setTitle:@"加载中" forState:MJRefreshStateIdle];
    [self setTitle:@"加载中" forState:MJRefreshStatePulling];
    [self setTitle:@"加载中" forState:MJRefreshStateRefreshing];
    [self setTitle:@"加载中" forState:MJRefreshStateWillRefresh];
    self.stateLabel.font = [UIFont systemFontOfSize:13];
    self.stateLabel.textColor = kColorRGBValue(0xa5a5a5);
}

@end
