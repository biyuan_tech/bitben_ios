//
//  BYReportSheetView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/19.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYReportSheetView : UIView

/** 举报场景 */
@property (nonatomic ,assign) BY_THEME_TYPE theme_type;
/** 举报用户id */
@property (nonatomic ,copy) NSString *theme_id;
/** 举报对象id */
@property (nonatomic ,copy) NSString *user_id;


+ (instancetype)initReportSheetViewShowInView:(UIView *)view;

- (void)showAnimation;
- (void)hiddenAnimation;

// ares
+(NSArray *)actionItemsArr;

@end

NS_ASSUME_NONNULL_END
