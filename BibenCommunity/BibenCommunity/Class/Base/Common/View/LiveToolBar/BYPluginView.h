//
//  BYPluginView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYBaseInputViewPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPluginView : UIControl

/** 点击回调 */
@property (nonatomic ,copy) void (^didTapPluginView)(BYInputPluginType type,BOOL selected);
/** 数据源 */
@property (nonatomic ,strong) BYBaseInputViewPlugin *pluginData;


- (void)configUIWithPluginData:(BYBaseInputViewPlugin *)pluginData;


@end

NS_ASSUME_NONNULL_END
