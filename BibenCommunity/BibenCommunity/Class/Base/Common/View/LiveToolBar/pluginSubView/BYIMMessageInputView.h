//
//  BYIMMessageInputView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class BYIMMessageInputView;
@protocol BYIMMessageInputViewDelegate <NSObject>

- (BOOL)messageInputViewShouldBeginEditing:(BYIMMessageInputView *)inputView;
//- (void)messageInputView
- (BOOL)messageInputViewShouldEndEditing:(BYIMMessageInputView *)inputView;

- (void)messageInputViewDidChange:(BYIMMessageInputView *)inputView text:(NSString *)text;

- (BOOL)messageInputView:(BYIMMessageInputView *)inputView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;

@end

@class BYIMMessageTextView;
@interface BYIMMessageInputView : UIView

/** 消息输入 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** delegate */
@property (nonatomic ,weak) id <BYIMMessageInputViewDelegate>delegate;
@end

@interface BYIMMessageTextView : UITextView

@end

NS_ASSUME_NONNULL_END
