//
//  BYPluginAudioMoreView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,BYRecordAudioStatus) {
    BYRecordAudioStatusTapNormal = 0,   // 单击录制音频默认状态
    BYRecordAudioStatusTapPress,    // 单击录制音频按压后状态
    BYRecordAudioStatusTapFinish,   // 单击录制音频结束后状态
    BYRecordAudioStatusLongNormal,  // 长按录制音频默认状态
    BYRecordAudioStatusLongPress,   // 长按录制音频按钮后状态
    BYRecordAudioStatusLongWillCancel,   // 长按录制音频将取消状态
    BYRecordAudioStatusCancel       // 录制音频取消后状态

};

@interface BYPluginAudioMoreView : UIView

/** 音频录制状态 */
@property (nonatomic ,assign) BYRecordAudioStatus status;

/** 音频录制回调 */
@property (nonatomic ,copy) void (^finishRecordAudioHandle)(NSURL *audioPath);


@end

NS_ASSUME_NONNULL_END
