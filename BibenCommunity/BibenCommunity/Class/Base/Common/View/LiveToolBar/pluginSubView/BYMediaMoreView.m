//
//  BYMediaMoreView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/4.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYMediaMoreView.h"

static NSInteger baseTag = 0x541;
static CGFloat   pluginW = 60;
@interface BYMediaMoreView ()

/** Plugin间隙 */
@property (nonatomic ,assign) CGFloat spaceWidth;


@end

@implementation BYMediaMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configSubView];
    }
    return self;
}

- (void)configSubView{
    self.spaceWidth = (kCommonScreenWidth - 4*pluginW)/5;
    UIView *pluginView =[self addPluginView:@"input_plug_icon_image" title:@"图片"];
    pluginView.tag = baseTag;
    [self addSubview:pluginView];
    @weakify(self);
    [pluginView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(pluginW);
        make.left.mas_equalTo(self.spaceWidth);
        make.top.mas_equalTo(14);
        make.height.mas_greaterThanOrEqualTo(90);
    }];
    
}

- (UIView *)addPluginView:(NSString *)imageName title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = 2*baseTag;
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(pluginW);
        make.height.mas_equalTo(pluginW);
        make.left.mas_equalTo(0);
    }];
    
    
    CGSize size = [title getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:60];
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.text = title;
    titleLab.font = [UIFont systemFontOfSize:12];
    titleLab.textColor = kColorRGBValue(0x353535);
    titleLab.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.top.mas_equalTo(button.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(size.height > 28 ? 28 : size.height);
        make.bottom.mas_equalTo(0);
    }];
    
    return view;
}

- (void)tapAction:(UIButton *)sender{
    if (self.didTapPluginAtIndexHandle) {
        self.didTapPluginAtIndexHandle(sender.superview.tag - baseTag);
    }
}

@end
