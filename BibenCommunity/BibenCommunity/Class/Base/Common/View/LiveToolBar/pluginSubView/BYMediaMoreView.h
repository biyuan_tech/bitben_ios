//
//  BYMediaMoreView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/4.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYMediaMoreView : UIView

/** 点击插件回调 */
@property (nonatomic ,copy) void (^didTapPluginAtIndexHandle)(NSInteger index);


@end

NS_ASSUME_NONNULL_END
