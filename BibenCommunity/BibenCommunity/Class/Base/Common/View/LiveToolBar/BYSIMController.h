//
//  BYSIMController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYSIMController : UIViewController


@property (nonatomic,assign)BOOL isHost;
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** superController */
@property (nonatomic ,weak) UIViewController *superController;


/** 观众端接收到下播消息 */
@property (nonatomic,copy) void (^didFinishLiveHandle)(void);
/** 观众端接收到开播消息 */
@property (nonatomic ,copy) void (^didBeginLiveHandle)(void);
/** 直播结束回调 */
@property (nonatomic ,copy) void (^didReceiveLiveEndMsgHandle)(void);
@property (nonatomic,copy) void (^scrollViewDidScroll)(UIScrollView *scrollView);
/** 上传ppt图片组 */
@property (nonatomic, copy) void (^didUpdatePPTImgs)(NSArray *imgs);

@end

NS_ASSUME_NONNULL_END
