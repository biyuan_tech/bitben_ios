//
//  BYLiveToolBar.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYBaseInputViewPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BYLiveToolBarPluginDelegate <NSObject>

/** 音频录制回调 */
- (void)recordAudioDidFinish:(NSURL *)audioPath;

@end

@interface BYLiveToolBar : UIView

/** 点击回调 */
@property (nonatomic ,copy) void (^didTapPluginView)(BYInputPluginType type);
/** delegate */
@property (nonatomic ,weak) id <BYLiveToolBarPluginDelegate>delegate;
/** superView */
@property (nonatomic ,weak) UIView *fatherView;

- (instancetype)initToolBar:(NSArray <BYBaseInputViewPlugin *>*)inputViewPlugins;

- (void)destory;

- (void)hiddenToolBar;


@end

NS_ASSUME_NONNULL_END
