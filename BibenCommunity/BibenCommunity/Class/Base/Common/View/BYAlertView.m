//
//  BYAlertView.m
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYAlertView.h"
#import "CenterSignView.h"
#import "JCAlertView.h"


static NSInteger maskViewTag = 0x949;
@interface BYAlertView()

@property (nonatomic ,weak)   UIView *superView;
@property (nonatomic ,assign) BYAlertViewStyle style;
@property (nonatomic ,strong) UIView *contentView;
// ** 记录当前添加的btnAction数
@property (nonatomic ,strong) NSMutableArray *subBtnViews;
// ** 取消按钮(默认存在)
@property (nonatomic ,strong) UIButton *cancleBtn;
// ** 标题
@property (nonatomic ,strong) UILabel *titleLab;
// ** 内容
@property (nonatomic ,strong) UILabel *messageLab;
@end

static NSInteger alertW = 248;
static NSInteger alertH = 134;

@implementation BYAlertView

+ (instancetype)initWithTitle:(NSString *)title message:(NSString *__nullable)message inView:(UIView *)view alertViewStyle:(BYAlertViewStyle)alertViewStyle{
    BYAlertView *alertView = [[BYAlertView alloc] initWithFrame:CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)];
    alertView.title = nullToEmpty(title);
    alertView.message = nullToEmpty(message);
    alertView.superView = view;
    alertView.style = alertViewStyle;
    return alertView;
}

+ (id)showAlertAnimationWithTitle:(NSString *)title message:(NSString *__nullable)message inView:(UIView *)view{
    BYAlertView *alertView = [[BYAlertView alloc] initWithFrame:CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)];
    alertView.title = nullToEmpty(title);
    alertView.message = nullToEmpty(message);
    alertView.superView = view;
    alertView.style = UIAlertControllerStyleAlert;
    [alertView addActionCancleTitle:@"确定" handle:NULL];
    [alertView showAnimation];
    return alertView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    _titleLab.text = title;
}

- (void)setMessage:(NSString *)message{
    _message = message;
    _messageLab.text = message;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    @weakify(self);
    [_messageLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        if (self.title.length)
            make.top.equalTo(self.titleLab.mas_bottom).with.offset(10);
        else
            make.top.mas_equalTo(26);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
    
    // 当只有cancleBtn时return
    if (_subBtnViews.count == 1) return;
    CGFloat btnW = CGRectGetWidth(self.contentView.frame)/self.subBtnViews.count;
    [_cancleBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.equalTo(self.contentView.mas_width).with.offset(-btnW*(self.subBtnViews.count -1));
    }];
    
    for (int i = 1; i < self.subBtnViews.count; i++) {
        UIButton *customBtn = self.subBtnViews[i];
        [customBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btnW*i);
            make.width.mas_equalTo(btnW);
        }];
    }
    
    // 创建按钮的分割线
    for (int i = 1; i < self.subBtnViews.count; i++) {
        UIView *verticalLineView = [_contentView viewWithTag:0x9842 + i];
        if (verticalLineView) {
            [verticalLineView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(btnW*i);
            }];
            return;
        }
        verticalLineView = [self getVerticalLineView];
        verticalLineView.tag = 0x9842 + i;
        [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btnW*i);
            make.height.mas_equalTo(41);
            make.width.mas_equalTo(0.5);
            make.bottom.mas_equalTo(0);
        }];
        
    }
}

- (void)addActionCancleTitle:(NSString *)title handle:(void(^)())handle{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0x686868)}];
    [self addActionCancleAttributedString:attributedString handle:handle];
}

- (void)addActionCustomTitle:(NSString *)title handle:(void(^)())handle{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0x404040)}];
    [self addActionCustomAttributedString:attributedString handle:handle];
}

- (void)addActionCancleAttributedString:(NSAttributedString *)attributedString handle:(void(^)())handle{
    [_cancleBtn setAttributedTitle:attributedString forState:UIControlStateNormal];
    if (!handle) return;
    objc_setAssociatedObject(self.cancleBtn, @selector(cancleBtnAction), handle, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)addActionCustomAttributedString:(NSAttributedString *)attributedString handle:(void(^)())handle{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setAttributedTitle:attributedString forState:UIControlStateNormal];
    [customBtn addTarget:self action:@selector(cunstomBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:customBtn];
    [_subBtnViews addObject:customBtn];
    [customBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(0);
    }];
    [self layoutIfNeeded];
    if (!handle) return;
    objc_setAssociatedObject(customBtn, @selector(cunstomBtnAction:), handle, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setMaskViewStyle:(BYAlertViewMaskStyle)style{
    UIView *maskView = [self viewWithTag:maskViewTag];
    [maskView removeFromSuperview];
    maskView = nil;
    if (style == BYAlertViewMaskStyleBlurEffect) {
        // 添加毛玻璃效果
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        effectView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        effectView.layer.opacity = 0.7;
        effectView.tag = maskViewTag;
        [self addSubview:effectView];
        [self sendSubviewToBack:effectView];
    }
    else {
        UIView *maskView = [UIView by_init];
        maskView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        [maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
        maskView.tag = maskViewTag;
        [self addSubview:maskView];
        [self sendSubviewToBack:maskView];
    }
}

- (void)showAnimation{
    [self.superView addSubview:self];
    [self.superView bringSubviewToFront:self];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    self.contentView.alpha = 1.0;
    self.contentView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.1];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
}

- (void)hiddenAnimation{
    [UIView beginAnimations:@"hidden" context:nil];
    [UIView setAnimationDuration:0.2];
    self.contentView.alpha = 0;
    [UIView commitAnimations];
    [self removeFromSuperview];
}

- (void)cancleBtnAction{
    [self hiddenAnimation];
    id block = objc_getAssociatedObject(self.cancleBtn, @selector(cancleBtnAction));
    if (block) {
        void (^cancleHandle)(void) = block;
        cancleHandle();
    }
}

- (void)cunstomBtnAction:(UIButton *)sender{
    [self hiddenAnimation];
    id block = objc_getAssociatedObject(sender, @selector(cunstomBtnAction:));
    if (block) {
        void (^customHandle)(void) = block;
        customHandle();
    }
}

- (void)setContentView{
    // 添加毛玻璃效果
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    effectView.layer.opacity = 0.7;
    effectView.tag = maskViewTag;
    [self addSubview:effectView];
    
    self.subBtnViews = [NSMutableArray array];
    
    if (_style == BYAlertViewStyleAlert)
        [self layoutAlertSubView];
    else
        [self layoutSheetSubView];
    
}

#pragma mark - configSubView Method

- (UIView *)getVerticalLineView{
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:kColorRGBValue(0xdbdbdb)];
    [_contentView addSubview:lineView];
    return lineView;
}

- (void)layoutAlertSubView{
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
//    [contentView layerCornerRadius:5 size:CGSizeMake(alertW, alertH)];
    contentView.layer.cornerRadius = 5.0f;
    contentView.layer.borderWidth = 0.2;
    contentView.layer.borderColor = kColorRGBValue(0xdbdbdb).CGColor;
    [self addSubview:contentView];
    contentView.alpha = 0.0;
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(alertW);
        make.height.mas_equalTo(alertH);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x404040);
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.numberOfLines = 0;
    [contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(26);
        make.centerX.mas_equalTo(0);
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_greaterThanOrEqualTo(17);
    }];
    
    // 内容
    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = [UIFont systemFontOfSize:14];
    messageLab.textColor = kColorRGBValue(0x5a5a5a);
    messageLab.textAlignment = NSTextAlignmentCenter;
    messageLab.numberOfLines = 0;
    [contentView addSubview:messageLab];
    _messageLab = messageLab;
    @weakify(self);
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        if (self.title.length)
            make.top.equalTo(titleLab.mas_bottom).with.offset(10);
        else
            make.top.mas_equalTo(26);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
    
    // 取消按钮
    UIButton *cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    NSAttributedString *cancleAttributedString = [[NSAttributedString alloc] initWithString:@"取消" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0x686868)}];
    [cancleBtn setAttributedTitle:cancleAttributedString forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(cancleBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:cancleBtn];
    _cancleBtn = cancleBtn;
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(0);
        make.width.lessThanOrEqualTo(contentView.mas_width).with.offset(0);
        make.width.equalTo(contentView.mas_width).with.offset(0);
        make.bottom.mas_equalTo(0);
    }];
    
    // 创建横线
    UIView *lineView = [self getVerticalLineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-41);
        make.height.mas_equalTo(0.5);
    }];
    
    // 将默认的cancleBtn加入数组中
    [_subBtnViews addObject:cancleBtn];
    
}

- (void)layoutSheetSubView{
    
}



#pragma mark - mark ares
+(instancetype)sharedAlertView{
    static BYAlertView *_sharedAlertView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAlertView = [[BYAlertView alloc] init];
    });
    return _sharedAlertView;
}

-(void)showAlertSignWithTitle:(NSInteger )bbt block:(void(^)())block{
    CenterSignView *signView = [[CenterSignView alloc]initWithFrame:CGRectMake(0, 0, LCFloat(262), LCFloat(267))];
    signView.transferBBT = bbt;
    JCAlertView *alert;
    __weak typeof(self)weakSelf = self;
    alert = [[JCAlertView alloc]initWithCustomView:signView dismissWhenTouchedBackground:YES];
    [signView actionClickWithSignBlock:^{
        if (!weakSelf){
            return ;
        }
        [alert dismissWithCompletion:NULL];
        if (block){
            block();
        }
    }];
    
    [alert show];
}


@end
