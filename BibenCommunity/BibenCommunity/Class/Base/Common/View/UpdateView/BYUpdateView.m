//
//  BYUpdateView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYUpdateView.h"
#import <YYLabel.h>

#define kLeftRightSpace (iPhone5 ? 30 : 55)
@interface BYUpdateView ()

/** maskView遮罩 */
@property (nonatomic ,strong) UIView *maskView;
/** contenView */
@property (nonatomic ,strong) UIView *contentView;
/** 版本号 */
@property (nonatomic ,strong) UILabel *versionLab;
/** 更新内容 */
@property (nonatomic ,strong) UILabel *updateContent;
/** 暂不更新 */
@property (nonatomic ,strong) UIButton *notUpdateBtn;
/** 更新 */
@property (nonatomic ,strong) UIButton *updateBtn;
/** 竖线 */
@property (nonatomic ,strong) UIView *verticalLineView;


@end

@implementation BYUpdateView

+ (BOOL)verifyIsUpdate:(ServerRootConfigModel *)model{
    if (model.version_status == BY_VERSION_STATUS_FORCED ||
        model.version_status == BY_VERSION_STATUS_UPDATE) {
        if (!model.last_version_number.length && !model.last_version_content.length) {
            return NO;
        }
        return YES;
    }
    return NO;
}

+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    static BYUpdateView *updateView;
    dispatch_once(&onceToken, ^{
        updateView = [[BYUpdateView alloc] init];
    });
    return updateView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setModel:(ServerRootConfigModel *)model{
    _model = model;
    [self refreshView];
}

- (void)refreshView{
    self.versionLab.text = [NSString stringWithFormat:@"最新版本%@",self.model.last_version_number];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 8.0f;
    NSDictionary *attributes = @{NSForegroundColorAttributeName:kColorRGBValue(0x959595),
                                 NSParagraphStyleAttributeName:style,
                                 NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 };
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithString:nullToEmpty(self.model.last_version_content) attributes:attributes];
    CGFloat maxWidth = kCommonScreenWidth - 2*kLeftRightSpace - 2*23;
    CGSize size = [self.model.last_version_content getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxWidth, MAXFLOAT)];
    self.updateContent.attributedText = attributedStr;
    [self.updateContent mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceilf(size.height));
    }];
    
    if (self.model.version_status == BY_VERSION_STATUS_FORCED) { // 强制
        self.notUpdateBtn.layer.opacity = 0.0f;
        self.verticalLineView.layer.opacity = 0.0f;
        [self.updateBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
        }];
    }
}

- (void)showAnimation{
    [kCommonWindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        self.maskView.layer.opacity = 1.0f;
        self.contentView.layer.opacity = 1.0f;
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.2 animations:^{
        self.maskView.layer.opacity = 0.0f;
        self.contentView.layer.opacity = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)notUpdateBtnAction{
    [self hiddenAnimation];
}

- (void)updateBtnAction{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.model.last_version_download_address]]) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.model.last_version_download_address] options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.model.last_version_download_address]];
        }
    }
    if (self.model.version_status == BY_VERSION_STATUS_FORCED) {
        return;
    }
    [self hiddenAnimation];
}

- (void)setContentView{
    self.maskView = [UIView by_init];
    [self.maskView setBackgroundColor:kColorRGB(28, 28, 28, 0.78)];
    self.maskView.layer.opacity = 0.0f;
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.contentView = [UIView by_init];
    self.contentView.layer.opacity = 0.0f;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.cornerRadius = 10.0f;
    self.contentView.clipsToBounds = YES;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth - kLeftRightSpace*2);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
    UIImageView *headerImgView = [UIImageView by_init];
    [headerImgView setImage:[[UIImage imageNamed:@"common_update_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 160, 0, 94)]];
    [headerImgView by_setImageName:@"common_update_header"];
    [self.contentView addSubview:headerImgView];
    [headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(88);
    }];
    
    // 版本号
    UILabel *versionLab = [UILabel by_init];
    [versionLab setBy_font:13];
    versionLab.textColor = kColorRGBValue(0x959595);
    [self.contentView addSubview:versionLab];
    self.versionLab = versionLab;
    [versionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(23);
        make.top.mas_equalTo(headerImgView.mas_bottom).mas_offset(23);
        make.right.mas_equalTo(-23);
        make.height.mas_equalTo(stringGetHeight(@"最新版本", 13));
    }];
    
    UILabel *updateLab = [UILabel by_init];
    updateLab.text = @"更新内容";
    updateLab.textColor = kColorRGBValue(0x383838);
    [updateLab setBy_font:13];
    [self.contentView addSubview:updateLab];
    [updateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(23);
        make.top.mas_equalTo(versionLab.mas_bottom).mas_offset(18);
    }];
    
    // 更新内容
    UILabel *updateContent = [UILabel by_init];
    [updateContent setBy_font:13];
    updateContent.textColor = kColorRGBValue(0x959595);
    updateContent.numberOfLines = 0;
    [self.contentView addSubview:updateContent];
    self.updateContent = updateContent;
    [updateContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(23);
        make.top.mas_equalTo(updateLab.mas_bottom).mas_offset(11);
        make.right.mas_equalTo(-23);
        make.height.mas_greaterThanOrEqualTo(13);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xcccccc)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(updateContent.mas_bottom).mas_offset(15);
        make.height.mas_equalTo(0.5);
    }];
    
    CGFloat btnWidth = (kCommonScreenWidth - kLeftRightSpace*2)/2;
    // 暂不更新
    self.notUpdateBtn = [UIButton by_buttonWithCustomType];
    [self.notUpdateBtn setBy_attributedTitle:@{@"title":@"暂不更新",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x838383)} forState:UIControlStateNormal];
    [self.notUpdateBtn addTarget:self action:@selector(notUpdateBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.notUpdateBtn];
    [self.notUpdateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(48);
        make.width.mas_equalTo(btnWidth);
    }];
    
    self.updateBtn = [UIButton by_buttonWithCustomType];
    [self.updateBtn setBy_attributedTitle:@{@"title":@"立即更新",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xd84a27)} forState:UIControlStateNormal];
    [self.updateBtn addTarget:self action:@selector(updateBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.updateBtn];
    [self.updateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(btnWidth);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(48);
        make.right.mas_equalTo(0);
    }];
    
    UIView *verticalLineView = [UIView by_init];
    verticalLineView.backgroundColor = kColorRGBValue(0xcccccc);
    [self.contentView addSubview:verticalLineView];
    self.verticalLineView = verticalLineView;
    [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(48);
        make.bottom.mas_equalTo(0);
    }];
    
    @weakify(self);
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.mas_equalTo(self.updateBtn.mas_bottom).mas_offset(0);
    }];
    
}

@end
