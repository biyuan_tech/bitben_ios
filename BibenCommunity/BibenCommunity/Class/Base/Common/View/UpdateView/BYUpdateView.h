//
//  BYUpdateView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerRootModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BYUpdateView : UIView

/** model */
@property (nonatomic ,strong) ServerRootConfigModel *model;


/** 校验是否需要更新 */
+ (BOOL)verifyIsUpdate:(ServerRootConfigModel *)model;

+ (instancetype)shareManager;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
