//
//  BYILivePlayerView.h
//  BY
//
//  Created by 黄亮 on 2018/8/16.
//  Copyright © 2018年 BY. All rights reserved.
//



#import <UIKit/UIKit.h>

@class  BYIlivePlayerModel;
@interface BYILivePlayerView : UIView


/**
 设置播放器的父视图，用于实现可拖动的窗口播放
 */
@property (nonatomic ,strong) UIView *fatherView;

/**
 播放

 @param playerModel 数据源
 */
- (void)playWithModel:(BYIlivePlayerModel *)playerModel;

@end



@interface BYIlivePlayerModel : NSObject

// ** 直播网络封面图 */
@property (nonatomic , copy) NSString *placeHolderImageURLString;

// ** 直播封面图 */
@property (nonatomic ,strong) UIImage *placeHolderImage;

// ** 直播推流URL */
@property (nonatomic ,copy) NSString *liveURL;

@end
