//
//  BYCommonLiveModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonLiveModel.h"

static BYCommonLiveModel *_liveModel;
@implementation BYCommonLiveModel

//+ (instancetype)shareManager{
//    return [[BYCommonLiveModel alloc] init];
//}
//
//+ (instancetype)allocWithZone:(struct _NSZone *)zone{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        liveModel = [super allocWithZone:zone];
//    });
//    return liveModel;
//}

+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _liveModel = [[BYCommonLiveModel alloc] init];
    });
    return _liveModel;
}

@end
