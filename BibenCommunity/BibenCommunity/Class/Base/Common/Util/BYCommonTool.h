//
//  BYCommonTool.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/15.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYCommonTool : NSObject

/** 单例创建 */
+ (instancetype)shareManager;

/** 倒计时(param: days:天 hours:时 minute:分 second:秒) */
- (void)timerCutdown:(NSString *)beginTime cb:(void(^)(NSDictionary *param,BOOL isFinish))cb;

/** 计时器 */
- (void)timerBeginTime:(NSString *)beginTime cb:(void(^)(NSDictionary *param))cb;
- (void)stopTimer;

#pragma mark - 美颜设置

/** 设置默认的美颜值 */
- (void)setDefultBeautyValue;

@end

NS_ASSUME_NONNULL_END
