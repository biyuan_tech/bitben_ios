//
//  BYServiceManager.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kServiceHost [BYServiceManager getServiceHost]
#define kSercicePort [BYServiceManager getServicePort]

#define kSocketHost [BYServiceManager getSocketHost]
#define kSocketPort [BYServiceManager getSocketPort]

NS_ASSUME_NONNULL_BEGIN

@interface BYServiceManager : NSObject

/** 注册获取默认值 */
+ (void)registerSettingDefultValue;
/** 获取网关 */
+ (NSString *)getServiceHost;
/** 获取端口 */
+ (NSString *)getServicePort;
/** 获取socket网关 */
+ (NSString *)getSocketHost;
/** 获取socket端口 */
+ (NSInteger )getSocketPort;


@end

NS_ASSUME_NONNULL_END
