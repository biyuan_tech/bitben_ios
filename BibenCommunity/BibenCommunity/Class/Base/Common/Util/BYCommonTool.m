//
//  BYCommonTool.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/15.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonTool.h"

@interface BYCommonTool ()

@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation BYCommonTool

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    return [BYCommonTool shareManager];
}

+ (instancetype)shareManager{
    static BYCommonTool *userModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userModel = [[super allocWithZone:NULL] init];
    });
    return userModel;
}

- (id)copyWithZone:(NSZone *)zone{
    return [BYCommonTool shareManager];
}

- (void)timerCutdown:(NSString *)beginTime cb:(void(^)(NSDictionary *param,BOOL isFinish))cb{
    NSDate *endDate = [NSDate by_dateFromString:beginTime dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:[NSDate by_date] endTime:endDate];
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(self.timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self.timer);
                    self.timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cb(nil,YES);
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cb(@{@"days":@(days),@"hours":@(hours),@"minute":@(minute),@"second":@(second)},NO);
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(self.timer);
        }
        else
        {
            cb(nil,YES);
        }
    }
}

- (void)timerBeginTime:(NSString *)beginTime cb:(void (^)(NSDictionary * _Nonnull))cb{
    NSDate *beginDate = [NSDate by_dateFromString:beginTime dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:beginDate endTime:[NSDate by_date]];
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
        dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
        dispatch_source_set_event_handler(self.timer, ^{
            // 计时计算 时/分/秒
            NSInteger days = (int)(timeout/(3600*24));
            NSInteger hours = (int)((timeout-days*24*3600)/3600);
            NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
            NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
            dispatch_async(dispatch_get_main_queue(), ^{
                cb(@{@"hours":@(hours),@"minute":@(minute),@"second":@(second)});
            });
            timeout++; // 递增 倒计时+1(总时间以秒来计算)
        });
        dispatch_resume(self.timer);
    }
}

- (void)stopTimer{
    if (_timer) {
        dispatch_source_cancel(self.timer);
        self.timer = nil;
    }
}

- (void)setDefultBeautyValue{
//    [[NSUserDefaults standardUserDefaults] setValue:6.0 forKey:<#(nonnull NSString *)#>]
}

@end
