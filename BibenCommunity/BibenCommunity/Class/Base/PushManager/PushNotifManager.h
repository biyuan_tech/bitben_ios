//
//  PushNotifManager.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,PushType) {
    PushTypeLive = 100,                             /**< 直播*/
    PushTypeLiveAppointment = 101,                  /**< 直播预约关注*/
    PushTypeLiveChat = 102,                         /**< 直播私聊*/
    PushTypeLiveProfit = 103,                       /**< 直播昨日收益*/
    PushTypeLiveDelete = 104,                       /**< 某直播删除下线*/
    PushTypeLiveMyLink = 105,                       /**< 我关注的将要开播*/
    PushTypeLiveMyAppointment = 106,                /**< 我预约的将要开播*/
    PushTypeLiveSuport = 107,                       /**< 直播评论*/
    PushTypeLiveSubSuport = 108,                    /**< 直播评论的评论*/
    
    // 关注
    PushTypeUser = 200,                             /**< 用户*/
    PushTypeUserBeConcerned = 201,                  /**< 被关注*/
    PushTypeUserBeRelease = 202,                    /**< 关注的 人发布了内容*/
    PushTypeUserAuth = 203,                         /**< 认证*/
    
    // 文章
    PushTypeArticle = 300,                          /**< 文章*/
    PushTypeArticleDelete = 301,                    /**< 文章被删除*/
    PushTypeArticleInteraction = 302,               /**< 互动*/
    PushTypeArticleProfit = 303,                    /**< 文章昨日收益*/
    PushTypeArticleSupport = 304,                   /**< 文章赞*/
    PushTypeArticleSubSupport = 305,                /**< 文章评论赞*/
    PushTypeArticleComment = 306,                   /**< 文章评论*/
    PushTypeArticleSubComment = 307,                /**< 文章评论的评论*/
    
    // 系统
    PushTypeSystem = 400,                           /**< 系统*/
    PushTypeSystemMail = 401,                           /**< 站内信*/
    PushTypeSystemMsg = 402,                        /**< z系统消息*/
    PushTypeSystemOther = 403,                      /**< APP 推送*/
    PushTypeSystemReport = 404,                     /**< app 举报*/
    PushTypeSystemBeReport = 405,                     /**< app 被举报*/
    PushTypeSystemRecharge = 406,                   /**< app 充值*/
    PushTypeSystemTransaction = 407,                   /**< app 交易*/
    
    // 网页
    PushTypeWeb = 500,
    
    // 钱包
    PushTypeWalletTransferSuccess = 1009,            /**< 钱包转账通过*/
    PushTypeWalletTransferFail = 1010,               /**< 钱包转账驳回*/
};

@interface PushNotifManager : FetchModel

@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *identify;

+(void)showMessageWithTitle:(NSString *)title desc:(NSString *)desc backBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
