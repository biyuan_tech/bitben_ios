//
//  WebSocketTestModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebSocketTestModel : FetchModel

@property (nonatomic,assign)NSInteger number;
@property (nonatomic,copy)NSString *smart;


@end

NS_ASSUME_NONNULL_END
