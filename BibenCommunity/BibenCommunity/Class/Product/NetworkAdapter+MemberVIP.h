//
//  NetworkAdapter+MemberVIP.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "ProductVipMemberModel.h"
#import "ProductVipConfirmModel.h"
#import "WalletPwdErrorCountModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (MemberVIP)

-(void)sendRequestToGetMemberCenterInfoWithBlock:(void(^)(ProductVipMemberModel *productModel))block;

// 下订单
-(void)sendRequestToMakeOrderWithProductId:(NSString *)productId amount:(NSInteger)amount block:(void(^)(ProductVipConfirmModel *productModel))block;

// 产品支付
-(void)sendRequestToPayMemberProductManager:(NSString *)orderId blockPassword:(NSString *)blockPassword block:(void(^)(WalletPwdErrorCountModel *countModel))block;
@end

NS_ASSUME_NONNULL_END
