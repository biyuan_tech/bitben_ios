//
//  ProductVipMainCardTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ProductVipMemberModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipEquityView:UIView

-(void)infoStr:(NSString *)infoStr name:(NSString *)name fixedName:(NSString *)fixedName;

+(CGSize)calculationSize;
@end

@interface ProductVipMainCardTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ProductVipMemberModel *transferMemberModel;
-(void)actionClickWithLeftBtn:(void(^)())block;
-(void)actionClickWithRightBtn:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
