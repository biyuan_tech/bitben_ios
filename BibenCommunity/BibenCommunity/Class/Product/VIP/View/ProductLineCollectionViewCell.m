//
//  ProductLineCollectionViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductLineCollectionViewCell.h"

@interface ProductLineCollectionViewCell()
@property (nonatomic,strong)PDImageView *productImgView;
@end

@implementation ProductLineCollectionViewCell


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.productImgView = [[PDImageView alloc]init];
    CGFloat size_width = LCFloat(120);
    self.productImgView.frame = CGRectMake((self.size_width - size_width) / 2., (self.size_height - size_width) / 2., size_width, size_width);
    self.productImgView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.productImgView.layer.borderWidth = 3;
    self.productImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.productImgView.layer.cornerRadius = 5;
    self.productImgView.clipsToBounds = YES;
    [self addSubview:self.productImgView];
}

-(void)setTransferVipProductModel:(ProductVipMemberProductsListSingleModel *)transferVipProductModel{
    _transferVipProductModel = transferVipProductModel;
    if (transferVipProductModel.product_img.length){
        [self.productImgView uploadMainImageWithURL:transferVipProductModel.product_img placeholder:nil imgType:PDImgTypeOriginal callback:NULL];
    } else {
        self.productImgView.image = [UIImage imageNamed:@"icon_product_vip_member_1"];
    }
    
}


@end
