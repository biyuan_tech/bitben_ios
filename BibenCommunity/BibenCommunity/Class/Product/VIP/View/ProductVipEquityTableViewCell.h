//
//  ProductVipEquityTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ProductVipMemberModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipEquityTableViewCellSingleView : UIView
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

+(CGFloat)calculationSizeHeight;

@end

@interface ProductVipEquityTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ProductVipMemberProductsListSingleModel *transferProductSingleModel;

@end

NS_ASSUME_NONNULL_END
