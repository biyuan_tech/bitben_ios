//
//  ProductVipMemberTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipMemberTitleTableViewCell.h"

@interface ProductVipMemberTitleTableViewCell()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *lineView2;
@end

@implementation ProductVipMemberTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"606060"];
    [self addSubview:self.lineView];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"606060"];
    [self addSubview:self.lineView2];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"393939"];
    [self addSubview:self.titleLabel];    
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2., 0, titleSize.width, LCFloat(44));
    
    self.lineView.frame = CGRectMake(self.titleLabel.orgin_x - LCFloat(88) - LCFloat(11), LCFloat(22), LCFloat(88), 1);
    self.lineView2.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), LCFloat(22), self.lineView.size_width, 1);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(40);
}

@end
