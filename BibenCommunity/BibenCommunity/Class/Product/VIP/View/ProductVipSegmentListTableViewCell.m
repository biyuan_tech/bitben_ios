//
//  ProductVipSegmentListTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipSegmentListTableViewCell.h"

static char actionClickWithItemsBlockKey;
@interface ProductVipSegmentListTableViewCell()
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;

@end

@implementation ProductVipSegmentListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"会员权益"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &actionClickWithItemsBlockKey);
        if (block){
            block(index);
        }
    }];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(54));
    [self.segmentList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self addSubview:self.segmentList];

}

-(void)actionClickWithItemsBlock:(void(^)(NSInteger index))block{
    objc_setAssociatedObject(self, &actionClickWithItemsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(55);
}

@end
