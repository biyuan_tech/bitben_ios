//
//  ProductVipSuperMemberTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipSuperMemberTableViewCell.h"

@interface ProductVipSuperMemberTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *infoLabel;
@end

@implementation ProductVipSuperMemberTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.image = [UIImage imageNamed:@"icon_pool_normal_icon"];
    self.iconImgView.frame = CGRectMake(LCFloat(15), LCFloat(11), LCFloat(55), LCFloat(55));
    [self addSubview:self.iconImgView];
    
    self.infoLabel = [GWViewTool createLabelFont:@"14" textColor:@"565656"];
    self.infoLabel.numberOfLines = 0;
    [self addSubview:self.infoLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    self.infoLabel.text = transferTitle;
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.iconImgView.frame) - LCFloat(20) - LCFloat(16);
    CGSize titleSize = [self.infoLabel.text sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.infoLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(20), LCFloat(11), width, titleSize.height);
}

+(CGFloat)calculationCellHeightWithTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    
    CGFloat width = kScreenBounds.size.width - LCFloat(55) - LCFloat(15) - LCFloat(20) - LCFloat(16);
    CGSize titleSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    
    cellHeight = MAX(titleSize.height + 2 * LCFloat(11), LCFloat(11) * 2 + LCFloat(55));
    return cellHeight;
}


@end
