//
//  ProductComfirmTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductComfirmTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

-(void)actionClickWithCancelButton:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
