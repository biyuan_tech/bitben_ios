//
//  ProductConfirmButtonTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductConfirmButtonTableViewCell.h"

static char buttonKey;

@interface ProductConfirmButtonTableViewCell()
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation ProductConfirmButtonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    self.actionButton.layer.cornerRadius = LCFloat(4);
    self.actionButton.clipsToBounds = YES;
    self.actionButton.backgroundColor = [UIColor hexChangeFloat:@"EE4944"];
    [self.actionButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.actionButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(170)) / 2., LCFloat(25), LCFloat(170), LCFloat(40));
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buttonKey);
        if (block){
            block();
        }
    }];
}

-(void)buttonClickManager:(void(^)())block{
    objc_setAssociatedObject(self, &buttonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(90);
}

@end
