//
//  WalletCheckResultModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletCheckResultModel : FetchModel

@property (nonatomic,assign)BOOL checkResult;

@property (nonatomic,copy)NSString *result;
@property (nonatomic,copy)NSString *resultMsg;
@property (nonatomic,copy)NSString *type;

@end

NS_ASSUME_NONNULL_END
