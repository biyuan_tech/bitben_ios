//
//  WalletBiSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletBiSingleModel : FetchModel

@property (nonatomic,copy)NSString *biName;
@property (nonatomic,copy)NSString *number;
@property (nonatomic,strong)UIImage *iconImg;
@property (nonatomic,copy)NSString *address;

@end

NS_ASSUME_NONNULL_END
