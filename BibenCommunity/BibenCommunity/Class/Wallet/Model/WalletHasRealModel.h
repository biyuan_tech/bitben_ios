//
//  WalletHasRealModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletHasRealModel : FetchModel

@property (nonatomic,copy)NSString *block_address;
@property (nonatomic,assign)BOOL user_cer_status;
@property (nonatomic,assign)BOOL wallet_status;

@end

NS_ASSUME_NONNULL_END
