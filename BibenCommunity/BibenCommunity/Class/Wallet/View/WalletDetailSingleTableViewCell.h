//
//  WalletDetailSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "WalletTransferDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletDetailSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong) WalletTransferDetailModel *transferDetailModel;

@end

NS_ASSUME_NONNULL_END
