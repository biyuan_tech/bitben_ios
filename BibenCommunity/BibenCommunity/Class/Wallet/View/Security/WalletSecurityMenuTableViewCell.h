//
//  WalletSecurityMenuTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN


@interface WalletSecurityInputTableViewCellModel : FetchModel
@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign)BOOL successed;
@property (nonatomic,assign)BOOL hasTitle;
@property (nonatomic,assign)NSInteger index;
@end




@interface WalletSecurityMenuTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)WalletSecurityInputTableViewCellModel *transferModel;

-(void)iconImgStatus:(BOOL)status;

+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
