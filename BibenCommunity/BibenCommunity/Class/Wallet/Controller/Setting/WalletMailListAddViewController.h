//
//  WalletMailListAddViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "WalletMailListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,WalletMailListAddViewControllerType) {
    WalletMailListAddViewControllerTypeAdd,
    WalletMailListAddViewControllerTypeEdit,
};

@interface WalletMailListAddViewController : AbstractViewController

@property (nonatomic,assign)WalletMailListAddViewControllerType transferType;
@property (nonatomic,strong)WalletMailListSingleModel *transferSingleModel;

-(void)deleteAddressBlock:(void(^)(WalletMailListSingleModel *deleteModel))block;
-(void)addAddressBlock:(void(^)(WalletMailListSingleModel *deleteModel))block;

@end

NS_ASSUME_NONNULL_END
