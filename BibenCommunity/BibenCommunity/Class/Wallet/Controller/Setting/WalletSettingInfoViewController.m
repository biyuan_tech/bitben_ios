//
//  WalletSettingInfoViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSettingInfoViewController.h"

@interface WalletSettingInfoViewController ()

@end

@implementation WalletSettingInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self smart];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"钱包简介";
}

-(void)smart{
    [self.view showPrompt:@"当前没有简介" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
}
@end
