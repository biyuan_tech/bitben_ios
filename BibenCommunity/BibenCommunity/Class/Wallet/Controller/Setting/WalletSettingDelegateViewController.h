//
//  WalletSettingDelegateViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletSettingDelegateViewController : AbstractViewController

@end

NS_ASSUME_NONNULL_END
