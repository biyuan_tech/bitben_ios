//
//  WalletSecuritySettingPwdViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "WalletFindPwdTempModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,WalletSecuritySettingPwdViewControllerType) {
    WalletSecuritySettingPwdViewControllerTypeCreate,                   /**< 创建*/
    WalletSecuritySettingPwdViewControllerTypeUpdate,                   /**< 修改*/
    WalletSecuritySettingPwdViewControllerTypeFind,                     /**< 找回*/
};

@interface WalletSecuritySettingPwdViewController : AbstractViewController

@property(nonatomic,copy)NSString *transferOldPwd;
@property (nonatomic,assign)WalletSecuritySettingPwdViewControllerType transferType;
@property (nonatomic,strong)WalletFindPwdTempModel *transferFindBlockPwdModel;



-(void)walletCreateSuccessBlock:(void(^)())block;
-(void)walletUpdateSuccessBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
