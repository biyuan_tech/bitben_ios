//
//  WalletSecuritySettingPwdViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecuritySettingPwdViewController.h"
#import "WalletSecurityInputTableViewCell.h"
#import "WalletSecurityMenuTableViewCell.h"
#import "NetworkAdapter+Wallet.h"
#import "WalletRootViewController.h"
#import "WalletSettingViewController.h"

@interface WalletSecuritySettingPwdViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *inputTextField;
}
@property (nonatomic,strong)UITableView *walletTableView;
@property (nonatomic,strong)NSMutableArray *walletMutableArr;
@property (nonatomic,strong)NSMutableArray *securityMutableArr;
@end

@implementation WalletSecuritySettingPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置安全密码";
}

-(void)arrayWithInit{
    self.walletMutableArr = [NSMutableArray array];
    [self.walletMutableArr addObjectsFromArray:@[@[@"安全密码标题",@"输入安全密码"],@[@"密码强度信息",@"大写字母",@"一个数字",@"小写字母",@"8-32字符"],@[@"下一步"]]];
    self.securityMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < 5;i++){
        WalletSecurityInputTableViewCellModel *singleModel = [[WalletSecurityInputTableViewCellModel alloc]init];
        singleModel.index = i;
        if (i == 0){
            singleModel.hasTitle = YES;
            singleModel.title = @"为了您的资金安全，密码强度应包括";
            singleModel.successed = NO;
        } else if (i == 1){
            singleModel.hasTitle = NO;
            singleModel.title = @"至少一个大写字母";
            singleModel.successed = NO;
        } else if (i == 2){
            singleModel.hasTitle = NO;
            singleModel.title = @"至少一个数字";
            singleModel.successed = NO;
        } else if (i == 3){
            singleModel.hasTitle = NO;
            singleModel.title = @"至少一个小写字母";
            singleModel.successed = NO;
        } else if (i == 4){
            singleModel.hasTitle = NO;
            singleModel.title = @"8~32个字符";
            singleModel.successed = NO;
        }
        [self.securityMutableArr addObject:singleModel];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.walletTableView){
        self.walletTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.walletTableView.dataSource = self;
        self.walletTableView.delegate = self;
        [self.view addSubview:self.walletTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.walletMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.walletMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = @"请设置您的安全密码";
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowOne1 = @"cellIdentifyWithRowOne1";
            WalletSecurityInputTableViewCell *cellWithRowOne1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne1];
            if (!cellWithRowOne1){
                cellWithRowOne1 = [[WalletSecurityInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne1];
            }
            __weak typeof(self)weakSelf = self;
            inputTextField = cellWithRowOne1.inputTextField;
            [inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if ([strongSelf->inputTextField isFirstResponder]){
                    [strongSelf->inputTextField resignFirstResponder];
                }
            }];
            [cellWithRowOne1 inputTextFiledTextDidChanged:^(NSString * _Nonnull info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf inputManager];
            }];
            return cellWithRowOne1;
        }
    } else if(indexPath.section == 1){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletSecurityMenuTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletSecurityMenuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        WalletSecurityInputTableViewCellModel *singleModel = [self.securityMutableArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferModel = singleModel;
        
        return cellWithRowOne;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        if (self.transferType == WalletSecuritySettingPwdViewControllerTypeCreate){
            cellWithRowThr.transferTitle = @"下一步";
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.transferType == WalletSecuritySettingPwdViewControllerTypeCreate){
                    [strongSelf sendRequestToCreateWallet];
                }
            }];
        } else if (self.transferType == WalletSecuritySettingPwdViewControllerTypeUpdate){
            cellWithRowThr.transferTitle = @"完成";
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf= weakSelf;
                [strongSelf sendRequestToUpdatePwd:strongSelf.transferOldPwd newPwd:inputTextField.text];
            }];
        } else if (self.transferType == WalletSecuritySettingPwdViewControllerTypeFind){
            cellWithRowThr.transferTitle = @"完成";
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf= weakSelf;
                [strongSelf findPwdManager];
            }];
        }
        
       
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [WalletSecurityInputTableViewCell calculationCellHeight];
        }
    } else if(indexPath.section == 1){
        WalletSecurityInputTableViewCellModel *singleModel = [self.securityMutableArr objectAtIndex:indexPath.row];
        return [WalletSecurityMenuTableViewCell calculationCellHeightWithIndex:singleModel.index];
    } else if (indexPath.section == 2){
        return [GWButtonTableViewCell calculationCellHeight];
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)inputManager{
    WalletSecurityInputTableViewCellModel *singleModel1 = [self.securityMutableArr objectAtIndex:1];
    WalletSecurityInputTableViewCellModel *singleModel2 = [self.securityMutableArr objectAtIndex:2];
    WalletSecurityInputTableViewCellModel *singleModel3 = [self.securityMutableArr objectAtIndex:3];
    WalletSecurityInputTableViewCellModel *singleModel4 = [self.securityMutableArr objectAtIndex:4];
    
    // 2.是否有大写字母
    BOOL hasBigZimu = [self hasBigZimu:inputTextField.text];
    if (hasBigZimu){
        singleModel3.successed = YES;
    } else {
        singleModel3.successed = NO;
    }
    
    // 3. 是否有数字
    BOOL hasNumber = [self hasNumber:inputTextField.text];
    if (hasNumber){
        singleModel2.successed = YES;
    } else {
        singleModel2.successed = NO;
    }

    BOOL hasSmallZimu = [self hasSmallZimu:inputTextField.text];
    if (hasSmallZimu){
        singleModel1.successed = YES;
    } else {
        singleModel1.successed = NO;
    }
    
    // 4. 8 - 32
    if (inputTextField.text.length >= 8 && inputTextField.text.length < 32){
        singleModel4.successed = YES;
    } else {
        singleModel4.successed = NO;
    }
    
    
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:1];
    [self.walletTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
}

-(BOOL)hasBigZimu:(NSString *)zimu{
    BOOL hasBigZimu = NO;
    for (int i = 0; i<zimu.length; i++) {
        char commitChar = [zimu characterAtIndex:i];
        if((commitChar>96)&&(commitChar<123)){
            return YES;
        }
    }
    return hasBigZimu;
}

-(BOOL)hasNumber:(NSString *)number{
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]"options:NSRegularExpressionCaseInsensitive error:nil];
    
    //符合数字条件的有几个字节
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:number options:NSMatchingReportProgress range:NSMakeRange(0, number.length)];
    if (tNumMatchCount > 0){
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)hasSmallZimu:(NSString *)number{
    BOOL hasSmallZimu = NO;
    for (int i = 0; i<number.length; i++) {
        char commitChar = [number characterAtIndex:i];
        if((commitChar>64)&&(commitChar<91)){
            hasSmallZimu = YES;
        }
    }
    return hasSmallZimu;
}


#pragma mark - Interface
-(void)sendRequestToCreateWallet{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletCreateWalletWithPwd:inputTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"钱包创建成功"];
            WalletRootViewController *walletRootVC = [[WalletRootViewController alloc]init];
            [strongSelf.navigationController pushViewController:walletRootVC animated:YES];
        }
    }];
}


#pragma mark - 修改密码
-(void)sendRequestToUpdatePwd:(NSString *)oldPwd newPwd:(NSString *)newPwd{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletChangePwd:oldPwd newPwd:newPwd successBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [[UIAlertView alertViewWithTitle:@"密码修改成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [strongSelf dismissToController];
            }]show];
        }
    }];
}

#pragma mark - 找回密码
-(void)findPwdManager{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] walletFindPwdInfoManagerWithNewPwd:inputTextField.text name:self.transferFindBlockPwdModel.real_name number:self.transferFindBlockPwdModel.ID_number phone:self.transferFindBlockPwdModel.phone smsCode:self.transferFindBlockPwdModel.sms_code block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"密码找回成功，请妥善保管"];
            [strongSelf dismissToController];
        }
    }];
}

-(void)dismissToController{
    for (UIViewController *controller in self.navigationController.viewControllers){
        if ([controller isKindOfClass:[WalletSettingViewController class]]){
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}


-(void)walletUpdateSuccessBlock:(void(^)())block{
    
}

-(void)walletCreateSuccessBlock:(void(^)())block{
    
}
@end
