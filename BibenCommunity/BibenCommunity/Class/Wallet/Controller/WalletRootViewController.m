//
//  WalletRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletRootViewController.h"
#import "WalletSettingViewController.h"
#import "WalletRootSingleTableViewCell.h"
#import "WalletDetailViewController.h"
#import "NetworkAdapter+Wallet.h"

@interface WalletRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *walletTableView;
@property (nonatomic,strong)NSMutableArray *walletRootMutableArr;
@property (nonatomic,strong)NSMutableArray *walletInfoMutableArr;
@property (nonatomic,strong)NSMutableArray *bannerMutableArr;

@end

@implementation WalletRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetWalletListManager];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"Bitben Wallet";
    self.view.backgroundColor = RGB(243, 245, 249, 1);
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_wallet_slider"] barHltImage:[UIImage imageNamed:@"icon_wallet_slider"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletSettingViewController *walletSettingVC = [[WalletSettingViewController alloc]init];
        [strongSelf.navigationController pushViewController:walletSettingVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.walletInfoMutableArr = [NSMutableArray array];
    self.walletRootMutableArr = [NSMutableArray array];
    self.bannerMutableArr = [NSMutableArray array];
    
    [self.walletRootMutableArr addObject:@[@"banner"]];
    [self.walletRootMutableArr addObject:self.walletInfoMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.walletTableView){
        self.walletTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.walletTableView.delegate = self;
        self.walletTableView.dataSource = self;
        [self.view addSubview:self.walletTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.walletRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.walletRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.hasOneView = YES;
        cellWithRowOne.transferImgArr = self.bannerMutableArr;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        WalletRootSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[WalletRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        WalletYuESingleModel *singleModel = [[self.walletRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowTwo.transferSingleModel = singleModel;
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section != 0){
        WalletDetailViewController *walletDetailVC = [[WalletDetailViewController alloc]init];
        WalletYuESingleModel *singleModel = [[self.walletRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        walletDetailVC.transferSingleModel = singleModel;
        [self.navigationController pushViewController:walletDetailVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        return [GWBannerTableViewCell calculationCellHeight];
    } else {
        return [WalletRootSingleTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    } else {
        return LCFloat(12);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 获取接口
#pragma mark 获取钱包余额
-(void)sendRequestToGetWalletListManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletGetYueListManagerWithBlock:^(BOOL isSuccessed, WalletYuEModel * _Nonnull yueModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            // 1. banner
            [strongSelf.bannerMutableArr addObjectsFromArray:yueModel.banner];
            // 2. info
            for (int i = 0 ; i < yueModel.currency.count;i++){
                WalletYuESingleModel *singleModel = [yueModel.currency objectAtIndex:i];
                [strongSelf.walletRootMutableArr addObject:@[singleModel]];
            }
            [strongSelf.walletTableView reloadData];
        }
    }];
}

@end
