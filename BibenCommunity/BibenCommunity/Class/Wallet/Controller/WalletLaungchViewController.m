//
//  WalletLaungchViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletLaungchViewController.h"
#import "NetworkAdapter+Wallet.h"
#import "WalletRootViewController.h"
#import "WalletSecuritySettingPwdViewController.h"
#import "WalletDelegateViewController.h"

@interface WalletLaungchViewController ()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UIButton *createButton;
@property (nonatomic,strong)UILabel *subLabel;
@property (nonatomic,strong)UILabel *bitbenLabel;
@property (nonatomic,strong)UILabel *bitbenNameLabel;
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UIButton *backButton;

@end

@implementation WalletLaungchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self  createBgView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - createBgView
-(void)createBgView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor hexChangeFloat:@"327AFF"];
    self.bgImgView.frame = kScreenBounds;
    [self.view addSubview:self.bgImgView];
    
    self.createButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.createButton setTitle:@"创建钱包" forState:UIControlStateNormal];
    self.createButton.backgroundColor = [UIColor whiteColor];
    [self.createButton setTitleColor:[UIColor hexChangeFloat:@"327AFF"] forState:UIControlStateNormal];
    self.createButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    self.createButton.layer.cornerRadius = LCFloat(10);
    self.createButton.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    [self.createButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletDelegateViewController *walletSecuritySettingVC = [[WalletDelegateViewController alloc]init];
        
//        WalletSecuritySettingPwdViewController *walletSecuritySettingVC = [[WalletSecuritySettingPwdViewController alloc]init];
//        walletSecuritySettingVC.transferType = WalletSecuritySettingPwdViewControllerTypeCreate;
        [strongSelf.navigationController pushViewController:walletSecuritySettingVC animated:YES];
    }];
    self.createButton.frame = CGRectMake(LCFloat(15), kScreenBounds.size.height - LCFloat(33) - LCFloat(46), kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(46));
    [self.view addSubview:self.createButton];
    
    self.subLabel = [GWViewTool createLabelFont:@"15" textColor:@"FFFFFF"];
    
    self.subLabel.attributedText = [Tool rangeLabelWithContent:@"支持ETH和符合ERC20协议的token！" hltContentArr:@[@"ETH",@"ERC20"] hltColor:[UIColor hexChangeFloat:@"FFC500"] normolColor:[UIColor hexChangeFloat:@"FFFFFF"]];
    [self.view addSubview:self.subLabel];
    self.subLabel.frame = CGRectMake(LCFloat(32), self.createButton.orgin_y - LCFloat(86), kScreenBounds.size.width - 2 * LCFloat(32), [NSString contentofHeightWithFont:self.subLabel.font]);
    
    self.bitbenLabel = [GWViewTool createLabelFont:@"26" textColor:@"FFFFFF"];
    self.bitbenLabel.text = @"Bitben Wallet";
    [self.view addSubview:self.bitbenLabel];
    self.bitbenLabel.frame = CGRectMake(LCFloat(32), self.subLabel.orgin_y - LCFloat(42) - [NSString contentofHeightWithFont:self.bitbenLabel.font], kScreenBounds.size.width - 2 * LCFloat(32), [NSString contentofHeightWithFont:self.bitbenLabel.font]);
    
    self.bitbenNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"FFFFFF"];
    self.bitbenNameLabel.text = @"币本钱包";
    self.bitbenNameLabel.frame = CGRectMake(LCFloat(32), self.bitbenLabel.orgin_y - [NSString contentofHeightWithFont:self.bitbenNameLabel.font], kScreenBounds.size.width - 2 * LCFloat(32), [NSString contentofHeightWithFont:self.bitbenNameLabel.font]);
    [self.view addSubview:self.bitbenNameLabel];
    
    self.imgView = [[PDImageView alloc]init];
    self.imgView.image = [UIImage imageNamed:@"icon_wallet_laungch_info"];
    self.imgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(276)) / 2., self.bitbenNameLabel.orgin_y - LCFloat(65) - LCFloat(199), LCFloat(276), LCFloat(199));
    [self.view addSubview:self.imgView];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backButton.frame = CGRectMake(LCFloat(20), [BYTabbarViewController sharedController].statusHeight, LCFloat(44), LCFloat(44));
    [self.backButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
    [self.backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.backButton];
}


@end
