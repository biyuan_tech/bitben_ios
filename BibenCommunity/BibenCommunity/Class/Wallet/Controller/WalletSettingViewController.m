//
//  WalletSettingViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSettingViewController.h"
#import "WalletSecuritySettingPwdViewController.h"
#import "SettingHelpCenterViewController.h"
#import "WalletSettingNotifyCenterViewController.h"
#import "WalletSettingDelegateViewController.h"
#import "WalletSettingInfoViewController.h"
#import "WalletTongxunluViewController.h"
#import "WalletSecurityOldPwdViewController.h"
#import "CenterMessageRootViewController.h"
#import "WalletSecurityResetPwdViewController.h"

@interface WalletSettingViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;
@end

@implementation WalletSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
    self.view.backgroundColor = RGB(243, 245, 249, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.settingArr = @[@[@"通讯录",@"通知中心",@"帮助中心"],@[@"修改安全密码",@"找回安全密码"],@[@"用户协议",@"币本钱包简介"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.settingTableView.dataSource = self;
        self.settingTableView.delegate = self;
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWitRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if(!cellWitRowOne){
        cellWitRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWitRowOne.transferCellHeight = cellHeight;
    cellWitRowOne.transferTitle = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cellWitRowOne.transferHasArrow = YES;

    return cellWitRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 0){
        WalletTongxunluViewController *tongxunluVC = [[WalletTongxunluViewController alloc]init];
        [self.navigationController pushViewController:tongxunluVC animated:YES];
    } else if(indexPath.section == 1 && indexPath.row == 0){
        WalletSecurityOldPwdViewController *securityVC = [[WalletSecurityOldPwdViewController alloc]init];
        [self.navigationController pushViewController:securityVC animated:YES];
    } else if(indexPath.section == 1 && indexPath.row == 1){
        WalletSecurityResetPwdViewController *securityVC = [[WalletSecurityResetPwdViewController alloc]init];
        [self.navigationController pushViewController:securityVC animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 2){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:wallet_help];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 1){
        CenterMessageRootViewController *notifyVC = [[CenterMessageRootViewController alloc]init];
        notifyVC.transferType = CenterMessageRootViewControllerTypeBlock;
        [self.navigationController pushViewController:notifyVC animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 0){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:wallet_user_delegate];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 1){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:wallet_about];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(44);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(37));
    
    UILabel *titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"B1B1B1"];
    [headerView addSubview:titleLabel];
    if (section == 0){
        titleLabel.text = @"";
    } else if (section == 1){
        titleLabel.text = @"安全中心";
    } else if (section == 2){
        titleLabel.text = @"其他";
    }
    titleLabel.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(37));
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return  LCFloat(10);
    } else {
        return LCFloat(37);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

@end
