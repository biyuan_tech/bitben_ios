//
//  AccountModel.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AccountModel.h"
#import "NetworkAdapter+Login.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}


#pragma mark - 1.判断是否登录
- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].account_id.length ? YES : NO);
}

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSString *loginUserType = [Tool userDefaultGetWithKey:LoginType];
    NSString *loginAccount = [Tool userDefaultGetWithKey:LoginAccount];
    NSString *loginPwd = [Tool userDefaultGetWithKey:LoginPassword];

    if (loginUserType.length && loginAccount.length && loginPwd.length){
        thirdLoginType loginType = (thirdLoginType)[loginUserType integerValue];
        [AccountModel sharedAccountModel].loginType = loginType;
        [[NetworkAdapter sharedAdapter] loginThirdType:loginType account:loginAccount pwd:loginPwd block:^(BOOL isSuccessed, LoginServerModelAccount *serverAccountModel) {
            if (!weakSelf){
                return ;
            }
            if (isSuccessed){
                if (block){
                    block(YES);
                }
            }
        }];
    }
}


#pragma mark - 3.登录IM
-(void)loginIMManager{
    __weak typeof(self)weakSelf = self;
    NSString *account = [AccountModel sharedAccountModel].loginServerModel.account._id;
    [[AliChatManager sharedInstance] loginWithUserName:account password:@"bitben" preloginedBlock:^{
        if (!weakSelf){
            return ;
        }
//        [StatusBarManager statusBarHidenWithText:@"IM登录成功"];
    } successBlock:^{
        if (!weakSelf){
            return ;
        }
//        [StatusBarManager statusBarHidenWithText:@"IM登录成功"];
    } failedBlock:^(NSError *err) {
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"IM登录失败,失败原因%@",err.localizedDescription]];        
    }];
}


#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler(YES);
    } else {
        NSString *pwd = [Tool userDefaultGetWithKey:LoginPassword];
        NSString *account = [Tool userDefaultGetWithKey:LoginAccount];
        NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
        if (pwd.length && account.length && loginType.length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                handler(isSuccessed);
            }];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请先进行登录"];
            LoginRootViewController *loginMainViewController = [[LoginRootViewController alloc]init];
            [loginMainViewController loginSuccess:^(BOOL success) {
                if (success){
                    [loginMainViewController dismissViewControllerAnimated:YES completion:NULL];
                    handler(YES);
                } else {
                    handler(NO);
                    [StatusBarManager statusBarHidenWithText:@"取消登录&登录失败"];
                }
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }
}

#pragma mark - 无UI效果的登录
-(void)autoLoginWithNoneWindowBlock:(void(^)(BOOL isLogin))handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler(YES);
    } else {
        NSString *pwd = [Tool userDefaultGetWithKey:LoginPassword];
        NSString *account = [Tool userDefaultGetWithKey:LoginAccount];
        NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
        if (pwd.length && account.length && loginType.length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                handler(isSuccessed);
            }];
        } else {
            handler(NO);
        }
    }
}

#pragma mark - 退出登录
-(void)logoutManagerBlock:(void(^)())block{
    // 单例置空
    [AccountModel sharedAccountModel].loginServerModel = nil;
    [AccountModel sharedAccountModel].token = @"";
    [AccountModel sharedAccountModel].account_id = @"";
    
    // userdefault 清空
    [Tool userDefaultDelegtaeWithKey:LoginType];
    [Tool userDefaultDelegtaeWithKey:LoginAccount];
    [Tool userDefaultDelegtaeWithKey:LoginPassword];
    
    [StatusBarManager statusBarHidenWithText:@"正在登出IM"];
    [[AliChatManager sharedInstance] aliLogoutWithBlock:^{
        [StatusBarManager statusBarHidenWithText:@"IM登出成功"];
        if (block){
            block();
        }
    }];
}

-(BOOL)isShenhe{
    return NO;
}

@end
