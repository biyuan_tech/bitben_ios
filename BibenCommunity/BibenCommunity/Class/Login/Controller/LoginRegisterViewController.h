//
//  LoginRegisterViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "LoginRootViewController.h"
@class LoginRootViewController;
typedef NS_ENUM(NSInteger,LoginRegisterType) {
    LoginRegisterTypeRegister = 0,              /**< 注册*/
    LoginRegisterTypeBinding = 1,               /**< 进行绑定*/
};

@interface LoginRegisterViewController : AbstractViewController

@property (nonatomic,assign)LoginRegisterType transferType;         /**< 登录类型*/
@property (nonatomic,copy)NSString *transferAuthToken;              /**< 传递过来的微信token*/

@end
