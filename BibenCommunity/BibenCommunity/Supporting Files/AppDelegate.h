//
//  AppDelegate.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

