//
//  BYTabbarViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYTabbarViewController.h"
#import "BYNavigationController.h"
#import "PDGradientNavBar.h"
#import "PopMenu.h"
#import "ArticleReleaseRootViewController.h"
#import "ArticleDetailRootViewController.h"
#import "BYUpdateView.h"
#import "CenterMessageMainViewController.h"

@interface BYTabbarViewController ()<ZYTabBarDelegate>
@property (nonatomic, strong) PopMenu *popMenu;

@end

@implementation BYTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomerTabBar];
    [self pageSetting];
    
    [self.customerTabBar addCenterBtn];
    self.customerTabBar.plusBtn.frame = CGRectMake((kScreenBounds.size.width - self.customerTabBar.plusBtn.size_width) / 2., - LCFloat(10), self.customerTabBar.plusBtn.size_width, self.customerTabBar.plusBtn.size_height);
    
    [[UITabBar appearance]  addSubview:self.customerTabBar.plusBtn];
}

+(instancetype)sharedController{
    static BYTabbarViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[BYTabbarViewController alloc] init];
    });
    return _sharedAccountModel;
}


#pragma mark page
-(void)pageSetting{
    BYNavigationController *navigationController = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    UIColor *color1 = [UIColor colorWithCustomerName:@"白"];
    UIColor *color6 = [UIColor colorWithCustomerName:@"白"];
    NSArray *navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
    
    
    UIImage *unselectedImage = [UIImage imageNamed:@"icon_tabbar_home_nor"];
    UIImage *selectedImage = [UIImage imageNamed:@"icon_tabbar_home_hlt"];
    
    // 1. 【首页】
    BYLiveHomeController *homeViewController = [[BYLiveHomeController alloc]init];
    BYNavigationController *pandaNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [pandaNav setViewControllers:@[homeViewController]];
    pandaNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    pandaNav.navigationBar.layer.shadowOpacity = 2.0;
    pandaNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    homeViewController.tabBarItem.tag = 1;
    homeViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"直播" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 2. 【分类】
    ArticleRootViewController *productViewController = [ArticleRootViewController sharedController];
    productViewController.transferType = ArticleRootViewControllerTypeNormal;
    BYNavigationController *productNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [productNav setViewControllers:@[productViewController]];
    
    productNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    productNav.navigationBar.layer.shadowOpacity = 2.0;
    productNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    productViewController.tabBarItem.tag = 2;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_find_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_find_hlt"];
    productViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"观点" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    // 3.【消息】
    CenterMessageMainViewController *findViewController = [[CenterMessageMainViewController alloc]init];;
    BYNavigationController *findNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [findNav setViewControllers:@[findViewController]];
    
    findNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    findNav.navigationBar.layer.shadowOpacity = 2.0;
    findNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    findViewController.tabBarItem.tag = 4;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_class_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_class_hlt"];
    findViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    // 4.【个人中心】
    CenterRootViewController *centerViewController = [CenterRootViewController sharedController];
    BYNavigationController *centerNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [centerNav setViewControllers:@[centerViewController]];
    
    centerNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    centerNav.navigationBar.layer.shadowOpacity = 2.0;
    centerNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    centerViewController.tabBarItem.tag = 5;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_center_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_center_hlt"];
    centerViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    if ([AccountModel sharedAccountModel].isShenhe){
        self.viewControllers = @[productNav,centerNav];
    } else {
        self.viewControllers = @[pandaNav,productNav,findNav,centerNav];
    }
    
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"EE4C47"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"353535"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateNormal];
    
    UITabBar *tabBar = self.tabBar;
    
    centerNav.popGestureRecognizer = YES;
    pandaNav.popGestureRecognizer = YES;
    productNav.popGestureRecognizer = YES;
    findNav.popGestureRecognizer = YES;
    
    
    [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    // 设置背景颜色
    
    if ([tabBar respondsToSelector:@selector(setBarTintColor:)]){
        [tabBar setBarTintColor:[UIColor whiteColor]];
    }else{
        for (UIView *view in tabBar.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TabBarBackgroundView"]) {
                [view removeFromSuperview];
                break;
            }
        }
    }
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width,self.tabBarHeight)];
    backView.backgroundColor = [UIColor whiteColor];;
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = NO;
}

-(void)createCustomerTabBar{
    self.customerTabBar = [[ZYTabBar alloc]init];;
    self.customerTabBar.tabbarDelegate = self;
    self.customerTabBar.backgroundColor = [UIColor whiteColor];
    self.customerTabBar.tintColor = [UIColor whiteColor];
    self.customerTabBar.pathButtonArray = nil;
    self.customerTabBar.basicDuration = 0.5;
    self.customerTabBar.bloomRadius = 100;
    self.customerTabBar.allowCenterButtonRotation = NO;
    self.customerTabBar.bloomAngel = 100;
    self.customerTabBar.allowSubItemRotation = NO;
    [self setValue:self.customerTabBar forKeyPath:@"tabBar"];
}


-(CGFloat)tabBarHeight{
    if (IS_iPhoneX){
        return  49 + 34;
    } else {
        return  49 ;
    }
}
-(CGFloat)navBarHeight{
    if (IS_iPhoneX){
        return  44 + 44;
    } else {
        return  20 + 44;
    }
}

-(CGFloat)statusHeight{
    if (IS_iPhoneX){
        return  44 ;
    } else {
        return  20 ;
    }
}

-(void)showMessage{
    self.selectedIndex = TabbarTypeMessage;
}

-(void)showCenter{
    self.selectedIndex = TabbarTypeCenter;
}

-(void)showTabbar{
    NSArray *views = self.tabBarController.view.subviews;
    UIView *contentView = [views objectAtIndex:0];
    contentView.size_height -= self.tabBarHeight;
    self.tabBarController.tabBar.hidden = NO;
}

-(void)hiddenTabbar{
    //    NSArray *views = [BYTabbarViewController sharedController].tabBar.view.subviews;
    UIView *contentView = [BYTabbarViewController sharedController].customerTabBar;
    contentView.size_height += self.tabBarHeight;
    self.tabBarController.tabBar.hidden = YES;
}

-(void)directToController:(NSInteger)controllerIndex{
    self.selectedIndex = controllerIndex;
}

-(void)centerItemBtnClick{
    // 埋点
    [MTAManager event:MTATypeTabAdd params:nil];
    [self showMenu];
}

#pragma mark - 显示菜单
- (void)showMenu {
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
    
    MenuItem *menuItem =  [MenuItem itemWithTitle:@"长文" iconName:@"icon_tabbar_menu_1" glowColor:RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem];
    
    
    MenuItem * menuItem1 = [MenuItem itemWithTitle:@"短图文" iconName:@"icon_tabbar_menu_2" glowColor: RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem1];
    
    
    if (![AccountModel sharedAccountModel].isShenhe){
        MenuItem *menuItem2 = [MenuItem itemWithTitle:@"直播" iconName:@"icon_tabbar_menu_3" glowColor: RGB(232, 198, 131, .4f)];
        [items addObject:menuItem2];
    }
    
    if (!self.popMenu) {
        self.popMenu = [[PopMenu alloc] initWithFrame:window.bounds items:items];
        self.popMenu.menuAnimationType = kPopMenuAnimationTypeNetEase;
        self.popMenu.perRowItemCount = items.count;;
    }
    if (self.popMenu.isShowed) {
        return;
    }
    __weak typeof(self)weakSelf = self;
    weakSelf.popMenu.didSelectedItemCompletion = ^(MenuItem *selectedItem) {
        if (!weakSelf){
            return ;
        }
        if (selectedItem == nil){
            return;
        }
        if([selectedItem.title isEqualToString:@"长文"]){                 // 跳转长文
            // 埋点
            [MTAManager event:MTATypeTabAddLongArticle params:nil];
            
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferPageType = ArticleDetailRootViewControllerTypeArticle;
            self.selectedIndex = 1;
            [[ArticleRootViewController sharedController].navigationController pushViewController:articleVC animated:YES];
        } else if ([selectedItem.title isEqualToString:@"短图文"]){        // 跳转短图文
            // 埋点
            [MTAManager event:MTATypeTabAddShortArticle params:nil];
            
            [self shortImgTextInfoManager];
        } else if ([selectedItem.title isEqualToString:@"直播"]){         // 跳转直播
            // 埋点
            [MTAManager event:MTATypeTabAddLive params:nil];
            
            [BYCommonViewController centerItemBtnClickAction];
        }
    };
    
    CGPoint startPoint = CGPointMake(kScreenBounds.size.width / 2., self.customerTabBar.orgin_y);
    [_popMenu showMenuAtView:window startPoint:startPoint endPoint:startPoint];
    
}


#pragma mark - 发布短图文
-(void)shortImgTextInfoManager{
    AbstractViewController *absVC = [[AbstractViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [absVC authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            ArticleReleaseRootViewController *articleReleaseVC = [[ArticleReleaseRootViewController alloc]init];
            [articleReleaseVC actionWithReleaseSuccessedBlock:^{            // 跳转到观点
                strongSelf.selectedIndex = 1;
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:articleReleaseVC];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

-(void)shenheManager{



}

- (void)versionUpdate{
    ServerRootConfigModel *config = [AccountModel sharedAccountModel].serverModel.config;
    if ([BYUpdateView verifyIsUpdate:config]) {
        BYUpdateView *updateView = [BYUpdateView shareManager];
        updateView.model = config;
        [updateView showAnimation];
    }
}

-(void)messageAddBirdge{
    [CenterMessageRootViewController sharedController].tabBarItem.badgeValue = [NSString stringWithFormat:@"%li",[[CenterMessageRootViewController sharedController].tabBarItem.badgeValue integerValue] + 1];
}

-(void)cleanMessageBirdge{
    [CenterMessageRootViewController sharedController].tabBarItem.badgeValue = nil;
}

- (UIViewController *)getCurrentController{
    UITabBarController *tabBarController = [BYTabbarViewController sharedController];
    UINavigationController *navigationController = [tabBarController selectedViewController];
    UIViewController *viewController = [navigationController.viewControllers lastObject];
    UIViewController *currentController;
    if ([viewController isKindOfClass:[RTContainerController class]]) {
        currentController = ((RTContainerController *)viewController).contentViewController;
    }
    else{
        currentController = viewController;
    }
    return currentController;
}
@end
