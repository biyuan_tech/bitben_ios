//
//  UIScrollView+PDPullToRefresh.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIScrollView+PDPullToRefresh.h"
#import <MJRefresh.h>
#import "BYRefreshNormalHeader.h"
#import "BYRefreshNormalFooter.h"
#import <objc/runtime.h>

static char pageKey;
static char isXiaLaKey;              //
static char setHasLoadKey;
@implementation UIScrollView (PDPullToRefresh)


-(void)appendingPullToRefreshHandler:(void (^)())block{             // 下啦刷新
    // 修改page = 0
    self.currentPage = 0;
    self.hasLoad = YES;
    __weak typeof(self)weakSelf = self;
    if (!self.mj_header){
        self.mj_header = [BYRefreshNormalHeader headerWithRefreshingBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage = 0;
            strongSelf.isXiaLa = YES;
            if (block){
                block();
            }
            strongSelf.hasLoad = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                strongSelf.hasLoad = YES;
            });
        }];
    }
    //  1. 一开始设置footer隐藏
    if (self.mj_footer){
        self.mj_footer.hidden = YES;
    }
}

-(void)stopPullToRefresh{
    if (self.mj_header){
        [self.mj_header endRefreshing];
    }
    if (self.mj_footer){
        if (self.mj_footer.hidden == YES){
            self.mj_footer.hidden = NO;
        }
    }
}

-(void)appendingFiniteScrollingPullToRefreshHandler:(void (^)())block{              // 上啦加载
    __weak typeof(self)weakSelf = self;
    if (!self.mj_footer){
        self.mj_footer = [BYRefreshNormalFooter footerWithRefreshingBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf.hasLoad){
                return;
            }
            strongSelf.currentPage ++;
            strongSelf.isXiaLa = NO;
            if (block){
                block();
            }
        }];
    }
    self.mj_footer.hidden = YES;
}

-(void)stopFinishScrollingRefresh{
    if (self.mj_footer){
        [self.mj_footer endRefreshing];
    }
}



#pragma mark - Properties
-(void)setCurrentPage:(NSInteger)currentPage{
    objc_setAssociatedObject(self, &pageKey, @(currentPage), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)currentPage{
    NSInteger currrentPage = [objc_getAssociatedObject(self, &pageKey) integerValue];
    return currrentPage;
}

-(void)setIsXiaLa:(BOOL)isXiaLa{
    objc_setAssociatedObject(self, &isXiaLaKey, @(isXiaLa), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isXiaLa{
    BOOL xiala = [objc_getAssociatedObject(self, &isXiaLaKey) integerValue];
    return xiala;
}

-(void)setHasLoad:(BOOL)hasLoad{
    objc_setAssociatedObject(self, &setHasLoadKey, @(hasLoad), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)hasLoad{
    BOOL hasLoad = [objc_getAssociatedObject(self, &setHasLoadKey) integerValue];
    return hasLoad;
}

@end
