//
//  UIView+PDBadgeNumber.m
//  PandaKing
//
//  Created by Cranz on 16/11/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIView+PDBadgeNumber.h"

#define PDBadgeTag 1104
@implementation UIView (PDBadgeNumber)
- (void)addNormalBadge {
    [self addNormalBadgeWithColor:[UIColor redColor]];
}

- (void)addNormalBadgeWithColor:(UIColor *)color {
    [self addNormalBadgeWithColor:color borderColor:nil];
}

- (void)addNormalBadgeWithColor:(UIColor *)color borderColor:(UIColor *)bColor {
    CGSize size = self.bounds.size;
    CGFloat w = 7;
    UIView *badgeView = [[UIView alloc] initWithFrame:CGRectMake(size.width / 2 * (1 + 1 / M_SQRT2) , size.height / 2 - size.width / 2 / M_SQRT2 - 3, w, w)];
    badgeView.backgroundColor = color;
    badgeView.layer.cornerRadius = w / 2;
    badgeView.clipsToBounds = YES;
    if (bColor) {
        badgeView.layer.borderColor = bColor.CGColor;
        badgeView.layer.borderWidth = 1.5;
    }

    badgeView.tag = PDBadgeTag;
    [self addSubview:badgeView];
}

- (void)clearNormalBadge {
    UIView *badgeView = [self viewWithTag:PDBadgeTag];
    if (!badgeView) {
        return;
    }
    
    [badgeView removeFromSuperview];
}

@end
