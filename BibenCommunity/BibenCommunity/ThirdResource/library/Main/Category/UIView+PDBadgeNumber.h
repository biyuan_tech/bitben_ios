//
//  UIView+PDBadgeNumber.h
//  PandaKing
//
//  Created by Cranz on 16/11/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PDBadgeNumber)
- (void)addNormalBadge;
- (void)addNormalBadgeWithColor:(UIColor *)color;
- (void)addNormalBadgeWithColor:(UIColor *)color borderColor:(UIColor *)bColor;
- (void)clearNormalBadge;
@end
