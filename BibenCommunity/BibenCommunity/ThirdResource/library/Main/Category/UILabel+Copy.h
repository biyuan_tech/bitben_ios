//
//  UILabel+Copy.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/17.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Copy)
@property (nonatomic,assign) BOOL isCopyable;


@end

NS_ASSUME_NONNULL_END
