//
//  UIScrollView+Customise.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "UIScrollView+Customise.h"

static char scrollViewDidEndDeceleratingKey;

@implementation UIScrollView (Customise)

+(UIScrollView *)createScrollViewScrollEndBlock:(void (^)(UIScrollView *scrollView))block{
    UIScrollView *mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.delegate = mainScrollView;
    mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.pagingEnabled = YES;
    mainScrollView.alwaysBounceHorizontal = NO;
    mainScrollView.alwaysBounceVertical = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
//    mainScrollView.bounces = YES;

    objc_setAssociatedObject(mainScrollView, &scrollViewDidEndDeceleratingKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    return mainScrollView;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    void(^block)(UIScrollView *scrollView) = objc_getAssociatedObject(scrollView, &scrollViewDidEndDeceleratingKey);
    if (block){
        block(scrollView);
    }
}

@end
