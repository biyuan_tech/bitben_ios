//
//  Tool.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/28.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>

extern CGFloat LCFloat(CGFloat floatValue);
extern CGFloat LCFloatWithPadding(CGFloat floatValue, CGFloat padding);

@interface Tool : NSObject

// 判断是否为空
+ (BOOL)isEmpty:(NSString *)string;
+(NSString *)replaceSpace:(NSString *)string;
// 验证号码
+ (BOOL)validateMobile:(NSString *)mobile;
+ (BOOL)isPureNumandCharacters:(NSString *)string;
+ (BOOL)validatePassword:(NSString *)password;
+ (BOOL)validateUserName:(NSString *)userName;
+ (BOOL)isValidateEmail:(NSString *)email;
+ (BOOL)validateChinese:(NSString *)string;             /// 只能输入汉字
+ (BOOL)validateIdentityCard:(NSString *)string;        /// 身份证位数验证 15位或18位
+ (BOOL)validateOnlyNumbers:(NSString *)string;         /// 是否是纯数字
+ (BOOL)validateNumberAndCharacter:(NSString *)string;  /// 数字或26位字母组成的字符串
+ (BOOL)validateFloatNumber:(NSString *)string ;        /// 判断数字

+ (NSUInteger)calculateCharacterLengthForAres:(NSString *)str ;
// 拉伸图片
+ (UIImage *)stretchImageWithName:(NSString *)name;
// 返回imageViewBackGround
+(UIImage *)addBackgroundImageViewWithCellWithDataArray:(NSArray *)array indexPath:(NSIndexPath *)indexPath;
+(UIImage *)addBackgroundImageViewWithRefundCellWithDataArray:(NSArray *)dataArray indexPath:(NSIndexPath *)indexPath;
// 抖动
+(void)animationClickShowWithView:(UIButton *)button block:(void(^)())block;
+ (void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection callback:(void(^)())callbackBlock;

// 拨打客服电话
+ (void)callCustomerServerPhoneNumber:(NSString *)phoneNumber;
// 打开网址
+(void)openURLWithURL:(NSString *)url;

// rangeLabel
+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor normolColor:(UIColor *)normolColor;

+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor hltFont:(UIFont *)hltFont normolColor:(UIColor *)normolColor normalFont:(UIFont *)normalFont;


#pragma mark - userDefault
+(void)userDefaulteWithKey:(NSString *)key Obj:(NSString *)objString;
+(NSString *)userDefaultGetWithKey:(NSString *)key;
+(void)userDefaultDelegtaeWithKey:(NSString *)key;


#pragma mark - 号码裁剪
+(NSString *)numberCutting:(NSString *)number;
+(NSString *)numberCuttingWithBankCard:(NSString *)number;

+(NSString *)appName;
+(NSString *)appVersion;
+(NSString *)getBundleID;

// 复制
+(void)copyWithString:(NSString *)text callback:(void(^)())callBack;

+(CGFloat)smartFloat:(CGFloat)floatValue;
+ (UIImage*) createImageWithColor: (UIColor*) color frame:(CGRect)rect ;



+(void)clickZanWithView:(UIView *)button block:(void(^)())block;


#pragma mark-- 转化时间为相应格式
//+ (NSString *)getTimeGap:(NSTimeInterval)updateTime;

+ (UIImage *)imageByComposingImage:(UIImage *)image withMaskImage:(UIImage *)maskImage ;
+(CGSize)makeSizeWithLabel:(UILabel *)label;

#pragma mark - md5加密
+ (NSString *)md5:(NSString *)inPutText;

+(NSString *)transferWithInfoNumber:(NSInteger)number;
+ (UIWindow *)lastWindow;

+(void)setTransferSingleAsset:(UIImage *)transferSingleAsset imgView:(PDImageView *)imgView convertView:(UIView *)convertView;

#pragma mark - 获取当前视频的图片
+(UIImage *)getPlayerThumbnailImageWithURL:(NSString *)url;

#pragma mark 转换url
+(NSURL *)transformationUrl:(NSString *)url;

#pragma mark - 分割线
+(NSMutableAttributedString *)addLineWithStr:(NSString *)str;
@end
