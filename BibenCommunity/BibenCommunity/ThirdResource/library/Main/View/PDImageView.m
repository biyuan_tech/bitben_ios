//
//  PDImageView.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDImageView.h"
#import "PDImageView+DefultBg.h"
#import "PDHUD.h"
#import "CenterShareImgModel.h"

@implementation PDImageView


// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
     [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeNormal callback:callbackBlock];
}

-(void)uploadImageOriginalWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeOriginal callback:callbackBlock];
}

// 更新用户头像图片
-(void)uploadImageWithRoundURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeRound callback:callbackBlock];
}

- (void)uploadHDImageWithURL:(NSString *)urlString callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:nil imgType:PDImgTypeHD callback:callbackBlock];
}

#pragma mark - 更新图片 主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock{
    if ((![urlString isKindOfClass:[NSString class]])){
        return;
    }
    // 判断是否为本地默认图
    if ([self verifyIsLocalDefultBg:urlString]) {
        self.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",urlString]];
        if (callbackBlock){
            callbackBlock(self.image);
        }
        return;
    }
    // 1. 判断placeholder
    if (placeholder == nil){
        placeholder = [CenterShareImgModel createImgManagerWithPlacehorderWithRect:self.frame.size];
    }
    
    // 2. 判断url
    NSString *mainImgUrl = @"";
    NSURL *imgUrl;
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]){
        mainImgUrl = urlString;
        imgUrl =  [NSURL URLWithString:mainImgUrl];
    } else {
        NSString *baseURL = [PDImageView getUploadBucket:urlString];
        if (type == PDImgTypeOriginal || type == PDImgTypeOriginalWithLoading){
            mainImgUrl = [NSString stringWithFormat:@"%@%@",baseURL,urlString];
        }
        else if (type == PDImgTypeHD){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/2/w/%li/q/75|imageslim",baseURL,urlString,(long)(self.size_width*[UIScreen mainScreen].scale)];
        } else if(type == PDImgTypeTask){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/1/w/%li/h/%li/format/png/q/100|imageslim",baseURL,urlString,(long)self.size_width * 2,(long)self.size_height * 2];
        } else if (type == PDImgTypeAvatar){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/5/w/%li/h/%li/q/75|imageslim",baseURL,urlString,(long)self.size_width * 3,(long)self.size_height * 3];
        } else {
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/1/w/%li/h/%li/q/75|imageslim",baseURL,urlString,(long)self.size_width * 2,(long)self.size_height * 2];
        }
        imgUrl = [Tool transformationUrl:mainImgUrl];
    }
    
    // 如果是原图需要进行loading
    if (type == PDImgTypeOriginalWithLoading){              // 如果是进行loading的话
        placeholder = nil;
        [PDHUD showHUDProgress:@"正在加载中…" diary:0];
    }
    
    // 3. 进行加载图片
    @weakify(self);
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [PDHUD dismissManager];
        @strongify(self);
        if (image){
            if (type == PDImgTypeNormal){
                self.image = image;
            } else if (type == PDImgTypeRound){
                self.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
            }else if (type == PDImgTypeDrawRound){
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = [image addCornerRadius:image.size.width/2 size:image.size];
                });
            }
        }
        if (callbackBlock){
            callbackBlock(self.image);
        }
    }];
}

#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    __weak typeof(self)weakSelf = self;
    [[SDWebImageManager sharedManager].imageCache clearDiskOnCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

+(NSString *)diskCount{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    NSString *clearCacheName = tempSizeWithM >= 1 ? [NSString stringWithFormat:@"%.2fM",tempSizeWithM] : [NSString stringWithFormat:@"%.2fK",tempSizeWithM * 1024];
    return clearCacheName;
}

+(CGFloat)diskCountFloat{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    return tempSizeWithM;
}

+(NSString *)appendingImgUrl:(NSString *)url{
   NSString *mainImgUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,url];
    return mainImgUrl;
}

+ (NSString *)getUploadBucket:(NSString *)url{
    if ([url hasPrefix:@"avatar"]) { // 头像上传至https
        return imageBaseURL_SLL;
    }
    return imageBaseUrl;
}

@end
