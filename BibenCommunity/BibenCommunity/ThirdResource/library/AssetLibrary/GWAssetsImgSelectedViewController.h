//
//  GWAssetsImgSelectedViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "ArticleImageSelectedTableViewCell.h"               // 文章里面使用到
#import "ArticleRootTableViewCell.h"
#import "ArticleDetailInfoTableViewCell.h"
#import "MineRealChoosePhotoTableViewCell.h"

@interface GWAssetsImgSelectedViewController : AbstractViewController

@property (nonatomic,assign)CGRect webViewSelectedRect;

-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(id )cell;

- (void)dismissFromView:(UIViewController *)viewController;

@end
