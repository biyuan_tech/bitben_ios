//
//  BYInputViewPluginPPTOperator.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/11.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYInputViewPluginPPTOperator.h"

@implementation BYInputViewPluginPPTOperator
@synthesize delegate;
@synthesize inputViewRef;
@synthesize pluginIconImage;
@synthesize pluginName;

- (YWConversationViewController *)conversationViewController{
    if ([self.inputViewRef.controllerRef isKindOfClass:[YWConversationViewController class]]) {
        return (YWConversationViewController *)self.inputViewRef.controllerRef;
    } else {
        return nil;
    }
}

#pragma mark - YWInputViewPluginProtocol

/**
 * 您需要实现以下方法
 */

// 插件图标
- (UIImage *)pluginIconImage
{
    return [UIImage imageNamed:@"input_plug_ico_ppt_nor"];
}

// 插件名称
- (NSString *)pluginName
{
    return @"课件";
}

// 插件对应的view，会被加载到inputView上
- (UIView *)pluginContentView
{
    return nil;
}

// 插件被选中运行
- (void)pluginDidClicked
{
    if (_pluginHandle) {
        _pluginHandle();
    }
}

- (YWInputViewPluginType)pluginType {
    return YWInputViewPluginTypeDefault;
}



@end
