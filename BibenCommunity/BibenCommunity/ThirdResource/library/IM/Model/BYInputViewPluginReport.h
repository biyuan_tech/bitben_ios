//
//  BYInputViewPluginReport.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/10.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYInputViewPluginReport : NSObject<YWInputViewPluginProtocol>

// 加载该插件的inputView
@property (nonatomic, weak) YWMessageInputView *inputViewRef;
@property (nonatomic, copy) void (^pluginHandle)(void);

@end

NS_ASSUME_NONNULL_END
