//
//  AliChatManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/25.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
#import <WXOUIModule/YWUIFMWK.h>
#import <WXOUIModule/YWIndicator.h>
#import <objc/runtime.h>
#import <WXOpenIMSDKFMWK/YWTribeSystemConversation.h>
#import "GWCurrentIMViewController.h"

#define kBYCustomizeMessageType @"CustomizeMessageType"

// 结束直播
static NSString * const BYCustomizeMessageType_EndLive = @"ENDLIVE";
// 开始直播
static NSString * const BYCustomizeMessageType_BeginLive = @"BEGINLIVE";
// 打赏
static NSString * const BYCustomizeMessageType_Reward = @"REWARD";

@protocol AliChatManagerDelegate <NSObject>
-(void)openProfileWithPersonId:(NSString *)personId;
@optional
-(void)listenNewMessageManagerWithMessage:(id<IYWMessage>)message;
@end

@interface AliChatManager : NSObject

+ (instancetype)sharedInstance;
@property (nonatomic,weak)id<AliChatManagerDelegate> messageDelegate;

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (nonatomic, assign) YWIMConnectionStatus lastConnectionStatus;
//@property (nonatomic, strong ,readonly) YWPerson *loginPerson;

#pragma mark - 1.Launch初始化页面进行调用
- (void)callThisInDidFinishLaunching;

#pragma mark - 2.1 登录注册
-(void)loginWithUserName:(NSString *)userName password:(NSString *)password preloginedBlock:(void(^)())aPreloginedBlock successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock ;

#pragma mark - 2.2 登出
-(void)aliLogoutWithBlock:(void (^)())logoutBlock;


#pragma mark 打开单聊页面
-(void)openConversationWithPerson:(YWPerson *)person controllerClass:(Class)controllerClass callBack:(void(^)(YWConversationViewController *conversationController))block;
- (void)exampleOpenConversationViewControllerWithPerson:(YWPerson *)aPerson fromNavigationController:(UINavigationController *)aNavigationController callBcak:(void (^)(YWConversationViewController *conversationController))block;

#pragma makr - 自定义插件
/** 主播操作插件 */
- (void)exampleAddHostInputViewPluginToConversationController:(YWConversationViewController *)aConversationController pluginHandle:(void(^)(void))pluginHandle;
/** 课件添加插件 */
- (void)exampleAddHostPPTInputViewPluginToConversationController:(YWConversationViewController *)aConversationController pluginHandle:(void(^)(void))pluginHandle;
/** 关闭直播插件添加 */
- (void)exampleAddHostExitLiveInputViewPluginToConversationController:(YWConversationViewController *)aConversationController pluginHandle:(void(^)(void))pluginHandle;
/** 举报插件添加 */
- (void)exampleAddGuestReportInputViewPluginToConversationConstroller:(YWConversationViewController *)aConversationController pluginHandle:(void(^)(void))pluginHandle;
/** 移除多余不用的plugin */
- (void)removeExcessPlugin:(YWConversationViewController *)aConversationController;

#pragma mark - 创建群聊页面
-(void)createTribeWithSuccessBlock:(void(^)(NSString *tribeNumber))block;
#pragma mark - 加入群
-(void)joinTribeWithNumber:(NSString *)tribeNumber fromNav:(UINavigationController *)nav controller:(Class)controller callBcak:(void (^)(YWConversationViewController *conversationController))block;

// 删除某个回话
-(void)deleteConversationWithId:(NSString *)conversationId block:(void(^)(NSError *error))block;

- (void)enableAtAllForAllTribeMember:(BOOL)enable forTribe:(NSString *)tribeId;

#pragma mark - 发送自定义红包消息
- (void)exampleSendRewardMessageWithConversationController:(YWConversationViewController *)aConversationController amount:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name;

#pragma mark - 发送结束直播消息
- (void)exampleSendFinishLiveMessageWithConversationController:(YWConversationViewController *)aConversationController;
#pragma mark - 发送开始直播消息
- (void)exampleSendBeginLiveMessageWithConversationController:(YWConversationViewController *)aConversationController;

#pragma mark - 禁言
- (void)disableSendMsgWithConversationController:(YWConversationViewController *)aConversationController;
#pragma mark - 禁止发送语音
- (void)disableVoiceWithConversationController:(YWConversationViewController *)aConversationController;
@end

