//
//  BYEndLiveBubbleChatView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYEndLiveBubbleChatView.h"
#import "BYEndLiveBubbleViewModel.h"

@implementation BYEndLiveBubbleChatView

- (instancetype)init
{
    self = [super init];
    if (self) {
        UILabel *label = [UILabel by_init];
        label.text = @"直播已经结束";
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        CGFloat height = stringGetHeight(label.text, 12) + 12;
        CGFloat width = stringGetWidth(label.text, 12) + 30;
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        
        UIView *maskView = [UIView by_init];
        maskView.backgroundColor = kColorRGBValue(0x424242);
        maskView.layer.cornerRadius = height/2;
        [self addSubview:maskView];
        [self sendSubviewToBack:maskView];
        [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        
        self.frame = CGRectMake(0, 0, width, height);
        
    }
    return self;
}

- (CGSize)getBubbleContentSize{
    return self.frame.size;
}

- (NSString *)viewModelClassName{
    return NSStringFromClass([BYEndLiveBubbleViewModel class]);
}

@end
