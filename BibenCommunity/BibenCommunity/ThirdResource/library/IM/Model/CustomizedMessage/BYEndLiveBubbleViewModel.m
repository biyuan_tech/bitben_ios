//
//  BYEndLiveBubbleViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYEndLiveBubbleViewModel.h"

@implementation BYEndLiveBubbleViewModel

- (instancetype)initWithMessage:(id<IYWMessage>)aMessage{
    self = [super init];
    if (self) {
        self.bubbleStyle = BubbleStyleCustomize;
        self.layout = WXOBubbleLayoutCenter;
    }
    return self;
}

@end
