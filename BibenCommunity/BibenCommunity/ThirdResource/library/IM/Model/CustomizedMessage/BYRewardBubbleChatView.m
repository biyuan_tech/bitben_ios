//
//  BYRewardBubbleChatView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYRewardBubbleChatView.h"
#import "BYRewardBubbleViewModel.h"

@interface BYRewardBubbleChatView ()

@property (nonatomic, strong) BYRewardBubbleViewModel *viewModel;

@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) UIView *maskView;

@end

@implementation BYRewardBubbleChatView
@dynamic viewModel;

- (instancetype)init
{
    self = [super init];
    if (self) {
        UILabel *label = [UILabel by_init];
        label.text = self.viewModel.rewardText;
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        self.label = label;
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        
        UIView *maskView = [UIView by_init];
        maskView.backgroundColor = kColorRGBValue(0x424242);
        [self addSubview:maskView];
        [self sendSubviewToBack:maskView];
        self.maskView = maskView;
        [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
}

- (CGSize)getBubbleContentSize{
    return self.frame.size;
}

- (void)updateBubbleView{
    self.label.text = self.viewModel.rewardText;
    CGFloat height = stringGetHeight(self.label.text, 12) + 12;
    CGFloat width = stringGetWidth(self.label.text, 12) + 30;
    self.frame = CGRectMake(0, 0, width, height);
    self.maskView.layer.cornerRadius = height/2;
}

- (NSString *)viewModelClassName{
    return NSStringFromClass([BYRewardBubbleViewModel class]);
}

@end
