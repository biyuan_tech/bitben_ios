//
//  BYEmptyBubbleViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYEmptyBubbleViewModel.h"

@implementation BYEmptyBubbleViewModel
- (instancetype)initWithMessage:(id<IYWMessage>)aMessage{
    self = [super init];
    if (self) {
        self.bubbleStyle = BubbleStyleCustomize;
        self.layout = WXOBubbleLayoutCenter;
    }
    return self;
}

@end
