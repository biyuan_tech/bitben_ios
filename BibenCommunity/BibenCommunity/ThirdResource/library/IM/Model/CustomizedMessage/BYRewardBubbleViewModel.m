//
//  BYRewardBubbleViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYRewardBubbleViewModel.h"

@implementation BYRewardBubbleViewModel

- (instancetype)initWithMessage:(id<IYWMessage>)aMessage{
    self = [super init];
    if (self) {
        YWMessageBodyCustomize *bodyCustomize = (YWMessageBodyCustomize *)[aMessage messageBody];
        
        self.rewardText = bodyCustomize.summary;
        
        self.bubbleStyle = BubbleStyleCustomize;
        self.layout = WXOBubbleLayoutCenter;
    }
    return self;
}

@end
