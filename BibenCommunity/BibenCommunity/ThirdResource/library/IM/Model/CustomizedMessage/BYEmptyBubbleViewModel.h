//
//  BYEmptyBubbleViewModel.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYEmptyBubbleViewModel : YWBaseBubbleViewModel

- (instancetype)initWithMessage:(id<IYWMessage>)aMessage;

@end

NS_ASSUME_NONNULL_END
