//
//  BYRewardBubbleViewModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import <WXOUIModule/YWUIFMWK.h>
#import <WXOpenIMSDKFMWK/YWFMWK.h>
NS_ASSUME_NONNULL_BEGIN

@interface BYRewardBubbleViewModel : YWBaseBubbleViewModel

/** 赞赏文案 */
@property (nonatomic ,copy) NSString *rewardText;

- (instancetype)initWithMessage:(id<IYWMessage>)aMessage;

@end

NS_ASSUME_NONNULL_END
