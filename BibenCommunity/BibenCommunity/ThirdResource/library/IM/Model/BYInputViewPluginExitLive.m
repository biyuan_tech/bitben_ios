//
//  BYInputViewPluginExitLive.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYInputViewPluginExitLive.h"

@implementation BYInputViewPluginExitLive
@synthesize delegate;
@synthesize inputViewRef;
@synthesize pluginIconImage;
@synthesize pluginName;


#pragma mark - YWInputViewPluginProtocol

/**
 * 您需要实现以下方法
 */

// 插件图标
- (UIImage *)pluginIconImage
{
    return [UIImage imageNamed:@"input_plug_ico_exitLive_nor"];
}
// 插件名称
- (NSString *)pluginName
{
    return @"结束直播";
}

// 插件对应的view，会被加载到inputView上
- (UIView *)pluginContentView
{
    return nil;
}

// 插件被选中运行
- (void)pluginDidClicked
{
    if (_pluginHandle) {
        _pluginHandle();
    }
}

- (YWInputViewPluginType)pluginType {
    return YWInputViewPluginTypeDefault;
}

@end
