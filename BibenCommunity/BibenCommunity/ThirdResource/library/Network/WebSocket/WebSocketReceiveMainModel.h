//
//  WebSocketReceiveMainModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"
#import "WebSocketTestModel.h"

NS_ASSUME_NONNULL_BEGIN

@class WebSocketConnection;
typedef NS_ENUM(NSInteger ,WebSocketType) {
    WebSocketTypeIM = 1,                    /**< IM 使用*/
};


@interface WebSocketReceiveMainModel : FetchModel

@property (nonatomic,assign)WebSocketType socketType;
@property (nonatomic,strong)NSDictionary *receiveDataDic;                /**< dictionary*/
@property (nonatomic,copy)NSString *receiveDataJson;                /**< Json*/

// 业务使用
@property (nonatomic,strong)WebSocketTestModel *webSocketModel;


+(WebSocketReceiveMainModel *)socketReceiceMainEcode:(WebSocketReceiveMainModel *)responseModelObject;

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
@end

NS_ASSUME_NONNULL_END
