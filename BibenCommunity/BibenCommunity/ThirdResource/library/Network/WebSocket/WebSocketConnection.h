//
//  WebSocketConnection.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>

static NSString *endCode = @"Yml0YmVu";


typedef NS_ENUM(NSInteger ,SocketConnectType) {
    SocketConnectTypeNotConnected,                  /**< socket没有连接过*/
    SocketConnectTypeOpen = 1,                      /**< 连接中*/
    SocketConnectTypeReconnection,              /**< 正在重新连接中*/
    SocketConnectTypeClosedByServer,            /**< 由服务端发起已断线*/
    SocketConnectTypeClosedByCustomer,          /**< 由客户端发起已断线*/
};

@protocol WebSocketConnectionDelegate <NSObject>

@optional
-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message;
-(void)webSocketDidConnectedWithSocket:(SRWebSocket *)webSocket;                /**< webSocket 连接成功后操作*/
-(void)webSocketDidConnectedErrorTimes:(NSInteger)times connectType:(SocketConnectType)connectType errorString:(NSString *)err;
@end

@interface WebSocketConnection : NSObject

@property (nonatomic,weak)id<WebSocketConnectionDelegate> delegate;
@property (nonatomic,strong)SRWebSocket *webSocket;
@property (nonatomic,assign)SocketConnectType socketConnectType;

// 序列号
@property (nonatomic,assign)NSInteger serialAutoNumber;

// 用作分包黏包
@property (nonatomic,assign)NSInteger socketTempLength;                                                /**< 临时调用计算*/
@property (nonatomic,copy)NSString *socketTempData;
@property (nonatomic,assign)NSInteger socketTempWillLength;                                            /**< socket 将要接收的数据*/
@property (nonatomic,copy)NSString *socketWillTempData;                                                /**< socket 已经过滤掉的数据*/

- (void)webSocketConnectWithHost:(NSString *)hostName port:(NSInteger)port;                            /**< 连接到服务器*/
- (void)webSocketDisconnect;                                                                           /**< 断开服务*/
- (void)webSocketWriteData:(NSString *)info;                                                           /**< 发送信息*/

@end
