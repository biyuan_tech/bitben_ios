//
//  WebSocketReceiveMainModel.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WebSocketReceiveMainModel.h"

@implementation WebSocketReceiveMainModel

+(WebSocketReceiveMainModel *)socketReceiceMainEcode:(WebSocketReceiveMainModel *)responseModelObject{
    NSDictionary *descDic = [WebSocketReceiveMainModel dictionaryWithJsonString:responseModelObject.receiveDataJson];
    responseModelObject.receiveDataDic = descDic;
    
    // 【解析】
    if (descDic){
        if (responseModelObject.socketType == WebSocketTypeIM){     // IM
            WebSocketTestModel *testModel = [[WebSocketTestModel alloc]initWithJSONDict:descDic];
            responseModelObject.webSocketModel = testModel;
        } 
    }
    return responseModelObject;
}


#pragma mark - Other
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString{
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSString *jsonStr = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&err];
    if ([jsonStr isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary *)jsonStr;
    }
    NSData *mainData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responsDic = [NSJSONSerialization JSONObjectWithData:mainData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return responsDic;
}

@end
