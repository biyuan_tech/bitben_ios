//
//  ShareSDKManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/4/25.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "ShareSDKManager.h"
#import <ShareSDK/ShareSDK.h>
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <ShareSDKConnector/ShareSDKConnector.h>

#import "WXApi.h"
#import "NetworkAdapter+Task.h"

@implementation ShareSDKManager

+(void)registeShareSDK{
    [ShareSDK registerActivePlatforms:@[@(SSDKPlatformTypeWechat),@(SSDKPlatformTypeQQ),@(SSDKPlatformTypeSinaWeibo)] onImport:^(SSDKPlatformType platformType) {
        if (platformType == SSDKPlatformTypeWechat){
            [ShareSDKConnector connectWeChat:[WXApi class] delegate:self];
        } else {
            [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
        }
    } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
        if (platformType == SSDKPlatformTypeWechat){
            [appInfo SSDKSetupWeChatByAppId:WeChatAppID appSecret:WeChatAppSecret];
        } else {
            [appInfo SSDKSetupQQByAppId:QQAppID appKey:QQAppAppSecret authType:SSDKAuthTypeBoth];
        }
    }];
}

+(void)thirdLoginWithType:(thirdLoginType)thirdLoginType successBlock:(void(^)(SSDKUser *user))successHandle failBlock:(void(^)())failHandle{
    [SSEThirdPartyLoginHelper loginByPlatform:SSDKPlatformTypeWechat onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
        if (successHandle){
            successHandle(user);
        }
    } onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            if (successHandle){
                successHandle(nil);
            }
        } else if (state == SSDKResponseStateFail){
            if (failHandle){
                failHandle();
            }
        }
    }];
}


+(void)shareManagerWithType:(thirdLoginType)type title:(NSString *)title desc:(NSString *)desc img:(id)img url:(NSString *)url callBack:(void(^)(BOOL success))callback{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSURL *mainUrl = [NSURL URLWithString:url];
    
//    if (!img){
//        img = [UIImage imageNamed:@"icon_main_logo"];
//    }
    
    if (title.length == 0 && desc.length == 0){
        
    } else {
        img = [UIImage imageNamed:@"icon_share_logo"];
    }
    
    [params SSDKSetupShareParamsByText:desc images:img url:mainUrl title:title type:SSDKContentTypeAuto];
    
    SSDKPlatformType mainType;
    if (type == thirdLoginTypeWechat){
        mainType = SSDKPlatformTypeWechat;
    } else if (type == thirdLoginTypeQQ){
        mainType = SSDKPlatformTypeQQ;
    } else if (type == thirdLoginTypeWeibo){
        mainType = SSDKPlatformTypeSinaWeibo;
    } else if (type == thirdLoginTypeWechatFirend){
        mainType = SSDKPlatformSubTypeWechatTimeline;
    } else {
        mainType = SSDKPlatformTypeWechat;
    }
    
    [ShareSDK share:mainType parameters:params onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        if (state == SSDKResponseStateSuccess){
            [StatusBarManager statusBarHidenWithText:@"分享成功"];
        } else if (state == SSDKResponseStateFail){
            [StatusBarManager statusBarHidenWithText:@"分享失败"];
        }
    }];
}

#pragma mark - 分享成功提交后台
+(void)shareSuccessBack:(shareType)type block:(void(^)())block{
    [[NetworkAdapter sharedAdapter] taskShareManager:type block:block];
}

@end
