//
//  QAVExtend
//  QAVSDK
//
//  Created by zhuojuntian on 16/1/20.
//  Copyright (c) 2016年 zhuojuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAVExtend.h"

/**
 @brief
 */
@interface QAVRoomIntServerInfo: NSObject{
}

@property(assign, nonatomic) UInt32 ip;    ///<
@property(assign,nonatomic) UInt16 port;    ///<
@property(assign, nonatomic) bool tcp;  ///<

@end


/**
 @brief
 */
@interface QAVContextExtendForEdu : QAVContextExtend{
    
}

+(QAVContextExtendForEdu*)CreateQAVContextExtendForEdu;
/**
 @brief
 */
-(NSString*)getLogPath;
/**
 @brief
 */
-(int)getLastEnterServerErrorCode;
/**
 @brief
 */
-(NSString*)getLastEnterServerRetMessage;
/**
 @brief
 */
-(void)setBussType:(uint32_t)type;
/**
 @brief
 */
-(void)setAuthType:(uint32_t)type;
/**
 @brief
 */
-(void)setExtraData:(NSData*)buff;

@end


/**
 @brief
 */
@protocol QAVRoomExtendEduDelegate <NSObject>

@required

-(void)onRoomEmbedS2CNotify:(uint16_t)cmd buff:(NSData*)data buffXml:(NSData*)data;

-(void)onRoomEmbedC2SCallback:(QAVResult)result cmd:(uint16_t)subcmd buff:(NSData*)data;

@end


typedef NS_ENUM(NSInteger, QAVEmbedRecode){
    QAV_Embed_OK = 0,       /// 成功
    QAV_Embed_Failed = 1,   /// 失败
    QAV_Embed_Timeout = 2,  /// 超时
};

typedef NS_ENUM(NSInteger, QAVEmbedSubCmd){
    
    QAV_Embed_SubCmd_HotKeySpeak = 1,            /// 按键说话子命令
    QAV_Embed_SubCmd_PlayMediaNotifyAll = 2,     /// 播放影片通知命令
    QAV_Embed_SubCmd_BanOpenVideo = 3,           /// 仅管理员可以上台
    QAV_Embed_SubCmd_BanSpeak = 4,               /// 仅台上的人和管理员可以说话
    QAV_Embed_SubCmd_StopMediaNotifyAll = 5,     /// 停止播片通知命令
    QAV_Embed_SubCmd_ModeChange = 6,             /// 模式更改：1自由模式，2麦序模式 4主席模式(讲师模式)
    QAV_Embed_SubCmd_EduClassState = 7,          /// 上课状态：1上课 2下课
    QAV_Embed_SubCmd_GwinTabSwitch = 8,          /// 教育模式讲师切换Tab通知 1 PPT , 2 屏幕分享 , 3 播放影
    
    QAV_Embed_SubCmd_PPTState = 9,               /// PPT状态： 1开始上传 ，2上传完成
    QAV_Embed_SubCmd_ApplyBussiness = 10,        /// 举手 [已废弃，功能移接到业务后台]
    QAV_Embed_SubCmd_PIPState = 11,              /// PIP 画面切换。
    
    QAV_Embed_SubCmd_TeacherQuality = 13,        /// 老师选择编码质量.
};


/**
 @brief
 */
@interface QAVMultiRoomExtendForEdu : QAVMultiRoomExtend{
    
}

+(QAVMultiRoomExtendForEdu*)CreateQAVMultiRoomExtendForEdu;
/**
 @brief
 */
-(NSString*)getQualityJsonString;

/**
 @brief
 */
-(NSArray*)getIntServerInfoList;

/**
 @brief
 */
-(void)setDelegate:(id<QAVRoomExtendEduDelegate>)dlg;

/**
 @brief
 */
-(void)sendEmbedChn:(uint32_t)cmd buff:(NSData*)buff;

/**
 @brief
 */
-(void)setMicMode:(unsigned long long)uin mode:(uint8_t)mode;

/**
 @brief
 */
-(void)setHotKeySpeakType:(unsigned long long)uin type:(uint8_t)speekType res:(uint32_t)reserve;

/**
 @brief
 */
-(void)setBanSpeak:(unsigned long long)uin op:(uint8_t)option res:(uint32_t)reserve;

/**
 @brief
 */
-(void)setBanOpenvideo:(unsigned long long)uin op:(uint8_t)option res:(uint32_t)reserve;

/**
 @brief
 */
-(void)playMediaNotify:(unsigned long long)uin ocstring:(NSString*)fileName;

/**
 @brief
 */
-(void)synPPTUpload:(unsigned long long)uin res:(uint32_t)reserve;

/**
 @brief
 */
-(void)synUploadComplete:(unsigned long long)uin ocstring:(NSString*)key res:(uint32_t)reserve;

/**
 @brief
 */
-(void)synUploadError:(unsigned long long)uin ocstring:(NSString*)key res:(uint32_t)reserve;

/**
 @brief
 */
-(void)synStopPPTShare:(unsigned long long)uin res:(uint32_t)reserve;

/**
 @brief
 */
-(void)eduModeTabSwitch:(unsigned long long)uin type:(uint8_t)type res:(uint32_t)reserve;

/**
 @brief
 */
-(void)changeEduClassState:(unsigned long long)uin state:(uint8_t)classState res:(uint32_t)reserve;

/**
 @brief
 */
-(void)sendPIPState:(unsigned long long)uin bool:(bool)isStart type:(uint8_t)type;

/**
 @brief
 */
-(void)setTeacherQuality:(unsigned long long)uin type:(uint32_t)srcType type:(uint8_t)chnType ind:(uint16_t)index;

/**
 @brief
 */
-(void)disableUDTV3;



@end

/**
 @brief
 */
@interface QAVEndpointExtendForEdu : QAVEndpointExtend{
    
}

+(QAVEndpointExtendForEdu*)CreateQAVEndpointExtendForEdu;


-(BOOL)hasMeidaVideo;


@end
