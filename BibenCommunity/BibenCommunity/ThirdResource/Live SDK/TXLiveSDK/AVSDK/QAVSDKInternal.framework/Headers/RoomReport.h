//
//  Header.h
//  QAVSDK
//
//  Created by  sx on 16/10/11.
//  Copyright © 2016年 xianhuan. All rights reserved.
//

#ifndef ROOM_REPORT_H
#define ROOM_REPORT_H

/// 音频帧网络信息类型
@interface QAVNetworkInfo : NSObject  {
    
}
@property(assign, nonatomic) UInt32 ip;  ///< ip
@property(assign, nonatomic) UInt32 port;  ///端口
@end

/// 音频帧进房时间耗时细节
@interface QAVTimeConsumeInfo : NSObject  {
    
};
@property(assign, nonatomic) UInt64 enterroom_start;
@property(assign, nonatomic) UInt64 enterroom_request_auth_key_and_int_svr_conn_info_start;
@property(assign, nonatomic) UInt64 enterroom_net_channel_send_app_cmd_start;
@property(assign, nonatomic) UInt64 enterroom_net_channel_send_app_cmd_end;
@property(assign, nonatomic) UInt64 enterroom_request_auth_key_and_int_svr_conn_info_end;
@property(assign, nonatomic) UInt64 enterroom_init_avsdk_core_start;
@property(assign, nonatomic) UInt64 enterroom_init_avsdk_core_end;
@property(assign, nonatomic) UInt64 enterroom_init_audio_device_start;
@property(assign, nonatomic) UInt64 enterroom_init_audio_device_end;
@property(assign, nonatomic) UInt64 enterroom_init_video_device_start;
@property(assign, nonatomic) UInt64 enterroom_init_video_device_end;
@property(assign, nonatomic) UInt64 enterroom_request_enter_room_start;
@property(assign, nonatomic) UInt64 enterroom_connect_start;
@property(assign, nonatomic) UInt64 enterroom_connect_end;
@property(assign, nonatomic) UInt64 enterroom_send_request_enter_room_cmd_start;
@property(assign, nonatomic) UInt64 enterroom_send_request_enter_room_cmd_end;
@property(assign, nonatomic) UInt64 enterroom_request_enter_room_end;
@property(assign, nonatomic) UInt64 enterroom_end;

//进房细分耗时
@property(assign, nonatomic) int enterroom_total;
@property(assign, nonatomic) int enterroom_request_auth_key_and_int_svr_conn_info;  //请求授权key和接口机IP
@property(assign, nonatomic) int enterroom_net_channel_send_app_cmd;                //请求授权key和接口机IP中其中一个步骤--向imsdk发信令
@property(assign, nonatomic) int enterroom_init_avsdk_core;                         //初始化avsdk中的核心部分
@property(assign, nonatomic) int enterroom_init_audio_device;                       //初始化音频设备
@property(assign, nonatomic) int enterroom_init_video_device;                       //初始化视频设备
@property(assign, nonatomic) int enterroom_request_enter_room;                      //请求加入房间
@property(assign, nonatomic) int enterroom_connect;                                 //请求加入房间步骤一--连接IP
@property(assign, nonatomic) int enterroom_send_request_enter_room_cmd;             //请求加入房间步骤二--发送信令

@property(assign, nonatomic) UInt64 requestview_start;
@property(assign, nonatomic) UInt64 requestview_send_request_cmd_start;
@property(assign, nonatomic) UInt64 requestview_send_request_cmd_end;
@property(assign, nonatomic) UInt64 requestview_end;

@property(assign, nonatomic) UInt64 recvdata_start;
@property(assign, nonatomic) UInt64 recvdata_recv_first_udt_pkg;
@property(assign, nonatomic) UInt64 recvdata_send_first_pkg_to_dec;
@property(assign, nonatomic) UInt64 recvdata_decode_first_frame_end;
@property(assign, nonatomic) UInt64 recvdata_discard_first_frame;
@property(assign, nonatomic) UInt64 recvdata_end;

//获取首帧细分耗时
@property(assign, nonatomic) int recv_rcvf_total;                       //recv_first_pkg-->on_preview
@property(assign, nonatomic) int recv_rcvf_recv_first_udt_pkg;          //recv_first_pkg-->recv_first_udt_pkg
@property(assign, nonatomic) int recv_rcvf_send_first_pkg_to_dec;       //recv_first_udt_pkg-->send_first_pkg_to_dec OR recv_first_pkg-->send_first_pkg_to_dec
@property(assign, nonatomic) int recv_rcvf_decode_first_frame;          //send_first_pkg_to_dec-->decode_first_frame_end
@property(assign, nonatomic) int recv_rcvf_discard_first_frame;         //decode_first_frame_end-->discard_first_frame
@property(assign, nonatomic) int recv_rcvf_send_first_frame_to_render;  //decode_first_frame_end-->end
@property(assign, nonatomic) BOOL recv_rcvf_is_first_iframe;            //wether first frame is iframe
//获取转tinyID细分耗时
@property(assign, nonatomic) int arc_vid_total;
@property(assign, nonatomic) int arc_vid_tinyid_to_id;                  //调用tinyID转换

@end

#endif /* Header_h */
