//
//  InternalEnterParam.h
//  QAVSDK
//
//  Created by  sx on 16/11/28.
//  Copyright © 2016年 xianhuan. All rights reserved.
//

#ifndef InternalEnterParam_h
#define InternalEnterParam_h

#import "QAVSDK/QAVCommon.h"

@interface QAVInternalEnterParam : QAVMultiParam {
    
}

@property (assign, nonatomic) int bussType;
@property (assign, nonatomic) int relationType;
@property (copy, nonatomic) NSString *extraData;
@property (assign, nonatomic) int backgroundMusicMode;
@property (assign, nonatomic) int videoTransformType;//进房自动申请视频对应格式(H264/H265)
/*!
 @abstract      免流签名，全民K歌/QQ音乐专用
 */
@property (copy, nonatomic) NSData *freeFlowSignature;

-(id)initWithParam:(QAVMultiParam*)param;
@end


@interface QAVChangeRoomInfoInternal : QAVChangeRoomInfo {

}
@property (copy, nonatomic) NSData *freeFlowSignature;

@end

#endif /* InternalEnterParam_h */
