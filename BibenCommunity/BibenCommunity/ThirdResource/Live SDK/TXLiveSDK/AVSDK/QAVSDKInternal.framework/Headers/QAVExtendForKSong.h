//
//  QAVExtendForKSong.h
//  QAVSDK
//
//  Created by 祝熙怡 on 2017/8/28.
//  Copyright © 2017年 xianhuan. All rights reserved.
//

#ifndef QAVExtendForKSong_h
#define QAVExtendForKSong_h

#import "QAVSDK/QAVRoomMulti.h"
#import "QAVSDK/QAVAudioCtrl.h"
#import "QAVSDK/QAVVideoCtrl.h"
#import "QAVSDK/QAVContext.h"
#import "QAVCollectorCtrl.h"



@interface QAVAudioCtrl (QAVAudioCtrlInternal)

/*!
 @abstract      获取音频引擎的参数,目前为了获取业务[K歌ios耳返]延迟大小增加的参数
 @param         strParamName        参数名称。
 @return        返回值的结果，用json封装，如果结果为nil表示获取失败。
 */
- (NSString*)getAudioEngineParam:(NSString*)strParamName;

@end



/*!
 @discussion    主播上行码流转码配置
 */
@interface QAVVideoTransDetail : NSObject
/*!
 @abstract      视频类型，2个字节：1-n：转码后的码流；（0用于表示原始码流，这里值不能为零）
 */
@property (assign, nonatomic) int transformType;

/*!
 @abstract      编解码器类型，1个字节，1：H264；2：H265
 */
@property (assign, nonatomic) int codecType;

/*!
 @abstract      视频分辨率高，2个字节
 */
@property (assign, nonatomic) int height;

/*!
 @abstract      视频分辨率宽，2个字节
 */
@property (assign, nonatomic) int width;

/*!
 @abstract      视频码率，2个字节
 */
@property (assign, nonatomic) int bitrate;

/*!
 @abstract      视频帧率，1个字节
 */
@property (assign, nonatomic) int fps;
@end


@interface QAVVideoCtrl (QAVVideoCtrlInternal)

/*!
 @abstract      设置视频转码的配置参数表
 @param         transformDetails    配置参数表,object为QAVVideoTransDetails。
 */
- (QAVResult)setVideoTranformDetail:(NSMutableArray*)transformDetails;

@end

@interface QAVContext (QAVContextInternal)

/*!
 @abstract      上报信息收集控制器
 */
@property (readonly, nonatomic) QAVCollectorCtrl *collectorCtrl;

@end

@interface QAVRoomMulti (QAVRoomMultiInternal)


- (void)requestViewList:(NSArray*)identifierList srcTypeList:(NSArray*)srcTypeList codecTypeList:(NSArray *)codecTypeList ret:(RequestViewListBlock)block;
@end

#endif /* QAVExtendForKSong_h */
