//
//  ILiveOperationQueueManager.h
//  ILiveSDK
//
//  Created by ericxwli on 2018/8/1.
//  Copyright © 2018年 AlexiChen. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ILiveOperationQueueBlock)();

@interface ILiveOperationQueueManager : NSObject
@property(nonatomic, strong) NSMutableArray *queue;
+ (instancetype)getInstance;
- (void)addTask:(ILiveOperationQueueBlock)task;
- (void)taskBegin;
- (void)currentTaskFinished;
@end
