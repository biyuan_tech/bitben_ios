//
//  QAVVideoEffect.h
//  QAVVideoEffect
//
//  Created by roclan on 2016/12/27.
//  Copyright © 2016年 roclan. All rights reserved.
//
#ifndef QAVEFFECT_QAVVideoEffect_h
#define QAVEFFECT_QAVVideoEffect_h
#import "QAVEffectDefine.h"

@interface QAVVideoEffect : NSObject

///---------------------------------------------------------------------------------------
/// for应用层调用
///---------------------------------------------------------------------------------------
/*!
 @abstract      创建QAVVideoEffect对象。
 
 @return        成功则返回QAVVideoEffect的实例指针；否则返回nil。
 */
+ (QAVVideoEffect *)shareContext;

/*!
 @abstract      设置滤镜
 @param         path           当前设置滤镜的本地路径
 @discussion    所设置滤镜的资源文件路径。路径内包含滤镜的配置文件及资源文件。
 */
- (QAVEFFResult)setFilter:(NSString*)path;

/*!
 @abstract      设置挂件
 @param         path           当前设置挂件的本地路径
 @discussion    所设置挂件的资源文件路径。路径内包含挂件的配置文件及资源文件。。
 */
- (QAVEFFResult)setPendant:(NSString*)path;

/*!
 @abstract      销毁QAVVideoEffect对象。
 */
- (void)destroy;
///---------------------------------------------------------------------------------------
/// end
///---------------------------------------------------------------------------------------


///---------------------------------------------------------------------------------------
/// for OPENSDK内部调用。外部调用会引发错误。
///---------------------------------------------------------------------------------------
/*!
 @abstract      开始工作
 @discussion    需要在开启摄像头之后之前调用。
 */
- (QAVEFFResult)start;

/*!
 @abstract      停止工作
 @discussion    需要在每次关闭摄像头之后调用。
 */
- (QAVEFFResult)stop;

/*!
 @abstract      输入视频帧
 @discussion    向SDK内输入视频帧，输入格式为NV12，输出格式为I420。
 */
- (QAVEFFResult)onInputFrame:(unsigned char *)pBuf toBuf:(unsigned char *)dstBuf BufferSize:(size_t)nBufferSize Width:(size_t)width HeightY:(size_t)heightOfYPlane colorFormat:(QAVEFFColorFormat)colorFormat;
///---------------------------------------------------------------------------------------
/// end
///---------------------------------------------------------------------------------------


@end
#endif
