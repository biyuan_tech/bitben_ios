precision highp float;
varying vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
uniform float percent1;
uniform float percent2;
uniform float percent3;
uniform float percent4;
uniform float percent5;
uniform float percent6;
uniform float percent7;

float percent_value(float min_percent, float percent)
{
    float result_percent = 0.0;
    if(percent > 0.0)
        result_percent = min(min_percent, percent);
    else
        result_percent = max(min_percent - 1.0, percent);
    
    return result_percent;
}

void main()
{
    
    vec3 rgb = texture2D(inputImageTexture, textureCoordinate).rgb;
    float r = rgb.r;
    float g = rgb.g;
    float b = rgb.b;
    
    float max_c = max(r, max(g, b));
    float min_c = min(r, min(g, b));
    float median = r + g + b - max_c - min_c;
    
    float amount = max_c - median;
    float amount2 = median - min_c;
    
    float min_percent_r = 1.0 - r;
    float min_percent_g = 1.0 - g;
    float min_percent_b = 1.0 - b;
    
    float p;
    float gray = dot(rgb, vec3(0.299, 0.587, 0.114));
    
    float tmp_r = r;
    float tmp_g = g;
    float tmp_b = b;
    if(tmp_r == max_c) //red
    {
        float tmp_percent7 = percent7 * (1.0 - gray);
        
        p = percent_value(min_percent_r, percent1 + tmp_percent7);
        r = r + amount * p;
        
        p = percent_value(min_percent_g, percent4 + tmp_percent7);
        g = g + amount * p;
        
        p = percent_value(min_percent_b, percent6 + tmp_percent7);
        b = b + amount * p;
    }
    
    if(tmp_b == max_c) //blue
    {
        p = percent_value(min_percent_r, percent3);
        r = r + amount * p;
    }
    
    if(tmp_b == min_c) //yellow
    {
        p = percent_value(min_percent_b, percent2);
        b = b + amount2 * p;
        
        p = percent_value(min_percent_g, percent5);
        g = g + amount2 * p;
    }
    
    gl_FragColor = vec4(r, g, b, 1.0);
}
