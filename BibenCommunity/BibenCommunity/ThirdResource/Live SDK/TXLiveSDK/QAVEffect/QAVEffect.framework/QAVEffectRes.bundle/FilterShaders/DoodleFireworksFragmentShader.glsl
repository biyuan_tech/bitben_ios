precision highp float;
varying float blendAlpha;
uniform int blendMode;
uniform int drawType;

void main(void)
{
    if (drawType == 1) {
        if (blendAlpha <= 0.8) {
            gl_FragColor = vec4(1, 1, 1, 0.6);
        } else {
            gl_FragColor = vec4(1.0, 0.65, 0, 0.5);
        }
    } else {
        gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
}
