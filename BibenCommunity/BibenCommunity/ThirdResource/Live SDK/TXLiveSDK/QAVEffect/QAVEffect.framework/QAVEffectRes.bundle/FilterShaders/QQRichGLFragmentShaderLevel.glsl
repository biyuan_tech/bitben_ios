//
//  QQRichGLFragmentShaderLevel.glsl
//  QQMSFContact
//
//  Created by hodxiang on 27/8/24.
//
//

#define GammaCorrection(color, gamma)                                           pow(color, 1.0 / gamma)
#define LevelsControlInputRange(color, minInput, maxInput)                      min(max(color - minInput, vec3(0.0)) / (maxInput - minInput), vec3(1.0))
#define LevelsControlInput(color, minInput, gamma, maxInput)                    GammaCorrection(LevelsControlInputRange(color, minInput, maxInput), gamma)
#define LevelsControlOutputRange(color, minOutput, maxOutput)                   mix(minOutput, maxOutput, color)
#define LevelsControl(color, minInput, gamma, maxInput, minOutput, maxOutput) 	LevelsControlOutputRange(LevelsControlInput(color, minInput, gamma, maxInput), minOutput, maxOutput)

varying highp vec2   textureCoordinate;

uniform sampler2D    inputImageTexture;
uniform mediump vec3 levelMinimum;
uniform mediump vec3 levelMiddle;
uniform mediump vec3 levelMaximum;
uniform mediump vec3 minOutput;
uniform mediump vec3 maxOutput;

void main()
{
    mediump vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
    gl_FragColor = vec4(LevelsControl(textureColor.rgb, levelMinimum, levelMiddle, levelMaximum, minOutput, maxOutput), textureColor.a);
}                                           
