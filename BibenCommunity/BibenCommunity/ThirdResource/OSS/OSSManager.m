//
//  OSSManager.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/4.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "OSSManager.h"
#import "NetworkAdapter.h"
#import <QiniuSDK.h>

typedef  NS_ENUM(NSInteger, OSS_UPLOAD_TYPE){
    OSS_UPLOAD_TYPE_IMG,                // 上传图片
    OSS_UPLOAD_TYPE_AUDIO               // 上传音频文件
};

@interface OSSManager()
@property (nonatomic,strong)NSArray *imgArr;
@end

@implementation OSSManager

+ (OSSManager *)sharedUploadManager{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}
//    if ([fileModel.objcName hasPrefix:@"avatar"]) { // 头像上传至https

#pragma mark - 设置上传空间
- (NSString *)getUploadBucket:(OSSFileModel *)fileModel uploadType:(OSS_UPLOAD_TYPE)type{
    if (type == OSS_UPLOAD_TYPE_AUDIO) {
        return @"audio";
    }
    if ([fileModel.objcName hasPrefix:@"avatar"]) { // 头像上传至https
        return @"img-ssl";
    }
    return @"image";
}

#pragma mark - 上传图片方法
-(void)uploadImageManagerWithImageList:(NSArray<OSSFileModel> *)imgList withUrlBlock:(void(^)(NSArray *imgUrlArr))block{
    [self uploadManagerWithList:imgList uploadType:OSS_UPLOAD_TYPE_IMG withUrlBlock:block];
}

- (void)uploadAudioManagerWithModel:(OSSFileModel *)model suc:(void(^)(NSString *url))suc {
    [self uploadManagerWithList:[@[model] copy] uploadType:OSS_UPLOAD_TYPE_AUDIO withUrlBlock:^(NSArray *urlArr) {
        if (suc) {
            suc(urlArr[0]);
        }
    }];
}

- (void)uploadManagerWithList:(NSArray<OSSFileModel> *)list uploadType:(OSS_UPLOAD_TYPE)type withUrlBlock:(void(^)(NSArray *urlArr))block{
    __weak typeof(self)weakSelf = self;
    if (!list.count) return;
    self.imgArr = list;
    NSString *bucket = [self getUploadBucket:list[0] uploadType:type];
    [self sendRequestToGetUploadTokenManagerWithBucket:bucket uploadType:type cb:^(NSString *token) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (token.length){
            [strongSelf uploadDataAllManager:list token:token uploadType:type block:^(NSArray *urlArr) {
                if (urlArr){
                    if (block){
                        block(urlArr);
                    }
                }
            }];
        }
    }];
}

#pragma mark - 1. 获取当前的上传权限

- (void)sendRequestToGetUploadTokenManagerWithBucket:(NSString *)bucket uploadType:(OSS_UPLOAD_TYPE)type cb:(void(^)(NSString *token))cb{
    __weak typeof(self)weakSelf = self;
    if (self.imgArr.count != 1){
        NSString *title = type == OSS_UPLOAD_TYPE_IMG ? @"图片" : @"音频";
        NSString *text = [NSString stringWithFormat:@"正在上传%@",title];
        [StatusBarManager statusBarShowWithText:text];
    }
    
    NSDictionary *param = @{@"bucket":bucket};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"get_upload_token" requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            NSString *token = [responseObject objectForKey:@"token"];
            if (token.length && cb){
                cb(token);
            }
        } else {
            if (cb){
                cb(@"");
            }
        }
    }];
}

//-(void)uploadFileManager:(OSSFileModel *)fileModel token:(NSString *)token blockArr:(void(^)(NSString *imgUrl))block{
//
//    QNUploadManager *upManager = [[QNUploadManager alloc] init];
//
//    // 1. 获取当前的图片对象
//    UIImage *image = fileModel.objcImage;
//    NSData *objcData = UIImageJPEGRepresentation(image,.6f);
//
//    NSString *ossMainObjcName = fileModel.objcName;
//    if (!ossMainObjcName.length){
//        NSInteger width = image.size.width;
//        NSInteger height = image.size.height;
//
//        ossMainObjcName = [NSString stringWithFormat:@"%@-%@_%li!!%li!!%li",@"other",[NSDate getCurrentTimeWithFileName],(long)(arc4random() % 100000000),(long)width,(long)height];
//    }
//
//    // 2. 上传图片信息
//    [upManager putData:objcData key:ossMainObjcName token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
//        if (key.length && block){
//            block(key);
//        }
//    } option:NULL];
//}

-(void)uploadFileManager:(OSSFileModel *)fileModel token:(NSString *)token uploadType:(OSS_UPLOAD_TYPE)type blockArr:(void(^)(NSString *url))block{
    QNUploadManager *upManager = [[QNUploadManager alloc] init];
    
    NSData *objcData;
    NSString *ossMainObjcName = fileModel.objcName;
    if (type == OSS_UPLOAD_TYPE_IMG) {
        // 1. 获取当前的图片对象
        UIImage *image = fileModel.objcImage;
        objcData = UIImageJPEGRepresentation(image,.6f);
        if (!ossMainObjcName.length){
            NSInteger width = image.size.width;
            NSInteger height = image.size.height;
            
            ossMainObjcName = [NSString stringWithFormat:@"%@-%@_%li!!%li!!%li",@"other",[NSDate getCurrentTimeWithFileName],(long)(arc4random() % 100000000),(long)width,(long)height];
        }
    }
    else if (type == OSS_UPLOAD_TYPE_AUDIO){
//        objcData = [NSData dataWithContentsOfURL:fileModel.objcPath];
        objcData = [NSData dataWithContentsOfFile:fileModel.objcPath];
        if (!ossMainObjcName.length){
            ossMainObjcName = [NSString stringWithFormat:@"%@-%@_%li",@"audio",[NSDate getCurrentTimeWithFileName],(long)(arc4random() % 100000000)];
        }
    }
    
    // 2. 上传图片信息
    [upManager putData:objcData key:ossMainObjcName token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        if (key.length && block){
            block(key);
        }
    } option:NULL];
}

#pragma mark - 异步上传多张图片的方法
//-(void)uploadImgAllManager:(NSArray *)imgList token:(NSString *)token block:(void(^)(NSArray *imgUrlArr))block{
//    NSMutableArray *imgUrlMutableArr = [NSMutableArray array];
//    __block NSInteger count = 0;
//    for (int i = 0 ; i < imgList.count;i++){
//        OSSFileModel *fileModel = [imgList objectAtIndex:i];
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
//            [self uploadFileManager:fileModel token:token blockArr:^(NSString *imgUrl) {
//                if (imgUrl.length){
//                    dispatch_async(dispatch_get_main_queue(), ^{            // 回到主线程
//                        count ++;
//
//                        if (self.imgArr.count != 1){
//                            [StatusBarManager statusBarShowWithText:[NSString stringWithFormat:@"正在上传第%li张图片",(long)count]];
//                        }
//                        [imgUrlMutableArr addObject:imgUrl];
//                        if (count == imgList.count){
//                            if (block){
//
//                                if (self.imgArr.count != 1){
//                                    [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"上传成功"]];
//                                }
//                                block(imgUrlMutableArr);
//                            }
//                        }
//                    });
//                }
//            }];
//        });
//    }
//}

-(void)uploadDataAllManager:(NSArray *)paths token:(NSString *)token uploadType:(OSS_UPLOAD_TYPE)type block:(void(^)(NSArray *urlArr))block{
    NSMutableArray *urlMutableArr = [NSMutableArray array];
    __block NSInteger count = 0;
    for (int i = 0 ; i < paths.count;i++){
        OSSFileModel *fileModel = [paths objectAtIndex:i];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [self uploadFileManager:fileModel token:token uploadType:type blockArr:^(NSString *url) {
                if (url.length){
                    dispatch_async(dispatch_get_main_queue(), ^{            // 回到主线程
                        count ++;
                        
                        if (self.imgArr.count != 1){
                            NSString *title = type == OSS_UPLOAD_TYPE_IMG ? @"张图片" : @"个音频";
                            [StatusBarManager statusBarShowWithText:[NSString stringWithFormat:@"正在上传第%li%@",(long)count,title]];
                        }
                        [urlMutableArr addObject:url];
                        if (count == paths.count){
                            if (block){
                                if (self.imgArr.count != 1){
                                    [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"上传成功"]];
                                }
                                block(urlMutableArr);
                            }
                        }
                    });
                }
            }];
        });
    }
}


-(void)sendRequestToGetUploadTokenManagerWithBlock:(void(^)(NSString *token))block{
    
}
@end
