//
//  OSSManager.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/4.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSSFileModel.h"

@interface OSSManager : NSObject

+ (OSSManager *)sharedUploadManager;

#pragma mark - 1. 获取当前的上传权限
-(void)sendRequestToGetUploadTokenManagerWithBlock:(void(^)(NSString *token))block;

#pragma mark - 2. 上传图片
-(void)uploadImageManagerWithImageList:(NSArray<OSSFileModel> *)imgList withUrlBlock:(void(^)(NSArray *imgUrlArr))block;

/** 上传音频文件 */
- (void)uploadAudioManagerWithModel:(OSSFileModel *)model suc:(void(^)(NSString *url))suc ;

@end
