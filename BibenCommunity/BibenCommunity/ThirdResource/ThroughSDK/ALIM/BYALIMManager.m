//
//  BYALIMManager.m
//  BY
//
//  Created by 黄亮 on 2018/8/29.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYALIMManager.h"
#import "BYIMCommon.h"
#import <WXOpenIMSDKFMWK/YWAPI.h>

@interface BYALIMManager()<NSCopying,YWMessageLifeDelegate>

@end

@implementation BYALIMManager

static BYALIMManager *_manager;

+ (instancetype)shareInstance{
    return [[self alloc] init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_manager) {
            _manager = [super allocWithZone:zone];
        }
    });
    return _manager;
}

- (id)copyWithZone:(NSZone *)zone{
    return _manager;
}

- (void)initSDK{
    [self exampleSetCerName];
    if ([self exampleInit]) {
        
        // 在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
        [self exampleHandleAPNSPush];
        
        // 监听消息生命周期回调
        [self exampleListenMyMessageLife];
    }
    else
    {
        showToastView(@"ALIMSDK 初始化失败,请稍后重试", kCommonWindow);
    }
}

// sdk初始化
- (BOOL)exampleInit{
    NSError *error = nil;
    // 开启日志
    [[YWAPI sharedInstance] setLogEnabled:YES];
    // 设置环境
    [[YWAPI sharedInstance] setEnvironment:YWEnvironmentRelease];
    [[YWAPI sharedInstance] syncInitWithOwnAppKey:@"25047349" getError:&error];
    
    self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
    if (error.code != 0 && error.code != YWSdkInitErrorCodeAlreadyInited) {
        // 初始化失败
        NSLog(@"ALIM初始化失败");
        return NO;
    }
    else
    {
        if (error.code == 0) {
            /// 首次初始化成功
            /// 获取一个IMKit并持有
            self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
            [[self.ywIMKit.IMCore getContactService] setEnableContactOnlineStatus:YES];
        } else {
            /// 已经初始化
        }
        NSLog(@"ALIM初始化成功");
        return YES;
    }
    return NO;
}

// 设置证书名
- (void)exampleSetCerName{
    [[[YWAPI sharedInstance] getGlobalPushService] setXPushCertName:@"sendAPNS"];
}

- (void)exampleHandleAPNSPush
{
    __weak typeof(self) weakSelf = self;
    
    [[[YWAPI sharedInstance] getGlobalPushService] addHandlePushBlockV4:^(NSDictionary *aResult, BOOL *aShouldStop) {
        BOOL isLaunching = [aResult[YWPushHandleResultKeyIsLaunching] boolValue];
        UIApplicationState state = [aResult[YWPushHandleResultKeyApplicationState] integerValue];
        NSString *conversationId = aResult[YWPushHandleResultKeyConversationId];
        Class conversationClass = aResult[YWPushHandleResultKeyConversationClass];
        
        
        if (conversationId.length <= 0) {
            return;
        }
        
        if (conversationClass == NULL) {
            return;
        }
        
        if (isLaunching) {
            /// 用户划开Push导致app启动
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if ([self exampleIsPreLogined]) {
                    /// 说明已经预登录成功
                    [weakSelf exampleHandleAPNSPushControllerManagerWithClass:conversationClass aConversationId:conversationId];
                }
            });
            
        } else {
            /// app已经启动时处理Push
            
            if (state != UIApplicationStateActive) {
                if ([self exampleIsPreLogined]) {
                    /// 说明已经预登录成功
                    [weakSelf exampleHandleAPNSPushControllerManagerWithClass:conversationClass aConversationId:conversationId];
                }
            } else {
                /// 应用处于前台
                /// 建议不做处理，等待IM连接建立后，收取离线消息。
            }
        }
    } forKey:self.description ofPriority:YWBlockPriorityDeveloper];
}

-(void)exampleHandleAPNSPushControllerManagerWithClass:(__unsafe_unretained Class)aConversationClass aConversationId:(NSString *)aConversationId{
    if ([self exampleIsPreLogined]){                                        // 预登录成功 ,创建会话
        YWConversation *conversation = nil;
        if (aConversationClass == [YWP2PConversation class]){               // 单聊对话
            conversation = [YWP2PConversation fetchConversationByConversationId:aConversationId creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
        } else if (aConversationClass == [YWTribeConversation class]){      // 群聊会话
            conversation = [YWTribeConversation fetchConversationByConversationId:aConversationId creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
        }
        if (conversation){          // 如果有会话，就打开某个会话
            //#warning 打开某个会话
        }
    }
    
}


// 监听自己发送的消息的生命周期
- (void)exampleListenMyMessageLife
{
    [[self.ywIMKit.IMCore getConversationService] addMessageLifeDelegate:self forPriority:YWBlockPriorityDeveloper];
}

// 预登陆判断
- (BOOL)exampleIsPreLogined
{
    /// 这个是Demo中判断是否已经进入IM主页面的方法，你需要修改成你自己的方法
    return [[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[UITabBarController class]];
    
}

#pragma mark - 登录
// 登录
- (void)loginWithUserId:(NSString *)userId password:(NSString *)password suc:(void(^)(void))suc fail:(void(^)(NSError *error))fail{
    
    if (userId.length > 0 && password.length > 0){
        [self examplePreLoginWithLoginId:userId successBlock:suc];                                              // 1. 预登录
        [self exampleLoginWithUserID:userId password:password successBlock:suc failedBlock:fail];               // 2. 真正登录
    } else {
        if (fail){
            fail([NSError errorWithDomain:YWLoginServiceDomain code:YWLoginErrorCodePasswordError userInfo:nil]);
        }
    }
}

-(void)examplePreLoginWithLoginId:(NSString *)loginId successBlock:(void (^)(void))aPreligindBlock{
    if ([[self.ywIMKit.IMCore getLoginService] preLoginWithPerson:[[YWPerson alloc] initWithPersonId:loginId]]){
        if (aPreligindBlock){
            aPreligindBlock();
        }
    }
}

#pragma mark  2.8.2 真正登录
- (void)exampleLoginWithUserID:(NSString *)aUserID password:(NSString *)aPassword successBlock:(void(^)(void))aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock {
    __weak typeof(self) weakSelf = self;
    aSuccessBlock = [aSuccessBlock copy];
    aFailedBlock = [aFailedBlock copy];
    
    /// 当IM向服务器发起登录请求之前，会调用这个block，来获取用户名和密码信息。
    [[self.ywIMKit.IMCore getLoginService] setFetchLoginInfoBlock:^(YWFetchLoginInfoCompletionBlock aCompletionBlock) {
        aCompletionBlock(YES, aUserID, aPassword, nil, nil);
    }];
    
    [[self.ywIMKit.IMCore getLoginService] asyncLoginWithCompletionBlock:^(NSError *aError, NSDictionary *aResult) {                /// 发起登录
        if (aError.code == 0 || [[weakSelf.ywIMKit.IMCore getLoginService] isCurrentLogined]) {
#ifdef DEBUG
            showToastView(@"ALIM登录成功", kCommonWindow);
#endif
            if (aSuccessBlock) {
                aSuccessBlock();
            }
        }
        else
        {
#ifdef DEBUG
            showToastView(kError_Room(@"ALIM登录", (long)aError.code, aError.description), kCommonWindow);
#endif
            if (aFailedBlock) {
                aFailedBlock(aError);
            }
        }
    }];
}

#pragma mark - YWMessageLifeDelegate

/// 当你监听了消息生命周期，IMSDK会回调以下两个函数
- (YWMessageLifeContext *)messageLifeWillSend:(YWMessageLifeContext *)aContext
{
    /// 你可以通过返回context，来实现改变消息的能力
    if ([aContext.messageBody isKindOfClass:[YWMessageBodyText class]]) {
        NSString *text = [(YWMessageBodyText *)aContext.messageBody messageText];
        if ([text rangeOfString:@"发轮功事件"].location != NSNotFound) {
            YWMessageBodySystemNotify *bodyNotify = [[YWMessageBodySystemNotify alloc] initWithContent:@"消息包含违禁词语"];
            [aContext setMessageBody:bodyNotify];
            
            NSDictionary *params = @{kYWMsgCtrlKeyClientLocal:@{kYWMsgCtrlKeyClientLocalKeyOnlySave:@(YES)}};
            [aContext setControlParameters:params];
            
            return aContext;
        }
    }
    
    return nil;
}

- (void)messageLifeDidSend:(NSString *)aMessageId conversationId:(NSString *)aConversationId result:(NSError *)aResult
{
    /// 你可以在消息发送完成后，做一些事情，例如播放一个提示音等等
}


@end
