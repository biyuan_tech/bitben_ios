//
//  BYIMManager+SendMsg.h
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager.h"

@interface BYIMManager (SendMsg)


/**
 群聊模式设置群聊id

 @param groupId 群聊id
 */
- (void)setConversationGroupId:(NSString *)groupId;

/**
 coc模式(用户对用户)设置回话的用户id

 @param userId 用户id
 */
- (void)setConversationUserId:(NSString *)userId;

/**
 设置消息回调监听
 */
- (void)setMessageListener:(void(^)(NSArray *msgs))msgs;

/**
 移除消息监听
 */
- (void)removeMessageListener;

/**
 发送文本消息

 @param text 消息内容
 @param succ succ description
 @param fail fail description
 */
- (void)sendTextMessage:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail;

/**
 发送图片消息

 @param imagePath 本地iamge路径
 @param succ succ description
 @param fail fail description
 */
- (void)sendImageMessage:(NSString *)imagePath succ:(sucCallback)succ fail:(failCallback)fail;

/** 打赏消息 */
- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name succ:(sucCallback)succ fail:(failCallback)fail;

/** 发送开播通知 */
- (void)sendOpenLiveMessage:(sucCallback)succ fail:(failCallback)fail;

/** 发送上麦请求 */
- (void)sendUpVideoMessage:(sucCallback)succ fail:(failCallback)fail;
/** 发送同意上麦请求 */
- (void)sendAgreeUpVideoMessage:(NSString *)userId stream_id:(NSString *)stream_id succ:(sucCallback)succ fail:(failCallback)fail;
/** 发起下麦请求 */
- (void)sendDownVideoMessage:(NSString *)userId role:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail;

/** 发送结束直播消息 */
- (void)sendStopPushStreamMessage:(sucCallback)succ fail:(failCallback)fail;

- (void)getLocalMessage:(int)count succ:(sucCallback)succ fail:(failCodeCallback)fail;

- (void)getMessage:(int)count succ:(sucMsgsCallback)succ fail:(failCodeCallback)fail;
@end
