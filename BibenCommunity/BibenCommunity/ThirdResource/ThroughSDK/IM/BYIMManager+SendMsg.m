//
//  BYIMManager+SendMsg.m
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager+SendMsg.h"
#import <TILLiveSDK/TILLiveSDK.h>

@interface BYIMManager()<TIMMessageListener,TIMMessageReceiptListener>

// ** 群聊会话
@property (nonatomic ,strong) TIMConversation *groupConversation;

// ** 单聊会话
@property (nonatomic ,strong) TIMConversation *c2cConversation;

// ** 会话类型
@property (nonatomic ,assign) BYIMConversationType conversationType;

@property (nonatomic ,copy) void (^messageHandle)(NSArray *msgs);

@end

static const char groupCon;
static const char c2cCon;
static const char conType;
static const char msgHandle;
@implementation BYIMManager (SendMsg)

#pragma mark - initMethod

- (TIMConversation *)groupConversation{
    TIMConversation *conversation = objc_getAssociatedObject(self, &groupCon);
    return conversation;
}

- (void)setGroupConversation:(TIMConversation *)groupConversation{
    objc_setAssociatedObject(self, &groupCon, groupConversation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.conversationType = BYIM_CVT_TYPE_GROUP;
}

- (TIMConversation *)c2cConversation{
    TIMConversation *conversation = objc_getAssociatedObject(self, &c2cCon);
    return conversation;
}

- (void)setC2cConversation:(TIMConversation *)c2cConversation{
    objc_setAssociatedObject(self, &c2cCon, c2cConversation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.conversationType = BYIM_CVT_TYPE_C2C;
}

- (BYIMConversationType)conversationType{
    return [objc_getAssociatedObject(self, &conType) integerValue];
}

- (void)setConversationType:(BYIMConversationType)conversationType{
    objc_setAssociatedObject(self, &conType, @(conversationType), OBJC_ASSOCIATION_ASSIGN);
}

- (void)setMessageHandle:(void (^)(NSArray *))messageHandle{
    objc_setAssociatedObject(self, &msgHandle, messageHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSArray *))messageHandle{
    return objc_getAssociatedObject(self, &msgHandle);
}

#pragma mark - Listener

// 设置消息监听
- (void)setMessageListener:(void (^)(NSArray *))msgs{
    self.messageHandle = msgs;
    [[TIMManager sharedInstance] setMessageListener:self];
    [[TIMManager sharedInstance] setMessageReceiptListener:self];
}

// 移除消息监听
- (void)removeMessageListener{
    [[TIMManager sharedInstance] removeMessageListener:self];
}

// 接收消息回调
- (void)onNewMessage:(NSArray *)msgs{
    if (self.messageHandle) {
        self.messageHandle(msgs);
    }
    for (TIMMessage *msg in msgs) {
        if ([[msg getElem:0] isMemberOfClass:[TIMGroupSystemElem class]]) break;
        if ([[msg getElem:0] isMemberOfClass:[TIMProfileSystemElem class]]) break;
        if ([[msg getElem:0] isMemberOfClass:[TIMCustomElem class]]) {
            TIMCustomElem *elem = (TIMCustomElem *)[msg getElem:0];
            NSString *receiveStr = [[NSString alloc]initWithData:elem.data encoding:NSUTF8StringEncoding];
            NSData * data = [receiveStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            // 判断当前是否为开播消息
            if ([dic[@"opera_type"] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) {
                break;
            }
        }
        [msg convertToImportedMsg];
//        TIMTextElem *elem = (TIMTextElem *)[msg getElem:0];
//        dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [BYToastView tostViewPresentInView:kCommonWindow title:elem.text duration:1.0 complete:^{
//                dispatch_semaphore_signal(semaphore);
//            }];
//        });
    }
}

- (void) onRecvMessageReceipts:(NSArray*)receipts{
    NSLog(@"%s",__func__);
    NSLog(@"%@",receipts);
}

#pragma mark - customMethod

// 获取消息会话
- (void)setConversationGroupId:(NSString *)groupId{
    if ([[TIMManager sharedInstance] getLoginStatus] == 3) {
        [[BYIMManager sharedInstance] loginILive:^{
            self.groupConversation = [[TIMManager sharedInstance] getConversation:2 receiver:groupId];
        } fail:^{
            
        }];
        return;
    }
    self.groupConversation = [[TIMManager sharedInstance] getConversation:2 receiver:groupId];
}

- (void)setConversationUserId:(NSString *)userId{
    self.c2cConversation = [[TIMManager sharedInstance] getConversation:1 receiver:userId];
}

- (void)sendTextMessage:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail{
//    if (!text.length) return;
//    TIMTextElem *textElem = [[TIMTextElem alloc] init];
//    [textElem setText:text];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:textElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        [msg convertToImportedMsg];
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail(code);
//        showToastView(kError_Room(@"文本消息发送", code, msg), kCommonWindow);
//    }];
    NSString *string = [text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    BYSIMMessage *message = [[BYSIMMessage alloc] init];
    message.msg_type = BY_SIM_MSG_TYPE_TEXT;
    message.msg_sender = [AccountModel sharedAccountModel].account_id;
    message.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    message.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    message.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    message.elem = elem;
    [[BYSIMManager shareManager] sendMessage:message suc:succ fail:fail];
}

- (void)sendImageMessage:(NSString *)imagePath succ:(sucCallback)succ fail:(failCallback)fail{
    
}

- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name succ:(sucCallback)succ fail:(failCallback)fail{
    if (amount <= 0) return;
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
    NSString *text = [NSString stringWithFormat:@"%@打赏了%@ %.2fBP",[AccountModel sharedAccountModel].loginServerModel.user.nickname,receicer_user_Name,amount];
    NSDictionary *dic = @{@"amount":@(amount),
                          @"receicer_user_Name":receicer_user_Name,
                          @"text":text,
                          @"opera_type":@(2)
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_REWARD data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"打赏消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"打赏消息发送失败", kCommonWindow);
    }];
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
//        [msg convertToImportedMsg];
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showToastView(kError_Room(@"打赏消息发送", code, msg), kCommonWindow);
//    }];
}

- (void)sendOpenLiveMessage:(sucCallback)succ fail:(failCallback)fail{
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
//    NSDictionary *dic = @{@"opera_type":@(3)};
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
////        [msg convertToImportedMsg];
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showToastView(kError_Room(@"开播消息发送", code, msg), kCommonWindow);
//    }];
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_RECEIVE_STREAM data:nil suc:^{
        if (succ) succ();
        showDebugToastView(@"开播消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"开播消息发送失败", kCommonWindow);
    }];
}

- (void)sendUpVideoMessage:(sucCallback)succ fail:(failCallback)fail{
    
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
    NSDictionary *dic = @{@"opera_type":@(4)};
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_UP_VIDEO data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"上麦消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"上麦消息发送失败", kCommonWindow);
    }];
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showToastView(kError_Room(@"上麦消息发送", code, msg), kCommonWindow);
//    }];
}

- (void)sendAgreeUpVideoMessage:(NSString *)userId stream_id:(NSString *)stream_id succ:(sucCallback)succ fail:(failCallback)fail{
    NSString *role = [self verifyIsMaxUpVideo];
    if (!role.length) {
        showToastView([NSString stringWithFormat:@"当前最多只可连麦%i位观众",MAX_UPVIDEO_NUM], kCommonWindow);
        return;
    }
    NSDictionary *dic = @{@"opera_type":@(5),
                          @"stream_id":nullToEmpty(stream_id),
                          @"user_id":userId,
                          @"role":role,
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"同意上麦消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"同意上麦消息发送失败", kCommonWindow);
    }];
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        for (BYUpVideoCellModel *model in self.upToVideoMembers) {
//            if ([model.userId isEqualToString:userId]) {
//                model.liveRole = role;
//            }
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showToastView(kError_Room(@"同意上麦消息发送", code, msg), kCommonWindow);
//    }];
}

- (void)sendDownVideoMessage:(NSString *)userId role:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail{
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
    NSDictionary *dic = @{@"opera_type":@(6),
                          @"user_id":userId,
                          @"role":role,
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_DOWN_VIDEO data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"下麦消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"下麦消息发送失败", kCommonWindow);
    }];
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showToastView(kError_Room(@"下麦消息发送", code, msg), kCommonWindow);
//    }];
}

- (void)sendStopPushStreamMessage:(sucCallback)succ fail:(failCallback)fail{
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
        if (succ) succ();
        showDebugToastView(@"结束直播消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"结束直播消息发送失败", kCommonWindow);
    }];
//    TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
//    NSDictionary *dic = @{@"opera_type":@(7)};
//    NSData* data=[NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//    [customElem setData:data];
//    TIMMessage *msg = [[TIMMessage alloc] init];
//    [msg addElem:customElem];
//    [msg setTime:[NSDate date].timeIntervalSince1970];
//    [msg setSender:[AccountModel sharedAccountModel].account_id];
//    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
//    [conversation sendMessage:msg succ:^{
//        // 消息导入本地
//        if (self.messageHandle) {
//            self.messageHandle(@[msg]);
//        }
//        if (succ) succ();
//    } fail:^(int code, NSString *msg) {
//        if (fail) fail();
//        showDebugToastView(kError_Room(@"结束直播消息发送", code, msg), kCommonWindow);
//    }];
}

- (void)getLocalMessage:(int)count succ:(sucCallback)succ fail:(failCodeCallback)fail{
    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
    [conversation getLocalMessage:count last:nil succ:^(NSArray *msgs) {
        NSLog(@"%@",msgs);
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        if (fail) fail(code);
    }];
}

- (void) getMessage:(int)count succ:(sucMsgsCallback)succ fail:(failCodeCallback)fail{
    TIMConversation *conversation = self.conversationType == BYIM_CVT_TYPE_GROUP ? self.groupConversation : self.c2cConversation;
    [conversation getMessage:count last:nil succ:^(NSArray *msgs) {
        if (succ) succ(msgs);
    } fail:^(int code, NSString *msg) {
        
    }];
}

// 校验最大上麦数
- (NSString *)verifyIsMaxUpVideo{
    NSMutableArray *tmpArr = [NSMutableArray array];
    NSMutableArray *guestMembers = [NSMutableArray array];
    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
        if (model.isUpVideo) {
            [tmpArr addObject:model];
            [guestMembers addObject:model.liveRole];
        }
    }
    if (!guestMembers.count) {
        return LIVEGUEST_MEMBERS[0];
    }
    
    if (guestMembers.count >= MAX_UPVIDEO_NUM) {
        return @"";
    }
    
    NSString *role = @"";
    for (NSString *tmpRole in LIVEGUEST_MEMBERS) {
        if ([guestMembers indexOfObject:tmpRole] == NSNotFound) {
            role = tmpRole;
            return role;
            break;
        }
    }
    return role;
}


@end
