//
//  BYIMManager.m
//  BY
//
//  Created by 黄亮 on 2018/8/22.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager.h"
#import "BYToastView.h"
#import "BYRequestManager.h"
#import <sqlite3.h>
#import "BYIMManager+SendMsg.h"
#import "BYLiveRPCDefine.h"
#import <ILiveSDK/ILiveLoginManager.h>

static char const _upToVideoMebers;
@interface BYIMManager()<NSCopying,TIMConnListener,TIMCallback>
{
    sucCallback _callBack;
}
@property (nonatomic ,strong) BYToastView *connectingToast;
@property (nonatomic ,copy) void (^iLiveLoginHandle)(BOOL isSuccess);

@end

@implementation BYIMManager

static BYIMManager *_manager;
+ (BYIMManager *)sharedInstance{
    return [[self alloc] init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_manager) {
            _manager = [super allocWithZone:zone];
        }
    });
    return _manager;
}

- (id)copyWithZone:(NSZone *)zone{
    return _manager;
}

- (void)initSdk{
    TIMManager *manager = [TIMManager sharedInstance];
    //    NSString *logPath = [NSSearchPathForDirectoriesInDomains(NSTemporaryDirectory(), NSLocalDomainMask, YES) lastObject]
#if DEBUG
    [manager setLogLevel:2];
#else
    [manager disableCrashReport];
#endif

    [manager setConnListener:self];
//    [manager initSdk:kTXTIMAppid accountType:kTXTIMAccountType];
    // 获取数据库文件的路径
    sqlite3 *db;
    NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fileName = [doc stringByAppendingPathComponent:@"tim.sqlite"];
    const char *cfileName = fileName.UTF8String;
    // 数据库不存在则创建
    sqlite3_open(cfileName, &db);
    // 设置消息本地存储数据库
    BOOL isReadyMessageDB = [manager setDBPath:fileName];
    if (!isReadyMessageDB)
        NSLog(@"\n设置用户自定义消息数据库路径成功");
    else
        NSLog(@"\n设置用户自定义消息数据库路径失败");
}
///÷ 
/** 互动直播登录 */
- (void)loginILive:(sucCallback)suc  fail:(failCallback)fail{
    @weakify(self);
    [self getSigRequest:^(NSString *sig) {
        @strongify(self);
        [self loginILive:[AccountModel sharedAccountModel].account_id sig:sig suc:suc fail:fail];
    }];
}

- (void)loginILive:(NSString *)userId sig:(NSString *)sig suc:(sucCallback)suc  fail:(failCallback)fail{
    @weakify(self);
    [[ILiveLoginManager getInstance] iLiveLogin:userId sig:sig succ:^{
        @strongify(self);
//        [StatusBarManager statusBarHidenWithText:@"腾讯云 登录成功"];
        if (self.iLiveLoginHandle) self.iLiveLoginHandle(YES);
        if (suc) suc();
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        @strongify(self);
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"腾讯云 登录失败%@",errMsg]];
        if (self.iLiveLoginHandle) self.iLiveLoginHandle(NO);
        if (fail) fail();
    }];
}

- (void)loginILiveSuccessNotif:(void (^)(BOOL))cb{
    self.iLiveLoginHandle = cb;
}

/** 互动直播登出 */
- (void)loginOutILive:(sucCallback)suc{
    [[ILiveLoginManager getInstance] iLiveLogout:^{
        if (suc) suc();
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        showToastView(kError_Room(@"互动直播登出", errId, errMsg), kCommonWindow);
    }];
}

// 获取登录sig
- (void)getSigRequest:(void(^)(NSString *sig))suc{
    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig
                                    parameters:nil
                                 operationType:BYOperationTypePost
                                  successBlock:^(id result) {
                                      NSString *sig = result[@"userSig"];
                                      NSAssert(sig, @"sig为空");
                                      if (suc) suc(sig);
                                  } failureBlock:^(NSError *error) {
                                      NSLog(@"获取聊天室sig失败");
                                  }];
}

- (void)createAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail{
    if ([[TIMManager sharedInstance] getLoginStatus] == 3) {
        [[BYIMManager sharedInstance] loginILive:^{
            [[BYIMManager sharedInstance] createAVChatRoomGroupId:groupId succ:succ fail:fail];
        } fail:^{
            
        }];
        return;
    }
    NSString *groupName = [NSString stringWithFormat:@"%@-%@",[AccountModel sharedAccountModel].account_id,groupId];
    TIMCreateGroupInfo *groupInfo = [[TIMCreateGroupInfo alloc] init];
    groupInfo.group = groupId;
    groupInfo.groupName = groupName;
    groupInfo.groupType = kGroupType;
    groupInfo.setAddOpt = false;
    groupInfo.addOpt = TIM_GROUP_ADD_ANY;
    [[TIMGroupManager sharedInstance] CreateGroup:groupInfo succ:^(NSString *groupId) {
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
//        @strongify(self);
//        if (code == 10025) { // 聊天室被自己创建过则直接进入
//            [self joinAVChatRoomGroupId:groupId succ:^{
//                if (succ) succ();
//            } fail:nil];
//            return ;
//        }
        if (fail) fail();
        showToastView(kError_Room(@"创建聊天室", code, msg), kCommonWindow);
    }];
}

- (void)modifyGroupMemberVisible:(NSString *)groupId{
    [[TIMGroupManager sharedInstance] ModifyGroupMemberVisible:groupId visible:NO succ:^{
        
    } fail:^(int code, NSString *msg) {
        showToastView(kError_Room(@"聊天室设置不可被搜索", code, msg), kCommonWindow);
    }];
}

- (void)joinAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail{
    if ([[TIMManager sharedInstance] getLoginStatus] == 3) {
        [[BYIMManager sharedInstance] loginILive:^{
            [[BYIMManager sharedInstance] joinAVChatRoomGroupId:groupId succ:succ fail:fail];
        } fail:^{
            
        }];
        return;
    }
    [[TIMGroupManager sharedInstance] JoinGroup:groupId msg:kJoinMeg succ:^{
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        showToastView(kError_Room(@"进入聊天室", code, msg), kCommonWindow);
        if (fail) fail();
    }];
}

- (void)modifyGroupMemberInfoSetRole:(NSString *)userId role:(BYIMGroupMemberRole)role succ:(sucCallback)succ fail:(failCallback)fail{
    TIMGroupMemberRole memberRole = (int)role;
    [[TIMGroupManager sharedInstance] ModifyGroupMemberInfoSetRole:K_C_U_M.user_roomId user:userId role:memberRole succ:^{
        if (succ) succ();
    } fail:^(int code, NSString *msg) {
        if (fail) fail();
        showToastView(kError_Room(@"修改成员角色", code, msg), kCommonWindow);
    }];
}

- (void)receiveUpToVideoRequest:(BYUpVideoCellModel *)model{
    if (![self verifyIsInUpVideoList:model]) {
        NSMutableArray *mubArray = [NSMutableArray arrayWithArray:self.upToVideoMembers];
        [mubArray addObject:model];
        self.upToVideoMembers = mubArray;
    }
}

- (void)upToVideoRole:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail{
    ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
    @weakify(self);
    [roomManager changeRole:role succ:^{
        @strongify(self);
        [self openCameraAndMic:^{
            if (succ) succ();
        } fail:^{
            if (fail) fail();
        }];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        if (fail) fail();
        PDLog(@"errMsg -- %@",errMsg);
        showDebugToastView(@"修改为上麦角色失败！", kCommonWindow);
    }];
}

- (void)downToVideoRole:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail{
    ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
    [roomManager changeRole:role succ:^{
        cameraPos pos = [[ILiveRoomManager getInstance] getCurCameraPos];
        [roomManager enableCamera:pos enable:NO succ:^{
            NSLog(@"关闭相机");
            [roomManager enableMic:NO succ:^{
                NSLog(@"关闭麦克");
                if (succ) succ();
            } failed:^(NSString *module, int errId, NSString *errMsg) {
                if (fail) fail();
                showDebugToastView(@"下麦关闭mic失败", kCommonWindow);
            }];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            if (fail) fail();
            showDebugToastView(@"下麦关闭相机失败", kCommonWindow);
        }];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        if (fail) fail();
        PDLog(@"errMsg -- %@",errMsg);
        showDebugToastView(@"修改角色失败！", kCommonWindow);
    }];
}

- (NSArray *)getInUpVideoMembers{
    NSMutableArray *tmpArr = [NSMutableArray array];
    if (!self.upToVideoMembers.count) {
        return tmpArr;
    }
    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
        if (model.isUpVideo) {
            [tmpArr addObject:model];
        }
    }
    return tmpArr;
}

// 打开相机与mic
- (void)openCameraAndMic:(sucCallback)succ fail:(failCallback)fail{
    ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
    [roomManager enableCamera:CameraPosFront enable:YES succ:^{
        [roomManager enableMic:YES succ:^{
            if (succ) succ();
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            if (fail) fail();
            showDebugToastView(@"上麦打开麦克失败！", kCommonWindow);
        }];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        if (fail) fail();
        showDebugToastView(@"上麦打开相机失败！", kCommonWindow);
    }];
}

//- (void)setUpVideoMemberStatus:(NSString *)userId isUp:(BOOL)isUp{
//    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
//        if ([model.userId isEqualToString:userId]) {
//            model.isUpVideo = isUp;
//        }
//    }
//}

// 分配连麦观众标识(nil 表示当前上麦达到上限，无可分配的标识)
- (NSString *)allotLiveGuestIdentifier{
    NSArray *identifiers = [self getCurrentAllUpVideoMembers];
    if (!identifiers || !identifiers.count) {
        return LIVE_GUEST_ONE;
    }
    else{
        if (identifiers.count >= MAX_UPVIDEO_NUM) {
            return nil;
        }
        else{
            NSMutableArray *tmpArr = [NSMutableArray array];
            [tmpArr addObjectsFromArray:LIVEGUEST_MEMBERS];
            [tmpArr addObjectsFromArray:identifiers];
            NSSet *set = [NSSet setWithArray:tmpArr];
            return set.allObjects[0];
        }
    }
}

//- (void)setNickName:(NSString *)nick{
//    if (!nick.length) return;
//    int success = [[TIMFriendshipManager sharedInstance] SetNickname:nick succ:nil fail:nil];
//    NSLog(@"%d",success);
//}
//
//- (void)setHeadImg:(NSString *)headImg{
//    if (!headImg.length) return;
//    int success = [[TIMFriendshipManager sharedInstance] SetFaceURL:headImg succ:nil fail:nil];
//    NSLog(@"%d",success);
//}

#pragma mark - TIMCallback(TIM登录回调)
- (void)onSucc{
    if (_callBack) _callBack();
    showToastView(@"TIM登录成功", kCommonWindow);
}

- (void)onErr:(int)errCode errMsg:(NSString *)errMsg{
    showToastView(kError_Room(@"登录", errCode, errMsg), kCommonWindow);
}

#pragma mark - TIMConnListener(网络监听)

- (void)onConnSucc{
    [_connectingToast dissmissToastView];
}

- (void)onConnFailed:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connFailed duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onDisconnect:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connlost duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onConnecting{
    _connectingToast = [BYToastView tostViewPresentInView:kCommonWindow title:connecting duration:MAXFLOAT complete:nil];
}

#pragma mark - init
- (void)setUpToVideoMembers:(NSArray<BYUpVideoCellModel *> *)upToVideoMembers{
    objc_setAssociatedObject(self, &_upToVideoMebers, upToVideoMembers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    self.upToVideoMembersValueDidChangle();
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotification_upToVideoMembersValueDidChangle object:upToVideoMembers];
}

- (NSArray<BYUpVideoCellModel *> *)upToVideoMembers{
    NSArray *array = objc_getAssociatedObject(self, &_upToVideoMebers);
    if (!array) {
        array = [NSArray array];
        objc_setAssociatedObject(self, &_upToVideoMebers, array, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return array;
}

// 获取当前上麦user_id
- (NSArray *)getCurrentAllUpVideoMembers{
    TILLiveManager *manager = [TILLiveManager getInstance];
    NSArray *renderViews = [manager getAllAVRenderViews];
    NSMutableArray *members = [NSMutableArray array];
    if (!renderViews.count) {
        return nil;
    }
    for (ILiveRenderView *view in renderViews) {
        if (view.identifier.length) {
            [members addObject:view.identifier];
        }
    }
    return members;
}

// 上麦列表去重
- (BOOL)verifyIsInUpVideoList:(BYUpVideoCellModel *)model{
    if (!self.upToVideoMembers.count) {
        return NO;
    }
    for (BYUpVideoCellModel *tmpModel in self.upToVideoMembers) {
        if ([model.userId isEqualToString:tmpModel.userId]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - 移除所有上麦者
- (void)removeAllUpToVideoMember{
    NSArray *members = [[BYIMManager sharedInstance] getInUpVideoMembers];
    if (members.count) {
        for (BYUpVideoCellModel *model in members) {
            if (model.isUpVideo) {
                [[BYIMManager sharedInstance] sendDownVideoMessage:model.userId role:model.liveRole succ:nil fail:nil];
            }
        }
    }
    // 清空上麦成员
    [BYIMManager sharedInstance].upToVideoMembers = @[];
}
@end
